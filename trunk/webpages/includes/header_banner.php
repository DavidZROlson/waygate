
	<body>
    <?php include_once("includes/analyticstracking.php"); ?>
        <!-- Header Wrapper -->
        <div id="header-wrapper">
        
            <!-- Header -->
            <div style="width: 100%; height:100px; position:absolute; background-color:#D4D6D7;"></div>
            <div id="header" class="container" style="padding-bottom: 0px;">
            
                <!-- Logo -->
                <div id="logo">
                    <div style="display:inline-block">
                        <img src="<?=$IMG_PATH ?>RosinCloudYellowMedium.png" alt="Rolsincloud Logo" style="max-height:130px">
                    </div>
                    <div style="display: inline-block; position: relative; top: 25px; vertical-align: top">
                        <h1 style="line-height:1; text-transform:none; font-variant:small-caps;">
                            <a href="<?=$ROOT_PATH ?>index.php">RosinCloud</a>
                        </h1>
                        <p style="margin-top: 0px; text-transform:none; font-variant:small-caps; font-size:1.2em;">
                        	Making Complex Systems Easy</p>
                    </div>
                    <div style="display: inline-block; font-size: 3em; position: relative; top: 40px; vertical-align: top;">
                        <p style="margin:0;">
                            <a href="callto://+1-800-531-0892" style="color: #FFFFFF; text-decoration: none; border-bottom:none;">1-800-531-0892</a>
                        </p>
                    </div>
                    <div class="login">
                        <a href="#" style="border:none; display: none;">LOGIN</a>
                    </div>
                </div>
                <!-- END LOGO -->
                
                <!-- Nav -->
				<script type="text/javascript">
                function clicker(e){/*
                    < ?php $_POST['TITLE'] = e.target.innerHTML; ?>*/
                };
                $(document).click(function(event) {
                    var disallow = { "A":1, "IMG":1, "INPUT":1 }; 
                    if(!disallow[event.target.tagName]) {
                        $(':checkbox', this).trigger(clicker(event));
                    }
                });
                </script>
                <script type="text/javascript">
				function nav_float(){
					
					$(document).scroll(function(){
						var nav = $("#nav");
						var distanceFromTop = nav.offset().top - $(window).scrollTop();
						var top = $(document).scrollTop();
						if(distanceFromTop <= 0){
							if(nav.css("position") != "fixed"){
								nav.css("left", "0");
								nav.css("top", "0");
								nav.css("padding-top", "5px");
								nav.css("z-index", "1000");
								//nav.css("background-color", "#D4D6D7");
								$("#nav ul li").css("box-shadow", "1em 0.3em 0.5em 0.1em");
								$("#nav ul li").css("border-radius", "15px");
								nav.css("position", "fixed");
								
								$("#nav ul li ul li").css("box-shadow", "");
							}						
						}
						if(top <= 45){
								$("#nav ul li").css("box-shadow", "");
								nav.css("padding-top", "0");
								nav.css("top", "45px");
								nav.css("position", "absolute");								
						}	
						$('#skel-panels-pageWrapper').css("backface-visibility", "visible");
					});
				}
				$(window).load(nav_float());
				
                </script>
                <nav id="nav" style=" width:100%;">
                    <ul style="margin-bottom: 0px;">
                        <li><a class="icon icon-home" href="http://www.RosinCloud.com"><span>RosinCloud.com</span></a></li>
                        <li class="opener">
                            <a class="icon icon-bar-chart" href="<?=$ROOT_PATH ?>services.php"><span>Services</span></a>
                            <ul class="dropotron dropotron-level-0 left">
                                <li><a href="<?=$ROOT_PATH ?>service_proITsupport.php">Managed IT Support</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_proITconsulting.php">Professional IT Consulting</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_virtualization.php">Virtualization</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_migration.php">Migration</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_development.php">Development</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_disasterRecovery.php">Disaster Recovery</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_hosting.php">Hosting</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_support.php">Support</a></li>
                                <li><a href="<?=$ROOT_PATH ?>service_Cat6.php">Cat6 Cabling</a></li>
                            </ul>
                        </li>
                        <li class="opener">
                        	<a class="icon icon-user" href="<?=$ROOT_PATH ?>company.php"><span>Company</span></a>
                            <ul class="dropotron dropotron-level-0 left">
                                <li><a href="<?=$ROOT_PATH ?>company.php">About</a></li>
                                <li><a href="<?=$ROOT_PATH ?>company_clients.php">Clients</a></li>
                                <li><a href="<?=$ROOT_PATH ?>company_partners.php">Partners</a></li>
                                <li class="opener">
                                	<a>Careers</a>
                                    <ul class="dropotron dropotron-level-1">
                                        <li>
                                        	<a href="<?=$ROOT_PATH ?>company_carrers_Help-Desk.php">
                                            Help Desk Support I,II,III</a>
                                        </li>
                                        <li>
                                        	<a href="<?=$ROOT_PATH ?>company_carrers_Network-Administrator.php">
                                            Network Administrator I,II,III</a>
                                        </li>
                                        <li>
                                        	<a href="<?=$ROOT_PATH ?>company_carrers_Network-Engineer.php">
                                            Network Engineer</a>
                                        </li>
                                    </ul>
                                </li>  
                               <!--  <li><a href="<?=$ROOT_PATH ?>company.php">Vendors</a></li>
                                <li><a href="< ?=$ROOT_PATH ?>company_agent-resellers.php">Agent / Resellers</a></li>                           
                        		<li><a href="< ?=$ROOT_PATH ?>company.php">Blog</a></li> -->
                            </ul>
                        </li>
                        <li class="opener">
                        	<a class="icon icon-folder-open" href="<?=$ROOT_PATH ?>white_papers.php"><span>White Papers</span></a>
                            <ul class="dropotron dropotron-level-0 left">
                                <li><a href="<?=$ROOT_PATH ?>white_paper_ITaas_Transform.php">What Makes ITaaS Transformational?</a></li>
                                <li><a href="<?=$ROOT_PATH ?>white_paper_outsource.php">Outsource IT or In-House IT?</a></li>
                                <li><a href="<?=$ROOT_PATH ?>white_paper_ITAAS.php">IT As A Service (ITAAS)</a></li>
                                <li><a href="<?=$ROOT_PATH ?>white_paper_Cap_vs_Op.php">CapEx vs OpEx</a></li>
                                <li><a href="<?=$ROOT_PATH ?>white_paper_DC Relocation.php">Data Center Relocation</a></li>
                                <li><a href="<?=$ROOT_PATH ?>white_paper_P2V_consolidation.php">P2V Server Consolidation</a></li>
                                <!-- <li><a href="< ?=$ROOT_PATH ?>white_papers.php#">Network System Managment System</a></li>
                                <li><a href="< ?=$ROOT_PATH ?>white_papers.php#">Virtualization</a></li>
                                -->
                            </ul>
                        </li>
                        <li><a class="icon icon-envelope" href="#footer"><span>Contact</span></a></li>
                        <li class="opener">
                        	<a class="icon icon-file" href="<?=$ROOT_PATH ?>policy_privacy.php"><span>Policies</span></a>
                            <ul class="dropotron dropotron-level-0 left">
                            	<li><a href="<?=$ROOT_PATH ?>policy_return.php">Return Policy</a></li>
                                <li><a href="<?=$ROOT_PATH ?>policy_privacy.php">Privacy Policy</a></li>
                            </ul>
                        </li>
                    </ul>
                </nav>
                <!-- END NAV -->
                
            </div>
            <!-- END HEADER -->
            
        </div>
        <!-- END HEADER-WRAPPER -->
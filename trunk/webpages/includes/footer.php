		<!-- Footer Wrapper -->
        <div id="footer-wrapper" style="background-color: #D4D6D7; padding:0;">

            <!-- Footer -->
            <div id="footer" class="container">
                <header>
                    <h2>Questions or comments?
                    	<strong>
                        	<span style="
                            color: #0066CC;
                            font-weight: 700;">
                            	<a href="mailto:Cloud@RosinCloud.com">Get in touch:</a>
                            </span>
                        </strong>
                    </h2>
                </header>
                <div class="row">
                	<div class="6u" style="
                    	padding-bottom: 2em;">
                        <section>
                            <form id="contactForm" action="">
                            <fieldset>
                                <div class="row half">
                                    <div class="6u">
                                        <input name="name" placeholder="Name" type="text" class="text" required="" minlength="4" />
                                    </div>
                                    <div class="6u">
                                        <input name="email" placeholder="Email" class="text" type="email" required="" />
                                    </div>
                                </div>
                                <div class="row half">
                                    <div class="6u">
                                        <select id="subject" class="required" name="subject">
                                        	<option value = ""><span>&lt;Select a Subject&gt;</span></option>
                                        	<option value = "Services">Services</option>
                                            <option value = "Support">Support</option>
                                        	<option value = "Careers">Careers</option>
                                        	<option value = "Training">Training</option>
                                        	<option value = "Other">Other</option>
                                        </select>
                                    </div>
                                    <div class="6u">
                                        <input id="Other_subject" name="Other_subject" placeholder="Subject" type="text" class="text" />
                                    </div>
                                </div>
                                <div class="row half">
                                    <div class="12u">
                                        <textarea name="message" minlength="7" placeholder="Message" required=""></textarea>
                                    </div>
                                </div>
                                <div class="row half">
                                <div class="4u">
                                	<img id="captcha" src="<?=$ROOT_PATH ?>/securimage/securimage_show.php" alt="CAPTCHA Image" />
                                    <input type="text" name="captcha_code" size="10" maxlength="6" />
									<a href="#" onclick="document.getElementById('captcha').src = '<?=$ROOT_PATH ?>/securimage/securimage_show.php?' + Math.random(); return false"><img alt="Refresh" src="<?=$ROOT_PATH ?>securimage/images/refresh.png" width="20px" height="20px" /></a>

                                </div>
                                    <div class="8u">
                                    <button id="send" class="button button-icon icon icon-envelope" type="submit" style="float: right;">
                                        Send Message
                                    </button>
                                    </div>
                                </div>
                                </fieldset>
                            </form>
                            
                            <script type="text/javascript">
							
							$(document).ready(function(){ // ran when the document is fully loaded
										
								$("#contactForm").validate();
								$("#Other_subject").hide();
								  // retrieve the jQuery wrapped dom object identified by the selector '#mySel'
								  var sel = $('#subject');
								  // assign a change listener to it
								  sel.change(function(){ //inside the listener
									// retrieve the value of the object firing the event (referenced by this)
									var value = $(this).val();
									if(value == "Other"){
										$("#Other_subject").show();
									}else{
										
										$("#Other_subject").val("");
										$("#Other_subject").hide();
									}
								  }); // close the change listener
								}); // close the ready listener 
								
								//ELIMANATE THE NORMAL FORM OPERATION SO IT DOESN'T SEND TWICE
								$("#contactForm").unbind('submit');
								
								//ADD ONCLICK EVENT HANDLER
								$("#contactForm").submit(function(event){
									/* stop form from submitting normally */
									event.preventDefault();
									var validator = $( "#contactForm" ).validate();
																		
									if(validator.form() == true){
										
										$('#validation').html("<p>Sending Message...</p>");
									
										$.ajax({
											type: 'POST',
											data: $('#contactForm').serialize(),
											url: 'send_message.php',
											complete: function(data) {
												$('#validation').html(data.responseText);
												}//end ajax:success function
												
										});
									}	
								});
								
                            </script>
                        </section>
                    </div>
                    <div class="6u">
                        <section>
                            <div class="row">
                                <ul class="icons 6u">
                                    <li class="icon icon-home">
                                    	4969 HWY 101 Unit#2B<br />
                                        FLORENCE, OR 97439<br />
                                        USA
                                    </li>
                                    <li class="icon icon-phone">
                                        <a href="callto://+1-800-531-0892"><span style="font-size:1.7em;">1-800-531-0892</span></a>
                                    </li>
                                    <li class="icon icon-envelope">
                                        <a href="mailto:Cloud@RosinCloud.com">Cloud@RosinCloud.com</a>
                                    </li>
                                </ul>
                                <ul class="icons 6u">
                                    <li class="icon icon-twitter">
                                        <a href="http://twitter.com/RosinCloud2013">@RosinCloud2013</a>
                                    </li>
                                    <li class="icon icon-facebook">
                                        <a href="http://www.facebook.com/RosinCloud">facebook.com/RosinCloud</a>
                                    </li>
                                    <li class="icon icon-youtube-sign">
                                        <a href="http://www.youtube.com/user/ProCloudServices/videos">youtube.com/ProCloudServices</a>
                                    </li>
                                    <li class="icon icon-linkedin">
                                        <a href="http://www.linkedin.com/company/rosincloud">linkedin.com/company/RosinCloud</a>
                                    </li>
                                </ul>
                            </div>
                             <div class="row">
                                    <div id="validation" class="12u">
                                    </div>
                                </div>
                        </section>
                    </div>
                </div>
            </div>
            <!-- END Footer -->

            <!-- Copyright -->
            <div id="copyright" class="container" style="
            	margin-top: 0;
                padding-top: 2em;">
                <ul class="links">
                    <li>&copy;2013 RosinCloud, Inc. All rights reserved.</li>
                </ul>
            </div>
            <!-- END Copyright -->

        </div>
        <!-- END Footer Wrapper -->

	</body>
</html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>RosinCloud.com</title>
        <meta content="en-US" http-equiv="content-language">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta content="&copy;2013 RosinCloud US, Inc." name="copyright">
        <meta content="RosinCloud.com can migrate all or part of your company&rsquo;s data, applications and services from on-site premises behind the firewall to the cloud, where the information can be provided over the Internet on an on-demand basis." name="description" />
        <meta content="Index, Follow" name="Robots" />
        <meta content="rosincloud, rosincloud.com, server, data migration, migration, cloud, hosting, managed hosting, cloud hosting, web hosting, florence oregon, oregon, IT" name="keywords" />
        <meta content="width=1040" name="viewport" />
        
        <!-- Meta Geotags -->
        <meta content="4969 HWY 101 Unit# 2B, Florence, OR 97439, USA" name="geo.placename">
        <meta content="44.009795;-124.102453" name="geo.position">
        <meta content="US-OR" name="geo.region">
        <meta content="44.009795, -124.102453" name="ICBM">
                
		<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600|Arvo:700" rel="stylesheet" type="text/css" />
        <link type="text/css" rel="stylesheet" href="http://fonts.googleapis.com/css?family=Play:n,b,i,bi|Open+Sans:n,b,i,bi|" />
		
        <link href="images/favicon.ico" rel="shortcut icon">
        <link href="images/R.png" rel="apple-touch-icon">
        <!--[if lte IE 8]><script src="js/html5shiv.js"></script><![endif]-->
        
        
        <!-- REQUIRED BXSLIDER FILES -->
            <!-- jQuery library (served from Google) -->
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
            <!-- bxSlider Javascript file -->
            <script src="jquery_bxslider/js/plugins/jquery.easing.1.3.js"></script>
            <script src="jquery_bxslider/js/jquery.bxslider.min.js"></script>
            <!-- bxSlider CSS file -->
            <link href="jquery_bxslider/lib/css/jquery.bxslider.css" rel="stylesheet" />
        
        <!-- REQUIRED FOR NAV MENU -- MUST COME AFTER BXSLIDER REFERENCES -->  
            <script type="text/javascript" src="<?=$JS_PATH ?>jquery.dropotron.js"></script>
            <script type="text/javascript" src="<?=$JS_PATH ?>config.js"></script>
            <script type="text/javascript" src="<?=$JS_PATH ?>skel.min.js"></script>
            <script type="text/javascript" src="<?=$JS_PATH ?>skel-panels.min.js"></script>
            
        <!-- REQUIRED FOR FORM VALIDATION -->
    		<script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
   
		<noscript>
			<link rel="stylesheet" href="<?=$CSS_PATH ?>skel-noscript.css" />
			<link rel="stylesheet" href="<?=$CSS_PATH ?>style.css" />
			<link rel="stylesheet" href="<?=$CSS_PATH ?>style-desktop.css" />
            <link rel="stylesheet" href="<?=$CSS_PATH ?>Fonts_and_Colors.css" />
		</noscript>
        
	</head>
    
<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc outside none;;
		text-align: justify;
		padding: .25% 5% 1.5% 7%
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Disaster Recovery</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  	<section class="cell">
										<a href="<?=$ROOT_PATH ?>services.php" class="image image-left">
                                        	<img src="<?=$IMG_PATH ?>disasterrecovery.png" alt="disaster recovery" />
                                        </a>
										<header>
                                            <h2 style="text-align: center;">
                                                Frequently asked questions
                                            </h2>
                                            <h3 style="text-align: center;">
                                                Disaster Recovery<br />
                                                &amp;<br />
                                                Business Continuity Planning
                                            </h3>
										</header>
<!-- Begin TEXT -->
<h3 style="margin-left:2.5%;">
Why should I have a disaster recovery or a business continuity plan?
</h3>
<p>
    Simply put, to survive a disaster (large or small), you need a plan to get critical data and operations back as 
    quickly as possible. Without a plan your business can suffer lost sales revenue, lost data and loss of confidence. 
    But beyond the IT considerations, where would your employees go if your office was flooded? Would you have a backup 
    office space available, or a phone line that would be accessible outside the office to direct calls or messages? 
    These are simple considerations, but ones that can start the conversation on business continuity.
</p>

<h3 style="margin-left:2.5%;">
Who Should Create the Plan at my Office?
</h3>
<p>
Historically, it used to be that the "IT guys" had the responsibility for creating a plan. Because it was on the 
shoulders of the IT team it tended to deal only with network and IT functionality. While this is definitely part of the 
process, leaving the planning to the network/IT/IS team can leave the needs of the rest of the business exposed. So 
while it's good that your network is active again, you can't ignore special equipment, communications systems, work 
space and inventory, customer information, paper backup, receipts and critical resources. It's become clear that a 
disaster recovery plan must take business process into consideration so that the full spectrum of business requirements 
can be understood.
</p>

<h3 style="margin-left:2.5%;">
Who Needs a Business Continuity and/or Disaster Recovery Plan?<br />
You should consider a plan if you have:
</h3>
<ul style="list-style: decimal outside none;">
<li>Stored data about customers, business processes or products.</li>
<li>Rely on email, phone systems or computers for keeping your business active.</li>
<li>Are a public company, or have investors.</li>
<li>Customers or partners who rely on your products and services.</li>
<li>If you have any company directors who would be liable for damages should a business interruption cause loss of customers, revenue or cause a business shut down.</li>
<li>Workspace where employees conduct their jobs.</li>
</ul>

<h3 style="margin-left:2.5%;">
I'm a one-man show, is there a plan that makes sense?
</h3>
<p>
Short answer: yes. No matter the size of the company, a BC/DR plan is smart. If you rely on your business for income, or 
others rely on you, it's time to consider some "what if" scenarios and put together a plan that's "right"-sized for your 
business.
</p>

<h3 style="margin-left:2.5%;">
I don't have a lot of computers, IT infrastructure or high-tech equipment, what type of plan do I need?
</h3>
<p>
All disaster recovery and business continuity plans include information about how to maintain important business 
processes, like phones or email, workspace, order processing, delivery, paperwork, people, special equipment or safe 
evacuation. Disasters aren't just about your workspace or your IT center, they are a combination of both.
</p>

<h3 style="margin-left:2.5%;">
I'm a public company and I have data backed up on an external network drive. What else do I need?
</h3>
<p>
As a public company, you are required by the SEC to have a BC/DR plan. Backing up data at a second site is not enough. A 
formal plan with satisfy your SEC requirements and give your business the ability to survive is any type of interruption 
may occur.
</p>

<h3 style="margin-left:2.5%;">
I'm responsible for this project at my company, how can I create a successful plan?
</h3>
<ul>
    <li>Management needs to know how much time and effort is required to develop and maintain an effective recovery plan.</li>
    <li>Management needs to commit to supporting and participating in the effort.</li>
    <li>All the business functions must provide recovery requirements; not just the IT team.</li>
    <li>Conduct an audit of all the ways an extended loss to operations and key business functions could impact your company.</li>
    <li>Focus on disaster prevention and impact minimization, as well as orderly recovery.</li>
    <li>Select project teams that will ensure a proper balance across the organization.</li>
    <li>Create a continuity plan that's understandable, easy to use and easy to maintain.</li>
    <li>Consider how your planning activities will tie into business planning and system development processes so your plan remains viable over time.</li>
</ul>

<h3 style="margin-left:2.5%;">
What disasters are covered in your plans?
</h3>
<p>
We will develop, or support your development of a plan that's unique to your potential business interruptions. So we 
could include any of the following or more depending on all the factors that are unique to your business.
</p>
<ul>
<li>Storm</li>
<li>Fire</li>
<li>Employee Strike</li>
<li>Tornado</li>
<li>Hurricane</li>
<li>Flood</li>
<li>Malicious or accidental employee damage to systems</li>
<li>Hardware or software failure</li>
<li>Virus</li>
<li>Theft or robbery</li>
</ul>

<h3 style="margin-left:2.5%;">
What type of plan do I need? DRP, BRP, BCP...they all sound important, similar and expensive?!
</h3>
<p>
Like any niche business, this one of Disaster Recovery Planning and Business Continuity Planning has its own jargon and 
terminology. Often for people new to thinking about and considering how their company can be best protected in the event 
of a disaster, this jargon becomes overwhelming. So to help clear the air and keep things simple, lets review some of 
the key terms of our industry - hopefully making it easier for you to discuss Disaster Recovery Planning and Business 
Continuity Planning with your colleagues.
</p>
<p>
<dl>
<dt style="padding-left: 2.5%;">Disaster Recovery Planning (DRP):</dt>
    <dd style="padding-left: 5%;">
    refers to the recovery of IT services following a major hit or service interruption.<br />
    </dd>
<dt style="padding-left: 2.5%;">Business Recovery Planning (BRP):</dt>
    <dd style="padding-left: 5%;">
    a superset of DRP and refers to the recovery of IT and all other aspects of the business. This includes everything from finance to administration to engineering to manufacturing.
    </dd>
</dl>
<!-- END TEXT -->
                                </section>
                                <!-- END cell -->

							</div>
                            <!-- END 12u -->
							
						</div>
                        <!-- END row -->
					</section>
			
			</div>
            <!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
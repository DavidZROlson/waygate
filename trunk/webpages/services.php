<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<style type="text/css">
	section .cell
	{
		height: 550px;
		padding: 5%;
	}
	.cell h3
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		text-align: left;
		padding: 2em .5em .5em;
		color: #D4D6D7;
	}
</style>
<!-- Features Wrapper -->
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">Services</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header><div>
						<div class="row">
							<div class="4u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>service_proITsupport.php" class="image image-full">
                                        	<img src="<?=$IMG_PATH ?>cropped-Computer-repair-pc-repair-IT-Support.png" alt="support hand" />
                                        </a>
										<header>
                                            <h3>
                                                <a href="<?=$ROOT_PATH ?>service_proITsupport.php">
                                                    Managed IT Support
                                                </a>
                                            </h3>
										</header>
										<p style="text-align:justify;">
                                        	With RosinCloud, you have the flexibility to start with the managed IT 
                                            services that make the most sense for your organization, and then grow 
                                            with us as your needs grow. Whether you are a small or large business, 
                                            we want you to think of us as an extension of your IT department. Our 
                                            talented staff is here to support all of your technology needs.
                                        </p>
									</section>

							</div>
							<div class="4u">

								<!-- Feature -->
                                <section class="cell">
                                        <a href="<?=$ROOT_PATH ?>service_proITconsulting.php" class="image image-full">
                                            <img src="<?=$IMG_PATH ?>itcs.png" alt="IT Consulting" />
                                        </a>
                                        <header>
                                        <h3>
                                            <a href="<?=$ROOT_PATH ?>service_proITconsulting.php">
                                            	Professional IT Consulting
                                            </a>
                                        </h3>
                                        </header>
                                    <p style="text-align:justify;">
                                        With RosinCloud, you receive our commitment to clearly explain the technology 
                                        situation you are facing, present you with the best options available as well 
                                        as recommend solutions we believe will serve your company best. Our goal is 
                                        to be your trusted technology advisor that helps you to run your business as 
                                        efficiently as possible.
                                    </p>
                                </section>
									
							</div>
							<div class="4u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>service_virtualization.php" class="image image-full">
                                        	<img src="<?=$IMG_PATH ?>Cloud-Development_1.png" alt="cloud development" />
                                        </a>
										<header>
											<h3>
                                                <a href="<?=$ROOT_PATH ?>service_virtualization.php">
                                                	Virtualization
                                                </a>
                                            </h3>
										</header>
										<p style="text-align:justify;">
                                        	Virtualization is the most effective way to reduce IT expenses while 
                                            increasing efficiency for businesses of all sizes. From consolidating 
                                            hardware to simplifying management and deployment of new applications 
                                            RosinCloud will work with you to evaluate your current infrastructure and 
                                            implement the best solution. 
                                        </p>
									</section>

							</div>
						</div>
                        <div class="row">
							<div class="4u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>service_migration.php" class="image image-full">
                                        	<img src="<?=$IMG_PATH ?>cloud-migration.png" alt="cloud migration" />
                                        </a>
										<header>
										  <h3>
                                              <a href="<?=$ROOT_PATH ?>service_migration.php">
                                              	Migration
                                              </a>
                                          </h3>
										</header>
										<p style="text-align:justify;">
                                        	Whether P2V or V2V, we have what it takes to get the job done efficiently. 
                                            Migrating to the cloud will help you to reduce capital expenditures and 
                                            operating costs while also benefiting from the dynamic scaling, high 
                                            availability, and effective resource allocation advantages cloud-based 
                                            computing offers.
                                        </p>
									</section>

							</div>
							<div class="4u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>service_development.php" class="image image-full">
                                        	<img src="<?=$IMG_PATH ?>development.png" alt="development" />
                                        </a>
										<header>
										  <h3>
                                              <a href="<?=$ROOT_PATH ?>service_development.php">
                                              	Development
                                              </a>
                                          </h3>
										</header>
										<p style="text-align:justify;">
                                        	Our extensive background with designing, building and deploying large scale 
                                            and highly scalable cloud computing solutions brings you the experience 
                                            required in migrating your infrastructure and applications to the cloud. 
                                            We have a professional staff that has many years of experience programming 
                                            on all platforms.
                                        </p>
									</section>
									
							</div>
							<div class="4u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>service_disasterRecovery.php" class="image image-full">
                                        	<img src="<?=$IMG_PATH ?>disasterrecovery.png" alt="disaster recovery" />
                                        </a>
										<header>
										  <h3>
                                              <a href="<?=$ROOT_PATH ?>service_disasterRecovery.php">
                                              	Disaster Recovery
                                              </a>
                                          </h3>
										</header>
										<p style="text-align:justify;">
                                        	Simply put, to survive a disaster (large or small), you need a plan to get 
                                            critical data and operations back as quickly as possible. Without a plan 
                                            your business can suffer lost sales revenue, lost data and loss of 
                                            confidence. Let RosinCloud help you prepare for anything that may come 
                                            your way.
                                        </p>
									</section>

							</div>
						</div>
                        <div class="row">
							<div class="4u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>service_hosting.php" class="image image-full">
                                        	<img src="<?=$IMG_PATH ?>Hosting.png" alt="network" />
                                        </a>
										<header>
										  <h3>
                                              <a href="<?=$ROOT_PATH ?>service_hosting.php">
                                              	Hosting
                                              </a>
                                          </h3>
										</header>
										<p style="text-align:justify;">
                                        	Troubles will arise and when you or your team is distracted from your core 
                                            business initiatives because of IT issues, your business can be negatively 
                                            impacted. RosinCloud will monitor your network 24x7x365 and provide monthly 
                                            reports of your system performance. With network management services, your 
                                            business can be proactive instead of reactive. Problems will be detected 
                                            sooner and faster, therefore lowering IT costs.
                                        </p>
									</section>

							</div>
							<div class="4u">

								<!-- Feature -->
                   			  	<section class="cell">
                                    <a href="<?=$ROOT_PATH ?>service_support.php" class="image image-full">
                                        <img src="<?=$IMG_PATH ?>help-desk.png" alt="help desk support" />
                                    </a>
                                    <header>
                                      <h3>
                                          <a href="<?=$ROOT_PATH ?>service_support.php">
                                            Support
                                          </a>
                                      </h3>
                                    </header>
                                    <p style="text-align:justify;">
                                        Working hard 24x7x365 to support you is more than our job. It's who we are. 
                                        Our driving purpose is to take care of your business, to make sure things 
                                        go as smoothly as possible. And if for some reason they don't, you'll be 
                                        surprised at the lengths we go to make things right, so you'll never have 
                                        to worry about it again. Ever.
                                    </p>
                                </section>
									
							</div>
							<div class="4u">

                   			  	<section class="cell">
                                    <a href="service_Cat6.php" class="image image-full">
                                        <img src="images/thumbs_up.png" alt="thumb_up" />
                                    </a>
                                    <header>
                                        <h3>
                                            <a href="#">
                                                Cat6 Cabling
                                            </a>
                                        </h3>
                                    </header>
                                    <p style="text-align:justify;">
                                        Benefits like virtualization, scalability, elasticity and low capex are 
                                        tempting businesses into making the move into a cloud hosting solution, 
                                        along with improvements in cloud technology and security. Whether you're 
                                        just dipping your toes in the water or ready for a full-blown cloud hosting 
                                        environment, RosinCloud offers a variety of cloud hosting options to suit 
                                        your business needs.
                                    </p>
                                </section>

							</div>
						</div>
                        </div>
					</section>
			
			</div>
		
<?php
	require_once('includes/footer.php');
?>
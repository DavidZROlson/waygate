<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>			
<!-- Main Wrapper -->
<div id="main-wrapper" style="background: none;">
<style type="text/css">
section .cell img
{
	width: 150px;
	height: 150px;
}
section .cell ul
{
	display: table-cell;
	list-style: disc inside none;
	font-size: .80em;
}
section .cell li
{
	margin:0;
	padding:0;
	line-height:1;
}
</style>

    <!-- Main -->
    <div id="main" class="container">
        <div class="row">
        
            <!-- Content -->
            <div id="content" class="12u skel-cell-mainContent">

                <!-- Post -->
                <article class="is-post">
                    <header style="
                        border-bottom: 1px solid;
                        margin-bottom: 10px;">
                        <h2 style="
                            text-transform:none;
                            margin:0;
                            letter-spacing:0;">
                            Clients
                        </h2>
                    </header>
                    <p style="margin-left: 2.5em;">
                    <strong>Our client partnerships are the most valued, vital aspects of what we do and who we are.</strong><br />
                     Without our clients, our work would have no meaning. We aim to go above and beyond to exceed our 
                     clients' expectations. We are only successful when our clients are successful.
                     <br />
					We build meaningful, genuine relationships with our clients, and we succeed because of the people we 
                    hire. Our associates value our clients' trust and confidence, feelings they gain through providing 
                    our customers with the right answers and solutions. We hire individuals who want to be trusted 
                    advisors for our clients and want to maximize our customers' engagement with RosinCloud via our 
                    practices, services, and products.
                    </p>
                    <section>
                        <div class="cell">
                            <a href="http://www.hostway.com"><span class="image image-left"><img src="<?=$IMG_PATH ?>hostway.jpg" alt="" /></span></a>
                            <h3>Hostway</h3>
                            <p>
                            <span style="margin-left:2em;">Our</span> Partner Program was designed to give our partners 
                            aggressive incentives and generous volume discounts on our award-winning products. Our program 
                            was built to enable all types of businesses to incorporate IT hosting services into their current 
                            product and service offerings. Hostway is committed to providing you the tools necessary for 
                            success including marketing tools and initiatives, a direct channel manager, priority 24/7 live 
                            local technical support, as well as tailored lead generation activities to drive new business. 
                            We work closely with every partner to develop a personalized plan for success!
                            </p>
                        </div>
                        <div class="cell">
                            <a href="http://www.yellowpages.com/"><span class="image image-left"><img src="<?=$IMG_PATH ?>YP.jpeg" alt="" /></span></a>
                            <h3>Yellow Pages</h3>
                            <p>
                            <span style="margin-left:2em;">YP</span> is North America's largest local search, media and 
                            advertising company. Formerly AT&T Interactive and AT&T Advertising Solutions, YP launched 
                            in May 2012, bringing the two companies together with the mission of helping local 
                            businesses and communities grow. Millions of searches occur daily using YP℠ products to 
                            find, compare and select local merchants. The company's flagship consumer brands include the 
                            YP.com site, a top 40 U.S. Web domain1, the highly rated YP℠ app and the YP Real Yellow 
                            Pages directory, the largest Yellow Pages directory in the world by revenue.
                            </p>
                        </div>
                        <div class="cell">
                            <a href="http://www.rsc2lc.com/"><span class="image image-left"><img src="<?=$IMG_PATH ?>RSC-Logo.png" alt="" /></span></a>
                            <h3>Robert Stephen Consulting</h3>
                            <p>
                            <span style="margin-left:2em;">RSC</span> works closely with client staff, interior designers, 
                            AutoCAD designers and drafters, lease management and furniture warehousing companies to 
                            facilitate a fully integrated relational database containing precise and key information for 
                            facilities reporting needs. Some of the applications used by RSC include web tools utilizing 
                            dynamic queries, CAFM software such as Archibus, Manhattan/CenterStone, and Tririga, and 
                            other tools such as Crystal Reports, AutoCAD and Microsoft Access.
                            </p>
                        </div>
                        <div class="cell">
                            <a href="http://www.medallies.com/main.html"><span class="image image-left"><img src="<?=$IMG_PATH ?>med_allies.png" alt="" /></span></a>
                            <h3>MedAllies</h3>
                            <p>
                            <span style="margin-left:2em;">Unlike</span> the current health information exchange market, 
                            which is usually confined geographically (i.e., regionally focused), MedAllies believes 
                            Direct networks will soon evolve to a few large, leading-edge national networks. MedAllies 
                            Direct will be among them. MedAllies has already demonstrated the ability to create an 
                            integrated delivery network in any community. MedAllies Direct represents the next step.
                            </p>
                        </div>
                        <div class="cell">
                            <a href="http://www.mocandco.com/"><span class="image image-left"><img src="<?=$IMG_PATH ?>mary-o-connor-and-co.png" alt="" /></span></a>
                            <h3>Mary O'Connor and CO.</h3>
                            <p>
                            <span style="margin-left:2em;">Mary O’Connor and Co.</span> has partnered with exceptional 
                            clients to produce experiences that bring people together from all over the world. Our 
                            expertise includes complete service for:
                            </p>
                                <ul>
                                    <li>Meetings</li>
                                    <li>Events</li>
                                    <li>Incentive Trips</li>
                                    <li>Tradeshows</li>
                                    <li>Site Search</li>
                                    <li>Registration</li>
                                </ul>
                            <p>
							And when we say “complete” service, we mean it.  We are a single source for our clients, 
                            providing the consistency, convenience and value of one partner.
                            </p>
                        </div>
                        <div class="cell">
                            <a href="http://www.moviesunlimited.com/"><span class="image image-left"><img src="images/35mu_logo.jpg" alt="Movies Unlimited" longdesc="http://www.moviesunlimited.com" /></span></a>
                            <h3>Movies Unlimited</h3>
                            <p>
                            <span style="margin-left:2em;">Shop with confidence </span> from the world's oldest and most 
                            reliable source for video movies by mail-order. At Movies Unlimited, we're known for our 
                            vast selection of videos that we ship around the world every day. Choose from thousands of 
                            different titles in every imaginable category, including classics from the past, musicals, 
                            foreign films, "B" westerns, movie serials, TV shows, and much, much more.
                            </p>
                      </div>
                        
                    </section>
                </article>
            
            </div>
            <!-- Content END 12u -->
                
        </div>
    </div>

</div>


<?php
	require_once('includes/footer.php');
?>
<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc outside none;;
		text-align: justify;
		padding: .25% 5% 1.5% 7%
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Cat6 Cabling</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  	<section class="cell">
										<a href="<?=$ROOT_PATH ?>services.php" class="image image-left">
                                       		<img src="<?=$IMG_PATH ?>thumbs_up.png" alt="Thumbs-up">
                                            </a>
										<header>
                                            <h2>Cat6 Cabling</h2>
										</header>
<!-- Begin TEXT -->
<p>
Cat 6 Cabling services as well as Cat 5e, Cat 6a for 10 Gig, and Fiber Optics. With over 25 years of experience, 
RosinCloud addresses the needs of contractors and homeowners as well as small, medium, and large enterprise customers. 
RosinCloud has the experience and the expertise to get the job done.
</p>
<p>
Category 6 cable, commonly referred to as Cat 6, is a cable standard for Gigabit Ethernet and other network protocols 
that are backward compatible with the Category 5/5e and Category 3 cable standards. Compared with Cat-5 and Cat-5e, 
Cat-6 features more stringent specifications for crosstalk and system noise.
</p>
<p>
RosinCloud provides a wide and diverse set of capabilities in telecommunications. We use only materials manufactured by 
the leading companies in the industry and provide warranty on all parts and services.
</p>

<h3 style="margin-left:2.5%;">
Infrastructure cable plant services:
</h3>
<ul>
    <li>Fiber Optic Installation</li>
    <li>Cat 5e, and Cat 6, and Cat 6a cable plant installation for voice and data.</li>
    <li>Structured cable plant design and installation</li>
    <li>Voice and Data cable terminations and patch panel installations</li>
</ul>

<!-- END TEXT -->
                                </section>
                                <!-- END cell -->

							</div>
                            <!-- END 12u -->
							
						</div>
                        <!-- END row -->
					</section>
			
			</div>
            <!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
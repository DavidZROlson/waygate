<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>			
<!-- Main Wrapper -->
<div id="main-wrapper" style="background: none;">
<style type="text/css">
section .cell img
{
	width: 150px;
	height: 150px;
}
section .cell ul
{
	display: table-cell;
	list-style: disc inside none;
	font-size: .80em;
}
section .cell li
{
	margin:0;
	padding:0;
	line-height:1;
}
</style>

    <!-- Main -->
    <div id="main" class="container">
        <div class="row">
        
            <!-- Content -->
            <div id="content" class="12u skel-cell-mainContent">

                <!-- Post -->
                <article class="is-post">
                    <header style="
                        border-bottom: 1px solid;
                        margin-bottom: 10px;">
                        <h2 style="
                            text-transform:none;
                            margin:0;
                            letter-spacing:0;">
                            Official Partnerships
                        </h2>
                    </header>
                    <p style="margin-left: 2.5em;">
                    <strong>Our rock solid partnerships are a key to your success.</strong><br />
                    A predictable, honest and reliable relationship between RosinCloud and its agents/re-sellers goes a long way 
                    toward helping our clients succeed. Like a good friend, our successful agents/re-sellers relationships allow 
                    us to quickly find the proper solution that fits your business and workflow.
                    </p>
                    <section>
                        <div class="cell">
                            <a href="http://www.hostway.com"><span class="image image-left"><img src="<?=$IMG_PATH ?>hostway.jpg" alt="" /></span></a>
                            <h3>Hostway</h3>
                            <p>
                            <span style="margin-left:2em;">Our</span> Partner Program was designed to give our partners 
                            aggressive incentives and generous volume discounts on our award-winning products. Our program 
                            was built to enable all types of businesses to incorporate IT hosting services into their current 
                            product and service offerings. Hostway is committed to providing you the tools necessary for 
                            success including marketing tools and initiatives, a direct channel manager, priority 24/7 live 
                            local technical support, as well as tailored lead generation activities to drive new business. 
                            We work closely with every partner to develop a personalized plan for success!
                            </p>
                        </div>
                    </section>
                    <section>
                        <div class="cell">
                            <a href="http://www.123wiz.com"><span class="image image-left"><img src="<?=$IMG_PATH ?>123ComputerWiz.png" alt="" /></span></a>
                            <h3>123 Computer Wiz</h3>
                            <p>
                            <span style="margin-left:2em;">Whether</span> your computer just crashed, you need help with 
                            your new iPad, you need a website or you need help with your small business computers or 
                            network, 123 Computer Wiz can help.<br />
                            We specialize in:
                            <ul style="display: inline-block">
                                <li>Remote Support</li>
                                <li>Custom Business Solutions</li>
                                <li>Website Developement</li>
                                <li>Preventative Maintenance</li>
                            </ul>
                            <ul style="display: inline-block">
                                <li>Custom Training</li>
                                <li>PC Repair / Computer Repair</li>
                                <li>Upgrades</li>
                                <li>Networking</li>
                            </ul>
                            <ul style="display: inline-block">
                                <li>System Set-Up</li>
                                <li>Virus Detection and Recovery</li>
                                <li>Security and Performance</li>
                                <li>Hardware/Software Sales</li>
                            </ul>
                            </p>
                        </div>
                    </section>
                </article>
            
            </div>
            <!-- Content END 12u -->
                
        </div>
    </div>

</div>


<?php
	require_once('includes/footer.php');
?>
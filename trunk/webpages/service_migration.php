<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Migration Services</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  	<section class="cell">
										<a href="<?=$ROOT_PATH ?>services.php" class="image image-left">
                                       	<img src="<?=$IMG_PATH ?>cloud-migration.png" width="512" height="376" alt="Cloud Migration" longdesc="images/cloud-migration.png" /> </a>
										<header>
                                          <h2>Server Mirgration</h2>
										</header>
<!-- Begin TEXT -->
<p>
Server migrations are more than just moving data. There are fine-tuned application settings, critical email, and active 
databases that need to be transferred smoothly. Let our team of Certified technicians make sure it is done correctly the 
first time. We will work closely with you to make sure this a seamless transition.
</p>

<h3 style="margin-left:2.5%;">
We Are your Migration Experts
</h3>
<p>
Our expert technicians have a proven track record of successful migrations. From high-volume 24x7 eCommerce sites to corporate email, we ensure a smooth transition to our services. Let us focus on your migration so you can focus on your business.
</p>

<h3 style="margin-left:2.5%;">
And above all else, World Class Customer Service
</h3>
<p>
RosinCloud prides itself on our World Class Customer Service. We invite you to contact us today to discuss your needs. Read what customers are saying about RosinCloud in our testimonials section.
</p>

<h3 style="margin-left:2.5%;">
Server Migration Features:
</h3>
<ul>
    <li>Expand Your Services to Fit Your Needs</li>
    <li>Migration Services offered 24/7/365</li>
    <li>Hardware & Operating System Upgrades</li>
    <li>Receive Constant Status Updates</li>
    <li>Experienced Certified technicians</li>
    <li>Moving from a Previous Provider</li>
    <li>Reduced downtime</li>
    <li>No data-loss</li>
    <li>Save Time & Money</li>    
</ul>

<p>
Any company using technology, or hosting their own website will at some point need to conduct server migrations. This can 
be extremely stressful as you move your most valuable asset, your data, from your inhouse server to a hosted servers, 
physical or virtual.RosinCloud has been helping customers to plan and perform safe migrations for years. Lets us help 
you to minimize stress while your business operations remain operational during the transition
</p>

<h3 style="margin-left:2.5%;">
Our Process
</h3>
<ul style="list-style: decimal outside none; margin-bottom: 0;">
    <li>
    Our first step is always to assess our client's current situation. We start all migration projects with a discovery call 
    to identify customer priorities and critical points of the data and server migration.
    </li>
    <li>
    At that point we'll have remote desktop sessions or in-person meeting where our staff will complete a thorough analysis 
    of your servers, Active Directory, Exchange, SQL, IIS, and ask a series of questions to gather information not identified 
    during earlier discovery as well other other business requirements.
    </li>
    <li>
    Based on that information, we'll create an Initial estimate and a proposal will be provided. We'll go over that proposal 
    together and identify any outstanding issues or questions.
    </li>
    <li>
    Once the project is committed, RosinCloud will generate a migration change document noting each and every task that wil 
    be carried out in the form of a project plan.<br />
    The document will include details including:
    </li>    
</ul>
<ul>
	<li>Database(s) migrated</li>
	<li>Website(s) Migrated</li>
	<li>Host Headers</li>
	<li>Site Security</li>
	<li>Site Files</li>
	<li>Site DB Connections</li>
	<li>Backup configuation</li>
	<li>Finally, as a team, we'll plan the cut-over date</li>
	<li>RosinCloud will execute the cut-over</li>
</ul>

<p>
RosinCloud will provide post migration documentation and once you are entirely satisfied, you can sign off on the 
completion of the project.
</p>

<h3 style="margin-left:2.5%;">
Why It's Important To Work With Experts
</h3>
<ul>
	<li>RosinCloud has handled hundreds of server migrations, of varying configurations</li>
	<li>We have years of experience migrating customers to colocation and cloud solutions</li>
</ul>

<h3 style="margin-left:2.5%;">
Database Migration Services
</h3>
<p>
RosinCloud can transfer your SQL databases from your previous service to ours. We have a team of highly qualified and 
experienced professionals eager to assist you with all of your migration needs.
</p>

<h3 style="margin-left:2.5%;">
We'll move your database quickly and safely
</h3>
<p>
Rest assured that when our Certified Support Technicians manage your migration, you have hired the best in the industry! 
An improperly managed and executed migration can leave you with downtime, data loss, and countless, hidden problems that 
can crop up down the road. Don't let a bad migration happen to you.
</p>

<h3 style="margin-left:2.5%;">
And above all else, World Class Customer Service
</h3>
<p>
RosinCloud prides itself on our World Class Customer Service. We invite you to contact us today to discuss your needs. 
Read what customers are saying about RosinCloud in our testimonials section.
</p>

<h3 style="margin-left:2.5%;">
Leave nothing to chance
</h3>
<p>
A database migration can be a complicated and exhausting process, especially as the size and number of databases that 
need to be moved grows. We know that every piece of data is important, and we won't risk losing a single bit. With our 
Managed Migration Services, let RosinCloud take the task of your database migration off your hands. Each of our support 
technicians is well-experienced in database migrations, and will provide the focus and attention to detail that your 
data requires.
</p>
<!-- END TEXT -->
                                </section>
                                <!-- END cell -->

							</div>
                            <!-- END 12u -->
							
						</div>
                        <!-- END row -->
					</section>
			
			</div>
            <!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<style>
	.cell{/*
		height: 450px;*/
	}
	.cell ul
	{
		text-align: right;
	}
	.cell h3
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */
	
		text-align: left;
		padding: 2em .5em .5em;
		color: #D4D6D7;
	}
	.cell p
	{/*
		height: 250px;
		overflow: hidden;*/
		padding: 5%;
	}
</style>
<!-- Features Wrapper -->
<div id="features-wrapper" style="padding-bottom: 50px;">

	<!-- Features -->
  	<section id="features" class="container">
    <header>
        <h2 style="margin-bottom:0px;">White Papers</h2>
        <h3>Provided by <strong>RosinCloud</strong></h3>
	</header>
    	<!-- BEGIN ROW -->
        <div class="row">
        
        	<div class="4u">
                <!-- Feature -->
                <section class="cell">
                    <header>
                      <h3>What Makes ITaaS Transformational?</h3>
                    </header>
                    <blockquote>
                        “There’s nothing really new with cloud computing.  Don’t you remember mainframe and 
                        timesharing?”
                    </blockquote>
                    <p style="text-align:justify;">
                        Anyone involved with cloud in the early days of the market heard this naive and mildly annoying 
                        comment fairly frequently.  While thankfully it’s largely disappeared, a new version of the 
                        “nothing new here” meme has appeared in enterprise cloud.
                    </p>
                    <ul class="actions">
                        <li>
                            <a href="<?=$ROOT_PATH ?>white_paper_ITaas_Transform.php" class="button button-icon icon icon-file">
                                Read More
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
            <!-- END 3u -->
        
        	<div class="4u">
                <!-- Feature -->
                <section class="cell">
                    <header>
                      <h3>Should You Outsource or Keep IT In-House?</h3>
                    </header>
                    <p style="text-align:justify;">
                    	It's nice to have an IT staffer in house who has intimate knowledge of your business, your 
                        network and your needs. However, few individuals are experts in all areas, and those who have 
                        the requisite skills don't come cheap. No individual can be available 24/7 either. For many small 
                        and medium-sized businesses, outsourcing may be a better solution.
                    </p>
                    <ul class="actions">
                        <li>
                            <a href="<?=$ROOT_PATH ?>white_paper_outsource.php" class="button button-icon icon icon-file">
                                Read More
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
            <!-- END 3u -->
        
        	<div class="4u">
                <!-- Feature -->
                <section class="cell">
                    <header>
                      <h3>IT As A Service (ITAAS)</h3>
                    </header>
                    <p style="text-align:justify;">
                        For years, the promise of running IT departments like an internal service provider in an 
                        IT-as-a-Service (ITaaS) model has been elusive. Frameworks such as ITIL have provided an 
                        impetus for this service mentality, but with an emphasis on IT operations…
                    </p>
                    <ul class="actions">
                        <li>
                            <a href="<?=$ROOT_PATH ?>white_paper_ITAAS.php" class="button button-icon icon icon-file">
                                Read More
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
            <!-- END 3u -->

            
        </div>
    <!-- END row -->
        
    <!-- BEGIN ROW -->
        <div class="row">
        
            
            <div class="4u">
                <!-- Feature -->
                <section class="cell">
                    <header>
                      <h3>CapEx vs. OpEx</h3>
                    </header>
                    <p style="text-align:justify;">
                    <a href="<?=$ROOT_PATH ?>white_paper_Cap_vs_Op.php" class="image image-left">
                        <img src="images/capex-vs-opex-688x269.png" alt="CapEx vs OpEx" longdesc="images/capex-vs-opex-688x269.png" />
                    </a>
                        “The CFO is increasingly becoming the top technology investment decision-maker in many 
                        organizations,” according to a recent study by Gartner and the Financial Executives Research 
                        Foundation. In nearly 500 enterprises surveyed, 42 percent of all CIOs…
                    </p>
                    <ul class="actions">
                        <li>
                            <a href="<?=$ROOT_PATH ?>white_paper_Cap_vs_Op.php" class="button button-icon icon icon-file">
                                Read More
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
            <!-- END 3u -->
        
            <div class="4u">
                <!-- Feature -->
                <section class="cell">
                    <header>
                      <h3>Data Center Relocation</h3>
                    </header>
                    <p style="text-align:justify;">
                        During these rough economic times, more and more companies will relocate their data centers (DC). 
                        There are tons of business reasons for moving a data center to a new location – consolidation of 
                        regional centers into a single site, acquisitions and mergers, and corporate headquarters 
                        relocations to name a few. Aging infrastructure, insufficient power or cooling, space limitations 
                        and lease renewal also trigger relocation decisions. It doesn’t matter what the business decision 
                        is for relocation because the same level of careful planning, expert execution and experience is 
                        required for a successful move.
                    </p>
                    <ul class="actions">
                        <li>
                            <a href="<?=$ROOT_PATH ?>white_paper_DC Relocation.php" class="button button-icon icon icon-file">
                                Read More
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
            <!-- END 3u -->
        
        	<div class="4u">
            <!-- Feature -->
                <section class="cell">
                    <header>
                      <h3>Technology Thoughts on P2V Server Consolidation Initiatives</h3>
                    </header>
                    <p style="text-align:justify;">
                    The hardest part of a virtualization project is not making the initial decision to virtualize. Hardware 
                    utilization and energy cost savings together provide enough incentive for an organization to make the 
                    decision. The management concerns about reliability and effectiveness have also been mostly rebutted 
                    as virtualization has proven to be an effective computing platform with solutions that are becoming 
                    more capable and easy to use. No, the hardest part of going virtual is the conversion from physical 
                    systems. This can be due to staff availability and expertise, budget, creating realistic conversion 
                    windows, and downtime which can all be major roadblocks in the road to a successful P2V conversion.
                    </p>
                    <ul class="actions">
                        <li>
                            <a href="<?=$ROOT_PATH ?>white_paper_P2V_consolidation.php" class="button button-icon icon icon-file">
                                Read More
                            </a>
                        </li>
                    </ul>
                </section>
                
            </div>
            <!-- END 3u -->
        
        </div>
    <!-- END ROW -->
    
    </section>
    <!-- END features -->
</div>
<!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Professional IT Consulting</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>services.php" class="image image-left">
                                        	<img src="<?=$IMG_PATH ?>itcs.png" alt="IT Consulting" />
										</a>
										<header>
										  <h2>Professional IT Consulting</h2>
										</header>
<!-- Begin TEXT -->
<p>
We want you to think of us as an extension of your own company or IT department.
</p>
<p>
When we are consulting to you and your company about designing, building, and maintaining a network, we think of it as 
our own and provide the best solutions possible.
</p>
<p>
RosinCloud operates a highly available network serving hundreds of customers, using a diverse mix of hardware and 
software. The network has to run smoothly 24 hours a day, 7 days a week, 365 days a year, with as few performance 
problems or outages as possible. Whether you have 5 or 500 employees, we can apply our industry experience to helping 
your business.
</p>
<p>
Bringing your servers, workstations, mobile devices, voice service and office phone system into 2013 is easier than you 
think.  RosinCloud will help you reduce your long-term costs, keep your data safe but still easily accessible, and help 
you and your employees work more efficiently.
</p>
<p>
Our approach starts with a consultation to consider your unique needs.  Then we provide services to power your complete 
IT infrastructure for one organized, sensible, and predictable monthly fee.
</p>
<p>
With RosinCloud, you receive our commitment to clearly explain the technology situation you are facing, present you with 
the best options available as well as recommend solutions we believe will serve your company best. Whether it’s a basic 
support call to technology planning, our goal is to be your trusted technology advisor that helps you to run your 
business as efficiently as possible.
</p>
<!-- END TEXT -->
									</section>

							</div>
							
						</div>
					</section>
			
			</div>
		
<?php
	require_once('includes/footer.php');
?>
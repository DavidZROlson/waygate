<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">White Papers</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header>
						<div class="row">
							
							<div class="12u">

                                <!-- Feature -->
                                <section class="cell">
                                    <header>
                                    	<h2>CapEx vs. OpEx</h2>
                                        <h3>Posted on February 15, 2013</h3>
                                    </header>
<!-- BEGIN TEXT -->
<style type="text/css">
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		font-size: 13pt;
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h3
	{
		text-align: left;
		padding-left: 2.5%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */

background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		margin-top: .25em;
		color: #D4D6D7;
	}
	.cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%;
	}
</style>

<img class="image image-left" src="images/capex-vs-opex-688x269.png" alt="CapEX vs. OpEx" longdesc="images/capex-vs-opex-688x269.png" />

<h3>
THINK LIKE A CFO
</h3>
<p>
<span style="margin-left:2em;">“The</span> CFO is increasingly becoming the top technology investment decision-maker in 
many organizations,” according to a recent study by Gartner and the Financial Executives Research Foundation. In nearly 
500 enterprises surveyed, 42 percent of all CIOs report to the CFO, and in three out of four companies, the CFO has a 
major hand in all IT spending. Unfortunately, not all CIOs and CFOs work well together: By not accounting for one 
another’s viewpoints, they often fail to build a good working relationship.
</p>
<h3>
CIO DIRECTION
</h3>
<p>
<span style="margin-left:2em;">The</span> study recommends this: “CIOs must understand the impact their CFOs have on 
technology decisions in their organizations, and ensure that they are… communicating the business value that can be 
achieved.” At a conference last fall, Gartner went even further. “2010 marks the year in which IT needs to demonstrate… 
business objectives for every investment,” said its analysts. “CIOs need to model the economic impact of IT on the 
overall financial performance of an organization.” For many CIOs, that’s a new way of looking at things. But why not 
start thinking more like a CFO? Why not find ways to stretch your IT budget like a CFO does? Part of being successful 
as a CIO means recognizing what the top-of-mind issues are for the CFO sitting across the hall.
</p>
<h3>
THE CAPEX VS. OPEX DIVIDE
</h3>
<p>
<span style="margin-left:2em;">One</span> classic way to stretch your budget is to shift capital expenses to operating 
expenses. Not sure of the difference?
</p>
<p>
<span style="margin-left:2em;">Capital</span> expenditures include acquiring fixed assets (tangible, e.g. machinery or 
intangible e.g. patents), fixing problems with an asset, preparing an asset to be used in business, restoring property 
so that value is added, or adapting it to a new or different use.
</p>
<p>
<span style="margin-left:2em;">Operating</span> expenditures include license fees, maintenance and repairs, advertising, 
office expenses, supplies, attorney fees and legal fees, utilities such as telephone, insurance, property management, 
property taxes, travel and vehicle expenses, leasing commissions, salary and wages, raw materials.
</p>
<p>
<span style="margin-left:2em;">As</span> you can see, capital expenses are long-term investments. The drawback is that 
you pay all the money up-front, but you get the value back in a trickle over the next few years. On the other hand, 
operating expenses are pay-as-you-go. You pay by the month, and you get value every month.
</p>
<h3>
OUT WITH CAPEX & IN WITH OPEX
</h3>
<p>
<span style="margin-left:2em;">As</span> a recent article in ComputerWorldUK put it, “In the pursuit of maintaining a 
lean balance sheet with optimum cash flow, boards are paring back on new capital investments (CapEx), opting wherever 
possible to fund projects from operating expenditures (OpEx) instead.” One reason for this is the rapid changes in 
technology. For example, new servers run faster, use less energy and provide more cores every year. It doesn’t make 
sense to sink money into equipment that’s surpassed by the very next model. The rapid rise of SaaS, outsourcing and 
managed services shows that many things that were once do-it-yourself are now outsourced to service providers. Costs 
are reduced and shifted from capital expenses to operating expenses, which save money and look better on financial 
statements.
</p>
<h3>
EXAMPLE: BUYING A LASER PRINTER
</h3>
<p>
<span style="margin-left:2em;">Suppose</span> a group of users complain they can’t get the 11×17-inch color printouts 
they need for trade shows. Your nearest color printer that can do the job is in another building half a mile away. Like 
a good CIO, you listen and propose to get them a new printer. Buying the printer means making a lump-sum capital 
expenditure. Of course, the printer will last longer than one year. So even though the cash to pay for it will be gone, 
your company won’t be able to expense that full cost this year. That asset will be carried on your company’s books for 
the next three years. If the printer costs $6,000 your company can claim $2,000 in expenses this year. Next year it can 
claim $2,000 in depreciation; then in Year 3 the final $2,000. What’s wrong with that? Nothing, if you have lots of 
money. But everything, when you’re under pressure to do more with less. When you propose this to your CFO, he will 
probably ask a lot of irritating questions:
</p>
<ul>
    <li>How vital is that new printer?</li>
    <li>Why can’t they get printouts delivered from the other building?</li>
    <li>What if we spent that money on something else?</li>
</ul>
<p>
<span style="margin-left:2em;">For</span> example, if your company spends $6,000 more on marketing, that can generate 
more sales. That $6,000 cash could become $12,000 or $20,000 in income, which looks much better on the books than a 
$2,000 capital expense plus $4,000 in future depreciation. The point is: if it’s not tied up in a new laser printer, 
that money could be working harder for your organization.
</p>
<h3>
ALTERNATE: OUTSOURCING THE COLOR PRINTING
</h3>
<p>
<span style="margin-left:2em;">Suppose</span> you find out your users need about 150 11×17-inch printouts a year, which 
they can get from an outside printing service for $4 a pop, with free delivery. That means they can get all the big 
color prints they need for $600 a year. This transforms a potential $6,000 capital expense into a $600 operating 
expense. Any CFO would prefer this option. Why? This saves your company $5,400 in cash to invest somewhere else, and it 
avoids carrying another $4,000 in depreciation for the next two years. Your users, perhaps with a little grumbling, will 
likely be satisfied. This is what Gartner means by modeling the economic impact of IT decisions on your company’s 
financial performance. CIOs who think this way are ready to form a powerful partnership with their CFOs.
</p>
<h3>
OUTSOURCING YOUR SERVER MANAGEMENT
</h3>
<p>
<span style="margin-left:2em;">To</span> carry on this thinking, here’s an area where most IT departments can easily do 
more with less: outsourcing your server management. Before you reject this idea, consider about these comments from CIO 
magazine.
</p>
<p>
<span style="margin-left:2em;">“As</span> one colleague who consults on financial strategy with many large companies 
said, ‘You know what we think of IT? We think it shows up, spouts unintelligible jargon, and asks for huge lumps of 
cash…’ “It’s easy to understand why any initiative that promises to reduce lumpy capital investment and transform it 
into smoother operational expenditure would be extremely attractive to bean counters.”
</p>
<p>
<span style="margin-left:2em;">If</span> your CFO could save money on servers, push capital expenses into operating 
expenses, and get the same level of service for your users, would he be interested? Of course he would. So why not 
consider this yourself, instead of waiting for him to suggest it?
</p>
<!-- END TEXT -->
									</section>
									
							</div>
                            
						</div>
					</section>
			
			</div>
            <!-- END 12u -->
		
<?php
	require_once('includes/footer.php');
?>
<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">White Papers</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header>
						<div class="row">
							
							<div class="12u">

                                <!-- Feature -->
                                <section class="cell">
                                    <header>
                                    	<h2>IT As A Service (ITAAS)</h2>
                                        <h3>Posted on February 16, 2013</h3>
                                    </header>
<!-- BEGIN TEXT -->
<style type="text/css">
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		font-size: 13pt;
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h3
	{
		text-align: left;
		padding-left: 2.5%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */

background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		margin-top: .25em;
		color: #D4D6D7;
	}
</style>
<p>
<span style="margin-left:2em;">For</span> years, the promise of running IT departments like an internal service provider in an IT-as-a-Service (ITAAS) model 
has been elusive. Frameworks such as ITIL have provided an impetus for this service mentality, but with an emphasis on 
IT operations and less focus on infrastructure and application development. The result was still a siloed IT environment 
held together by heroic efforts. The majority of IT spending is dedicated to “keep the lights on” activities, hindering 
IT’s ability to keep up with the pace of business innovation. Enter virtualization and cloud computing: essential 
building blocks for the agility, flexibility, and “services” focus that IT needs to deliver to the business. These 
advances couldn’t have come at a better time. The current business environment dictates that IT needs to respond to 
the business faster than ever before. This goes beyond being able to simply provide a new server; it is about the 
end-to-end IT services—spanning applications, infrastructure, and operations—that help the business improve its 
competitiveness and capture new revenue. Organizations strive to be able to instantly respond to change or dramatically 
improve a specific business process. This is not lost on IT—in fact, many organizations are beginning to change their 
investment justifications to ensure that any new technology purchase or operational initiative is geared toward business 
process improvement, a primary justification for new IT expenditures over the past few years. Clearly, IT is changing 
focus, moving away from cost optimization drivers fueled by macroeconomic woes. But focus on improving business processes 
is only part of the equation. IT departments are also looking to transform their operating models and infrastructure 
into environments previously seen in cloud computing providers and other external service providers.
</p>
<p>
<span style="margin-left:2em;">The</span> ITaaS approach requires a transformation, and the IT organization requires solutions that will help enable it. This 
includes deploying highly automated virtualization technologies, coupled with a private cloud framework, to provide 
on-demand computing resources in a shared, self-service model with a catalog of standardized service options. If the 
organization chooses to broker external services, then it will require policy-based controls and orchestration to manage 
this combination of internal (private cloud) and external services (public cloud). Critical to the success of this blend 
of approaches is maintaining clarity and uniformity in how users interact with IT and, in particular, how both IT and the 
business can gain access to resources across what would be a mix of providers.
</p>
<p>
<span style="margin-left:2em;">Often</span> underestimated aspects of success in major transitions are skills and behavioral change. Recent ESG research data 
points both to a problematic shortage of skilled resources to build virtual environments that will serve as the 
underpinnings of cloud, and also to a lack of internal skills as an inhibitor to fully achieving the benefits of 
virtualization and cloud. Cloud computing is as much about the operational model as it is about the technology. This 
requires expertise in areas such as resource pooling, metered usage, and self-service provisioning.
</p>
<p>
<span style="margin-left:2em;">In</span> addition to skills, organizations often ignore the changes needed in behavior. This includes the siloed nature of IT 
organizations, which makes it difficult to implement and take advantage of shared pools of resources in a pay-per- use 
model, as well as how business users, application developers, and IT architects are accustomed to requesting à la carte 
IT resources as one-off requests. Solutions that provide IT and the business with a “menu” of standard service options 
and a clear understanding of the trade-offs around unit cost and time to provisioning for “off the menu” choices will 
help facilitate the behavioral changes that need to take place. Ultimately, these tools must put the choice in the hands 
of the IT consumer so they can make informed decisions while providing the right governance and control to ensure that 
those choices comply with operational and security policies.
</p>
<p>
<span style="margin-left:2em;">Organizations</span> cannot make this transformation happen without help. Vendors need to step up and not only deliver the 
technology and infrastructure, but also assist with the frameworks, software, and process changes that will be required 
to adjust the fundamental behavior of the IT organization.
</p>

<h3>
IT-as-a-Service (ITAAS)
</h3>
<p>
<span style="margin-left:2em;">Why</span> are so many organizations interested in delivering ITaaS? If implemented correctly, it can deliver numerous 
capabilities which will ultimately translate to business benefits. Some of those capabilities include:
</p>

<h3>
Speed and agility.
</h3>
<p>
<span style="margin-left:2em;">IT’s</span> ability to respond more quickly translates into faster provisioning of services that fuel the business. By driving 
innovation and differentiation in today’s always-on business environment, IT can create a sustainable competitive 
advantage and go way beyond being “just” a cost center, which is how the static and inflexible legacy environments in so 
many data centers today are often perceived.
</p>

<h3>
Leveraging virtualization to enable cloud computing.
</h3>
<p>
<span style="margin-left:2em;">The</span> ITaaS model implies the adoption of cloud computing and requires a number of changes. Typically the first change has 
related to technology—more specifically, to server virtualization. In fact, ESG’s annual IT spending survey indicated 
that “increased use of server virtualization” was the number one IT priority for the last two years. It should also be 
noted that this research indicated that cloud computing was the fastest climber from 2010 to 2011, demonstrating that 
organizations were going beyond just the ”interested” stage and actually allocating IT budget to create cloud computing 
environments. To further underscore the relationship between server virtualization and cloud computing, a prior ESG 
survey of current server virtualization users revealed that building a private cloud registered among the top five IT 
priorities for these organizations (see Figure 3).4 Cloud computing is much more than virtualization; however, in most 
cases, maturity in server virtualization is a precursor to private cloud adoption.
</p>

<h3>
Unified, converged infrastructures.
</h3>
<p>
<span style="margin-left:2em;">Cloud</span> computing is also a new consumption model, which has an impact on how those environments are built. The new 
building blocks for the cloud are not just individual VMs, physical servers, networks, or storage equipment, but rather 
a new unit of tightly integrated and converged infrastructures. These new converged platforms are delivered either as a 
complete solution or in the form of a reference architecture that can be used to build out private cloud environments. 
This IaaS layer is the foundation for use cases ranging from application test/dev to virtual desktops. It shouldn’t be 
surprising, then, that many service providers have already adopted this architecture to deliver enterprise class cloud 
services as well.
</p>

<h3>
Scalability.
</h3>
<p>
<span style="margin-left:2em;">In</span> a highly dynamic environment, IT needs to rapidly scale. This is especially true for virtualized deployments. Indeed, 
ESG research indicates that virtual environments will be scaling up rapidly over the next two years, with the majority of 
organizations reporting significant growth in both the number of virtualized servers and the number of virtualized 
servers running in production environments. The latter is much more important as the production environment requires much 
higher levels of availability and protection. It should be noted, however, that the ability to scale does not only refer 
to long term growth. It also refers to the ability to burst applications for short timeframes. This could include extra 
capacity for end of month/end of the quarter processes or to handle a special promotion from marketing. In a hybrid cloud 
model, this implies a “build the base, rent the spike” approach. Scale also includes more than one dimension: in addition 
to scaling up, organizations also need to scale back down and return resources to a general pool for better 
optimization. This implies that a lifecycle approach is required, whether for shutting down public cloud instances or 
re-purposing unused virtual machines in a private cloud.
</p>

<h3>
Simplification.
</h3>
<p>
<span style="margin-left:2em;">This</span> is critical for the initial and long-term success of any ITaaS platform. The key is to have an easy to use and 
intuitive self-service portal interface for both IT and business users. This is certainly no easy task—the portal is 
abstracting all the complexity of the highly virtualized and rapidly changing infrastructure of the data center. The 
user, however, may not care about that. All the user knows is that they need a service to meet their needs—how many VMs 
and how much storage is required may be irrelevant. This is true across the board, whether it is a revenue- generating 
mission critical application or test/dev environment for QA. However, in this case, the environments should be vastly 
different in terms of availability and data protection. This will also translate into different costs to deliver the 
services, which must be clearly understood through the portal so the business can make informed and intelligent 
decisions. While simplicity is preferred for less technical users, the portal should also allow advanced IT users to 
have the option to view infrastructure details based on their role.
</p>

<h3>
Automation.
</h3>
<p>
<span style="margin-left:2em;">With</span> highly dynamic environments, rapid change can be accommodated by individual acts of heroism from the IT staff for a 
limited time, but it is not sustainable. The key to enabling long term repeatable success shouldn’t be based solely on 
the quality of the IT staff and their skill sets, but rather on policy-based automation and orchestration of manual 
repetitive tasks. This will allow the best and brightest on the IT staff to formalize best practices that will mitigate 
risk and enable more effective governance and control. It will also free staff to work on more strategic initiatives and 
help drive even more efficiencies for the business.
</p>

<h3>
Required Building Blocks for the Cloud Computing Era
</h3>
<p>
<span style="margin-left:2em;">In</span> order to meet the needs of the business, new operating models and architectures are being developed—ones that more 
closely resemble a service provider environment than in an enterprise. While the infrastructure plays a critical role, 
this transformation also requires new tools to help re-architect existing processes and automate or orchestrate 
previously manually-intensive operations. So while the new technology and infrastructure will be important building 
blocks, there are other required building blocks to enable a cloud service. They include:
</p>

<h3>
Service catalogs.
</h3>
<p>
<span style="margin-left:2em;">A</span> big part of developing a new operating model for cloud computing is establishing a means of standardization. A catalog 
of standard offerings is critical as it reduces complexity and eliminates time wasted by provisioning highly varied 
requests. Standardization of services is essential for on-demand provisioning: a well-defined set of standard service 
options results in greater benefits through automation and more cost-effective service delivery.
</p>

<h3>
Self-service portals.
</h3>
<p>
<span style="margin-left:2em;">In</span> order to deliver the agility and speed required in today’s fast-paced environment, organizations will also need to 
make self-service provisioning an essential component. This will require the creation of an easy to use portal with 
policy-based controls and governance. Keep in mind that the portal will be the main interface for both business and IT 
users to procure services regardless of their role and where they are located. This will be the face of the IT service 
broker.
</p>

<h3>
Process automation and orchestration.
</h3>
<p>
<span style="margin-left:2em;">The</span> service catalog and portal definitely increase flexibility and efficiency in the ordering of services; however, IT 
needs to be able to deliver those services almost as quickly as they were ordered. On the back end of the service 
catalog, organizations need ties into orchestration and automation engines to eliminate time-consuming and error-prone 
manual provisioning processes. This should also include the concept of lifecycle management for the ongoing maintenance 
and decommissioning of services.
</p>

<h3>
Highly virtualized, converged infrastructures.
</h3>
<p>
<span style="margin-left:2em;">As</span> previously mentioned, prepackaged (or at least pre-architected) environments purpose-built for the cloud are becoming 
the new units of compute. This consists of tightly integrated physical servers (typically blades), high performance 
networks, and enterprise-class storage designed for virtualization and built to handle scalable, dynamic environments. 
Ideally, these converged infrastructures will be integrated with the other building blocks outlined above and will help 
to remove the complexity typically associated with large scale IT environments. Because these units are preconfigured 
and tested—they should deploy faster and without the typical inter-technology domain integration pain points.
</p>
<!-- END TEXT -->
									</section>
									
							</div>
                            
						</div>
					</section>
			
			</div>
            <!-- END 12u -->
		
<?php
	require_once('includes/footer.php');
?>
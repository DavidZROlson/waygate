<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">White Papers</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header>
						<div class="row">
							
							<div class="12u">

                                <!-- Feature -->
                                <section class="cell">
                                    <header>
                                    	<h2>Technology Thoughts on P2V Server Consolidation Initiatives</h2>
                                        <h3>Posted on February 20, 2013</h3>
                                    </header>
<!-- BEGIN TEXT -->
<style type="text/css">
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		font-size: 13pt;
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h3
	{
		font-size: 13pt;
		text-align: left;
		padding-left: 2.5%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */

background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		margin-top: .25em;
		color: #D4D6D7;
	}
	.cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%;
	}
</style>
<p>
<span style="margin-left:2em;">The</span> hardest part of a virtualization project is not making the initial decision to 
virtualize. Hardware utilization and energy cost savings together provide enough incentive for an organization to make 
the decision. The management concerns about reliability and effectiveness have also been mostly rebutted as 
virtualization has proven to be an effective computing platform with solutions that are becoming more capable and easy to 
use. No, the hardest part of going virtual is the conversion from physical systems. This can be due to staff availability 
and expertise, budget, creating realistic conversion windows, and downtime which can all be major roadblocks in the road 
to a successful P2V conversion.
</p>
<p>
<span style="margin-left:2em;">Organizations</span> often conclude that a switch to virtual systems would be beneficial, 
but they are lacking the experience to effectively plan for the time and resources to make the switch. The projected 
costs for system conversion and integration can overshadow the costs of licensing the virtualization infrastructure and 
complementary applications. However, the development and introduction of resource-efficient and cost-effective conversion 
solutions is starting to change that.
</p>
<p>
<span style="margin-left:2em;">Resource</span> availability can be one of the biggest challenges. Organizations that are 
planning a large-scale conversion to virtualization using free/limited/semi-automated tools, or possibly even manual 
processes, may find that there are not enough off-peak hours or IT staff to successfully complete the conversions. 
Consider that taking inventory of current hardware and settings, scheduling downtime and reboots, and developing needed 
scripts and interfaces can be very time-consuming. This does not even include the large amount of time needed to copy, 
convert, and install files and systems. One manufacturer estimated that it would take 13.4 man hours per server to 
prepare and convert it using a free conversion tool from its virtual infrastructure. In fact, the labor cost to convert 
using this “free” tool was calculated at more than $4 million for the 1,800 – server virtualization project. The costs 
endangered the projects success until a faster and more automated P2V solution could be found. This solution would 
hopefully reduce the cost of P2V by 75% while still converting the same number of servers, in less time, and with fewer 
staff.
</p>
<p>
<span style="margin-left:2em;">Even</span> if the IT staff could be spared from their daily responsibilities, to 
participate in a migration project, they may not have the expertise to perform certain aspects of it. A lot of cloning 
and duplication tools are complex and not well supported. Fully automated ‘packaged solutions’ are easy to use, but are 
not available for all environments. Some package solutions are expensive and not user-friendly. The more time that 
internal staff spends learning about the intricacies of conversions and managing the transition, the more expensive the 
project becomes.
</p>
<p>
<span style="margin-left:2em;">Outsourcing</span> the task to a systems integrator or virtualization services provider 
can save an organization many conversion headaches. There is a price for convenience though, and the provider is only 
able to choose from the same options as the organization. The solution providers advantage is experience and familiarity 
with the solutions. This should save time and contain project costs. Often a combination of internal and external 
expertise is applied to execute the conversion project.
</p>
<h3>
What to Consider With P2V Conversion
</h3>
<p>
<span style="margin-left:2em;">There</span> are several questions to consider before making the decision on which path 
to take for P2V conversions. These questions will provide insight for the planning team and could be helpful in 
determining needs and narrowing solution options.
</p>
<ul>
    <li>How soon do you need to to establish a virtualized infrastructure, and a conversion solution?</li>
    <li>What are the current P2V skills and experience of your intended P2V administrators?</li>
    <li>How many servers must be converted, and are you taking a phased approach?</li>
    <li>What types of physical machines will be converted, and are you categorizing by spec, function and current utilization rates?</li>
    <li>How much time are you estimating per conversion (range, average)? Can you take down the primary system for the conversion, or must it remain available to business users during normal hours?</li>
    <li>How many conversions do you want to complete in each window of opportunity (e.g., evening or weekend sessions), and how large is that conversion window (e.g. 15 hours)?</li>
    <li>Is there a need to make conversions among different virtual platform formats (heterogeneous P2V, or V2V conversion)</li>
    <li>How important are the following aspects of the conversion solution: setup time, conversion speed, reliability and required physical server downtime?[1]</li>
</ul>
<p>
<span style="margin-left:2em;">Considering</span> these issues will help shape functional requirements, as well as determine how to best accomplish the P2V 
conversion, whether to approach expert systems integrators and what the conversion cost is likely to be. When finding 
the optimal solution there must be a good understanding of the available conversion approaches and a fundamental 
understanding of every step in P2V preparation and conversion.
</p>
<h3>
Migration Economics
</h3>
<p>
<span style="margin-left:2em;">Let’s</span> use a recent real-life server migration initiative to illustrate how conversion speed and comprehensiveness of the 
P2V solution relate to project risk and the likelihood of success. A manufacturer wanted to virtualize 1,800 servers. 
There was only a 16.5 hour window between Friday and Saturday of each week when the servers could be taken down for 
conversion.
</p>
<p>
<span style="margin-left:2em;">The</span> manufacturer had some conversion experience using the conversion tools provided by the virtual platform provider and 
was able to estimate that it would take 13.4 man hours to convert each server. This calculation allowed the manufacturer 
to see that converting to a virtual infrastructure would be too time consuming and costly. With the estimate putting the 
conversion at more than $4 million.
</p>
<p>
<span style="margin-left:2em;">The</span> project systems integrator investigated and recommended using a high-speed packaged conversion solution. The 
third-party package was developed for large-scale migrations and could reduce the man hours needed for the complete 
conversion by nearly 400%. The savings offset the software licensing cost and made it possible and logical to go virtual.
</p>
<h3>
Essential Conversion Activities
</h3>
<p>
<span style="margin-left:2em;">What</span> makes a successful P2V migration is much more than the conversion technology itself. Success is based on how dozens 
of tasks and details are handled. These tasks include pre-conversion planning, preparation, and post-conversion testing. 
These are all vital. Some solutions do not automate any of these important, and time-consuming, operations and instead 
focus solely on the actual physical-to-virtual data conversion. Other solutions provide more coverage and automation of 
the entire process.
</p>
<p>
<span style="margin-left:2em;">To</span> evaluate the comprehensiveness and automation value of individual projects, it 
is helpful to separate the migration process into three steps. Pre-migration, conversion, and post-conversion 
processing. This allows the ability to evaluate internal capabilities and needs for each individual step.
</p>
<h3>
Pre-Migration
</h3>
<p>
<span style="margin-left:2em;">The</span> first step before P2V is to allow IT and virtualization-specific preparation 
to be performed. The following list specifies what tasks should be done and does seem fairly extensive and 
time-consuming, but the tasks can be done manually or with software-supported features.
</p>
<ul>
    <li>Inventory the target servers, including IP addresses, configurations, software and licensing details</li>
    <li>Check for application dependencies</li>
    <li>Install all patches</li>
    <li>Review event logs and correct any problems</li>
    <li>Defragment drives</li>
    <li>Unzip all files and drivers</li>
    <li>Disable services and agents on target and host servers</li>
    <li>Clean registries and remove virtual machine references</li>
    <li>Set a deadline for change requests</li>
    <li>Schedule downtime for conversion with business owners, admin</li>
    <li>Create new IP addresses</li>
    <li>Create administrator accounts and passwords for migration</li>
    <li>Cache administrator IDs and passwords</li>
    <li>Set and verify the command route and binding orders</li>
    <li>Check network capacity (bandwidth and availability) to transfer images[1]</li>
</ul>
<h3>
Conversion
</h3>
<p>
<span style="margin-left:2em;">It</span> takes more than running a conversion application to convert physical servers to 
virtual machines. P2V setup, including installation and configuration, is required prior to execution, though the 
processes and efforts vary greatly depending on the approach taken. For example, some conversions are done by first 
duplicating target files with a common cloning or backup tool, and then running a separate conversion application. 
Others automate copying, conversion and transfer, and can convert multiple servers simultaneously. The following steps 
typically occur during conversion, though some may be invisible to the user because they can be performed automatically.
</p>
<ul>
    <li>Install conversion tools or software</li>
    <li>Configure for single or multiple processors</li>
    <li>Configure networking and binding order on the new VM</li>
    <li>Select the server (and/or drives, files or individual blocks) to be virtualized</li>
    <li>Rename the server</li>
    <li>Configure/optimize the VM size</li>
    <li>Select the virtual host</li>
    <li>Configure the virtual CPU, memory and network settings</li>
    <li>Run conversion software (or schedule targets for conversion)</li>
    <li>Transfer images/files/data</li>
    <li>Monitor the networks and error conditions during transfer</li>
    <li>Verify the hardware abstraction layer (HAL)</li>
    <li>Register the VM on the server</li>
    <li>Create partitions on the virtual host</li>
    <li>Reboot, if necessary</li>
    <li>Begin converting the next target [1]</li>
</ul>
<p>
<span style="margin-left:2em;">The</span> time that is takes for the actual conversion and copying of the machine 
depends on network speeds as well as the underlying processing speed of the P2V conversion tool.
</p>
<h3>
Post-Conversion Processing
</h3>
<p>
<span style="margin-left:2em;">Preparation</span> and conversion create many opportunities for error, so newly converted 
VMs should be tested to ensure that conversion was successful. Even a completely successful conversion has several 
post-conversion tasks that must be completed.
</p>
<ul>
    <li>Review logs</li>
    <li>Reactivate services and agents</li>
    <li>Update drivers</li>
    <li>Install and configure VM platform components (VMware, Tools)</li>
    <li>Run quality-control tests</li>
    <li>Run user-generated scripts</li>
    <li>Activate and synchronize VM</li>
    <li>Remove conversion software</li>
    <li>Create and file all documentation</li>
    <li>Set end-of-life for converted server</li>
</ul>
<p>
<span style="margin-left:2em;">A</span> high-speed converter can make the actual conversion the least time-consuming 
step. Conversion can take a few minutes or a few hours to complete per server, compared to the potentially many hours 
for preparation and post-processing. How quickly the conversion step executes is not a true indication of the solution’s 
speed. Total-time-elapsed depends upon the approach chosen and the extent to which the overall process is automated.
</p>
<h3>
Examining the Options
</h3>
<p>
<span style="margin-left:2em;">The</span> main differentiators for P2V conversion approaches are speed, ease-of-use, 
tool comprehensiveness, automation breadth, reliability, and cost.  Speed and automation are arguably the two most 
important, because they impact the other factors. Slow converters can take more that 20 hours for larger VMs. When only 
two, three, or four conversions can be run simultaneously then the number of conversions possible per conversion window 
is small. Faster converters with high-simultaneous capability can significantly increase the number of conversions per 
conversion window. This can reduce the overall project length from years to months.
</p>
<p>
<span style="margin-left:2em;">Automation</span> breadth is also important, many pre- and post- conversion activities 
that must be performed can be automated by the conversion tool. This speeds up the overall time, increases reliability, 
and simplifies the process for the conversion team.
</p>
<p>
<span style="margin-left:2em;">Ease-of-use</span> impacts the amount of time and skills required to set up and initiate 
conversions. Unreliable conversion tools ultimately add cost and effort because they require rework and may compromise 
data quality that can lead to a set of problems that can cripple the conversion project. Cloning tools offer the lowest 
level of functionality and require the most time and effort. Conversion tools from virtualization platform providers 
make the job easier, but many functions must still be done manually and IT support time is still required to use the 
tools. Third-party conversion tools are more comprehensive. They also vary greatly in functionality, price, and speed, 
so it is difficult to generalize their value.
</p>
<p>
<span style="margin-left:2em;">There</span> are multiple conversion options available to organizations. This will 
provide an overview of the main options.
</p>
<ul>
    <li>
        Cloning, copying and boot cds – Organizations often use legacy cloning software and supplement it with a virtual 
        machine conversion utility and boot CD to capture, convert and transfer files. Many professionals get their introduction 
        to P2V migration by using familiar utilities to copy files, which are converted into test VMs for evaluation. The 
        utilities are inexpensive but can be time-consuming to configure and use. Cloning and copying are extremely manual 
        approaches compared to other options. The target is cloned or copied using one utility and converted with another. It is 
        then saved to a boot CD for transfer to the virtual server. As such, support is limited, and the cloning/copying utility 
        may not have features to simplify pre-migration tasks specific to virtualization. Another drawback is that a fairly 
        high-level of skill and familiarity with the various utilities is required for success. Cloning and copying are best 
        suited for experienced administrators conducting one-off conversions or extremely small projects.
    </li>
    <li>
    	Free conversion tools – For users with a homogenous virtual environment, vendor-supplied free tools are a step 
        up from the copy-and-boot approach because they combine the copying, conversion and installation functions. The 
        leading limitations to free tools are their scaled-back functionality, lack of automation breadth and 
        scalability. Administrators inexperienced with virtualization may find these tools incomplete for larger 
        conversion projects, and will discover that many steps to conversion must still be completed manually. Because 
        free tools are focused on the conversion aspect and do not attempt to automate much of the overall conversion 
        procedure, the total time required to prep, convert and troubleshoot servers can be lengthy. Conversion speeds 
        can also be slow for larger machines – more than 20 hours in some cases – which can be crippling for larger 
        projects that must squeeze as many conversions as possible into conversion windows. Most free products currently 
        available do not support hot migration; therefore, downtime is required. Free tools are best suited for 
        low-volume or periodic conversions, not for large-scale, time- sensitive migrations.[1]
    </li>
</ul>
<p>
<span style="margin-left:2em;">A</span> restriction on the number of simultaneous possible conversions also affects the 
numbers for conversion windows. A recently studied project suffered a limitation of two to three simultaneous 
conversions, so any tool that can increase that to five, 10 or more than 15 simultaneous conversions obviously affects 
the overall project duration.
</p>
<p>
<span style="margin-left:2em;">Packaged</span> solutions automate many tasks relating to preparation, conversion and 
post-processing, and often provide a single user interface for these phases. Some P2V solutions incorporate efficiency 
algorithms for increasing conversion speeds, be sure to check technical materials and third-party benchmarks. They 
provide the shortest total overall migration time, and may include scheduling and simultaneous conversion functions are 
suitable for large-scale projects. Most tools offer the option of hot cloning, or cold cloning methods. Be aware that 
cold cloning can either be performed manually via a boot CD or automatically via a PXE service.
</p>
<p>
<span style="margin-left:2em;">A</span> reason to consider packaged solutions for large-scale, high-volume conversion 
projects is ease-of-use. Not only are many tasks automated, but users also are guided through the process, so conversion 
does not have to be done by administrators or systems integrators with extensive virtualization experience. Packaged 
solutions also stand out from alternatives because they typically support multiple virtual platforms a convenience 
valued by users in heterogeneous virtual environments.
</p>
<p>
<span style="margin-left:2em;">Pricing</span> can put some packaged solutions out of reach once the economics of a 
conversion initiative have been calculated. It is important to consider Total Cost of Ownership (TCO) for packaged 
solutions compared to alternatives. Packaged solutions typically save time by automating multiple tasks, increasing 
conversion success rates and saving expenses on highly trained IT specialists. Packaged solutions are also an excellent 
option for organizations undergoing a large- or medium- scale P2V initiative, and also for those that anticipate ongoing 
protection for their physical servers. Packaged solutions can be suitable for one-off and small conversion products, but 
licensing terms are important considerations.
</p>
<p>
<span style="margin-left:2em;">Outsourcing</span> may be the only practical option for organizations with unique needs 
that are not supported by packaged conversion applications. Outsourcing is appropriate for organizations with severe 
time or personnel constraints. Solution providers may include P2V conversion as part of their virtualization project 
proposals, and organizations often favor comprehensive hardware, software and services contracts. If the turnkey 
approach is favored, organizations should still request a detailed breakdown of conversion costs and services so they 
can analyze whether it is more practical to outsource them or take a DIY approach. Solution providers often perform 
conversions using the same set of software and tools available directly to end users, so organizations should evaluate 
whether the convenience of outsourcing is worth the cost.
</p>
<h3>
Other Factors to Consider
</h3>
<p>
<span style="margin-left:2em;">There</span> are many different differentiators among the specific P2V solutions. 
Organizations who are considering looking deeper into them should consider the following.
</p>
<ul>
    <li>
    	What is the clock speed for the converter? Perform a side-by-side comparison of the converters under 
        consideration, utilizing the same network and machine specifications. The impact of that factor is very 
        significant for total project duration.
    </li>
	<li>
    	What is the end-to-end speed for full conversion? Benchmark data on the clock speed for conversion is useful for 
        planning and comparison but is not the complete measure of conversion time. As noted, the conversion step 
        represents a portion of the total time required to convert a physical server to a virtual one. Setup and 
        installation time, the ability to simultaneously convert multiple servers, documentation requirements and other 
        pre-/post- processing factors must all be considered when comparing end-to-end conversion times.
    </li>
	<li>
    	Do the conversions need to be performed “hot” or “cold”, can servers be offline and are reboots 
        possible/required? Rebooting and downtime add to the total process time that would not be reflected in benchmark 
        data on time elapsed for conversion. System overhead and impact on CPU performance should also be considered.
    </li>
	<li>
    	Where does the actual conversion take place? Many conversion systems capture and convert data at the file level. 
        The practice is so widespread it may not even be mentioned in product spec sheets. Block-level conversion is 
        emerging as an alternative. Cloning at the block level keeps the entire file system intact, which reduces the 
        chance of file errors occurring during the conversion process. There is also more system overhead at the file 
        level than at the block level, so block-level cloning and conversion can execute more quickly. File level 
        cloning is typically performed for P2V disaster recovery, where incremental updates are performed. Check to see 
        if the conversion tool has both capture mechanisms.
   </li>
	<li>
    	How long does the conversion software take to install, setup and remove when finished? Set-up times and 
        ease-of-use not only contribute to end-to-end conversion time, but also dictate the skill level of user needed 
        to run the system.
   </li>
   <li>
       Does the solution cover post-conversion testing and documentation? This is another measure of comprehensiveness 
       and a factor in the end-to-end conversion time.[1]
   </li>
</ul>
<h3>
Conclusion
</h3>
<p>
<span style="margin-left:2em;">There</span> are many options for P2V. Practical options decline as the size of the 
project grows. Large-scale migrations usually find staff time is the most expensive element in the virtualization 
process. Availability and finding expertise are also common constraints. Conversion solutions that can save time and 
labor by automating various pre- and post- conversion tasks can reduce the total cost of the conversion greatly. 
Organizations must consider end-to-end conversion time when evaluating different options and selecting their conversion 
tool. Considering these variables allows organizations to find the most efficient solution for their needs. If you would 
like our help or even just a second opinion on your P2V migration give us a call.
</p>
<!-- END TEXT -->
									</section>
									
							</div>
                            
						</div>
					</section>
			
			</div>
            <!-- END 12u -->
		
<?php
	require_once('includes/footer.php');
?>
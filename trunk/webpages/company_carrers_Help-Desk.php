<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc outside none;;
		text-align: justify;
		padding: .25% .25% 0 7%;
		margin-bottom: 1em;
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Careers</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  	<section class="cell">
										<header>
                                            <h2>Help Desk Support Representative I, II, III</h2>                                            
										</header>
<!-- Begin TEXT -->
<h3 style="margin-left:2.5%;">
    Location: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Florence Oregon</span><br />
    Title: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Help Desk Support Representative</span> | <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Training is available</span><br />
    Posted: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">8/7/2013 - Until Filled</span><br />
    Interested applicants should contact: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;"><a href="mailto:Jobs@RosinCloud.com">Jobs@Rosincloud.com</a></span>
</h3>

<p>
    RosinCloud has a mulitple positions opening on the Oregon Coast for a Help Desk Support Representative.<br />
    0-2 years of relevant experience<br />
    Must be willing to work part-time 20hrs +<br />
    Candidates will need to be able to work a flexible shift for this position, including occasional work on Weekends.
</p>

<h3 style="margin-left:2.5%;">
	</h3>

 
<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Purpose</legend>
<p style="padding:0 0 0 1em;">
    Work with multiple departments and be highly collaborative internally to provide exceptional customer service with a 
    sense of urgency working closely with others to coordinate support.<br />
    Perform basic first-line troubleshooting and support to end-users / clientes or assign / escalate for resolution. This includes 
    troubleshooting and resolving issues regarding LAN issues, application issues, server, AS/400 devices, desktop issues, 
    proprietary applications, mobile devices, and PC support in a Windows environment. 
</p>
</fieldset>

<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Job Duties</legend>
<ul style="font-size: 11pt;">
    <li>
        Answers, evaluates, and prioritizes incoming telephone, e-mail, and self-service requests for assistance from users experiencing problems with hardware, software, networking, and other computer-related technologies.
    </li>
    <li>
        Responsible for documenting every call in the Service Desk tracking system and prioritizing according to the severity of the problem.
    </li>
    <li>
        Provide quality technical support services to customers
    </li>
    <li>
        Troubleshoot a variety of computer, peripheral, or related software and hardware issue
    </li>
    <li>
        Provide and apply accurate and efficient technical and practical solutions to solve user issues
    </li>
    <li>
        Answers inquiries by clarifying desired information, researching, locating, and providing information.
    </li>
    <li>
        Resolves problems by clarifying issues, researching and exploring answers and alternative solutions, 
        implementing solutions, and escalating unresolved problems.
    </li>
    <li>
        Fulfills requests by clarifying desired information, completing transactions, forwarding requests.
    </li>
    <li>
        Sells additional services by recognizing opportunities to up-sell accounts, explaining new features.
    </li>
    <li>
        Document detailed and accurate technical notes, regarding each user issue and steps taken to resolve it
    </li>
    <li>
        Working knowledge of Windows OS and MS product line, Lotus Notes, PC hardware, Desktop & thin client applications
    </li>
    <li>
        Exceptional and professional communication skills, both verbal and written are vital
    </li>
    <li>
        Good typing and grammatical skills
    </li>
    <li>
        Strong problem solving and troubleshooting abilities
    </li>
    <li>
        Strong organizational and planning skills
    </li>
    <li>
        Excellent multitasking and prioritizing skills
    </li>
    <li>
        Good time management; document information in a fashion that is easily understandable by other team members
    </li>
    <li>
        Must be self-directed and comfortable interacting with all level of end-users
    </li>
    <li>
        Log and thoroughly document all incoming client calls into the help desk database application
    </li>
    <li>
        Update the help desk database as required when the status of issues change
    </li>
    <li>
        Communicate effectively and courteously with the end user regarding the status and or resolution of each issue
    </li>
    <li>
        Advise team members and management of any issues that relate to customer satisfaction or if there are problems in getting issues resolved in a timely manner
    </li>
    <li>
        Keep peers and manager informed of trends, significant problems, unexpected delays
    </li>
</ul>
</fieldset>


<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Skills &amp; Qualifications</legend>
<p style="padding:0 0 0 1em;">
	Preffered, but not required, experience to include:
</p>
<ul style="font-size: 11pt;">
    <li>At least 6 months experience working in a high-volume IT Help Desk environment</li>
    <li>Excellent customer service and communication skills</li>
    <li>Experience using the following - MS Office, MS Outlook, PC Support, Windows XP, Windows 7, Active Directory</li>
</ul>
<p style="padding:0 0 0 1em;">
Applicants with proven experince in the follwing skills will be given preference:
</p>
<ul style="font-size: 11pt;">
    <li>Customer Focus</li>
    <li>Customer Service</li>
    <li>Data Entry Skills</li>
    <li>Effective Problem Resolution</li>
    <li>Phone Skills</li>
    <li>Writen and Spoken Professionalism</li>
    <li>Building Relationships</li>
    <li>People Skills</li>
    <li>Interpersonal Savvy</li>
    <li>Problem Solving</li>
    <li>Multi-tasking</li>
</ul>
</fieldset>
<h3 style="margin-left:2.5%;">
	Interested applicants should contact: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;"><a href="mailto:Jobs@RosinCloud.com">Jobs@Rosincloud.com</a></span>
</h3>
<!-- END TEXT -->
                                </section>
                                <!-- END cell -->

							</div>
                            <!-- END 12u -->
							
						</div>
                        <!-- END row -->
					</section>
			
			</div>
            <!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc outside none;;
		text-align: justify;
		padding: .25% .25% 0 7%;
		margin-bottom: 1em;
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Careers</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  	<section class="cell">
										<header>
                                            <h2>Network Administrator I, II, III</h2>                                            
										</header>
<!-- Begin TEXT -->
<h3 style="margin-left:2.5%;">
    Location: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Florence Oregon</span><br />
    Title: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Network Administrator</span> | <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Training is available</span><br />
    Posted: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">8/8/2013 - Until Filled</span><br />
    Interested applicants should contact: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;"><a href="mailto:Jobs@RosinCloud.com">Jobs@Rosincloud.com</a></span>
</h3>

<p>
    RosinCloud is currently looking at hiring Network Administrators I, II, III on the Oregon Coast.<br />
    0-2 years of relevant experience<br />
    Candidates will need to be able to work a flexible shift for this position, including occasional work on Weekends.
</p>

<h3 style="margin-left:2.5%;">
	</h3>

 
<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Purpose</legend>
<p style="padding:0 0 0 1em;">
	As a key member of the operations team the candidate will be directly responsible for the successful implementation and support 
    of our customers networks. Communication with customers and vendors in a clear and professional manner is an absolute 
    must.<br />
	This position will be responsible for maintaining computing environments by identifying network requirements, installing 
    upgrades, and monitoring network performance. Excellent communication skills and dedication to assisting internal 
    staff with a wide range of requests and problems, and to deal effectively with external contacts.
</p>
</fieldset>

<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Job Duties</legend>
<ul style="font-size: 11pt;">
	<li>
    	The Network Administrator reports to the Senior Network Administrator.
    </li>
    <li>
    	Establish relationships and work with current and potential vendors to deal with regular operations, changes, 
        new products and projects.
    </li>
    <li>
    	Interact with vendor technical support groups to resolve problems and bugs.
    </li>
    <li>
        Establishes network specifications by conferring with users; analyzing workflow, access, information, and 
        security requirements; designing router administration, including interface configuration and routing protocols.
    </li>
    <li>
        Establishes network by evaluating network performance issues including availability, utilization, throughput, 
        goodput, and latency; planning and executing the selection, installation, configuration, and testing of 
        equipment; defining network policies and procedures; establishing connections and firewalls.
    </li>
    <li>
        Maintains network performance by performing network monitoring and analysis, and performance tuning; 
        troubleshooting network problems; escalating problems to vendor.
    </li>
    <li>
        Secures network by developing network access, monitoring, control, and evaluation; maintaining documentation.
    </li>
    <li>
        Prepares users by designing and conducting training programs; providing references and support.
    </li>
    <li>
        Upgrades network by conferring with vendors; developing, testing, evaluating, and installing enhancements.
    </li>
    <li>
        Meets financial requirements by submitting information for budgets; monitoring expenses.
    </li>
    <li>
        Updates job knowledge by participating in educational opportunities; reading professional publications; 
        maintaining personal networks; participating in professional organizations.
    </li>
    <li>
        Protects organization's value by keeping information confidential.
    </li>
    <li>
        Accomplishes organization goals by accepting ownership for accomplishing new and different requests; exploring 
        opportunities to add value to job accomplishments.
    </li>
</ul>
</fieldset>


<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Skills &amp; Qualifications</legend>

<p style="padding:0 0 0 1em;">
	Preffered, but not required, experience to include:
</p>
<ul style="font-size: 11pt;">
    <li>Thorough knowledge of TCP/IP networking technologies</li>
    <li>Excellent customer service and communication skills</li>
    <li>Strong analytical, problem-solving and technical skills</li>
    <li>Ability to consistently, effectively and tactfully communicate with people at many levels</li>
    <li>Conduct a professional attitude at all times and maintain a high professional standard</li>
</ul>

<p style="padding:0 0 0 1em;">
	Applicants with proven experince in the follwing skills will be given preference:
</p>
<ul style="font-size: 11pt;">
	<li>Excellent technical skills, strong oral and written communication skills are required and you must be a team player</li>
	<li>Ability to work independently with limited supervision</li>
    <li>Ability to diagnose problems and provide solutions and/or escalate to the appropriate personnel</li>
    <li>Network Performance Tuning</li>
    <li>LAN Knowledge</li>
    <li>Network Design and Implementation</li>
    <li>Problem Solving</li>
    <li>Strategic Planning</li>
    <li>Multi-tasking</li>
    <li>Quality Focus</li>
    <li>Coordination</li>
    <li>Technical Understanding</li>
    <li>Quick Study</li>
    <li>Technical Zeal</li>
</ul>
</fieldset>
<h3 style="margin-left:2.5%;">
	Interested applicants should contact: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;"><a href="mailto:Jobs@RosinCloud.com">Jobs@Rosincloud.com</a></span>
</h3>
<!-- END TEXT -->
                                </section>
                                <!-- END cell -->

							</div>
                            <!-- END 12u -->
							
						</div>
                        <!-- END row -->
					</section>
			
			</div>
            <!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
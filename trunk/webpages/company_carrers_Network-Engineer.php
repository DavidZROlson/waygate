<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc outside none;;
		text-align: justify;
		padding: .25% .25% 0 7%;
		margin-bottom: 1em;
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Careers</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  	<section class="cell">
										<header>
                                            <h2>Network Engineer</h2>                                            
										</header>
<!-- Begin TEXT -->
<h3 style="margin-left:2.5%;">
    Location: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Florence Oregon</span><br />
    Title: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Network Engineer</span> | <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">Training is available</span><br />
    Posted: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;">8/13/2013 - Until Filled</span><br />
    Interested applicants should contact: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;"><a href="mailto:Jobs@RosinCloud.com">Jobs@Rosincloud.com</a></span>
</h3>

<p>
    RosinCloud is currently looking at hiring a Network Engineer on the Oregon Coast.<br />
    0-2 years of relevant experience<br />
    Candidates will need to be able to work a flexible shift for this position, including occasional work on Weekends.
</p>

<h3 style="margin-left:2.5%;">
	</h3>

 
<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Purpose</legend>
<p style="padding:0 0 0 1em;">
    The candidate will be responsible for the design, installation, analysis, troubleshooting and maintenance of 
    networks. He/She may function as a consultant and needs to be comfortable interacting with various clients. The goal 
    of this position will be to develop and maintain a network that can provide maximum and efficient performance 
    and reliability for users.
</p>
</fieldset>

<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Job Duties</legend>
<ul style="font-size: 11pt;">
    <li>
        Plan and assist with the design of complex global data & voice network infrastructures.
    </li>
    <li>
    	Making Knowledge Base, Making Standard Operating Procedures and Process Flowcharts , Making Change Controls, 
        Responsible for problem management, Configuration Management, Maintain and Update Knowledge Base
    </li>
    <li>
    	Respond to customer facing requests for connectivity or troubleshooting connectivity issues
    </li>
    <li>
    	Maintain and monitor the performance of the existing network, routers, switches and firewalls, both internal and customer facing
    </li>
    <li>
    	Daily maintenance and operations including port configurations, vlan changes, and ip address assignment
    </li>
    <li>
    	Monitoring and troubleshooting performance issues and outages
    </li>
    <li>
    	Producing network diagrams and configuration documentation
    </li>
    <li>
    	Upgrade planning for additional performance and capacity
    </li>
    <li>
        Stay abreast of current technologies
    </li>
    <li>
        Ensure policies and procedures are adhered to
    </li>
    <li>
        Interface with Operating Companies to develop and implement data solutions.
    </li>
    <li>
        Work on complex problems and provide innovative solutions.
    </li>
    <li>
        Identify inefficiencies and make suggestions for process improvements.
    </li>
    <li>
        Focus on teamwork and readily share information with others
    </li>
    <li>
        Actively seek customer input and take responsibility for customer situations
    </li>
    <li>
        Maintenance and Upgrades outside business hours
    </li>
    <li>
        Mentor and work within the network team
    </li>
    <li>
        Multi-Level escalation support
    </li>
</ul>
</fieldset>


<fieldset style="border: 1px solid; padding: 0 1em 0 1em; margin: 0 1em 0 1em;">
<legend>Skills &amp; Qualifications</legend>
<ul style="font-size: 11pt;">
    <li>Thorough knowledge of TCP/IP networking technologies</li>
    <li>Excellent customer service and communication skills</li>
    <li>Strong analytical, problem-solving and technical skills</li>
    <li>Ability to diagnose problems and provide solutions and/or escalate to the appropriate personnel</li>
    <li>Ability to consistently, effectively and tactfully communicate with people at many levels</li>
    <li>Conduct a professional attitude at all times and maintain a high professional standard</li>
	<li>Excellent technical skills</li>
    <li>Strong oral and written communication skills are required and you must be a team player</li>
</ul>

<p style="padding:0 0 0 1em;">
	Applicants with proven experince in the follwing skills will be given preference:
</p>

<ul style="font-size: 11pt;">
	<li>Tracking Budget Expenses</li>
    <li>Project Management</li>
    <li>Proxy Servers</li>
    <li>Networking Knowledge</li>
    <li>Network Troubleshooting</li>
    <li>Network Hardware Configuration</li>
    <li>People Management</li>
    <li>Ability to work independently with limited supervision</li>
    <li>Network Performance Tuning</li>
    <li>LAN Knowledge</li>
    <li>Network Design and Implementation</li>
    <li>Strategic Planning</li>
    <li>Multi-tasking</li>
    <li>Quality Focus</li>
    <li>Coordination</li>
    <li>Technical Understanding</li>
    <li>Quick Study</li>
    <li>Technical Zeal</li>
</ul>
</fieldset>
<h3 style="margin-left:2.5%;">
	Interested applicants should contact: <span style="text-decoration:underline; font-weight:100; letter-spacing: 0;"><a href="mailto:Jobs@RosinCloud.com">Jobs@Rosincloud.com</a></span>
</h3>
<!-- END TEXT -->
                                </section>
                                <!-- END cell -->

							</div>
                            <!-- END 12u -->
							
						</div>
                        <!-- END row -->
					</section>
			
			</div>
            <!-- END features-wrapper -->
		
<?php
	require_once('includes/footer.php');
?>
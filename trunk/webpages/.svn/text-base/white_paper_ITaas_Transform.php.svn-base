<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">White Papers</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header>
						<div class="row">
							
							<div class="12u">

                                <!-- Feature -->
                                <section class="cell">
                                    <header>
                                    	<h2>What Makes ITaaS Transformational?</h2>
                                        <h3>Posted on September 12, 2013</h3>
                                    </header>
<!-- BEGIN TEXT -->
<style type="text/css">
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		font-size: 13pt;
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h3
	{
		text-align: left;
		padding-left: 2.5%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */

background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		margin-top: .25em;
		color: #D4D6D7;
	}
	
	.cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%;
		margin-bottom: 0px;
	}
	.cell ul li{
		margin-bottom: 15px;
	}
</style>
<blockquote>“There’s nothing really new with cloud computing.  Don’t you remember mainframe and timesharing?”</blockquote>

<p>
<span style="margin-left:2em;">Anyone</span> involved with cloud in the early days of the market heard this naive and 
mildly annoying comment fairly frequently.  While thankfully it’s largely disappeared, a new version of the “nothing new 
here” meme has appeared in enterprise cloud.
</p>
<blockquote>“Yes, we’re doing ITaaS – we’re already implemented ITIL and an ITSM strategy!”</blockquote>
<p>
<span style="margin-left:2em;">For</span> those unfamiliar the IT-as-a-Service (ITaaS) vision is for IT to become a 
modern service provider that offers and orchestrates both internal and external IT services instead of organizing around 
traditional technology silos.  In this model, corporate IT offers a menu of SaaS, PaaS and IaaS options for business 
users via a centralized service catalog.  Business users are free to pick and choose cloud services that corporate IT 
has vetted, or provide themselves.  These users can make informed decisions based on service pricing and SLAs, and can 
in many cases can provision services on their own.  While definitions differ, these are basically the core concepts.
</p>
<p>
<span style="margin-left:2em;">IT</span> has always provided services to business users, particularly in organizations 
with shared service organizations.  While the idea of a service catalog with 3rd party cloud services may be new to some, 
at the end of the day IT always basically provided a set of IT services required by the business.  To better align 
services with the business, many IT organizations have implemented ITIL and ITSM processes and associated KPIs and 
metrics for service improvement.
</p>
<p>
<span style="margin-left:2em;">Unfortunately</span> ITIL and ITSM have lulled many IT organizations into a false sense of 
security that they’re already well down the road of delivering ITaaS.   Many have assumed that because they’ve adopted 
the ITIL framework for delivering IT services that cloud services will fold neatly in to their existing processes, and 
that inclusion of external cloud vendors doesn’t fundamentally change the model.  Even those that have done a reasonable 
job of adopting ITIL and ITSM are finding that cloud-enabled ITaaS is a new, different animal.  What’s new about the 
ITaaS model?
</p>
<ul>
    <li><span style="font-style: italic;">Competition –</span><span style="margin-left:2em;">yes,</span> IT has always 
    provided IT services to the business.   But the decision around what services were provided, whether they be 
    applications, infrastructure, network or other was determined largely by IT.  Business users could choose their 
    services, but it was like Henry Ford said of the Model T: “People can have the Model T in any color – so long as it’s 
    black.”  Facing new competition for budget from public cloud services, enterprise IT is now finding they need to 
    offer colors.  Designing and successfully implementing IT services, processes and KPIs will have a much higher degree 
    of difficulty now that IT no longer has a captive buying audience.
    </li>
    <li><span style="font-style: italic;">Customer knowledge –</span><span style="margin-left:2em;">in</span> the new ITaaS 
    environment, IT needs to provide services that are market competitive.  The first step in doing this is to “know thy 
    customer”.  This means deeply understanding customer needs and requirements, and delivering a compelling value 
    proposition.  Truly understanding customer needs is 90% of the job for sales and marketing employees, and most of them 
    aren’t that good at it.  What makes us think that IT is going to be better?  This is why the seemingly straightforward 
    task of defining a cloud service catalog is often a daunting challenge for corporate IT.  Customers often don’t really 
    know what they want or know the right questions to ask, and digging below the surface to truly understand needs is not 
    a traditional core competence of corporate IT.  
    </li>
    <li><span style="font-style: italic;">Price transparency –</span><span style="margin-left:2em;">with</span> published 
    service catalogs, SLAs, pricing and chargeback, buyers will now be comparison shoppers.  Don’t like the list price for a 
    Linux VM or a GB of storage on the internal private cloud?  Check it out against Amazon AWS VPC pricing.  While this 
    oversimplifies things quite a bit and comparisons aren’t that straightforward, IT will now face questioning on internal 
    pricing that it never did before.  Pricing also is no longer a cost-plus exercise.  Price internal private cloud 
    services too high and developers will go the shadow IT route.  Price too low and there may be capacity issues.  
    Unfortunately ITIL and ITSM don’t necessarily help provide useful guidance in determining how to optimize price and 
    other “marketing” levers CIOs now need to pull.
    </li>
    <li><span style="font-style: italic;">Commoditization –</span><span style="margin-left:2em;">in</span> an ITaaS 
    model IT needs to define the services in simple enough terms to enable user self-service for business users.  This 
    obviously requires drive towards simplification and standardization, not complexity.  Needless to say this has not 
    always been a strength of enterprise IT departments, despite the fact the fact that standardization can unlock 
    tremendous value.   Going through the exercise to determining how to standardize services can be deceptively hard, 
    and enterprise IT has historically not been inclined to commoditize their own services.
    </li>
    <li><span style="font-style: italic;">Co-opetition –</span><span style="margin-left:2em;">the</span> other concept 
    that IT is going to quickly learn is co-opetition.  IT will need to work with public cloud service providers to 
    determine how the services will fit into the ITaaS service catalog from a brokerage, integration, security and 
    governance perspective.  These same public cloud vendors may be competing against internal IT provided applications 
    and infrastructure services, especially around private clouds.  Navigating the nuances of these types of 
    relationships will be a new challenge for many IT organizations.</li>
</ul>
<p><span style="margin-left:2em;">All</span> of these factors have implications for organization design, processes, 
policies and skills that go far beyond what ITIL and ITSM strategies address.  The first step CIOs need to take is to 
make when considering ITaaS transformation an honest appraisal of how they’re going to compete and differentiate this 
new IT services market place.  We’re definitely not in Kansas anymore…
</p>

<p>
<span style="margin-left:2em;">ITAAS</span> Does not mean you have no internal IT. It can mean you farm out what makes 
sense and lower your IT costs while keeping your core innovators in house.
</p>
<p>Call us to find out how you can save over 40% on your IT support and Infrastructure costs.<br />
800-531-0892<br />
<span style="font-variant:small-caps;"><a href="http://www.RosinCloud.com">www.RosinCloud.com</a></span>
</p>
<!-- END TEXT -->
									</section>
									
							</div>
                            
						</div>
					</section>
			
			</div>
            <!-- END 12u -->
		
<?php
	require_once('includes/footer.php');
?>
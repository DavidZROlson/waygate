<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<style>
#features-wrapper, #features-wrapper p
{
	text-align:justify;
}
#features-wrapper h3
{
	text-decoration: underline;
}
.outer
{
	list-style: lower-roman inside none;
	margin-left: 2em;
	font-size: 14px;
}
.inner
{
	list-style: none inside none;
	margin-left: 2em;
	font-size: 14px;
}
</style>

<!-- Features Wrapper -->
<div id="features-wrapper" style="padding: 50px 0;">

    <!-- Features -->
    <section id="features" class="container">
        <div class="row">
            <div class="12u">

                <!-- Feature -->
                <section class="cell" style="padding: 5%;">
                    <header style="
                        border-bottom: 1px solid;
                        margin-bottom: 10px;">
                        <h2 style="
                            text-transform:none;
                            margin:0;
                            letter-spacing:0;
                            text-align:left;
                            font-variant:small-caps;">
                            Privacy Policy
                        </h2>
                    </header>
                    <p>
                        <span style="margin-left:2em;">This</span> Privacy Policy governs the manner in which RosinCloud collects, uses, maintains 
                        and discloses information collected from users (each, a “User”) of the 
                        www.rosincloud.com website (“Site”). This privacy policy applies to the Site and all 
                        products and services offered by RosinCloud.
                    </p>
                    <h3>
                        Personal identification information
                    </h3>
                    <p>
                        <span style="margin-left:2em;">We</span> may collect personal identification information from Users in a variety of ways, 
                        including, but not limited to, when Users visit our site, register on the site, place 
                        an order, subscribe to the newsletter, respond to a survey, fill out a form, and in 
                        connection with other activities, services, features or resources we make available 
                        on our Site. Users may be asked for, as appropriate, name, email address, mailing 
                        address, phone number, credit card information. Users may, however, visit our Site 
                        anonymously. We will collect personal identification information from Users only if 
                        they voluntarily submit such information to us. Users can always refuse to supply 
                        personally identification information, except that it may prevent them from engaging 
                        in certain Site related activities.
                    </p>
                    <h3>
                        Non-personal identification information
                    </h3>
                    <p>
                        <span style="margin-left:2em;">We</span> may collect non-personal identification information about Users whenever they 
                        interact with our Site. Non-personal identification information may include the 
                        browser name, the type of computer and technical information about Users means of 
                        connection to our Site, such as the operating system and the Internet service 
                        providers utilized and other similar information.                        
                    </p>
                    <h3>
                        Web browser cookies
                    </h3>
                    <p>
                        <span style="margin-left:2em;">Our</span> Site may use “cookies” to enhance User experience. User’s web browser places 
                        cookies on their hard drive for record-keeping purposes and sometimes to track 
                        information about them. User may choose to set their web browser to refuse cookies, 
                        or to alert you when cookies are being sent. If they do so, note that some parts of 
                        the Site may not function properly.
                    </p>
                    <h3>
                        How we use collected information
                    </h3>
                    <p>
                        RosinCloud may collect and use Users personal information for the following purposes:
                        <ul class="outer">
                            <li>
                            	To improve customer service
                                <ul class="inner">
                                    <li>
                                   		Information you provide helps us respond to your customer service requests and support needs more efficiently.
                                    </li>
                                </ul>
                            </li>
                            <li>
                            	To personalize user experience
                                <ul class="inner">
                                    <li>
                                    	We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.
                                    </li>
                                </ul>
                            </li>
                            <li>
                            	To improve our Site
                                <ul class="inner">
                                    <li>
                                    	We may use feedback you provide to improve our products and services.
                                    </li>
                                </ul>
                            </li>
                            <li>
                            	To process payments
                                <ul class="inner">
                                    <li>
                                    	We may use the information Users provide about themselves when placing an order only to provide service to that order. We do not share this information with outside parties except to the extent necessary to provide the service.
                                    </li>
                                </ul>
                            </li>
                            <li>
                            	To share your information with third parties
                                <ul class="inner">
                                    <li>
                                    	We may share or sell information with third parties for marketing or other purposes.
                                    </li>
                                </ul>
                            </li>
                            <li>
                            	To run a promotion, contest, survey or other Site feature
                                <ul class="inner">
                                    <li>
                                    	To send Users information they agreed to receive about topics we think will be of interest to them.
                                    </li>
                                </ul>
                            </li>
                            <li>
                            	To send periodic emails
                                <ul class="inner">
                                    <li>
                                    	We may use the email address to send User information and updates pertaining to their order. It may also be used to respond to their inquiries, questions, and/or other requests. If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, they may do so by contacting us via our Site.
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </p>
                    <h3>
                        How we protect your information
                    </h3>
                    <p>
                        <span style="margin-left:2em;">We</span> adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.
                        Sensitive and private data exchange between the Site and its Users happens over a SSL secured communication channel and is encrypted and protected with digital signatures. Our Site is also in compliance with PCI vulnerability standards in order to create as secure of an environment as possible for Users.
                    </p>
                    <h3>
                        Third party websites
                    </h3>
                    <p>
                        <span style="margin-left:2em;">Users</span> may find advertising or other content on our Site that link to the sites and services of our partners, suppliers, advertisers, sponsors, licensors and other third parties. We do not control the content or links that appear on these sites and are not responsible for the practices employed by websites linked to or from our Site. In addition, these sites or services, including their content and links, may be constantly changing. These sites and services may have their own privacy policies and customer service policies. Browsing and interaction on any other website, including websites which have a link to our Site, is subject to that website’s own terms and policies.
                    </p>
                    <h3>
                        Compliance with children’s online privacy protection act
                    </h3>
                    <p>                        
                        <span style="margin-left:2em;">Protecting</span> the privacy of the very young is especially important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.
                    </p>
                    <h3>
                        Changes to this privacy policy
                    </h3>
                    <p>
                        <span style="margin-left:2em;">RosinCloud</span> has the discretion to update this privacy policy at any time. When we do, we will revise the updated date at the bottom of this page and send you an email. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.
					</p>
                    <h3>
                    	Your acceptance of these terms
                    </h3>
                    <p>
                        <span style="margin-left:2em;">By</span> using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.
                    </p>
                    <h3>
                        Contacting us
                    </h3>
                    <p>
                        If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:<br />
                        RosinCloud<br />
                        www.rosincloud.com<br />
                        88510 A U.S. HWY 101 Florence, OR 97439<br />
                        1 (800) 531-0892<br />
                        cloud@rosincloud.com<br />
                        <br />
                        This document was last updated on February 20, 2013
                    </p>
                </section>

            </div>
            <!-- END 12u -->
        </div>
        <!-- END row -->
    </section>
    <!-- END features -->

</div>
		
<?php
	require_once('includes/footer.php');
?>
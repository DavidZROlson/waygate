<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";

	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>

<style type="text/css">
#features-wrapper
{
	background-color: rgb(2, 100, 127); padding-bottom: 20px;
	background: #02647F; /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */
}
p, ul, ol, dl, table, blockquote
{
	margin-bottom: 0;
}
        
#features-wrapper2 .service_breif
{
	text-align: center;
	padding-top: 2em;
}
#features-wrapper2 ul
{
	text-align: center;
	padding: 2em 0;
}
</style>

<script type="text/javascript">
function after_load(){
	$('.bxslider').bxSlider({
		  speed: 500,
		  mode: 'horizontal',
		  easing: 'ease-in-out',
		  pause: 7000,
		  //adaptiveHeight: true,
		  auto: true
		});
}

$(window).load(after_load());

</script>
        <div style="
        	background-color: #D4D6D7;
            height: 10px;
            width: 100%;">
        </div>
        
<ul class="bxslider">
<li>
		<!-- Features Wrapper -->
        
        <div id="features-wrapper">

            <!-- Features -->
            <section id="features" class="container">
                <div class="row">
                    <div class="5u">
                    
                        <!-- Feature -->
                        <section>
                        	<div style="text-align:left;">                
								<p style="
                                    font-family: Impact;
                                    font-size: 40px;
                                    font-style: normal;
                                    font-variant: normal;
                                    font-weight: normal;
                                    line-height: 1.4em;
                                    padding: 45px 0 0;
                                    display: inline-block;
                                    color: #D4D6D7;
                                    line-height: 38px;
                                    text-align:left;
                                    margin-bottom:0;">
                                    Making<br>
                                    Complex Systems<br>
                                    Easy
                              </p>
								<p style="
                                	text-align: justify;
                                	color: #D4D6D7;
                                    line-height:1.2;">
								    In Today's fast-paced world, speed, reliability, agility and scalability are paramount. Cloud computing is a significant shift in the business and economic models for provisioning and consuming information technology (IT) that can lead to a substantial cost savings. When migrated properly, the cloud computing economic model can drastically reduce the operations and maintenance cost of IT infrastructures.
								</p>
	                      </div>
                      </section>
                        <!-- END FEATURE -->

                    </div>
                    
                    <div class="7u">

                        <!-- Feature -->
                        <section>
                            	<img class="image image-full" style="border:none;" src="<?=$IMG_PATH ?>dual_windows.png" alt="dual_windows" longdesc="http://www.rosincloud.com">
						</section>
                        <!-- END FEATURE -->

                    </div>
                </div>
                <ul class="actions">
                    <li><a href="<?=$ROOT_PATH ?>company.php" class="button button-icon icon icon-file">Tell Me More</a></li>
                </ul>
            </section>

        
        </div>
        <!-- END FEATURE WRAPPER -->
                    
</li>
<li>
		
        <!-- Features Wrapper -->

        <div id="features-wrapper2" style="background: #D4D6D7;">

            <!-- Features -->
            <section id="features2" class="container">
                <div class="row">
                    <div class="4u">

                        <!-- Feature -->
                        <section class="service_breif">
                            <a href="<?=$ROOT_PATH ?>service_migration.php" class="image" style="border:none;">
                            	<img src="<?=$IMG_PATH ?>cloud_folder.png" alt="cloud folder">
                            </a>
                                <header>
                                    <h3 style="
                                        text-decoration:underline;
                                        font-variant:small-caps;
                                        text-transform:none;">
                                        <a href="<?=$ROOT_PATH ?>service_migration.php">
                                        	Professional Migration
                                        </a>
                                    </h3>
                                </header>
                            </a>
                            <p style="
                                text-align: justify;
                                line-height:1.2;">
                            	Any company using technology, will at some point need to conduct server migrations. This can be extremely stressful as you move your most valuable asset, your data, from your inhouse server to a hosted server, physical or virtual. RosinCloud has been helping customers to plan and perform safe migrations for years.
                            </p>
                        </section>

                    </div>
                    <div class="4u">

                        <!-- Feature -->
                        <section class="service_breif">
                            <a href="<?=$ROOT_PATH ?>service_support.php" class="image" style="border:none;">
                            	<img src="<?=$IMG_PATH ?>PC_Tower.png" alt="pc tower">
                            </a>
                            <header>
                                <h3 style="
                                    text-decoration:underline;
                                    font-variant:small-caps;
                                    text-transform:none;">
                                    <a href="<?=$ROOT_PATH ?>service_support.php">
                                    System Monitoring
                                    </a>
                                </h3>
                            </header>
                            <p style="
                                text-align: justify;
                                line-height:1.2;">
                                At Rosincloud, our full time job is both to keep everything running 24/7, and to be ready for the next big thing. And we have the expertise of doing this for organizations of all sizes, in a variety of industries. 
                            </p>
                        </section>

                  </div>
                    <div class="4u">

                        <!-- Feature -->
                        <section class="service_breif">
                            <a href="<?=$ROOT_PATH ?>service_proITconsulting.php" class="image" style="border:none;"><img src="<?=$IMG_PATH ?>Mobile.png" alt="mobile"></a>
                            <header>
                             	<h3 style="
                                	text-decoration:underline;
                                    font-variant:small-caps;
                                    text-transform:none;">
                                    <a href="<?=$ROOT_PATH ?>service_proITconsulting.php">
                                    	Professional IT Consulting
                                    </a>
                                </h3>
                            </header>
                            <p style="
                                text-align: justify;
                                line-height: 1.2;">
                                Think of us as an extension of your own company or IT department. Our approach starts with a consultation to consider your unique needs. We provide services to power your complete IT infrastructure for one organized, sensible, and predictable monthly fee.
                            </p>
                        </section>

                    </div>
                </div>
                <ul class="actions" style="margin: 0;">
                    <li><a href="<?=$ROOT_PATH ?>services.php" class="button button-icon icon icon-file">Tell Me More</a></li>
                </ul>
            </section>
        
        </div>
        <!-- END FEATURE WRAPPER -->
</li>
<li>
        
		<!-- Banner Wrapper -->
        <div id="banner-wrapper" style="background-color: #878787; border:none; padding:0; box-shadow:none;">
            <div class="inner">

                <!-- Banner -->
                <section id="banner" class="container" style="padding:0;">
                    <div id="collage">
                        <div class="12u">
                            <img class="image image-full" src="<?=$IMG_PATH ?>collage.png" alt="collage" style="margin:0;" >
                        </div>
                        <div class="5u fiveu">
                            <h3>
                                Helping You Find The Right IT Solutions
                            </h3>
                            <p>
                                Information Technology is the glue that holds your company together. Different kinds of technology and different components of your business come together to let you communicate with the world, get your product or service out the door, and keep track of everything needed to make this possible.
                            </p>
                        </div>
                    </div>
                </section>
                <!-- END Banner -->

            </div>
        </div>
		<!-- END Banner Wrapper -->
    </li>
</ul>
<?php
	require_once('includes/footer.php');
?>
		
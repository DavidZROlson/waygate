<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">White Papers</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header>
						<div class="row">
							
							<div class="12u">

                                <!-- Feature -->
                                <section class="cell">
                                    <header>
                                    	<h2>DC Relocation</h2>
                                        <h3>Posted on February 18, 2013</h3>
                                    </header>
<!-- BEGIN TEXT -->
<style type="text/css">
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		font-size: 13pt;
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h3
	{
		text-align: left;
		padding-left: 2.5%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */

background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		margin-top: .25em;
		color: #D4D6D7;
	}
</style>

<p>
<span style="margin-left:2em;">During</span> these rough economic times, more and more companies will relocate their 
data centers (DC). There are tons of business reasons for moving a data center to a new location – consolidation of 
regional centers into a single site, acquisitions and mergers, and corporate headquarters relocations to name a few. 
Aging infrastructure, insufficient power or cooling, space limitations and lease renewal also trigger relocation 
decisions. It doesn’t matter what the business decision is for relocation because the same level of careful planning, 
expert execution and experience is required for a successful move.
</p>
<p>
<span style="margin-left:2em;">When</span> contacted to assist with physical relocation one of the most common 
misconceptions that we hear is “How hard can this be? You just pick them up, move them to the new site and plug them 
in.” Without a clear understanding of the complexity of all of the steps and details required for a successful 
relocation you run the risk of being shut down for hours, days or weeks while issues are being resolved. Fortunately 
there are industry practices that exist and can explain the requirements that must be taken into account before the 
first server is uninstalled and moved to the new location.
</p>
<h3>
Project Management
</h3>
<p>
<span style="margin-left:2em;">A</span> very important decision to be made at the start of any project is selection of 
the Project Manager. An experienced Project Manager (PM) will ensure that every section of the pre-planning and 
execution is carefully detailed, tested and tested again. The PM is the single point of contact managing the timeline, 
people resources, and budget making sure that every step of the project plan is completed in the defined order and 
milestones are met.
</p>
<p>
<span style="margin-left:2em;">While</span> you may have a seasoned Project Manager on your staff, that person is not 
likely to have experience with Data Center Relocations. This role is crucial to the success of your DCR and if you do 
not have one on staff, look to the vendor you are partnering with to provide that skill set. Even if you appoint an 
internal PM (and you should) you will want this experienced professional as part of your team.
</p>
<p>
<span style="margin-left:2em;">Besides</span> leading the overall relocation project, the experienced PM will also 
provide knowledge transfer to your PM and your Technical Team which will help them with any future changes that may be 
required after the DCR is complete.
</p>
<h3>
Planning
</h3>
<p>
<span style="margin-left:2em;">Complete</span> and detailed planning is a must for successful data center relocation. 
One of the most common issues that impact the success of a DCR is the quality of the documentation. Technical teams will 
often have the documentation “in their heads” which creates a built in single point of failure. These documents must be 
written down, saved and approved by the combined technical team and the management team. There are four documents that 
must be created.
</p>
<p>
<strong># 1</strong> – Document the Present Method of Operation (PMO). In this phase of the project, the Project Manager 
and Project Team will gather information that will result in the complete and accurate documentation of the Present 
Method of Operation. The PMO includes documentation surrounding the interactions of all the components of the existing 
environment including the application interactions, storage requirements, existing backup plans, network connections, 
user locations, service level agreements, etc. This is a critical step in the relocation process as it provides the 
project starting point. The better able we are to understand the PMO and all of the inherent relationships of the 
existing environment; the more successful we will be in executing the move. At the end of this phase a Present Method 
of Operation (PMO) Document will be completed. The document includes diagrams, inventory lists, service level 
agreements, descriptions of support processes (i.e. Change Management, Configuration Management, Problem Management, 
etc.) currently in use and any other data that will insure that the PMO is completely understood. This will include 
logical and physical interactions between components (hardware or software). The goal is to fully document what is being 
moved.
</p>
<p>
<strong># 2</strong> – Desired Future State (DFS): In this phase the definition of success is determined. The 
documentation of the Desired Future State provides an end point for the project and defines the conditions of success. 
It includes any changes you want to make in conjunction with the relocation (i.e.,  virtualization,  enhanced storage, 
technology uplift for some or all servers, network upgrades, etc.). It documents the expected end state in sufficient 
detail to allow for the new environment to be managed using normal service management processes such as Change 
Management, Incident Management or Configuration Management. A fully documented DFS will allow you to progress toward 
an ITIL driven process oriented support environment. This document should be detailed enough to allow for the placement 
of all the moving components from their original location to the destination location.
</p>
<p>
<strong># 3</strong> – The next document to be created is the Design Plan. During this phase, the project team will 
develop the “roadmap” for getting from the PMO to the DFS. This will include the various move groups, any new hardware 
and/or software that might be required, pre-requisite steps required or desired (such as virtualization), known risks 
and contingency plans for the risks, a high level timeline, communication plan and the impact of client processes on the 
design. At the end of this task, you will have a good understanding of the process that will be used to accomplish the 
data center relocation and any incremental budget that might be required to acquire the enabling components.
</p>
<p>
<strong># 4</strong> – The Design Plan is the basis for the final planning step, Implementation Planning. The 
Implementation Plan will include all the steps, dates, and responsible parties for all the tasks that have to be 
accomplished in their proper order and with all the appropriate interactions and linkages defined. It will typically 
include a detailed Project Schedule.
</p>
<p>
<span style="margin-left:2em;">There</span> is other documentation that may or may not be required for your Data center 
Relocation Project.
</p>
<p>
<span style="margin-left:2em;">For</span> example, a Risk Management Plan is normally created for a project of this 
magnitude. The Risk Management Plan documents the level of complexity and risk associated with moving or migrating 
applications and ensuring that there are test plans and communication strategies in place to support their migration. 
Each risk will be fully documented and a mitigation strategy established. A Communications Plan will help you keep your 
stakeholders informed of the progress of the move. It may include a number of different levels, such as User 
Communication, Management Communication, and Technical Team Communication and in some cases, Customer Communication.
</p>
<p>
<span style="margin-left:2em;">Other</span> standard project plan components such as a Quality Plan, Resource Management 
Plan, and Financial Plan may be created. Often these ancillary documents will be created by an internal PM while the key 
documentation listed above is managed by the Vendor/Partner PM. While there is no cookie cutter approach to a DCR, most 
of the documents mentioned above are found in every successful DCR.
</p>
<h3>
Logistics
</h3>
<p>
<span style="margin-left:2em;">At</span> the end of the day, moving a data center successfully becomes a logistics 
exercise. Having the right people, with the right skill set at the right place at the right time with the right 
equipment is one of the keys to a successful DCR. During the planning phase you will have identified everything that is 
moving, the starting point and the ending point, the changes you plan to make along the way and the timeline for the 
move. Unless you have an experienced Logistics Specialist on your internal team, the vendor/partner you select to help 
you execute your move has to provide that skill and the resources to complete the project.
</p>
<p>
<span style="margin-left:2em;">These</span> professionals will determine the size and composition of the de-installation 
team, the packing team, the transportation team, the unpacking team and the re-installation team and supply those 
skilled technicians to work side by side with your team. A detailed day of move Script will be created so everyone knows 
what is to be done every minute of the move event. Depending on the complexity of the move, a Test Move may even be 
scheduled prior to the actual move event. This knowledge is not normally found in the internal resources of most firms 
and so selecting the right partner is critical to this step.
</p>
<h3>
Appropriate Resources
</h3>
<p>
<span style="margin-left:2em;">While</span> it is probably self-evident from everything that has been said above, a DCR 
project has to be resourced properly. Many times, the operations staff is asked to plan and execute these moves. The 
assumption that they have time to take on this daunting task is normally due to the underestimation of the complexity. 
In addition, in many cases, your technical team has pursued a career in operations because they don’t enjoy project 
work.
</p>
<p>
<span style="margin-left:2em;">To</span> be successful there needs to be the proper mix of resources who know the 
environment and those that know how to plan and execute a DCR. Normally a firm will engage a vendor/partner with the 
expertise and experience to augment in-house resources. The mix may vary from 25% vendor and 75% in-house (where you do 
the planning and project management and the vendor executes the move) to 90% vendor and 10% in-house (where the vendor 
provides the project management, planning and execution and you participate in the planning and execution as directed). 
Normally, the reality is somewhere in between. However, there will never be a successful move without some participation 
of the in-house resource.
</p>
<p>
<span style="margin-left:2em;">Each</span> move is different and the resources will be customized to the situation but 
the key is to make sure that you don’t ignore this area. The correct resources for planning and execution are an 
absolute requirement.
</p>
<h3>
Management Support
</h3>
<p>
<span style="margin-left:2em;">This</span> seems pretty self-explanatory but it is often overlooked. The management team 
of your company has to be supportive of the entire project. They need to be kept in the loop at all times and engaged to 
remove obstacles that may be encountered. A DCR is expensive and the management team has to understand the process at 
all times so they can keep their investors informed.
</p>
<p>
<span style="margin-left:2em;">The</span> management team also has to be supportive of potentially deferring operational 
changes during the move. A DC that is changing all the time is much harder to move than one that has been stabilized 
during the planning process. This deferral of operational changes also provides some time for the operational staff to 
participate in the process.
</p>
<p>
<span style="margin-left:2em;">A</span> DC is not a static environment. Over time equipment is upgraded, networks 
expand, applications get added as your business grows and changes. Understanding the environment, taking the time to 
plan for improvements and having a clear, documented implementation plan takes time and attention to detail. When the 
last device is installed and the lights are turned off at the previous location, you want the process to have gone so 
smoothly that hardly anyone knows you moved. Following these practices will go a long way in making that a reality.
</p>
<!-- END TEXT -->
									</section>
									
							</div>
                            
						</div>
					</section>
			
			</div>
            <!-- END 12u -->
		
<?php
	require_once('includes/footer.php');
?>
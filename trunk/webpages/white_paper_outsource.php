<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  <section id="features" class="container">
						<header>
                        	<h2 style="margin-bottom:0px;">White Papers</h2>
							<h3>Provided by <strong>RosinCloud</strong></h3>
				  </header>
						<div class="row">
							
							<div class="12u">

                                <!-- Feature -->
                                <section class="cell">
                                    <header>
                                    	<h2>Should You Outsource or Keep IT In-House?</h2>
                                        <h3>Posted on September 11, 2013</h3>
                                    </header>
<!-- BEGIN TEXT -->
<style type="text/css">
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		font-size: 13pt;
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h3
	{
		text-align: left;
		padding-left: 2.5%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */

background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		margin-top: .25em;
		color: #D4D6D7;
	}
	
	.cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%;
		margin-bottom: 0px;
	}
	.cell ul li{
		margin-bottom: 15px;
	}
</style>

<p>
<span style="margin-left:2em;">It's</span> nice to have an IT staffer in house who has intimate knowledge of your 
business, your network and your needs. However, few individuals are experts in all areas, and those who have the 
requisite skills don't come cheap. No individual can be available 24/7 either. For many small and medium-sized 
businesses, outsourcing may be a better solution.
</p>
<p>
<span style="margin-left:2em;">Maintaining</span> the network. Ensuring remote users have access to resources. Updating 
virus definitions. Troubleshooting email problems. Any number of IT issues can arise on a daily basis.
</p>
<p>
<span style="margin-left:2em;">So</span> does it make sense for small businesses to hire a full-time IT staffer or 
outsource their IT needs? That depends. Weighing the pros and cons in both scenarios can help determine which option 
is likely to best serve small businesses.
</p>
<h3>
In-House IT Support: Pros
</h3>
<ul>
    <li><span style="font-style: italic;">Easy access:</span><span style="margin-left:2em;">A</span> tech support person on staff can address issues immediately. Other 
    clients won't be competing for your IT staffer's time, though there may be other departments doing so.
    </li>
    <li><span style="font-style: italic;">Cost control:</span><span style="margin-left:2em;">As</span> a full-time 
    employee, your IT support staffer's salary remains the same, regardless of the tasks undertaken -- for example, 
    troubleshooting a printer problem, setting up a new server, or staying late on a Thursday night to complete an 
    operating system upgrade. This means that your costs remain steady even as your technology needs change. This can be 
    a double-edged sword, however.
    </li>
</ul>
<h3>
In-House IT Support: Cons
</h3>
<ul>
    <li><span style="font-style: italic;">Upfront and hidden costs:</span><span style="margin-left:2em;">Hiring</span> a 
    full-time IT professional is an expensive endeavor. Providing that pro with a computer, desk, telephone extension, 
    payroll account and benefits drives the cost up even higher. For many small businesses, having a full-time IT 
    specialist with a full-time salary working on staff is too cost-prohibitive to even be considered a viable option. 
    Not to mention the costs associated with ongoing training for IT personnel.
    </li>
    <li><span style="font-style: italic;">Limited technological expertise:</span><span style="margin-left:2em;">Your</span> 
    IT specialist may be good with Excel and handy when it comes to figuring out why the printer isn't working, but may not 
    be as savvy when it comes to diagnosing network security issues or upgrading the Exchange server. It's unlikely that one 
    IT professional will be able to provide expertise for all of your technological needs. If having one full-time person is 
    costly, you might not want to calculate the cost for a small team of specialists!
    </li>
</ul>
<h3>
Outsourcing IT Support: Pros
</h3>
<ul>
    <li><span style="font-style: italic;">Less expensive:</span><span style="margin-left:2em;">All</span> things 
    considered, outsourcing tends to be less expensive than hiring a full-time IT employee in-house. Many costs -- such 
    as overhead -- are spread over several clients via the agency model. Additionally, your small business doesn't have 
    to worry about costs associated with training or certifying IT staff.
    </li>
    <li><span style="font-style: italic;">Round-the-clock service:</span><span style="margin-left:2em;">Most</span> 
    professional IT help desk or tech support firms offer their customers 24/7 access to tech support specialists, 
    either by phone or through remote computer access. This means that you'll have someone to walk you through 
    resetting your email password -- even at 2 a.m. What's more, if your main contact is sick, there will be a 
    substitute that you can count on.
    </li>
</ul>
<h3>
Outsourcing IT Support: Cons
</h3>
<ul>
    <li><span style="font-style: italic;">Language or cultural differences:</span><span style="margin-left:2em;">Struggling</span> 
    to understand your tech support specialist can make a frustrating situation even worse. Unfortunately, many small 
    businesses choose offshore outsourcing as their least-expensive option, while not considering the time and 
    aggravation spent on communication issues. This can be mitigated either by carefully interviewing various offshore 
    firms and giving them a "test drive," or by hiring a local firm. The latter may also allow you to have the 
    specialist on-site, which is highly recommended for handling most IT support needs.
    </li>
    <li><span style="font-style: italic;">Not part of the team:</span><span style="margin-left:2em;">Because</span> 
    outsourced IT specialists are there only when scheduled or when you need them to fix a problem, you'll spend time 
    bringing them up to speed when issues do arise or when you want them to provide advice on future technology 
    initiatives. Again, there is a solution: Get an outsourced firm involved in your IT needs on an ongoing basis via 
    "managed services." This way, the firm can help with routine help desk and tech support issues, and will be more 
    fully plugged in to your needs and requirements when it comes time to upgrade the network.
    </li>
</ul>
<h3>
And the Winner Is: Outsource Locally
</h3>
<p><span style="margin-left:2em;">Certainly,</span> small businesses have a variety of options for solving their tech 
support issues. For most small businesses, however, outsourcing is the best option. Outsourcing tech support needs allows 
businesses to stay focused on their own core offerings without getting sidetracked on IT projects. It also allows 
businesses access to cutting-edge resources and expertise, without the costs typically associated with staying ahead of 
the technology curve.
</p>

<p>
<span style="margin-left:2em;">For</span> many small businesses, outsourcing to a local firm provides the right 
combination of cost savings, flexibility and round-the-clock support without the language or cultural issues that 
sometimes arise with offshore firms. Outsourcing locally also provides small business owners peace of mind that when 
they need on-site tech support, they can get it, thus allowing them to manage their business, not their network. 
</p>
<p>Call us to find out how you can save over 40% on your IT support and Infrastructure costs.<br />
800-531-0892<br />
<span style="font-variant:small-caps;"><a href="http://www.RosinCloud.com">www.RosinCloud.com</a></span>
</p>
<!-- END TEXT -->
									</section>
									
							</div>
                            
						</div>
					</section>
			
			</div>
            <!-- END 12u -->
		
<?php
	require_once('includes/footer.php');
?>
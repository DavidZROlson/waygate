<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Main Wrapper -->
<div id="main-wrapper">

    <!-- Main -->
    <div id="main" class="container">
        <div class="row">
        
            <!-- Content -->
            <div id="content" class="8u skel-cell-mainContent">

                <!-- Post -->
                <article class="is-post">
                  <header style="
                    	border-bottom: 1px solid;
                        margin-bottom: 10px;">
                        <h2 style="
                            text-transform:none;
                            margin:0;
                            letter-spacing:0;">
                            About
                        </h2>
                    </header>
                    <span class="image image-full" style="
                    	margin:0;">
                    	<img src="images/brochureImage1.png" alt="handshake" longdesc="images/brochureImage1.png" />
                    </span>
                    <h3>Right is also not left</h3>
                    <p>
                        RosinCloud provides professional enterprise cloud services that allow
                        you to compete in a global, technology-driven economy. We have the experience
                        and the IT personnel to provide reliable, affordable, world-wide business
                        data services. With excellent customer service and a 24/7/365 call center. 
                        <br />
                        In today’s fast-paced world, speed, reliability, agility and
                        scalability are paramount. Cloud computing is a significant shift in the
                        business and economic models for provisioning and consuming information
                        technology (IT) that can lead to a substantial cost savings. When migrated
                        properly, the cloud computing economic model can drastically reduce the
                        operations and maintenance cost of IT Infrastructures.
                    </p>
                    <span class="image image-full" style="
                    	margin:0;">
                    	<img src="images/CederPallace.jpg" alt="Cedar Pallace" longdesc="images/CederPallace.jpg" />
                    </span>
                    <!-- 
                    <p>Erat lorem ipsum veroeros consequat magna tempus lorem ipsum consequat 
                        Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit ligula 
                        vel quam viverra sit amet mollis tortor congue. Sed quis mauris sit amet 
                        magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada in, 
                        tristique at erat. Curabitur leo nibh, rutrum eu malesuada  in, tristique 
                        at erat lorem ipsum dolor sit amet lorem ipsum sed consequat magna 
                        tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros consequat 
                        magna tempus
                    </p>
                    <p>Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit 
                        ligula vel quam viverra sit amet mollis tortor congue. Sed quis mauris 
                        sit amet magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada 
                        in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat 
                        magna tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet 
                        mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique. 
                        Curabitur leo nibh, rutrum eu malesuada in, tristique at erat.
                    </p>
                    <h3>Accumsan lorem ipsum veroeros</h3>
                    <p>Consequat Phasellus laoreet massa id in, tristique at erat lorem 
                        ipsum dolor sit amet lorem ipsum sed consequat magna tempus veroeros 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet 
                        mollis tortor congue. Sed quis mauris sit amet magna.
                    </p>
                    <p>Phasellus laoreet massa id justo mattis pharetra. Fusce suscipit 
                        ligula vel quam viverra sit amet mollis tortor congue. Sed quis mauris 
                        sit amet magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada 
                        in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat 
                        magna tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet 
                        mollis tortor congue. Sed quis mauris sit amet magna accumsan tristique. 
                        Curabitur leo nibh, rutrum eu malesuada in, tristique at erat.
                    </p>
                    <h3>Ligula suspcipit fusce veroeros</h3>
                    <p>Nullam dolore etiam sed massa id in, tristique at erat lorem 
                        ipsum dolor sit amet lorem ipsum sed consequat magna tempus veroeros 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet 
                        mollis tortor congue. Sed quis mauris sit amet magna.
                    </p>
                    <p>Sed massa id justo mattis pharetra. Fusce suscipit 
                        ligula vel quam viverra sit amet mollis tortor congue. Sed quis mauris 
                        sit amet magna accumsan tristique. Curabitur leo nibh, rutrum eu malesuada 
                        in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        in, tristique at erat lorem ipsum dolor sit amet lorem ipsum sed consequat 
                        magna tempus veroeros lorem sed tempus aliquam lorem ipsum veroeros 
                        consequat magna tempus lorem ipsum consequat Phasellus laoreet massa id 
                        justo mattis pharetra. Fusce suscipit ligula vel quam viverra sit amet 
                        mollis tortor congue. Sed quis mauris sit amet magna accumsan.
                    </p>
                    -->
                </article>
            
            </div>
                
            <!-- Sidebar -->
            <div id="sidebar" class="4u">
            
                <!-- Excerpts -->
                <section>
                    <ul class="divided">
                        <li>

                            <!-- Excerpt -->
                            <article class="cell is-excerpt">
                                <header>
                                    <span class="date">June 19, 2013</span>
                                    <h3><a href="<?=$ROOT_PATH ?>white_paper_ITAAS.php">10 Ways Cloud Computing Is Revolutionizing Manufacturing</a></h3>
                                </header>
                                <p>
                                	The best manufacturers I’ve visited this year all share a common attribute: they are 
                                    obsessed with making themselves as easy as possible to work with from a supply chain, 
                                    distribution and services standpoint. Many are evaluating cloud-based manufacturing 
                                    applications including…
                                </p>
                                <ul class="actions">
                                    <li><a href="<?=$ROOT_PATH ?>white_paper_ITAAS.php" class="button button-icon icon icon-file">Learn More</a></li>
                                </ul>
                            </article>

                        </li>
                        <li>

                            <!-- Excerpt -->
                            <article class="cell is-excerpt">
                                <header>
                                    <span class="date">Feb 20, 2013</span>
                                    <h3><a href="<?=$ROOT_PATH ?>white_paper_P2V_consolidation.php">Technology Thoughts on P2V Server Consolidation Initiatives</a></h3>
                                </header>
                                <p>
                                	The hardest part of a virtualization project is not making the initial 
                                    decision to virtualize. Hardware utilization and energy cost savings together provide 
                                    enough incentive for an organization to make the decision. The management concerns 
                                    about reliability and effectiveness…
                                </p>
                                <ul class="actions">
                                    <li><a href="<?=$ROOT_PATH ?>white_paper_P2V_consolidation.php" class="button button-icon icon icon-file">Learn More</a></li>
                                </ul>
                            </article>

                        </li>
                        <li>

                            <!-- Excerpt -->
                            <article class="cell is-excerpt">
                                <header>
                                    <span class="date">Feb 18, 2013</span>
                                    <h3><a href="<?=$ROOT_PATH ?>white_paper_DC Relocation.php">Data Center Relocation</a></h3>
                                </header>
                                <p>
                                    During these rough economic times, more and more companies will relocate their data 
                                    centers (DC). There are tons of business reasons for moving a data center to a new 
                                    location – consolidation of regional centers into a single site, acquisitions…
                                </p>
                                <ul class="actions">
                                    <li><a href="<?=$ROOT_PATH ?>white_paper_P2V_consolidation.php" class="button button-icon icon icon-file">Learn More</a></li>
                                </ul>
                            </article>

                        </li>
                    </ul>
                </section>
                <?php
				/*
				            
                <!-- Highlights -->
                <section>
                    <ul class="divided">
                        <li>

                            <!-- Highlight -->
                            <article class="cell is-highlight">
                                <header>
                                    <h3><a href="#">Something of note</a></h3>
                                </header>
                                <a href="http://regularjane.deviantart.com/art/In-Your-Hands-356733525" class="image image-left">
                                	<img src="images/pic06.jpg" alt="" />
                                </a>
                                <p>
                                    Phasellus  sed laoreet massa id justo mattis pharetra. Fusce suscipit ligula vel quam 
                                    viverra sit amet mollis tortor congue magna lorem ipsum dolor et quisque ut odio facilisis 
                                    convallis. Etiam non nunc vel est suscipit convallis non id orci. Ut interdum tempus 
                                    facilisis convallis. Etiam non nunc vel est suscipit convallis non id orci.
                                </p>
                                <ul class="actions">
                                    <li><a href="#" class="button button-icon icon icon-file">Learn More</a></li>
                                </ul>
                            </article>

                        </li>
                        <li>

                            <!-- Highlight -->
                            <article class="cell is-highlight">
                                <header>
                                    <h3><a href="#">Something of less note</a></h3>
                                </header>
                                <a href="http://regularjane.deviantart.com/art/Pink-Rain-2009-134901803" class="image image-left"><img src="images/pic07.jpg" alt="" /></a>
                                <p>
                                    Phasellus  sed laoreet massa id justo mattis pharetra. Fusce suscipit ligula vel quam 
                                    viverra sit amet mollis tortor congue magna lorem ipsum dolor et quisque ut odio facilisis 
                                    convallis. Etiam non nunc vel est suscipit convallis non id orci. Ut interdum tempus 
                                    facilisis convallis. Etiam non nunc vel est suscipit convallis non id orci.
                                </p>
                                <ul class="actions">
                                    <li><a href="#" class="button button-icon icon icon-file">Learn More</a></li>
                                </ul>
                            </article>

                        </li>
                    </ul>
                </section>
				
				*/
				?>
            
            </div>

        </div>
    </div>

</div>

		
<?php
	require_once('includes/footer.php');
?>
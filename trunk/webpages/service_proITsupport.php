<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<!-- Features Wrapper -->

<style type="text/css">
	#features-wrapper
	{
		text-align: justify;
	}
	#features-wrapper .cell
	{
		margin: 0 5%;
	}
	#features-wrapper .cell p
	{
		text-align: justify;
		padding: .25% 5% 1.5% 7%;
	}
	#features-wrapper .cell h2
	{
		background: url("images/wings.png") no-repeat scroll right top #02647F; /* Old browsers */
		background: url("images/wings.png") no-repeat scroll right top, -moz-linear-gradient(top,  rgba(149,184,195,1) 0%, rgba(108,156,172,1) 15%, rgba(22,115,146,1) 67%, rgba(16,84,106,1) 83%, rgba(10,51,64,1) 100%); /* FF3.6+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(149,184,195,1)), color-stop(15%,rgba(108,156,172,1)), color-stop(67%,rgba(22,115,146,1)), color-stop(83%,rgba(16,84,106,1)), color-stop(100%,rgba(10,51,64,1))); /* Chrome,Safari4+ */
		background: url("images/wings.png") no-repeat scroll right top, -webkit-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Chrome10+,Safari5.1+ */
		background: url("images/wings.png") no-repeat scroll right top, -o-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* Opera 11.10+ */
		background: url("images/wings.png") no-repeat scroll right top, -ms-linear-gradient(top,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* IE10+ */
		background: url("images/wings.png") no-repeat scroll right top, linear-gradient(to bottom,  rgba(149,184,195,1) 0%,rgba(108,156,172,1) 15%,rgba(22,115,146,1) 67%,rgba(16,84,106,1) 83%,rgba(10,51,64,1) 100%); /* W3C */
		filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#95b8c3', endColorstr='#0a3340',GradientType=0 ); /* IE6-9 */

		padding: 2em .5em .5em;
		
		color: #D4D6D7;
		padding-left: 2.5%;
		margin-top: .25em;
	}
	
	#features-wrapper .cell ul
	{
		list-style: disc inside none;
		text-align: left;
		padding: .25% 5% 1.5% 7%
	}
</style>
			<div id="features-wrapper" style="padding-bottom: 50px;">

				<!-- Features -->
			  	<section id="features" class="container">
                    <header style="text-align: center;">
                        <h2 style="margin-bottom:0px;">Managed IT Support</h2>
                        <h3>Provided by <strong>RosinCloud</strong></h3>
				  	</header>
						<div class="row">
							<div class="12u">

								<!-- Feature -->
                   			  <section class="cell">
										<a href="<?=$ROOT_PATH ?>services.php" class="image image-left">
                                        	<img src="<?=$IMG_PATH ?>cropped-Computer-repair-pc-repair-IT-Support.png" alt="support hand" />
                                        </a>
										<header>
										  <h2>Managed IT Support</h2>
										</header>
<!-- Begin TEXT -->
<p>
Information Technology is the glue that holds your company together.  Different kinds of technology and different 
components of your business come together to let you communicate with the world, get your product or service out the 
door, and keep track of everything needed to make this possible.
</p>
<p>
Your IT department might just be one person (who may not be wearing his or her IT hat full-time), or a few people. 
While they're experts about your business and how you use technology, a potential weakness is that they're experts about 
only your business and how you use technology.  Hardware, software, and services are changing constantly. 
To stay current or get ahead of these changes is nearly a full-time job itself.
</p>
<p>
This is why RosinCloud is a wise choice to help your existing IT staff.  Our full time job is both to keep everything 
running 24/7, and to be ready for the next big thing.  And we have the expertise of doing this for organizations of all 
sizes, in a variety of industries.
</p>
<h3 style="margin-left:2.5%;">
	HOW WE CAN HELP
</h3>
<ul>
    <li>Be your IT department, or assist your existing IT staff</li>
    <li>Make sure your server(s) and business applications stay online</li>
    <li>Lower costs, improve productivity and flexibility</li>
    <li>Enable secure remote access to your company network</li>
    <li>Ensure your data is backed up, and can actually get restored </li>
    <li>Plan an office move or expansion so you don't have downtime</li>
</ul>
<!-- END TEXT -->
									</section>

							</div>
							
						</div>
					</section>
			
			</div>
		
<?php
	require_once('includes/footer.php');
?>
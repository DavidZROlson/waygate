<?php

	$ROOT_PATH = "";
	if (file_exists("ROOT.txt")) {
		$ROOT_PATH = "";
	} elseif(file_exists("../" . "ROOT.txt")) {
		$ROOT_PATH = "../";
	} elseif(file_exists("../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../";
	} elseif(file_exists("../../../" . "ROOT.txt")) {
		$ROOT_PATH = "../../../";
	}
	
	$CSS_PATH = $ROOT_PATH . "css/";
	$JS_PATH = $ROOT_PATH . "js/";
	$IMG_PATH = $ROOT_PATH . "images/";
	
	require_once('includes/pre_header.php');
	require_once('includes/header_banner.php');
?>
<style>
#features-wrapper, #features-wrapper p
{
	text-align:justify;
}
#features-wrapper h3
{
	text-decoration: underline;
}
.outer
{
	list-style: lower-roman inside none;
	margin-left: 2em;
	font-size: 14px;
}
.inner
{
	list-style: none inside none;
	margin-left: 2em;
	font-size: 14px;
}
</style>

<!-- Features Wrapper -->
<div id="features-wrapper" style="padding: 50px 0;">

    <!-- Features -->
    <section id="features" class="container">
        <div class="row">
            <div class="12u">

                <!-- Feature -->
                <section class="cell" style="padding: 5%;">
                    <header style="
                        border-bottom: 1px solid;
                        margin-bottom: 10px;">
                        <h2 style="
                            text-transform:none;
                            margin:0;
                            letter-spacing:0;
                            text-align:left;
                            font-variant:small-caps;">
                            Return Policy
                        </h2>
                    </header>
                    <p>
                    	<span style="margin-left:2em;">RosinCloud</span> is committed to ensuring satisfaction for all customers who purchase its products. 
                        As the leading retailer of digital products, RosinCloud relies on its Return and Cancellation 
                        Policy to help ensure this satisfaction.
                    </p>
                    <h3>
                        RosinCloud's Return Policy
                    </h3>
                    <p>
                    	Our return policy for all RosinCloud products is as follows:<br />
                        <br />
                        <span style="margin-left:2em;">RosinCloud</span> will, at its discretion, allow for the return 
                        or replacement of any product within 72 hours from the date of purchase. For recurring billing 
                        products, returns for more than one payment may be provided if requested within the standard 
                        72 hour return period.
                    </p>
                    <h3>
                        Defining Returns and Cancellations
                    </h3>
                    <p>
                        <span style="margin-left:2em;">When</span> a customer reaches us by phone or email, our first 
                        response is to assist in getting technical support or customer service for the customer for 
                        the product that they purchased. However, in some cases a product may be unsatisfactory to the 
                        customer for reasons completely beyond our control, in which case a return or a cancellation 
                        may be processed.<br />
                        <br />
						<span style="margin-left:2em;">If</span> a customer requests a return, the money for the 
                        requested transaction is refunded back to the customer, in part or whole. If the return is for 
                        a recurring billing product, then the return policy allows for the most recent payment to be 
                        returned. Multiple payment returns can be provided as long as they are within RosinCloud’s 
                        Return Policy. A return on a recurring billing product will also result in a cancellation.<br />
                        <br />
						<span style="margin-left:2em;">If</span> customers request a cancellation for their recurring 
                        billing product, no future rebills will be charged to their account. Cancellation will not 
                        generate a return – it will only stop any future rebills.                        
                    </p>
                    <h3>
                        Details of RosinCloud's Return Policy
                    </h3>
                    <p>
                        <span style="margin-left:2em;">Customers</span> requesting a return after 72 hours will be 
                        directed to the vendor for technical support or customer service. Vendors may request a return, 
                        on behalf of customers, of any purchase up to 72 hours after the date of the RosinCloud 
                        customer’s purchase.<br />
                        <br />
						<span style="margin-left:2em;">Returns</span> will only be credited back to the account used 
                        to make the original purchase. If the original account has been closed, the purchase is not 
                        eligible for return.<br />
                        <br />
						<span style="margin-left:2em;">When</span> a sale is returned, the customer normally receives 
                        a 100% refund. Payouts from that sale are debited back out of the corresponding vendor and 
                        affiliate accounts. Due to the digital format of RosinCloud products, the customer retains the 
                        product and, in many cases, the customer has received a benefit from the product. In such cases, 
                        the refund issued to the customer may be for an amount less than the full purchase price.<br />
                        <br />
						<span style="margin-left:2em;">Our</span> vendors are not permitted to make any guarantee that 
                        conflicts with our return policy. If you find a product sold through RosinCloud with any 
                        warranty that conflicts with our return policy, please bring it to our attention so that we 
                        may take corrective action.
                    </p>
                    <h3>
                        Reasons why a sale may be refunded
                    </h3>
                    <p>
                    	<span style="margin-left:2em;">RosinCloud</span> has every incentive to keep customers happy by providing quality products and service. 
                        To ensure customer satisfaction, RosinCloud reserves the right to reverse a sale, including but 
                        not limited to the following:<br />
                        <br />
                        <ul class="outer">
                            <li>
    							If the customer provides a valid reason for requesting a return within 72 hours of purchase;
                            </li>
                            <li>
    							If the vendor of the product requests a return on the customer’s behalf within 72 hours of purchase if our customer service team confirms there is a valid reason for the return;
                            </li>
                            <li>
    							If the customer provides a valid report that the charge was fraudulent or unauthorized;
                            </li>
                            <li>
    							In order to comply with credit card industry rules, ACH industry rules, PayPal terms of service, U.S. law, and requests from verified US judiciary or law enforcement agents; or
                            </li>
                            <li>
    							For any other reason RosinCloud deems appropriate.
                            </li>
                        </ul>
                    </p>
                    <h3>
                        RosinCloud's Cancellation Policy
                    </h3>
                    <p>
                        <span style="margin-left:2em;">The</span> customer may cancel a recurring billing subscription 
                        products at any time. A cancellation means that no future rebills are charged to the customer’s 
                        account. The cancellation of a recurring billing product does not generate a refund.
                    </p>
                    <h3>
                        Abuse of the Return Policy
                    </h3>
                    <p>                        
                        <span style="margin-left:2em;">Customers</span> requesting serial or repeated returns may be 
                        blocked from making further purchases. Customers that violate the terms of use of the product 
                        may have their right of return revoked.
                    </p>
                    <h3>
                        Note to RosinCloud's Clientes
                    </h3>
                    <p>                        
                        <span style="margin-left:2em;">This</span> Return and Cancellation Policy is part of, and 
                        incorporated within, the RosinCloud Client Contract. As a condition of registering with 
                        RosinCloud and using the RosinCloud Services, you expressly acknowledge that you have read 
                        and understood this Return and Cancellation Policy and you agree to be bound by the terms 
                        and conditions contained within. If at any time you disagree with this Return and Cancellation 
                        Policy or any part of it, your sole remedy is to cease all use of the RosinCloud Services and 
                        terminate your account. Please note, however, that any transactions which occurred prior to 
                        the date of such termination shall be governed and controlled in full by the terms of this 
                        Return and Cancellation Policy.
                    </p>
                </section>

            </div>
            <!-- END 12u -->
        </div>
        <!-- END row -->
    </section>
    <!-- END features -->

</div>
		
<?php
	require_once('includes/footer.php');
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WayGateCloud | Login</title>

<!-- links and recources -->
<!-- CSS -->
<link href="css/css2.css" rel="stylesheet" type="text/css" />

<!-- Font-Awesome http://fortawesome.github.io/Font-Awesome/icons/ -->
<link href="css/font-awesome/css/font-awesome.css" rel="stylesheet" />

<!-- JQUERY UI THEME AND SOURCES -->
<link rel="stylesheet"
	href="js/jquery-ui-1.10.3/themes/base/jquery.ui.all.css">

<script type="text/javascript" src="js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.3/ui/jquery-ui.js"></script>

<!-- CUSTOM JQUERY SCRIPTS
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/ux.js"></script> -->

<!-- REQUIRED FOR FORM VALIDATION http://jqueryvalidation.org/documentation/ -->
<script type="text/javascript"
	src="js/jquery-validation-1.11.1/dist/jquery.validate.js"></script>
<script type="text/javascript"
	src="js/jquery-validation-1.11.1/dist/additional-methods.js"></script>

<!-- REQUIRED FOR FORM MASKS http://igorescobar.github.io/jQuery-Mask-Plugin/ -->
<script src="js/jQuery-Mask-Plugin-master/jquery.mask.js"></script>

</head>

<?php
require_once(dirname(__FILE__) . "/pi_classes/Admin.php");
$obj=new Admin();

if($_SESSION['PSAdminID']!='')
{
	header('Location:'.SITE_PATH.'login.php');
}

$objlogin= new Login();
if($_POST['submit']){
	if(($_POST['user']!='')&&($_POST['password'])!='')
	{
		$uname=$_POST['user'];
		$pwd=$_POST['password'];
		$remember=$_POST['remember_me'];
		$pagebck=$_POST['pagebck'];
		$retdata=$objlogin->chkValidLogin($uname, $pwd,$remember);
		if($retdata=='success'){
			header('Location:'.SITE_PATH.'WayGate.php');
			/*if($pagebck!=''){
				$page=  base64_decode($pagebck);
			header('Location:'.$page);
			}else{
			header('Location:'.SITE_PATH);
			}*/

		}
		else{
			$msg="Wrong username or password.";
		}

	}else{
		$msg="Please enter username and password.";
	}
}

?>
<body>
	<!-- BEGIN HEADER -->
	<?php include_once('includes/front_header.php'); ?>
	<!-- END HEADER -->

	<!-- BEGIN MAIN_WRAPPER -->
	<div id="main_wrapper">

		<!-- BEGIN MAIN_CONTAINER -->
		<div id="main_container">

			<!-- BEGIN CONTENT_WRAPPER -->
			<div id="content_wrapper">
				<div id="content">
					<div id="login_cell">
						<form id="login_form" name="login_form" action="" method="post"
							style="text-align: center;">

							<div>
								<h1>Login</h1>
							</div>
							<div class="input-prepend">
								<span class="add-on"><i class="icon-user"></i> </span> <input
									class="span2" id="user" name="user" type="text"
									placeholder="Username" autofocus="autofocus" required />
							</div>
							<div class="input-prepend">
								<span class="add-on"><i class="icon-key"></i> </span> <input
									class="span2" id="password" name="password" type="password"
									placeholder="Password" required />
							</div>
							<div style="margin: 5px 0 5px;">
								<span
									style="background-color: white; border-radius: 4px 4px 4px 4px; padding: 5px 68px 5px 5px;">
									<input type="checkbox" name="remember_me" id="remember_me">Remember
									me
								</span>
							</div>
							<input id="login_submit" type="submit" name="submit"
								value="Login" />
						</form>
					</div>
				</div>


			</div>
			<!-- END CONTENT_WRAPPER -->

		</div>
		<!-- END MAIN_CONTAINER -->


	</div>
	<!-- END MAIN_WRAPPER -->

	<!-- BEGIN FOOTER -->
	<?php include_once('includes/footer.php'); ?>
	<!-- END FOOTER -->

	<script type="text/javascript">
		$(document).ready(function(e) {
			
			$('#login_form').validate();
			
			$(window).resize(function(e) {
				$('#main_container').height($(window).height() - 170);
				$('#content').height($('#main_container').height() -20);
			});
			
			$(window).trigger('resize');
		});
		
    </script>

</body>
</html>

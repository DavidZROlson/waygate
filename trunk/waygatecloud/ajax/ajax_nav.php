<?php

//TODO
/********************************************************
 *	PRIOR TO PAGE LOAD CHECK SERVER FOREACH
*		VENDORS:
*			NAME: string
*	AND POPULATE 'vendor' DROP DOWN SELECT LIST OPTIONS
********************************************************/
require_once(dirname(__FILE__) . '/../pi_classes/Admin.php');

//CHECK FOR POST REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	session_start();
	if(isset($_SESSION['PSAdminNM']) && $_POST['clicked'] === 'logout'){
		session_destroy();
		session_unset();

		echo json_encode(array('success' => true));
		exit();

	}else {
		echo json_encode(array('success' => false));
		exit();
	}
}

//CHECK FOR GET REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	/********************************************************
	 *														*
	*					NAV DROP DOWN OPTIONS				*
	*														*
	********************************************************/

	/********************************************************
	 *					VENDOR	SELECTED					*
	********************************************************/

	//TODO
	/***********************************************************
		*	WHEN A VENDOR IS SELECTED, CHECK SERVER FOREACH
	*		DATA_CENTERS:
	*			NAME: string
	*	AND POPULATE 'dcs' DROP DOWN SELECT LIST
	***********************************************************/

	//IF GET REQUEST IS FROM 'vendors'
	if(isset($_GET['vendors'])){
		$vendors = $_GET['vendors'];
		$objDcs = new Dcs();

		$arguments = array ('username' => $_SESSION['PSAdminNM'],
				'vendor' => $vendors,);

		$data = $objDcs->getdcsnames($arguments);
		$temp = $data[0];

		if($vendors == 'rackspace'){
			//load rackspace Datacenters
			$dcs = "";
			foreach ($data as $temp) {
				$dcs .= '<option value=' . $temp['idrights'] . '>'.$temp['dcname'].'</option>';
			}

			$result =  '<select name="dcs_sel" id="dcs_sel">
					<option value="">[Select...]</option>' .
					$dcs .
					'</select>';
		}elseif($vendors == 'amazon'){
			//load ec2 Datacenters
			$result =  '<select name="dcs_sel" id="dcs_sel">
					<option value="">[Select...]</option>
					<option value="az_1">Amazon_DC1</option>
					<option value="az_2">Amazon_DC2</option>
					<option value="az_3">Amazon_DC3</option>
					</select>';

		}elseif($vendors == 'rosincloud'){
			//load rosincloud Datacenters
			$result =  '<select name="dcs_sel" id="dcs_sel">
					<option value="">[Select...]</option>
					<option value="ord">ORD</option>
					</select>';
		}else{
			$result =  '<select name="dcs_sel" id="dcs_sel" disabled="disabled">
					<option value="">[Select...]</option>
					</select>';
		}
		echo $result;
		exit();
	}

	/********************************************************
	 *					DATA CENTER	SELECTED				*
	********************************************************/

	//IF GET REQUEST IS FROM 'dcs'
	if(isset($_GET['dcs'])){
		$DataCenter = $_GET['dcs'];
			
		if($DataCenter == 'rackspace'){
			//load rackspace
			//Query for Rackspace servers
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->

					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';

		}elseif($DataCenter == 'amazon'){
			//load amazon
			//Query for Amazon servers
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->

					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';

		}elseif($DataCenter == 'ord'){
			//load rosincloud
			//Query for Rosincloud servers
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->

					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';

		}else{
			//default tabs load
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->

					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';

		}

		echo json_encode(array('html_string' => $result));
		exit();
	}
}

?>
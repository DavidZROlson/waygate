// JavaScript Document
$(document)
		.ready(
				function() {

					/***********************************************************
					 * ***********************************NAV
					 * FUNCTIONS********************************************
					 **********************************************************/

					/***********************************************************
					 * LOGOUT BUTTON PRESS
					 **********************************************************/
					$('#logout')
							.click(
									function() {
										$
												.ajax({
													type : 'POST',
													url : 'ajax/ajax_nav.php',
													data : {
														clicked : 'logout'
													},
													cache : false,
													dataType : 'json',
													success : function(data) {
														if (data.success == true) {
															window.location = "login.php";
														} else {
															alert('ERROR:\nPSAdminNM was not found in $_SESSION["PSAdminNM"]');
														}
													}// end success function
												}); // end ajax call
									}); // end logout click

					/***********************************************************
					 * ADMIN SETTINGS BUTTON PRESS
					 **********************************************************/
					// CLEAR THE VENDOR AND DATACENTER NAV DROP DOWNS
					$('#admin_nav')
							.click(
									function() {
										$('#vendors>option:eq(0)').prop(
												'selected', true);
										var _selected = $('#vendors').val();

										$.ajax({
											type : 'GET',
											url : 'ajax/ajax_nav.php',
											data : {
												vendors : _selected
											},
											async : false,
											success : function(result) {
												$('#dcs_DD').html(result);
											}
										});

										$
												.ajax({
													type : 'GET',
													url : 'pages/admin/main_tabs.php',
													beforeSend : function() {
														$('#loading_div')
																.toggleClass(
																		"hidden");
													},
													success : function(data) {
														$('#content_wrapper')
																.html(data);
														/***********************
														 * ********************************DEFAULT
														 * ADMIN
														 * FUNCTIONS*************************************
														 **********************/
														$
																.ajax({
																	type : 'GET',
																	url : 'pages/admin/user_sub_tabs.php',
																	success : function(
																			data) {
																		$(
																				'#content_sub_tabs')
																				.html(
																						data);
																		// ADD
																		// USER
																		// LOADED
																		// BY
																		// DEFAULT
																		$
																				.ajax({
																					type : 'GET',
																					url : 'pages/admin/add_users.php',
																					success : function(
																							data) {
																						$(
																								'#content')
																								.html(
																										data);
																						$(
																								window)
																								.trigger(
																										'resize');
																						$(
																								'#loading_div')
																								.toggleClass(
																										"hidden");

																						$(
																								'#apply_btn')
																								.click(
																										function() {
																											$
																													.ajax({
																														type : 'POST',
																														url : 'pages/admin/add_users.php',
																														data : $(
																																'#add_form')
																																.serialize(),
																														beforeSend : function() {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																														},
																														success : function(
																																data) {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																															alert(data);
																															$(
																																	'#add_sub')
																																	.click(); // re-direct
																																				// to
																																				// [USERS
																																				// /
																																				// ADD]
																														}// END
																															// APPLY
																															// BUTTON
																															// SUCCESS
																													});// END
																														// AJAX
																										});// END
																											// APPLY
																											// BUTTON
																											// EVENT
																											// LISTENER

																						// ADD
																						// USER
																						// SUB
																						// MENU
																						// CLICK
																						$(
																								'#add_sub')
																								.click(
																										function() {
																											$
																													.ajax({
																														type : 'GET',
																														url : 'pages/admin/add_users.php',
																														beforeSend : function() {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																														},
																														success : function(
																																data) {
																															$(
																																	'#content')
																																	.html(
																																			data);
																															$(
																																	window)
																																	.trigger(
																																			'resize');
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");

																															$(
																																	'#apply_btn')
																																	.click(
																																			function() {
																																				$
																																						.ajax({
																																							type : 'POST',
																																							url : 'pages/admin/add_users.php',
																																							data : $(
																																									'#add_form')
																																									.serialize(),
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									data) {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																								alert(data);
																																								$(
																																										'#add_sub')
																																										.click(); // re-direct
																																													// to
																																													// [USERS
																																													// /
																																													// ADD]
																																							}// END
																																								// APPLY
																																								// BUTTON
																																								// SUCCESS
																																						});// END
																																							// AJAX
																																			});// END
																																				// APPLY
																																				// BUTTON
																																				// EVENT
																																				// LISTENER
																														}// END
																															// GET
																															// AJAX
																															// ADD_USER
																															// SUCCESS
																													});// END
																														// GET
																														// AJAX
																														// ADD_USER
																										});// END
																											// ADD
																											// USER
																											// SUB
																											// MENU
																											// CLICK

																						// EDIT
																						// USER
																						// SUB
																						// MENU
																						// CLICK
																						$(
																								'#edit_sub')
																								.click(
																										function() {
																											$
																													.ajax({
																														type : 'GET',
																														url : 'pages/admin/edit_users.php',
																														beforeSend : function() {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																														},
																														success : function(
																																data) {
																															$(
																																	'#content')
																																	.html(
																																			data);
																															$(
																																	window)
																																	.trigger(
																																			'resize');
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");

																															$(
																																	'#users_DD')
																																	.change(
																																			function() {
																																				var user = $(
																																						'#users_DD')
																																						.val();

																																				$
																																						.ajax({
																																							type : 'GET',
																																							url : 'pages/admin/edit_users.php',
																																							data : {
																																								user : user
																																							},
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									html) {
																																								$(
																																										'#user_info')
																																										.html(
																																												html);
																																								$(
																																										window)
																																										.trigger(
																																												'resize');
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");

																																								$(
																																										'#apply_btn')
																																										.click(
																																												function() {
																																													$
																																															.ajax({
																																																type : 'POST',
																																																url : 'pages/admin/edit_users.php',
																																																data : $(
																																																		'#edit_form')
																																																		.serialize(),
																																																beforeSend : function() {
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");
																																																},
																																																success : function(
																																																		data) {
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");
																																																	alert(data);
																																																	$(
																																																			'#edit_sub')
																																																			.click(); // re-direct
																																																						// to
																																																						// [USERS
																																																						// /
																																																						// EDIT]
																																																}// END
																																																	// APPLY
																																																	// BUTTON
																																																	// SUCCESS
																																															});// END
																																																// AJAX
																																												});// END
																																													// APPLY
																																													// BUTTON
																																													// EVENT
																																													// LISTENER
																																							}// END
																																								// GET
																																								// EDIT_USERS
																																								// SUCCESS
																																								// W/SELECTED
																																								// USER
																																						});// END
																																							// GET
																																							// AJAX
																																							// EDIT_USERS
																																			});// END
																																				// USERS_DD
																																				// CHANGE
																																				// EVENT
																																				// LISTENER

																														}// END
																															// GET
																															// EDIT
																															// USERS
																															// SUCCESS
																													});// END
																														// GET
																														// EDIT
																														// USERS
																														// AJAX
																										});// END
																											// EDIT
																											// USER
																											// SUB
																											// MENU
																											// CLICK

																						/******
																						 * ***********************************ADMIN
																						 * USERS
																						 * FUNCTIONS************************************
																						 *****/
																						// USERS
																						// TAB
																						// CLICK
																						// EVENT
																						// LISTENER
																						$(
																								'#admin_USERS_tab')
																								.click(
																										function() {

																											$
																													.ajax({
																														type : 'GET',
																														url : 'pages/admin/user_sub_tabs.php',
																														beforeSend : function() {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																														},
																														success : function(
																																data) {
																															$(
																																	'#content_sub_tabs')
																																	.html(
																																			data);
																															$
																																	.ajax({
																																		type : 'GET',
																																		url : 'pages/admin/add_users.php',
																																		success : function(
																																				data) {
																																			$(
																																					'#content')
																																					.html(
																																							data);
																																			$(
																																					window)
																																					.trigger(
																																							'resize');
																																			$(
																																					'#loading_div')
																																					.toggleClass(
																																							"hidden");
																																		}
																																	});

																															// ADD
																															// USER
																															// SUB
																															// MENU
																															// CLICK
																															$(
																																	'#add_sub')
																																	.click(
																																			function() {
																																				$
																																						.ajax({
																																							type : 'GET',
																																							url : 'pages/admin/add_users.php',
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									data) {
																																								$(
																																										'#content')
																																										.html(
																																												data);
																																								$(
																																										window)
																																										.trigger(
																																												'resize');
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");

																																								$(
																																										'#apply_btn')
																																										.click(
																																												function() {
																																													$
																																															.ajax({
																																																type : 'POST',
																																																url : 'pages/admin/add_users.php',
																																																data : $(
																																																		'#add_form')
																																																		.serialize(),
																																																beforeSend : function() {
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");
																																																},
																																																success : function(
																																																		data) {
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");
																																																	alert(data);
																																																	$(
																																																			'#add_sub')
																																																			.click(); // re-direct
																																																						// to
																																																						// [USERS
																																																						// /
																																																						// ADD]
																																																}// END
																																																	// APPLY
																																																	// BUTTON
																																																	// SUCCESS
																																															});// END
																																																// AJAX
																																												});// END
																																													// APPLY
																																													// BUTTON
																																													// EVENT
																																													// LISTENER
																																							}// END
																																								// GET
																																								// AJAX
																																								// ADD_USER
																																								// SUCCESS
																																						});// END
																																							// GET
																																							// AJAX
																																							// ADD_USER
																																			});// END
																																				// ADD
																																				// USER
																																				// SUB
																																				// MENU
																																				// CLICK

																															// EDIT
																															// USER
																															// SUB
																															// MENU
																															// CLICK
																															$(
																																	'#edit_sub')
																																	.click(
																																			function() {
																																				$
																																						.ajax({
																																							type : 'GET',
																																							url : 'pages/admin/edit_users.php',
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									data) {
																																								$(
																																										'#content')
																																										.html(
																																												data);
																																								$(
																																										window)
																																										.trigger(
																																												'resize');
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");

																																								$(
																																										'#users_DD')
																																										.change(
																																												function() {
																																													var user = $(
																																															'#users_DD')
																																															.val();

																																													$
																																															.ajax({
																																																type : 'GET',
																																																url : 'pages/admin/edit_users.php',
																																																data : {
																																																	user : user
																																																},
																																																beforeSend : function() {
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");
																																																},
																																																success : function(
																																																		html) {
																																																	$(
																																																			'#user_info')
																																																			.html(
																																																					html);
																																																	$(
																																																			window)
																																																			.trigger(
																																																					'resize');
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");

																																																	$(
																																																			'#apply_btn')
																																																			.click(
																																																					function() {
																																																						$
																																																								.ajax({
																																																									type : 'POST',
																																																									url : 'pages/admin/edit_users.php',
																																																									data : $(
																																																											'#edit_form')
																																																											.serialize(),
																																																									beforeSend : function() {
																																																										$(
																																																												'#loading_div')
																																																												.toggleClass(
																																																														"hidden");
																																																									},
																																																									success : function(
																																																											data) {
																																																										$(
																																																												'#loading_div')
																																																												.toggleClass(
																																																														"hidden");
																																																										alert(data);
																																																										$(
																																																												'#edit_sub')
																																																												.click(); // re-direct
																																																															// to
																																																															// [USERS
																																																															// /
																																																															// EDIT]
																																																									}// END
																																																										// APPLY
																																																										// BUTTON
																																																										// SUCCESS
																																																								});// END
																																																									// AJAX
																																																					});// END
																																																						// APPLY
																																																						// BUTTON
																																																						// EVENT
																																																						// LISTENER
																																																}// END
																																																	// GET
																																																	// EDIT_USERS
																																																	// SUCCESS
																																																	// W/SELECTED
																																																	// USER
																																															});// END
																																																// GET
																																																// AJAX
																																																// EDIT_USERS
																																												});// END
																																													// USERS_DD
																																													// CHANGE
																																													// EVENT
																																													// LISTENER

																																							}// END
																																								// GET
																																								// EDIT
																																								// USERS
																																								// SUCCESS
																																						});// END
																																							// GET
																																							// EDIT
																																							// USERS
																																							// AJAX
																																			});// END
																																				// EDIT
																																				// USER
																																				// SUB
																																				// MENU
																																				// CLICK

																														}// END
																															// SUCCESS
																													});// END
																														// AJAX
																										});// END
																											// USERS
																											// TAB
																											// CLICK
																											// EVENT
																											// LISTENER

																						/******
																						 * ***********************************ADMIN
																						 * RIGHTS
																						 * FUNCTIONS************************************
																						 *****/
																						// USERS
																						// TAB
																						// CLICK
																						// EVENT
																						// LISTENER
																						$(
																								'#admin_RIGHTS_tab')
																								.click(
																										function() {

																											$
																													.ajax({
																														type : 'GET',
																														url : 'pages/admin/rights_sub_tabs.php',
																														beforeSend : function() {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																														},
																														success : function(
																																data) {
																															$(
																																	'#content_sub_tabs')
																																	.html(
																																			data);
																															$
																																	.ajax({
																																		type : 'GET',
																																		url : 'pages/admin/edit_permissions.php',
																																		success : function(
																																				data) {
																																			$(
																																					'#content')
																																					.html(
																																							data);
																																			$(
																																					window)
																																					.trigger(
																																							'resize');
																																			$(
																																					'#loading_div')
																																					.toggleClass(
																																							"hidden");

																																			var edit_form = $('#edit_form');
																																			edit_form
																																					.validate({
																																						rules : {
																																							users_DD : {
																																								required : true
																																							},
																																							vendors_permissions_DD : {
																																								required : true
																																							}
																																						},
																																						messages : {
																																							users_DD : {
																																								required : ""
																																							},
																																							vendors_permissions_DD : {
																																								required : ""
																																							}
																																						}
																																					});

																																			$(
																																					'#load')
																																					.click(
																																							function() {

																																								if (edit_form
																																										.valid() == true) {
																																									var user = $(
																																											'#users_DD')
																																											.val();
																																									var vendor = $(
																																											'#vendors_permissions_DD')
																																											.val();

																																									$
																																											.ajax({
																																												type : 'GET',
																																												url : 'pages/admin/edit_permissions.php',
																																												data : {
																																													user : user,
																																													vendor : vendor
																																												},
																																												beforeSend : function() {
																																													$(
																																															'#loading_div')
																																															.toggleClass(
																																																	"hidden");
																																												},
																																												success : function(
																																														html) {
																																													$(
																																															'#user_info')
																																															.html(
																																																	html);
																																													$(
																																															window)
																																															.trigger(
																																																	'resize');
																																													$(
																																															'#loading_div')
																																															.toggleClass(
																																																	"hidden");

																																													$(
																																															'#apply_btn')
																																															.click(
																																																	function() {
																																																		$
																																																				.ajax({
																																																					type : 'POST',
																																																					url : 'pages/admin/edit_permissions.php',
																																																					data : $(
																																																							'#edit_form')
																																																							.serialize(),
																																																					beforeSend : function() {
																																																						$(
																																																								'#loading_div')
																																																								.toggleClass(
																																																										"hidden");
																																																					},
																																																					success : function() {
																																																						$(
																																																								'#loading_div')
																																																								.toggleClass(
																																																										"hidden");
																																																						alert('UPDATE SUCCESS');
																																																						$(
																																																								'#permissions_sub')
																																																								.click(); // re-direct
																																																											// to
																																																											// [USERS
																																																											// /
																																																											// EDIT]
																																																					}// END
																																																						// APPLY
																																																						// BUTTON
																																																						// SUCCESS
																																																				});// END
																																																					// AJAX
																																																	});// END
																																																		// APPLY
																																																		// BUTTON
																																																		// EVENT
																																																		// LISTENER
																																												}// END
																																													// GET
																																													// EDIT_USERS
																																													// SUCCESS
																																													// W/SELECTED
																																													// USER
																																											});// END
																																												// GET
																																												// AJAX
																																												// EDIT_USERS
																																								}// END
																																									// IF
																																									// FORM
																																									// IS
																																									// VALID
																																							});// END
																																								// vendor_permissions_DD
																																								// CHANGE
																																								// EVENT
																																								// LISTENER

																																		}// END
																																			// GET
																																			// EDIT_PERMISSIONS
																																			// SUCCESS
																																	});

																															// EDIT
																															// PERMISSIONS
																															// SUB
																															// MENU
																															// CLICK
																															$(
																																	'#permissions_sub')
																																	.click(
																																			function() {
																																				$
																																						.ajax({
																																							type : 'GET',
																																							url : 'pages/admin/edit_permissions.php',
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									data) {
																																								$(
																																										'#content')
																																										.html(
																																												data);
																																								$(
																																										window)
																																										.trigger(
																																												'resize');
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");

																																								var edit_form = $('#edit_form');
																																								edit_form
																																										.validate({
																																											rules : {
																																												users_DD : {
																																													required : true
																																												},
																																												vendors_permissions_DD : {
																																													required : true
																																												}
																																											},
																																											messages : {
																																												users_DD : {
																																													required : ""
																																												},
																																												vendors_permissions_DD : {
																																													required : ""
																																												}
																																											}
																																										});

																																								$(
																																										'#load')
																																										.click(
																																												function() {

																																													if (edit_form
																																															.valid() == true) {
																																														var user = $(
																																																'#users_DD')
																																																.val();
																																														var vendor = $(
																																																'#vendors_permissions_DD')
																																																.val();

																																														$
																																																.ajax({
																																																	type : 'GET',
																																																	url : 'pages/admin/edit_permissions.php',
																																																	data : {
																																																		user : user,
																																																		vendor : vendor
																																																	},
																																																	beforeSend : function() {
																																																		$(
																																																				'#loading_div')
																																																				.toggleClass(
																																																						"hidden");
																																																	},
																																																	success : function(
																																																			html) {
																																																		$(
																																																				'#user_info')
																																																				.html(
																																																						html);
																																																		$(
																																																				window)
																																																				.trigger(
																																																						'resize');
																																																		$(
																																																				'#loading_div')
																																																				.toggleClass(
																																																						"hidden");

																																																		$(
																																																				'#apply_btn')
																																																				.click(
																																																						function() {
																																																							$
																																																									.ajax({
																																																										type : 'POST',
																																																										url : 'pages/admin/edit_permissions.php',
																																																										data : $(
																																																												'#edit_form')
																																																												.serialize(),
																																																										beforeSend : function() {
																																																											$(
																																																													'#loading_div')
																																																													.toggleClass(
																																																															"hidden");
																																																										},
																																																										success : function() {
																																																											$(
																																																													'#loading_div')
																																																													.toggleClass(
																																																															"hidden");
																																																											alert('UPDATE SUCCESS');
																																																											$(
																																																													'#permissions_sub')
																																																													.click(); // re-direct
																																																																// to
																																																																// [USERS
																																																																// /
																																																																// EDIT]
																																																										}// END
																																																											// APPLY
																																																											// BUTTON
																																																											// SUCCESS
																																																									});// END
																																																										// AJAX
																																																						});// END
																																																							// APPLY
																																																							// BUTTON
																																																							// EVENT
																																																							// LISTENER
																																																	}// END
																																																		// GET
																																																		// EDIT_USERS
																																																		// SUCCESS
																																																		// W/SELECTED
																																																		// USER
																																																});// END
																																																	// GET
																																																	// AJAX
																																																	// EDIT_USERS
																																													}// END
																																														// IF
																																														// FORM
																																														// IS
																																														// VALID
																																												});// END
																																													// vendor_permissions_DD
																																													// CHANGE
																																													// EVENT
																																													// LISTENER

																																							}// END
																																								// GET
																																								// EDIT_PERMISSIONS
																																								// SUCCESS
																																						});// END
																																							// GET
																																							// EDIT_PERMISSIONS
																																							// AJAX
																																			});// END
																																				// EDIT_PERMISSIONS
																																				// SUB
																																				// MENU
																																				// CLICK

																														}// END
																															// SUCCESS
																													});// END
																														// AJAX
																										});// END
																											// RIGHTS
																											// TAB
																											// CLICK
																											// EVENT
																											// LISTENER

																						/******
																						 * ********************************ADMIN
																						 * DATACENTER
																						 * FUNCTIONS**********************************
																						 *****/
																						// DATACENTER
																						// TAB
																						// CLICK
																						// EVENT
																						// LISTENER
																						$(
																								'#admin_DC_tab')
																								.click(
																										function() {

																											$
																													.ajax({
																														type : 'GET',
																														url : 'pages/admin/dc_sub_tabs.php',
																														beforeSend : function() {
																															$(
																																	'#loading_div')
																																	.toggleClass(
																																			"hidden");
																														},
																														success : function(
																																data) {
																															$(
																																	'#content_sub_tabs')
																																	.html(
																																			data);
																															$
																																	.ajax({
																																		type : 'GET',
																																		url : 'pages/admin/add_dc.php',
																																		success : function(
																																				data) {
																																			$(
																																					'#content')
																																					.html(
																																							data);
																																			$(
																																					window)
																																					.trigger(
																																							'resize');
																																			$(
																																					'#loading_div')
																																					.toggleClass(
																																							"hidden");
																																		}
																																	});

																															// ADD
																															// DC
																															// SUB
																															// MENU
																															// CLICK
																															$(
																																	'#add_sub')
																																	.click(
																																			function() {
																																				$
																																						.ajax({
																																							type : 'GET',
																																							url : 'pages/admin/add_dc.php',
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									data) {
																																								$(
																																										'#content')
																																										.html(
																																												data);
																																								$(
																																										window)
																																										.trigger(
																																												'resize');
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							}
																																						});
																																			});// END
																																				// ADD
																																				// DC
																																				// SUB
																																				// MENU
																																				// CLICK

																															// EDIT
																															// DC
																															// SUB
																															// MENU
																															// CLICK
																															$(
																																	'#edit_sub')
																																	.click(
																																			function() {
																																				$
																																						.ajax({
																																							type : 'GET',
																																							url : 'pages/admin/edit_dc.php',
																																							beforeSend : function() {
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");
																																							},
																																							success : function(
																																									data) {
																																								$(
																																										'#content')
																																										.html(
																																												data);
																																								$(
																																										window)
																																										.trigger(
																																												'resize');
																																								$(
																																										'#loading_div')
																																										.toggleClass(
																																												"hidden");

																																								$(
																																										'#dc_DD')
																																										.change(
																																												function() {
																																													var dc_sel = $(
																																															'#dc_DD')
																																															.val();

																																													$
																																															.ajax({
																																																type : 'GET',
																																																url : 'pages/admin/edit_dc.php',
																																																data : {
																																																	dc_details : dc_sel
																																																},
																																																beforeSend : function() {
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");
																																																},
																																																success : function(
																																																		html) {
																																																	$(
																																																			'#user_info')
																																																			.html(
																																																					html);
																																																	$(
																																																			window)
																																																			.trigger(
																																																					'resize');
																																																	$(
																																																			'#loading_div')
																																																			.toggleClass(
																																																					"hidden");

																																																	$(
																																																			'#apply_btn')
																																																			.click(
																																																					function() {
																																																						$
																																																								.ajax({
																																																									type : 'POST',
																																																									url : 'pages/admin/edit_dc.php',
																																																									data : $(
																																																											'#edit_form')
																																																											.serialize(),
																																																									beforeSend : function() {
																																																										$(
																																																												'#loading_div')
																																																												.toggleClass(
																																																														"hidden");
																																																									},
																																																									success : function() {
																																																										$(
																																																												'#loading_div')
																																																												.toggleClass(
																																																														"hidden");
																																																										alert('UPDATE SUCCESS');
																																																										$(
																																																												'#edit_sub')
																																																												.click(); // re-direct
																																																															// to
																																																															// [DC
																																																															// /
																																																															// EDIT]
																																																									}// END
																																																										// APPLY
																																																										// BUTTON
																																																										// SUCCESS
																																																								});// END
																																																									// POST
																																																									// AJAX
																																																									// EDIT_DC.PHP
																																																					});// END
																																																						// APPLY
																																																						// BUTTON
																																																						// EVENT
																																																						// LISTENER
																																																}// END
																																																	// GET
																																																	// EDIT_DC
																																																	// SUCCESS
																																																	// W/SELECTED
																																																	// DC
																																															});// END
																																																// GET
																																																// AJAX
																																																// EDIT_DC
																																												});// END
																																													// DC_DD
																																													// CHANGE
																																													// EVENT
																																													// LISTENER

																																							}// END
																																								// GET
																																								// AJAX
																																								// EDIT_DC
																																								// SUCCESS
																																						});// END
																																							// GET
																																							// AJAX
																																							// EDIT_DC
																																			});// END
																																				// DC
																																				// USER
																																				// SUB
																																				// MENU
																																				// CLICK

																														}// END
																															// SUCCESS
																													});// END
																														// AJAX
																										});// END
																											// DATACENTER
																											// TAB
																											// CLICK
																											// EVENT
																											// LISTENER
																					}
																				});
																	}// END
																		// SUCCESS
																});// END AJAX
													}// END SUCCESS
												});// END AJAX
									});// END ADMIN SETTINGS BUTTON PRESS

					/***********************************************************
					 * DNS INFO BUTTON PRESS
					 **********************************************************/

					/***********************************************************
					 * SUB NAV SHOW/HIDE
					 **********************************************************/
					$(".nav_dir").hover(function() {
						$(this).find(".nav_sub").css("display", "block");
					}, function() {
						$(this).find(".nav_sub").css("display", "none");
					});

					/***********************************************************
					 * NAV DROPDOWN ON CHANGE - UPDATE/CLEAR OTHER NAV DROPDOWN
					 **********************************************************/

					/***********************************************************
					 * VENDORS ON CHANGE - UPDATE DATACENTER DROPDOWN
					 **********************************************************/
					$('#vendors')
							.change(
									function() {
										var _selected = $(this).val();
										if (_selected != "") {
											$
													.ajax({
														type : 'GET',
														url : "pages/vendor_info/"
																+ _selected
																+ ".php",
														success : function(
																result) {
															$(
																	'#content_wrapper')
																	.html(
																			result);
															$(window).trigger(
																	'resize');
														}
													});
										} else {
											$
													.ajax({
														type : 'GET',
														url : "pages/vendor_info/cost_compare.php",
														success : function(
																result) {
															$(
																	'#content_wrapper')
																	.html(
																			result);
															$(window).trigger(
																	'resize');
														}
													});
										}

										$('#content_wrapper').html();
										$
												.ajax({
													type : 'GET',
													url : 'ajax/ajax_nav.php',
													data : {
														vendors : _selected
													}
												})
												.done(
														function(result) {
															$('#dcs_DD').html(
																	result);
															/*******************
															 * DATACENTER ON
															 * CHANGE - load
															 * 'content_menu.php'
															 ******************/
															$('#dcs_sel')
																	.change(
																			function() {
																				var _selected = $(
																						this)
																						.val();
																				$
																						.ajax(
																								{
																									type : 'GET',
																									dataType : 'json',
																									url : 'ajax/ajax_nav.php',
																									data : {
																										dcs : _selected
																									}
																								})
																						.done(
																								function(
																										data) {
																									$(
																											'#content_wrapper')
																											.html(
																													data.html_string);
																									/******
																									 * CONTENT
																									 * MENU
																									 * TAB
																									 * CLICK
																									 * FUNCTIONS
																									 *****/

																									$
																											.ajax(
																													{ // load
																														// sub_tabs
																														// html
																														// into
																														// content_sub_tabs
																														type : 'GET',
																														url : 'pages/datacenter/sub_tabs.php' // by
																																								// default
																													})
																											.done(
																													function(
																															result) {
																														$(
																																'#content_sub_tabs')
																																.html(
																																		result);// load
																																				// sub_tabs
																																				// for
																																				// default
																																				// [servers]
																																				// tab
																														$
																																.ajax({
																																	type : 'GET',
																																	data : {
																																		dcs : _selected
																																	},
																																	url : 'pages/datacenter/servers_display.php',
																																	beforeSend : function() {
																																		$(
																																				'#loading_div')
																																				.toggleClass(
																																						"hidden");
																																	},
																																	success : function(
																																			data) {
																																		$(
																																				'#content')
																																				.html(
																																						data);
																																		$(
																																				'#loading_div')
																																				.toggleClass(
																																						"hidden");
																																		$(
																																				window)
																																				.trigger(
																																						'resize');
																																	}
																																});

																														/******
																														 * ***********************************DEFAULT
																														 * FUNCTIONS****************************************
																														 *****/

																														// DISPLAY
																														// SUB-TAB
																														// CLICKED
																														$(
																																'#display_sub')
																																.click(
																																		function() {
																																			var _selected = $(
																																					'#dcs_sel')
																																					.val();

																																			$
																																					.ajax({
																																						type : 'GET',
																																						data : {
																																							dcs : _selected
																																						},
																																						url : 'pages/datacenter/servers_display.php',
																																						beforeSend : function() {
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																						},
																																						success : function(
																																								data) {
																																							$(
																																									'#content')
																																									.html(
																																											data);
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																							$(
																																									window)
																																									.trigger(
																																											'resize');
																																						}
																																					});
																																		});

																														// CREATE
																														// SUB-TAB
																														// CLICKED
																														$(
																																'#create_sub')
																																.click(
																																		function() {
																																			$
																																					.ajax({
																																						type : 'GET',
																																						url : 'pages/datacenter/servers_create.php',
																																						data : {
																																							dcs : $(
																																									'#dcs_sel')
																																									.val()
																																						},
																																						beforeSend : function() {
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																						},
																																						success : function(
																																								data) {
																																							$(
																																									'#content')
																																									.html(
																																											data);
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																							$(
																																									window)
																																									.trigger(
																																											'resize');

																																							// CREATE
																																							// BUTTON
																																							// CLICK
																																							$(
																																									'#create_button')
																																									.click(
																																											function() {
																																												$
																																														.ajax({
																																															type : "POST",
																																															async : true,
																																															url : 'pages/datacenter/servers_create.php',
																																															data : {
																																																dcs : $(
																																																		'#dcs_sel')
																																																		.val(),
																																																images : $(
																																																		'#images')
																																																		.val(),
																																																flavor : $(
																																																		'#flavor')
																																																		.val(),
																																																name : $(
																																																		'#server_name_txt')
																																																		.val()
																																															},
																																															beforeSend : function() {
																																																$(
																																																		'#loading_div')
																																																		.toggleClass(
																																																				"hidden");
																																															},
																																															success : function() {
																																																$(
																																																		'#loading_div')
																																																		.toggleClass(
																																																				"hidden");
																																																$(
																																																		'#display_sub')
																																																		.click();
																																															}
																																														}); // END
																																															// $.ajax
																																											});// END
																																												// CREATE
																																												// BUTTON
																																												// CLICK
																																						}
																																					});
																																		});

																														// RESIZE
																														// SUB-TAB
																														// CLICKED
																														$(
																																'#resize_sub')
																																.click(
																																		function() {
																																			$
																																					.ajax({
																																						type : 'GET',
																																						url : 'pages/datacenter/servers_resize.php',
																																						data : {
																																							dcs : $(
																																									'#dcs_sel')
																																									.val()
																																						},
																																						beforeSend : function() {
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																						},
																																						success : function(
																																								data) {
																																							$(
																																									'#content')
																																									.html(
																																											data);
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																							$(
																																									window)
																																									.trigger(
																																											'resize');

																																							// RESIZE
																																							// BUTTON
																																							// CLICK
																																							$(
																																									'#resize_button')
																																									.click(
																																											function() {
																																												$
																																														.ajax({
																																															type : "POST",
																																															async : true,
																																															url : 'pages/datacenter/servers_resize.php',
																																															data : {
																																																dcs : $(
																																																		'#dcs_sel')
																																																		.val(),
																																																svr : $(
																																																		'#server_DD')
																																																		.val(),
																																																flavor : $(
																																																		'#flavor_DD')
																																																		.val()
																																															},
																																															beforeSend : function() {
																																																$(
																																																		'#loading_div')
																																																		.toggleClass(
																																																				"hidden");
																																															},
																																															success : function() {
																																																$(
																																																		'#loading_div')
																																																		.toggleClass(
																																																				"hidden");
																																																$(
																																																		'#display_sub')
																																																		.click();
																																															}
																																														}); // END
																																															// $.ajax
																																											}); // END
																																												// RESIZE
																																												// BUTTON
																																												// CLICK

																																						}// END
																																							// RESIZE
																																							// SUB-TAB
																																							// CLICKED
																																							// SUCCESS
																																					});
																																		});

																														// DELETE
																														// SUB-TAB
																														// CLICKED
																														$(
																																'#delete_sub')
																																.click(
																																		function() {
																																			$
																																					.ajax({
																																						type : 'GET',
																																						url : 'pages/datacenter/servers_delete.php',
																																						data : {
																																							dcs : $(
																																									'#dcs_sel')
																																									.val()
																																						},
																																						beforeSend : function() {
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																						},
																																						success : function(
																																								data) {
																																							$(
																																									'#content')
																																									.html(
																																											data);
																																							$(
																																									'#loading_div')
																																									.toggleClass(
																																											"hidden");
																																							$(
																																									window)
																																									.trigger(
																																											'resize');
																																							/******
																																							 * DELETE
																																							 * SERVERS
																																							 * FUNCTIONS
																																							 * GO
																																							 * HERE
																																							 *****/

																																							// ELIMANATE
																																							// THE
																																							// NORMAL
																																							// FORM
																																							// OPERATION
																																							// SO
																																							// IT
																																							// DOESN'T
																																							// SEND
																																							// TWICE
																																							$(
																																									"#delete_server_form")
																																									.unbind(
																																											'submit');

																																							// DELETE
																																							// ONCLICK
																																							// EVENT
																																							// HANDLER
																																							$(
																																									"#delete_button")
																																									.click(
																																											function(
																																													event) {
																																												/*
																																												 * stop
																																												 * form
																																												 * from
																																												 * submitting
																																												 * normally
																																												 */
																																												event
																																														.preventDefault();
																																												$(
																																														'#captcha_refresh')
																																														.click();

																																												$
																																														.ajax({
																																															type : 'POST',
																																															data : {
																																																dcs : $(
																																																		'#dcs_sel')
																																																		.val(),
																																																server : $(
																																																		'#server_DD')
																																																		.val(),
																																																captcha : $(
																																																		'input[name="captcha_code"]')
																																																		.val()
																																															},
																																															url : 'pages/datacenter/servers_delete.php',
																																															beforeSend : function() {
																																																$(
																																																		'#loading_div')
																																																		.toggleClass(
																																																				"hidden");
																																															},
																																															success : function() {
																																																$(
																																																		'#loading_div')
																																																		.toggleClass(
																																																				"hidden");
																																																$(
																																																		'#display_sub')
																																																		.click();
																																															},
																																															complete : function(
																																																	data) {
																																																$(
																																																		'#validation')
																																																		.html(
																																																				data.responseText);
																																															}// end
																																																// ajax:success
																																																// function
																																														});
																																											});// END
																																												// DELETE
																																												// BUTTON
																																												// CLICK

																																						}// end
																																							// SUB
																																							// TAB
																																							// CLICK
																																							// success
																																					});// END
																																						// ajax
																																		});// END
																																			// DELETE
																																			// SUB
																																			// TAB
																																			// CLICKED

																													});// end
																														// done
																														// DEFAULT
																														// LOAD
																														// [SERVER]
																														// sub_tabs
																														// functions
																														// and
																														// click
																														// listeners

																									// SERVER_TAB
																									// CLICK
																									$(
																											'#server_tab')
																											.click(
																													function() {
																														$
																																.ajax({
																																	type : 'GET',
																																	url : 'pages/datacenter/sub_tabs.php',
																																	beforeSend : function() {
																																		$(
																																				'#loading_div')
																																				.toggleClass(
																																						"hidden");
																																	},
																																	success : function(
																																			data) {
																																		$(
																																				'#content_sub_tabs')
																																				.html(
																																						data);
																																		$
																																				.ajax({
																																					type : 'GET',
																																					data : {
																																						dcs : _selected
																																					},
																																					url : 'pages/datacenter/servers_display.php',
																																					success : function(
																																							data) {
																																						$(
																																								'#content')
																																								.html(
																																										data);
																																						$(
																																								'#loading_div')
																																								.toggleClass(
																																										"hidden");
																																						$(
																																								window)
																																								.trigger(
																																										'resize');
																																					}
																																				});

																																		/******
																																		 * **********************************SERVER
																																		 * FUNCTIONS******************************************
																																		 *****/

																																		/******
																																		 * [SERVER][DISPLAY]
																																		 * CLICK
																																		 * LOAD
																																		 * FUNCTIONS
																																		 * GO
																																		 * HERE
																																		 *****/
																																		// DISPLAY
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#display_sub')
																																				.click(
																																						function() {
																																							var _selected = $(
																																									'#dcs_sel')
																																									.val();

																																							$
																																									.ajax({
																																										type : 'GET',
																																										data : {
																																											dcs : _selected
																																										},
																																										url : 'pages/datacenter/servers_display.php',
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													window)
																																													.trigger(
																																															'resize');
																																										}
																																									});
																																						});

																																		// CREATE
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#create_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/datacenter/servers_create.php',
																																										data : {
																																											dcs : $(
																																													'#dcs_sel')
																																													.val()
																																										},
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													window)
																																													.trigger(
																																															'resize');

																																											// CREATE
																																											// BUTTON
																																											// CLICK
																																											$(
																																													'#create_button')
																																													.click(
																																															function() {
																																																$
																																																		.ajax({
																																																			type : "POST",
																																																			async : true,
																																																			url : 'pages/datacenter/servers_create.php',
																																																			data : {
																																																				dcs : $(
																																																						'#dcs_sel')
																																																						.val(),
																																																				images : $(
																																																						'#images')
																																																						.val(),
																																																				flavor : $(
																																																						'#flavor')
																																																						.val(),
																																																				name : $(
																																																						'#server_name_txt')
																																																						.val()
																																																			},
																																																			beforeSend : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																			},
																																																			success : function(
																																																					data) {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																				$(
																																																						'#display_sub')
																																																						.click();
																																																			}
																																																		}); // END
																																																			// $.ajax
																																															});// END
																																																// CREATE
																																																// BUTTON
																																																// CLICK
																																										}
																																									});
																																						});

																																		// RESIZE
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#resize_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/datacenter/servers_resize.php',
																																										data : {
																																											dcs : $(
																																													'#dcs_sel')
																																													.val()
																																										},
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													window)
																																													.trigger(
																																															'resize');

																																											// RESIZE
																																											// BUTTON
																																											// CLICK
																																											$(
																																													'#resize_button')
																																													.click(
																																															function() {
																																																$
																																																		.ajax({
																																																			type : "POST",
																																																			async : true,
																																																			url : 'pages/datacenter/servers_resize.php',
																																																			data : {
																																																				dcs : $(
																																																						'#dcs_sel')
																																																						.val(),
																																																				svr : $(
																																																						'#server_DD')
																																																						.val(),
																																																				flavor : $(
																																																						'#flavor_DD')
																																																						.val()
																																																			},
																																																			beforeSend : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																			},
																																																			success : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																				$(
																																																						'#display_sub')
																																																						.click();
																																																			}
																																																		}); // END
																																																			// $.ajax
																																															}); // END
																																																// RESIZE
																																																// BUTTON
																																																// CLICK

																																										}// END
																																											// RESIZE
																																											// SUB-TAB
																																											// CLICKED
																																											// SUCCESS
																																									});
																																						});

																																		// DELETE
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#delete_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/datacenter/servers_delete.php',
																																										data : {
																																											dcs : $(
																																													'#dcs_sel')
																																													.val()
																																										},
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													window)
																																													.trigger(
																																															'resize');
																																											/******
																																											 * DELETE
																																											 * SERVERS
																																											 * FUNCTIONS
																																											 * GO
																																											 * HERE
																																											 *****/

																																											// ELIMANATE
																																											// THE
																																											// NORMAL
																																											// FORM
																																											// OPERATION
																																											// SO
																																											// IT
																																											// DOESN'T
																																											// SEND
																																											// TWICE
																																											$(
																																													"#delete_server_form")
																																													.unbind(
																																															'submit');

																																											// DELETE
																																											// ONCLICK
																																											// EVENT
																																											// HANDLER
																																											$(
																																													"#delete_button")
																																													.click(
																																															function(
																																																	event) {
																																																/*
																																																 * stop
																																																 * form
																																																 * from
																																																 * submitting
																																																 * normally
																																																 */
																																																event
																																																		.preventDefault();
																																																$(
																																																		'#captcha_refresh')
																																																		.click();

																																																$
																																																		.ajax({
																																																			type : 'POST',
																																																			data : {
																																																				dcs : $(
																																																						'#dcs_sel')
																																																						.val(),
																																																				server : $(
																																																						'#server_DD')
																																																						.val(),
																																																				captcha : $(
																																																						'input[name="captcha_code"]')
																																																						.val()
																																																			},
																																																			url : 'pages/datacenter/servers_delete.php',
																																																			beforeSend : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																			},
																																																			success : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																				$(
																																																						'#validation')
																																																						.html(
																																																								data.responseText);
																																																				$(
																																																						'#display_sub')
																																																						.click();
																																																			}// end
																																																				// ajax:success
																																																				// function
																																																		});
																																															});// END
																																																// DELETE
																																																// BUTTON
																																																// CLICK

																																										}// end
																																											// SUB
																																											// TAB
																																											// CLICK
																																											// success
																																									});// END
																																										// ajax
																																						});// END
																																							// DELETE
																																							// SUB
																																							// TAB
																																							// CLICKED

																																	} // end
																																		// success
																																});// end
																																	// ajax
																													});// end
																														// server
																														// tab
																														// click
																														// event

																									// IMAGES_TAB
																									// CLICK
																									$(
																											'#image_tab')
																											.click(
																													function() {
																														$
																																.ajax({
																																	type : 'GET',
																																	url : 'pages/images/sub_tabs.php',
																																	data : {
																																		dcs : $(
																																				'#dcs_sel')
																																				.val()
																																	},
																																	beforeSend : function() {
																																		$(
																																				'#loading_div')
																																				.toggleClass(
																																						"hidden");
																																	},
																																	success : function(
																																			data) {
																																		$(
																																				'#content_sub_tabs')
																																				.html(
																																						data);

																																		$
																																				.ajax({ // load
																																						// images_display.php
																																						// by
																																						// default
																																					type : 'GET',
																																					url : 'pages/images/images_display.php',
																																					data : {
																																						dcs : $(
																																								'#dcs_sel')
																																								.val()
																																					},
																																					success : function(
																																							data) {
																																						$(
																																								'#content')
																																								.html(
																																										data);
																																						$(
																																								'#loading_div')
																																								.toggleClass(
																																										"hidden");
																																						$(
																																								window)
																																								.trigger(
																																										'resize');
																																					}// END
																																						// SUCCESS
																																						// CALLBACK
																																				});// END
																																					// AJAX

																																		/******
																																		 * ***********************************IMAGE
																																		 * FUNCTIONS******************************************
																																		 *****/
																																		// DISPLAY
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#display_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/images/images_display.php',
																																										data : {
																																											dcs : $(
																																													'#dcs_sel')
																																													.val()
																																										},
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													window)
																																													.trigger(
																																															'resize');
																																										}// END
																																											// SUCCESS
																																											// CALLBACK
																																									});// END
																																										// AJAX
																																						});// END
																																							// DISPLAY
																																							// SUB-TAB
																																							// CLICK

																																		// CREATE
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#create_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/images/images_create.php',
																																										data : {
																																											dcs : $(
																																													'#dcs_sel')
																																													.val()
																																										},
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													window)
																																													.trigger(
																																															'resize');

																																											// CREATE
																																											// BUTTON
																																											// CLICK
																																											$(
																																													'#create_button')
																																													.click(
																																															function() {
																																																$
																																																		.ajax({
																																																			type : "POST",
																																																			async : true,
																																																			url : 'pages/images/images_create.php',
																																																			data : {
																																																				dcs : $(
																																																						'#dcs_sel')
																																																						.val(),
																																																				server : $(
																																																						'#server_DD')
																																																						.val(),
																																																				name : $(
																																																						'#image_name_txt')
																																																						.val()
																																																			},
																																																			beforeSend : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																			},
																																																			success : function(
																																																					data) {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																				$(
																																																						'#display_sub')
																																																						.click();
																																																			}
																																																		}); // END
																																																			// $.ajax
																																															});// END
																																																// CREATE
																																																// BUTTON
																																																// CLICK
																																										}// END
																																											// SUCCESS
																																											// CALLBACK
																																									});// END
																																										// AJAX
																																						});// END
																																							// CREATE
																																							// SUB
																																							// TAB
																																							// CLICK

																																		// DELETE
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#delete_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/images/images_delete.php',
																																										data : {
																																											dcs : $(
																																													'#dcs_sel')
																																													.val()
																																										},
																																										beforeSend : function() {
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																										},
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#loading_div')
																																													.toggleClass(
																																															"hidden");
																																											$(
																																													window)
																																													.trigger(
																																															'resize');
																																											/******
																																											 * DELETE
																																											 * SERVERS
																																											 * FUNCTIONS
																																											 * GO
																																											 * HERE
																																											 *****/

																																											// ELIMANATE
																																											// THE
																																											// NORMAL
																																											// FORM
																																											// OPERATION
																																											// SO
																																											// IT
																																											// DOESN'T
																																											// SEND
																																											// TWICE
																																											$(
																																													"#delete_server_form")
																																													.unbind(
																																															'submit');

																																											// DELETE
																																											// ONCLICK
																																											// EVENT
																																											// HANDLER
																																											$(
																																													"#delete_button")
																																													.click(
																																															function(
																																																	event) {
																																																/*
																																																 * stop
																																																 * form
																																																 * from
																																																 * submitting
																																																 * normally
																																																 */
																																																event
																																																		.preventDefault();
																																																$(
																																																		'#captcha_refresh')
																																																		.click();

																																																$
																																																		.ajax({
																																																			type : 'POST',
																																																			data : {
																																																				dcs : $(
																																																						'#dcs_sel')
																																																						.val(),
																																																				image : $(
																																																						'#image_DD')
																																																						.val(),
																																																				captcha : $(
																																																						'input[name="captcha_code"]')
																																																						.val()
																																																			},
																																																			url : 'pages/images/images_delete.php',
																																																			beforeSend : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																			},
																																																			success : function() {
																																																				$(
																																																						'#loading_div')
																																																						.toggleClass(
																																																								"hidden");
																																																				$(
																																																						'#validation')
																																																						.html(
																																																								data.responseText);
																																																				$(
																																																						'#display_sub')
																																																						.click();
																																																			}// end
																																																				// ajax:success
																																																				// function
																																																		});
																																															});// END
																																																// DELETE
																																																// BUTTON
																																																// CLICK

																																										}// end
																																											// SUB
																																											// TAB
																																											// CLICK
																																											// success
																																									});// END
																																										// ajax
																																						});// END
																																							// DELETE
																																							// SUB
																																							// TAB
																																							// CLICKED

																																	} // end
																																		// success
																																});// end
																																	// ajax
																													});// end
																														// image
																														// tab
																														// click
																														// event

																									// ACCOUNTING_TAB
																									// CLICK
																									$(
																											'#accounting_tab')
																											.click(
																													function() {
																														$
																																.ajax({
																																	type : 'GET',
																																	url : 'pages/accounting/sub_tabs.php',
																																	success : function(
																																			data) {
																																		$(
																																				'#content_sub_tabs')
																																				.html(
																																						data);
																																		$(
																																				window)
																																				.trigger(
																																						'resize');
																																		/******
																																		 * ********************************ACCOUNTING
																																		 * FUNCTIONS****************************************
																																		 *****/
																																		// CHK_ALL
																																		// event
																																		// listener
																																		function chk_all() {
																																			// CHECK
																																			// ALL
																																			// CHECK-BOXES
																																			$(
																																					'#chk_all')
																																					.change(
																																							function() {
																																								var chk_all_check;
																																								$
																																										.each(
																																												$('input[type=checkbox]:not(:disabled)'),
																																												function(
																																														k,
																																														v) {
																																													if (v['name'] == "chk_all_box") {
																																														chk_all_check = v['checked'];
																																													} else {
																																														v['checked'] = chk_all_check;
																																														toggle_input(v['id']);
																																													}
																																												});
																																							});
																																		}// end
																																			// chk_all
																																			// function
																																			// definition

																																		// TOGGLE_INPUT
																																		// funciton
																																		// definition
																																		function toggle_input(
																																				id) {
																																			if (id != "chk_all") {
																																				var num = id
																																						.split('_');
																																				var txt_input = $('#rename_to_'
																																						+ num[1]);
																																				var label = $("#name_"
																																						+ num[1]);
																																				var chk_box = $('#'
																																						+ id);
																																				if (txt_input
																																						.attr('disabled')
																																						&& chk_box[0].checked == true) {
																																					txt_input
																																							.removeAttr('disabled');
																																					label
																																							.css(
																																									'text-decoration',
																																									'line-through');
																																					txt_input
																																							.css(
																																									'text-decoration',
																																									'none');
																																					txt_input
																																							.addClass('active');
																																				} else if (chk_box[0].checked == false) {
																																					txt_input.value = "";
																																					txt_input
																																							.attr(
																																									'disabled',
																																									'disabled');
																																					label
																																							.css(
																																									'text-decoration',
																																									'none');
																																					txt_input
																																							.css(
																																									'text-decoration',
																																									'line-through');
																																					txt_input
																																							.removeClass('active');
																																				}// end
																																					// else
																																			}// end
																																				// if($(this
																																		}// end
																																			// toggle_input
																																			// function
																																			// definition

																																		// REPORT
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#report_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/accounting/report_query.php',
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													window)
																																													.trigger(
																																															'resize');
																																										}
																																									});
																																						});

																																		// ADD
																																		// /
																																		// EDIT
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#add_edit_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/accounting/add_edit_labels.php',
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													window)
																																													.trigger(
																																															'resize');
																																											/******
																																											 * ACCOUNTING
																																											 * ADD/EDIT
																																											 * FUNCTIONS
																																											 * GO
																																											 * HERE
																																											 *****/

																																											// CHECK
																																											// ALL
																																											// CHECK-BOXES
																																											// LISTENER
																																											chk_all();
																																											$(
																																													'input[type=checkbox]:not(:disabled #chk_all)')
																																													.change(
																																															function() {
																																																toggle_input(this.id);
																																															});

																																											// WHEN
																																											// CHECKED
																																											// ENABLE
																																											// TEXT
																																											// INPUT

																																											// CREATE
																																											// NEW
																																											// LABEL
																																											// ROW
																																											$(
																																													'#add_row_button')
																																													.click(
																																															function() {
																																																$(
																																																		'tbody')
																																																		.append(
																																																				'<tr><td class="checkbox"><input class="active" type="checkbox" checked="true" disabled="disabled" ></td><td>New Label</td><td><input class="active" type="text" id="rename_to_" name="rename_to_"  /></td></tr>');
																																															});
																																										}// end
																																											// success
																																											// function
																																									});// end
																																										// add
																																										// /
																																										// edit
																																										// sub
																																										// tab
																																										// click
																																						});

																																		// APPLY
																																		// SUB-TAB
																																		// CLICKED
																																		$(
																																				'#apply_sub')
																																				.click(
																																						function() {
																																							$
																																									.ajax({
																																										type : 'GET',
																																										url : 'pages/accounting/apply_labels.php',
																																										success : function(
																																												data) {
																																											$(
																																													'#content')
																																													.html(
																																															data);
																																											$(
																																													'#lbl_svr_radio')
																																													.buttonset();
																																											$(
																																													window)
																																													.trigger(
																																															'resize');

																																											/******
																																											 * RADIO
																																											 * BUTTON
																																											 * SELECTED
																																											 *****/
																																											// LABEL
																																											// RADIO
																																											// BUTTON
																																											// CLICKED
																																											$(
																																													'#lbl_radio')
																																													.click(
																																															function() {
																																																$
																																																		.ajax({
																																																			type : 'GET',
																																																			url : 'pages/accounting/apply_labels.php',
																																																			data : {
																																																				clicked : 'label'
																																																			},
																																																			cache : false,
																																																			dataType : 'json',
																																																			success : function(
																																																					data) {
																																																				$(
																																																						'#apply_dd')
																																																						.html(
																																																								data.dd);
																																																				$(
																																																						'#dd_result')
																																																						.html(
																																																								'');
																																																				lbls_svrs_dd_handler('Servers');
																																																			}// end
																																																				// success
																																																				// function
																																																		}); // end
																																																			// ajax
																																																			// call
																																															}); // end
																																																// label
																																																// radio
																																																// button
																																																// click

																																											// SERVER
																																											// RADIO
																																											// BUTTON
																																											// CLICKED
																																											$(
																																													'#svr_radio')
																																													.click(
																																															function() {
																																																$
																																																		.ajax({
																																																			type : 'GET',
																																																			url : 'pages/accounting/apply_labels.php',
																																																			data : {
																																																				clicked : 'server'
																																																			},
																																																			cache : false,
																																																			dataType : 'json',
																																																			success : function(
																																																					data) {
																																																				$(
																																																						'#apply_dd')
																																																						.html(
																																																								data.dd);
																																																				$(
																																																						'#dd_result')
																																																						.html(
																																																								'');
																																																				lbls_svrs_dd_handler('Labels');
																																																			}// end
																																																				// success
																																																				// function
																																																		}); // end
																																																			// ajax
																																																			// call
																																															}); // end
																																																// server
																																																// radio
																																																// button
																																																// click
																																											/******
																																											 * DROP
																																											 * DOWN
																																											 * SELECTED
																																											 *****/
																																											// LABEL
																																											// DROP
																																											// DOWN
																																											// SELECTED
																																											function lbls_svrs_dd_handler(
																																													type) {
																																												$(
																																														'#dd')
																																														.change(
																																																function() {
																																																	$
																																																			.ajax({
																																																				type : 'GET',
																																																				url : 'pages/accounting/apply_labels.php',
																																																				data : {
																																																					clicked : 'dd',
																																																					radio : type,
																																																					sel : $(
																																																							"#dd")
																																																							.val()
																																																				},
																																																				cache : false,
																																																				dataType : 'json',
																																																				success : function(
																																																						data) {
																																																					$(
																																																							'#dd_result')
																																																							.html(
																																																									data.resulting_table);
																																																					chk_all();
																																																				}// end
																																																					// success
																																																					// function
																																																			}); // end
																																																				// ajax
																																																				// call
																																																}); // end
																																																	// change
																																																	// dd
																																																	// click
																																											}// end
																																												// set_lbls_svrs_dd_handler()
																																												// function
																																												// declaration
																																											lbls_svrs_dd_handler('Servers');
																																										}
																																									});
																																						});

																																	} // end
																																		// success
																																});// end
																																	// ajax
																																	// to
																																	// display
																																	// [accounting][default]
																													});// end
																														// accounting
																														// tab
																														// click
																														// event

																								});// end
																									// done()
																									// loading
																									// main
																									// tabs
																			});// end
																				// dcs.change
														});// end done()
									});// end vendors.change

				});// END DOCUMENT.READY

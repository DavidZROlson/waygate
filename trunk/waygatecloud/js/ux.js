// JavaScript Document
	$(document).ready(function(e) {
		
		/***********
			UX FUNCTIONS GO HERE
		***********/
		
			//RESIZE 'main_content' on window resize
			
				$(window).resize(function(e) {
					$('#main_container').height($(window).height() - 170);
					$('#content').height($('#main_container').height() -70);
				});
				
				
			//RE-SIZE CONTENT WINDOW on first load
				$(window).trigger('resize');
				
				//$('#menu').menu();
	});//END DOCUMENT.READY
<?php

abstract class mysql {
	const HOST = 'localhost';
	const USER = 'jack';
	const PASS = 'duke';
	const DB = 'waygatecloud';

	public function connectpas() {
		if (!$dbh = mysqli_connect(self::HOST, self::USER, self::PASS)) {
			return $this->error("DATABASEERROR",  mysqli_connect_error($dbh));
		}

		if (!mysqli_select_db($dbh, self::DB)) {
			return $this->error("DATABASEERROR", mysqli_error($dbh));
		}

		return $dbh;
	}

	// used to create temporary tables
	public function create($dbh, $query) {

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		return mysqli_affected_rows($dbh);
	}

	// used to delete row form table
	public function delete($dbh, $query) {

		if (!$this->stringContains($query, "DELETE FROM", false)) {
			return $this->error("BADARGUMENT", "Not a delete statement");
		}

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		return mysqli_affected_rows($dbh);
	}

	// used to drop table
	public function drop($dbh, $query) {

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		return mysqli_affected_rows($dbh);
	}

	// returns errors form the database
	private function error($errtype, $errdetail) {
		$datalist = array ();

		$strline = "ERR";
		$datalist[] = $strline;
		$strline = "2";
		$datalist[] = $strline;
		$strline = "$errtype";
		$datalist[] = $strline;
		$strline = "$errdetail";
		$datalist[] = $strline;

		return $datalist;
	}

	// used to insert row into table
	public function insert($dbh, $query) {

		if (!$this->stringContains($query, "INSERT INTO", false)) {
			return $this->error("BADARGUMENT", "Not an insert statement");
		}

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		return mysqli_insert_id($dbh);
	}

	public function misc($dbh, $query) {

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		return mysqli_affected_rows($dbh);
	}

	// used to select row(s) from a table
	public function select($dbh, $query) {

		if (!$this->stringContains($query, "SELECT", false)) {
			return $this->error("BADARGUMENT", "Not a select statement");
		}

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		$rc = mysqli_num_rows($sth);
		if ($rc == 0) {
			return $this->error("NO_RECORDS", "No records returned!");
		}

		$rc = ($rc === false ? 0 : $rc);

		$datalist = array ();
		while ($row = mysqli_fetch_assoc($sth)) {
			$datalist[] = $row;
		}
		return $datalist;

	}

	// finds a word in a string
	private function stringContains($str, $sub) {
		if (strpos($str, $sub) === false) {
			return false;
		}
		return true;
	}

	// used to update a row in a table
	public function update($dbh, $query) {

		if (!$this->stringContains($query, "UPDATE", false)) {
			return $this->error("BADARGUMENT", "Not an update statement");
		}

		if (!($sth = mysqli_query($dbh, $query))) {
			return $this->error("BADARGUMENT", mysqli_error($dbh));
		}

		return mysqli_affected_rows($dbh);
	}
}
?>

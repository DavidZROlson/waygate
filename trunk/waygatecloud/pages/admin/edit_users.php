<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	$objUser = new User();
	$data = $objUser->getusers();

	/************************************
	 *  ACCESS API TO POPULATE D-DOWNS	*
	************************************/

	//$users_in_DB = array('Jim', 'Bob', 'Suzy', 'Glen', 'Ruth'); // populated by DB QUERY


	//BUILD DROP DOWN LIST OF USERS
	$user_DD_list = array('<option value="">[User...]</option>');
	$n = 0;
	foreach($data as $index => $temp){
		array_push($user_DD_list, '<option value="' . $n . '">' . $temp['username'] . '</option>');
		$n++;
	}

	//IF A USER IS SELECTED
	if(isset($_GET['user'])){
		/********************************************
		 *		QUERY DB FOR USER'S PRIVLIGES		*
		********************************************/

		//LOAD THIS HTML ONLY IF A $_GET['user'] IS SELECTED
		?>
<fieldset>
	<legend>Edit User's Credentials</legend>
	<label for="username">Username:</label>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-user"></i> </span> <input
			id="username" name="username" type="text" value=<?=$data[$_GET['user']]['username'] ?> placeholder="Username">
	</div>
	<label for="password">Password:</label>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-key"></i> </span> <input
			id="password" name="password" type="password" value="<?=$data[$_GET['user']]['password'] ?>" placeholder="Password">
	</div>
</fieldset>

<input
	id="apply_btn" value="Apply Changes" type="button" class="button">
<?php 

exit();
	} // END if USER is Selected
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

echo ("Selected User: " . $_POST['users_DD'] . "\n\r");
echo ("Chosen Username: " . $_POST['username'] . "\n\r");
echo ("Chosen Password :" . $_POST['password']);
	/***************************************************
	 ****************************************************
	****************************************************
	THE $_POST[] WILL RESEMBLE THIS SET UP OF VALUES

	$_POST = array(
			[users_DD] => 2
			[username] =>
			[password] =>
			)

	MAKE YOUR CALLS HERE WITHIN THE
	IF($_SERVER['REQUEST_METHOD'] === 'POST')
	*****************************************************
	*****************************************************
	*****************************************************/



	exit();
}//END POST
?>

<form id="edit_form" name="edit_form">
	<fieldset>
		<legend>Select a User</legend>
		<select name="users_DD" id="users_DD">
			<?php

			/****************************************
			 *	POPULATE THE SELECT LIST OPTIONS	*
			****************************************/

			if(count($user_DD_list) > 0){
                foreach($user_DD_list as $key => $value){
                echo $value;
                }
            }else{
                echo '<option value="">No Users</option>';
            }
            ?>
		</select>
	</fieldset>
	<div id="user_info"></div>
</form>

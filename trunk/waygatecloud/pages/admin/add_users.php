<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$objUser = new User();

	$username = $_POST['username'];
	$password = $_POST['password'];

	$arguments = array (
			'username' => $username,
			'password' => $password,
	);

	$data = $objUser->adduserinfo($arguments);
	if ($data != "Susscess") {
		echo($data);
	}

	exit();
}//END POST

?>

<form id="add_form" name="add_form">
	<fieldset>
		<legend>Add a New User's Credentials</legend>
		<label for="username">Username:</label>
		<div class="input-prepend">
			<span class="add-on"><i class="icon-user"></i> </span> <input
				id="username" name="username" type="text" placeholder="Username">
		</div>
		<label for="password">Password:</label>
		<div class="input-prepend">
			<span class="add-on"><i class="icon-key"></i> </span> <input
				id="password" name="password" type="password" placeholder="Password">
		</div>
	</fieldset>

	<input id="apply_btn" value="Create User" type="button" class="button">

</form>

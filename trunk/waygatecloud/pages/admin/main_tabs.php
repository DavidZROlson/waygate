<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

$idcustomers = $_SESSION['idcustomers'];
?>

<div id="content_menu">
	<ul>
		<li id="admin_USERS_tab">[Users]</li>
		<li id="admin_RIGHTS_tab">[Rights]</li>
		<? if($idcustomers == 1){?>
		<li id="admin_DC_tab">[Data Centers]</li>
		<? }?>
	</ul>
</div>
<!-- END CONTENT MENU -->
<div id="content_sub_tabs"></div>
<!-- END CONTENT_MENU -->

<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {


	/************************************
	 *  ACCESS API TO POPULATE D-DOWNS	*
	************************************/

	$users_in_DB = array('ORD', 'SYD', 'DFW'); // populated by DB QUERY


	//BUILD DROP DOWN LIST OF DCs
	$user_DD_list = array();
	foreach($users_in_DB as $key => $value){
		array_push($user_DD_list, '<option value="' . $key . '">' . $value . '</option>');
	}


	//IF A DC IS SELECTED
	if(isset($_GET['dc_details'])){
		/********************************************
		 *		QUERY DB FOR USER'S PRIVLIGES		*
		********************************************/


		//TESTING ARRAY[$dc][$index] = $value FOR DATACENTER PERMISSIONS
		$visable_dcs_in_DB = array( array(  'title' => 'ORD',
				'DC' => 'y',
				'Reports' => 'y',
				'Svr_Mgnt' => 'y'),
				array(  'title' => 'SYD',
						'DC' => 'n',
						'Reports' => 'n',
						'Svr_Mgnt' => 'y'),
				array(  'title' => 'DFW',
						'DC' => 'y',
						'Reports' => 'n',
						'Svr_Mgnt' => 'y')); // populated by DB QUERY

		$table_rows = array();

		//BUILD ROWS FOR TABLE OUTPUT
		$odd_even = 0;
		foreach($visable_dcs_in_DB as $key => $dc){
			// Start Row
			switch($odd_even % 2){
				case 0: array_push($table_rows, '<tr class="even">');
				break;
				case 1: array_push($table_rows, '<tr class="odd">');
				break;
				default: array_push($table_rows, '<tr>');
				break;
			}
				
			foreach($dc as $index => $value){
				//$dc may contains [title] [DC] [Reports] [Svr_Mgnt]
				// if $dc[] == y then create a row with a checked box, else, don't check the box
				if($index != 'title'){
					$value = ($value == 'y') ? '<td class="checkbox"><input name="' . $dc['title'] . '[' . $index . ']" type="checkbox" checked="checked"></td>' : '<td class="checkbox"><input name="' . $dc['title'] . '[' . $index . ']" type="checkbox"></td>';
					array_push($table_rows, $value);
				}else{
					// push the $dc[title] to the $table_rows array()
					array_push($table_rows, '<td>' . $value . '</td>');
				}
			}
			//end a row
			array_push($table_rows, '</tr>');
			$odd_even+=1;
		}

		//LOAD THIS HTML ONLY IF A $_GET['dc_details'] IS SELECTED
		?>
<fieldset>
	<legend>Edit User's Credentials</legend>
	<style type="text/css">
.usr_creds_chk {
	margin-right: 15px;
	background-color: #EEE; border 1px solid #CCC;
	border-radius: 4px;
	box-shadow: 0 1px 1px rgba(0, 0, 0, 0.75) inset;
	padding: 5px 5px 0px;
}
</style>
	<div>
		<span class="usr_creds_chk"> <label for="admin_chk">Admin</label> <input
			name="admin_chk" id="admin_chk" value="admin-chk" type="checkbox" />
		</span> <span class="usr_creds_chk"> <label for="dns_chk">DNS</label>
			<input name="dns_chk" id="dns_chk" value="dns_chk" type="checkbox" />
		</span>
	</div>
	<label for="username">Username:</label>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-user"></i> </span> <input
			id="username" name="username" type="text" placeholder="Username">
	</div>
	<label for="password">Password:</label>
	<div class="input-prepend">
		<span class="add-on"><i class="icon-key"></i> </span> <input
			id="password" name="password" type="password" placeholder="Password">
	</div>
</fieldset>
<fieldset>
	<legend>Permissions</legend>
	<table>
		<tr class="odd">
			<th>Data Center Name</th>
			<th>DC</th>
			<th>Reports</th>
			<th>Server Managment</th>
		</tr>

		<?php
		foreach($table_rows as $row)
			echo $row;
		?>
	</table>

</fieldset>
<input
	id="apply_btn" value="Apply Changes" type="button" class="button">
<?php 

exit();
	} // END if DC is Selected
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {


	/***************************************************
	 ****************************************************
	****************************************************
	THE $_POST[] WILL RESEMBLE THIS SET UP OF VALUES

	$_POST = array(
			[users_DD] => 2
			[username] =>
			[password] =>
			[ORD] => Array
			(
					[DC] => on
					[Reports] => on
					[Svr_Mgnt] => on
			)

			[SYD] => Array
			(
					[Svr_Mgnt] => on
			)

			[DFW] => Array
			(
					[DC] => on
					[Svr_Mgnt] => on
			)
			)

	MAKE YOUR CALLS HERE BELOW THE
	IF($_SERVER['REQUEST_METHOD'] === 'POST')
	*****************************************************
	*****************************************************
	*****************************************************/



	exit();
}//END POST
?>

<form id="edit_form" name="edit_form">
	<div>
		<label for="dc_DD">Select a Data Center:</label> <select name="dc_DD"
			id="dc_DD">
			<?php

			/****************************************
			 *	POPULATE THE SELECT LIST OPTIONS	*
			****************************************/

			if(count($user_DD_list) > 0){
                foreach($user_DD_list as $key => $value){
                echo $value;
                }
            }else{
                echo '<option value="">No Users</option>';
            }
            ?>
		</select>
	</div>
	<div id="user_info"></div>
</form>

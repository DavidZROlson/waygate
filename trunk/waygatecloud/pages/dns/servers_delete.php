<?php
//CHECK FOR GET REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
	//BEGIN CAPTCHA CHECKING
	session_start();
	include_once '../../securimage/securimage.php';
	
	$securimage = new Securimage();
	
	if ($securimage->check($_POST['captcha_code']) == false) {
	  // the code was incorrect
	  // you should handle the error so that the form processor doesn't continue
	  echo "The security code entered was incorrect.<br />";
	  echo 'Value of DropDown: '.($_POST['server_DD']);
	  exit;
	}else{
		//ON CAPTCHA SUCCESS
		//SEND DATA TO DELETE THE SELECTED
	  echo "Server is being marked as deleted<br />";
	  echo 'Value of DropDown: '.($_POST['server_DD']);
		exit;
	}
	//END CAPTCHA CHECKING
}
?>

<style type="text/css">
	form{
		margin: 0 auto;	
	}
	
	.label{
		text-align:right;
		padding-right: 10px;	
	}
</style>

<form id="delete_server_form">
	<fieldset>
    <legend>Delete an Existing Server</legend>
    <table style="display:inline-block;">
    	<tr>
            <td class="label">
                <label for="server_DD">Select a Server:</label>
            </td>
            <td>
                <select name="server_DD" id="server_DD">
                <!-- SET THE VALUE TO THE SERVER ID -->
                    <option value="1">Server 1</option>
                    <option value="2">Server 2</option>
                    <option value="3">Server 3</option>
                    <option value="4">Server 4</option>
                    <option value="5">Server 5</option>
                    <option value="6">Server 6</option>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="verify">Verify Intent:</label><br />
                <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" />
            </td>
            <td>
                    <input type="text" name="captcha_code" size="10" maxlength="6" />
                    <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><img alt="Refresh" src="securimage/images/refresh.png" width="20px" height="20px" /></a>
                
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;">
                <input id="delete_button" value="Delete Server" type="submit" />
            </td>
        </tr>
    </table>
    <div id="validation" style="display:inline-block; vertical-align:top;"></div>
    </fieldset>
</form>
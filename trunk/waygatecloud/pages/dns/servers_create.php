<style type="text/css">
	form{
		margin: 0 auto;	
	}
	
	.label{
		text-align:right;
		padding-right: 10px;	
	}
</style>

<form>
	<fieldset>
    <legend>Create a New Server</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="images_DD">Select an Image:</label>
            </td>
            <td>
                <select name="images_DD" id="images_DD">
                    <option value="1">Image 1</option>
                    <option value="2">Image 2</option>
                    <option value="3">Image 3</option>
                    <option value="4">Image 4</option>
                    <option value="5">Image 5</option>
                    <option value="6">Image 6</option>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="flavor_DD">Select a Flavor:</label>
            </td>
            <td>
                <select name="flavor_DD" id="flavor_DD">
                    <option value="0">512 MB</option>
                    <option value="1">1 GB</option>
                    <option value="2">2 GB</option>
                    <option value="4">4 GB</option>
                    <option value="8">8 GB</option>
                    <option value="15">15 GB</option>
                    <option value="30">30 Gb</option>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="server_name_txt">Server Name:</label>
            </td>
            <td>
                <input name="server_name_txt" id="server_name_txt" type="text" />*
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input id="create_button" value="Create" type="button" />
            </td>
        </tr>
    </table>
    </fieldset>
</form>
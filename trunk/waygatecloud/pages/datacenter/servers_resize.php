<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dc = $data[0]['dcname'];
		$apiusername = $data[0]['username'];
		$apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$rackspace = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dc);

		syncServer($cloudservers, $dc);

		$serv = $cloudservers->ServerList();
		$serv->Sort('name');

		$flavors = $cloudservers->FlavorList();
		$flavors->Sort('id');


	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	//IF GET REQUEST IS FROM 'dcs'
	if($_POST['dcs'] != ""){

		$DataCenter = $_POST['dcs'];
		$serverselect = $_POST['svr'];
		$flavorselect = $_POST['flavor'];

		echo '<p style="position: absolute; top: 50%; left: 50%;">';
		echo 'DataCenter: ' . $DataCenter . '<br />';
		echo 'Selected SVR: ' . $serverselect . '<br />';
		echo 'Selected Flavor: ' . $flavorselect . '</p>';

		list($flavorid, $ram) = explode("|", $flavorselect);
		$flavorId = (int)$flavorid;

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dcname = $data[0]['dcname'];
		$apiusername = $data[0]['username'];
		$apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];
			
		$ResizeServAuth = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $ResizeServAuth->Compute('cloudServersOpenStack', $dcname);

		$server =$cloudservers->Server($serverselect);
		$flavor = $cloudservers->Flavor($flavorid);
		$server->Resize($flavor);

		/*$serializeserver=serialize($server);
		 $_SESSION['server']=$serializeserver;
		$_SESSION['terminal'] = 'VERIFY_RESIZE';
		$_SESSION['timeout'] = 600;
		?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
		id="imgProgress" />
		<?php
		echo "<script type='text/javascript'>WaitForResize();</script>\n";*/

		$arguments = array ('dcname' => $dcname,
				'username' => $username,
				'instance_id' => $serverselect,
				'ram' => $ram,);
		$data = $objDcs->resizserver($arguments);


	}
	exit();
}

?>

<form>
	<fieldset>
		<legend>Resize an Existing Server</legend>
		<table>
			<tr>
				<td class="label"><label for="server_DD">Select a Server:</label>
				</td>
				<td><select name="server_DD" id="server_DD">
						<?php 
						while($servers = $serv->Next()) {
						$Id = $servers->id;
						$data = $objDcs->getCustomersServers();
						$temp = $data[0];
						if ($temp != "ERR") {
							foreach ($data as $temp) {
								if ($Id == $temp['instance_id']) {
									echo "<option value=".$servers->id.">" . $servers->name . "</option>";
								}
							}
						}
					}
					?>
				</select>*</td>
			</tr>
			<tr>
				<td class="label"><label for="flavor_DD">Select a Flavor:</label>
				</td>
				<td><select name="flavor_DD" id="flavor_DD">
						<?
						while($flavor = $flavors->Next()) {
						$item = $flavor->id."|".$flavor->ram;
						echo "<option value=".$item.">" . $flavor->name . "</option>";
					}
					?>
				</select>*</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><input
					id="resize_button" value="Resize" type="button" />
				</td>
			</tr>
		</table>
	</fieldset>
</form>

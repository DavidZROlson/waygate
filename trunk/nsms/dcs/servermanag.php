<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1s.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2s.php');
require_once('../includes/left_menu.php');
require_once '../lib/php-opencloud.php';
require_once('../common/common.php');
?>

<div class="right_content">
	<script type="text/javascript">
	function WaitForProgress()
	{
		$("#imgProgress").show();
		$.get("waitfor.php", function(data, textStatus)
		{
			$("#imgProgress").hide();
		});
	}
</script>
	<script type="text/javascript">
	function WaitForResize()
	{
		$("#imgProgress").show();
		$.get("waitforresize.php", function(data, textStatus)
		{
			$("#imgProgress").hide();
		});
	}
</script>
	<div class="form">
		<?
		$dc = $_GET['dc'];
		$option = $_GET['option'];
		session_start();

		$title = "";
		if ($dc != "") {
			$title = ucwords($dc). ":";
		}

		if ($option == "createserver") {
			$title = $title. " CREATE SERVER";
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dc);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoCreateAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoCreateAuth->Compute('cloudServersOpenStack', $dc);

			syncServer($cloudservers, $dc);

			$serv = $cloudservers->ServerList();
			$serv->Sort('name');

			$flavors = $cloudservers->FlavorList();
			$flavors->Sort('id');

			syncImage($cloudservers, $dc);
			
			$images = $cloudservers->ImageList();
			$images->Sort('name');
		} else if ($option == "deleteserver") { // get info to delete a server
			$title = $title. " DELETE SERVER";
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dc);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoDeleteAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoDeleteAuth->Compute('cloudServersOpenStack', $dc);

			syncServer($cloudservers, $dc);

			$serv = $cloudservers->ServerList();
			$serv->Sort('name');
		} else if ($option == "resizeserver") { // get info to resize a server
			$title = $title. " RESIZE SERVER";
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dc);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoResizeAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoResizeAuth->Compute('cloudServersOpenStack', $dc);

			syncServer($cloudservers, $dc);

			$serv = $cloudservers->ServerList();
			$serv->Sort('name');

			$flavors = $cloudservers->FlavorList();
			$flavors->Sort('id');
		} else if ($option == "createimage") { // get info to create a image
			$title = $title. " CREATE IMAGE";
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dc);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dc);

			syncServer($cloudservers, $dc);

			$serv = $cloudservers->ServerList();
			$serv->Sort('name');
		} else if ($option == "deleteimage") { // get info to delete image
			$title = $title. " DELETE IMAGE";
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dc);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoImDeAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoImDeAuth->Compute('cloudServersOpenStack', $dc);

			syncServer($cloudservers, $dc);

			$serv = $cloudservers->ServerList();
			$serv->Sort('name');

			syncImage($cloudservers, $dc);
			
			$images = $cloudservers->ImageList();
			$images->Sort('name');
		}

		if (isset ($_POST['createserv'])) { // create server
			$imageId = $_POST['imageselect'];
			$flavorselect = $_POST['flavorselect'];
			$servername = $_POST['severname'];

			// validate info
			if ($imageId == "Select a Image") {
				confirm("Select a Image");
			} elseif ($flavorselect == "Select a Flavor"){
				confirm($flavorselect);
			} elseif (empty($servername)){
				confirm("Please enter a Server Name");
			} else {
				$dcname = $_POST['dcname'];
				$username = $_SESSION['PSAdminNM'];
				list($flavorid, $ram) = explode("|", $flavorselect);
				$flavorId = (int)$flavorid;

				$objDcs = new Dcs();

				$arguments = array ('dcname' => $dcname);
				$data = $objDcs->getApiInfo($arguments);

				$apiusername = $data[0]['username'];
				$apikey= $data[0]['apikey'];
				$authurl = $data[0]['authurl'];
					
				$CreateServAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $CreateServAuth->Compute('cloudServersOpenStack', $dcname);

			$image = $cloudservers->Image($imageId);
			$flavor = $cloudservers->Flavor($flavorId);
			$server = $cloudservers->Server();
			$server->Create(array(
					'name'=>$servername,
					'image'=>$image,
					'flavor'=>$flavor
			));

			$serializeserver=serialize($server);
			$_SESSION['server']=$serializeserver;
			$_SESSION['terminal'] = 'ACTIVE';
			$_SESSION['timeout'] = 600;
			?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
			id="imgProgress" />
		<?php 
		echo "<script type='text/javascript'>WaitForProgress();</script>\n";

		if ($server->Status() == 'ERROR')
			die("Server create failed with ERROR\n");

		$rackspaceserver = $new-> {'server' };
		$idrackspace = $rackspaceserver-> {'id' };
		$addresses = $rackspaceserver-> {'addresses'};
		$public = $addresses-> {'public' }[0];

		$arguments = array ('dcname' => $dcname,
				'username' => $username,
				'servername' => $servername,
				'idrackspace' => $idrackspace,
				'ram' => $ram,
				'ip_address' => $public,);
			$data = $objDcs->adserver($arguments);
			}
		} elseif (isset ($_POST['deleteserv'])) { // delete server
		$serverselect = $_POST['serverselect'];

		// valid info
		if ($serverselect == "Select a Server") {
			confirm($serverselect);
		} else {
			$dcname = $_POST['dcname'];
			$username = $_SESSION['PSAdminNM'];
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dcname);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$DeleteServAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $DeleteServAuth->Compute('cloudServersOpenStack', $dcname);

			$server =$cloudservers->Server($serverselect);
			$server->Delete();

			$serializeserver=serialize($server);
			$_SESSION['server']=$serializeserver;
			$_SESSION['terminal'] = 'ACTIVE';
			$_SESSION['timeout'] = 600;
			?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
			id="imgProgress" />
		<?php 
		echo "<script type='text/javascript'>WaitForProgress();</script>\n";
			
		if ($server->Status() == 'ERROR')
			die("Server create failed with ERROR\n");

		$arguments = array ('dcname' => $dcname,
				'username' => $username,
				'idrackspace' => $serverselect);
			$data = $objDcs->removserver($arguments);
		}
	} elseif (isset ($_POST['resizeserv'])) { // resize server
		$serverselect = $_POST['serverselect'];
		$flavorselect = $_POST['flavorselect'];
		list($flavorid, $ram) = explode("|", $flavorselect);

		// validate info
		if ($serverselect == "Select a Server") {
			confirm($serverselect);
		} elseif ($flavorselect == "Select a Flavor"){
			confirm($flavorselect);
		} else {
			$dcname = $_POST['dcname'];
			$username = $_SESSION['PSAdminNM'];
			$flavorId = (int)$flavorid;
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dcname);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$ResizeServAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $ResizeServAuth->Compute('cloudServersOpenStack', $dcname);

			$server =$cloudservers->Server($serverselect);
			$flavor = $cloudservers->Flavor($flavorid);
			$server->Resize($flavor);

			$serializeserver=serialize($server);
			$_SESSION['server']=$serializeserver;
			$_SESSION['terminal'] = 'VERIFY_RESIZE';
			$_SESSION['timeout'] = 600;
			?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
			id="imgProgress" />
		<?php 
		echo "<script type='text/javascript'>WaitForResize();</script>\n";

		$arguments = array ('dcname' => $dcname,
				'username' => $username,
				'idrackspace' => $serverselect,
				'ram' => $ram,);
			$data = $objDcs->resizserver($arguments);
		}
	} elseif (isset ($_POST['createimag'])) { // create image
		$serverselect = $_POST['serverselect'];
		$imagename = $_POST['imagename'];

		// validate info
		if ($serverselect == "Select a Server") {
			confirm($serverselect);
		} elseif (empty($imagename)){
			confirm("Please enter a Image Name");
		} else {
			$dcname = $_POST['dcname'];
			$username = $_SESSION['PSAdminNM'];
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dcname);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dcname);

			$server = $cloudservers->Server($serverselect);
			$server->CreateImage($imagename);

			$serializeserver=serialize($server);
			$_SESSION['server']=$serializeserver;
			$_SESSION['terminal'] = 'ACTIVE';
			$_SESSION['timeout'] = 600;

			?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
			id="imgProgress" />
		<?php 
		echo "<script type='text/javascript'>WaitForProgress();</script>\n";

		$images = $cloudservers->ImageList();
		$images->Sort('name');

		$idrackspaceimage = "";
		while($image = $images->Next()) {
				if($imagename == $image->name) {
					$idrackspaceimage = $image->id;
				}
			}

			$arguments = array ('dcname' => $dcname,
								'username' => $username,
								'imagename' => $imagename,
								'idrackspaceimage' => $idrackspaceimage,);
			$data = $objDcs->addimag($arguments);
		}
	} elseif (isset ($_POST['deleteimag'])) { // delete image
		$imageId = $_POST['imageselect'];

		// validate info
		if ($imageId == "Select a Image") {
			confirm("Select a Image");
		} else {
			$dcname = $_POST['dcname'];
			$username = $_SESSION['PSAdminNM'];

			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dcname);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dcname);

			$image = $cloudservers->Image($imageId);
			$image->Delete();

			$arguments = array ('dcname' => $dcname,
					'username' => $username,
					'idrackspaceimage' => $imageId);
			$data = $objDcs->removimage($arguments);
		}
	}

	?>
		<div>
			<h2>
				<?php echo $title?>
			</h2>
		</div>
		<?php
		if ($option == "createserver") {
			?>

		<form action="servermanag.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl class="long">
					<dt>
						<label>Image<span style="color: red">*</span>
						</label>
					</dt>
					<dd class="long">
						<?
						echo "<select name=\"imageselect\" id=\"imageselect\" >";
						echo "<option selected>Select a Image</option>";
						while($image = $images->Next()) {
						$date = "";

						$created = $image->created;
						list($datecreated, $time) = explode("T", $created);
						if ($datecreated != "") {
							$date = date("m/d/Y", strtotime($datecreated));
						}
						$server = $image->server;
						$serverId = "";
						$servername = "";
						if (isset($server)) {
							$serverId = $server->id;
							while($servers = $serv->Next()) {
								$Id = $servers->id;
								if ($Id == $serverId){
									$servername = $servers->name;
								}
							}
							$serv->Reset();
						}

						$name = $image->name . " ". $date . " " . $servername;
						echo "<option value=".$image->id.">" . $name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Flavor<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<?
						echo "<select name=\"flavorselect\" id=\"flavorselect\" >";
						echo "<option selected>Select a Flavor</option>";
						while($flavor = $flavors->Next()) {
						$item = $flavor->id."|".$flavor->ram;
						echo "<option value=".$item.">" . $flavor->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<dl>
							<dd>
								<input class="text" type="hidden" name=addomainselect
									id="addomainselect" value="<?=$addomainselect?>" />
							</dd>
						</dl>
						<label>Server Name<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input class="text" name="severname" id="severname" size="35" />
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="createserv" id="createserv"
						value="Create" />
				</dl>

			</fieldset>
		</form>
		<?
		} else if ($option == "deleteserver") {
			?>
		<form action="servermanag.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Server<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<?
						echo "<select name=\"serverselect\" id=\"serverselect\" >";
						echo "<option selected>Select a Server</option>";
						while($servers = $serv->Next()) {
						echo "<option value=".$servers->id.">" . $servers->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="deleteserv" id="deleteserv"
						value="Delete" />
				</dl>
			</fieldset>
		</form>
		<?
		}else if ($option == "resizeserver") {
			?>
		<form action="servermanag.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Server<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<?
						echo "<select name=\"serverselect\" id=\"serverselect\" >";
						echo "<option selected>Select a Server</option>";
						while($servers = $serv->Next()) {
						echo "<option value=".$servers->id.">" . $servers->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Flavor<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<?
						echo "<select name=\"flavorselect\" id=\"flavorselect\" >";
						echo "<option selected>Select a Flavor</option>";
						while($flavor = $flavors->Next()) {
						$item = $flavor->id."|".$flavor->ram;
						echo "<option value=".$item.">" . $flavor->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="resizeserv" id="resizeserv"
						value="Resize" />
				</dl>
			</fieldset>
		</form>
		<?
		}else if ($option == "createimage") {
			?>
		<form action="servermanag.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Server<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<?
						echo "<select name=\"serverselect\" id=\"serverselect\" >";
						echo "<option selected>Select a Server</option>";
						while($servers = $serv->Next()) {
						echo "<option value=".$servers->id.">" . $servers->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Image Name<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input class="text" name="imagename" id="imagename" size="35" />
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="createimag" id="createimag"
						value="Create" />
				</dl>
			</fieldset>
		</form>
		<?
		} else if ($option == "deleteimage") {
			?>
		<form action="servermanag.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl class="long">
					<dt>
						<label>Image<span style="color: red">*</span>
						</label>
					</dt>
					<dd class="long">
						<?
						echo "<select name=\"imageselect\" id=\"imageselect\" >";
						echo "<option selected>Select a Image</option>";
						while($image = $images->Next()) {
						$date = "";

						$created = $image->created;
						list($datecreated, $time) = explode("T", $created);
						if ($datecreated != "") {
							$date = date("m/d/Y", strtotime($datecreated));
						}
						$server = $image->server;
						$serverId = "";
						$servername = "";
						if (isset($server)) {
							$serverId = $server->id;
							while($servers = $serv->Next()) {
								$Id = $servers->id;
								if ($Id == $serverId){
									$servername = $servers->name;
								}
							}
							$serv->Reset();
						}

						$name = $image->name . " ". $date . " " . $servername;
						echo "<option value=".$image->id.">" . $name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="deleteimag" id="deleteimag"
						value="Delete" />
				</dl>
			</fieldset>
		</form>
		<?
		}?>
	</div>
</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
<?
require_once('../includes/footer.php');
?>
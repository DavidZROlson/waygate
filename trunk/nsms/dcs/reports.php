<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var siteimg = "<?= SITEIMG ?>";</script>
<script
	src="<?= SITEJS ?>datetimepicker_css.js"></script>
<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
?>

<div class="right_content">
	<div class="form">
	<?
	$dcname = $_GET['dcname'];
	if ($dcname == "") {
		$dcname = $_POST['dcname'];
	}

	if (isset ($_POST['selectlabels'])) { // get report info
		include('../common/common.php');
		$selectlabels = $_POST['selectlabels'];
		$from = $_POST['from'];
		$to = $_POST['to'];
		$objDcs = new Dcs();

		$validfrom = is_date($from);
		$validto = is_date($to);
		if($validfrom == FALSE) {
			confirm("Enter valid date for FROM");
			$selectlabels = "";
		} elseif ($validto == FALSE) {
			confirm("Enter valid date for TO");
			$selectlabels = "";
		} else {
			$arguments = array ('dcname' => $dcname,
							'from' => $from,
							'to' => $to,
							'label' => $selectlabels,);

			$costdata = $objDcs->getcostdat($arguments);
		}
	}
	?>
		<form action="reports.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dt>
						<label>From:</label>
					</dt>
					<dd>
						<input type="Text" id="from" maxlength="10" size="10" name="from" />
						<img src="<?= SITEIMG ?>cal.gif"
							onclick="javascript:NewCssCal('from')" style="cursor: pointer" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>To:</label>
					</dt>
					<dd>
						<input type="Text" id="to" maxlength="10" size="10" name="to" /> <img
							src="<?= SITEIMG ?>cal.gif" onclick="javascript:NewCssCal('to')"
							style="cursor: pointer" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label><br> </label>
					</dt>
					<dd>
					<?
					$objDcs = new Dcs();

					$data = $objDcs->getlbels();
					$temp = $data[0];

					echo "<select name=\"selectlabels\" id=\"selectlabels\" onChange=\"document.frmUser.submit()\">";
					echo "<option selected>Select a Label</option>";
					echo "<option value=\"ALL\">ALL</option>";
					if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['labelname'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dcname?>" />
					</dd>
				</dl>
				<?if ($selectlabels != "") { ?>
				<dt>
				<? echo $from . " to " . $to . " $submit"?>
				</dt>
				<table id="rounded-corner" summary="Admin">
					<thead>
						<tr>
							<th scope="col" class="rounded">SERVER NAME</th>
							<th scope="col" class="rounded">SERVER ID</th>
							<th scope="col" class="rounded">CREATED</th>
							<th scope="col" class="rounded">DELETED</th>
							<th scope="col" class="rounded">COST</th>
						</tr>
					</thead>

					<tbody>
					<?php
					// display server info in table
					foreach ($costdata as $data) {
						$servername = $data['servername'];
						$idrackspace = $data['idrackspace'];
						$created = $data['created'];
						list($crdate, $time) = explode(" ", $created);
						$deleted = $data['deleted'];
						list($dedate, $time) = explode(" ", $deleted);
						$cost = $data['cost'];

						echo "<tr>";
						echo "<td class=\"labelcell\">".$servername."</td>";
						echo "<td class=\"labelcell\">".$idrackspace."</td>";
						echo "<td class=\"labelcell\">".$crdate."</td>";
						echo "<td class=\"labelcell\">".$dedate."</td>";
						echo "<td class=\"labelcell\">".$cost."</td>";

						echo "</tr>";
					}
					?>
					</tbody>
				</table>
				<? }?>
			</fieldset>
		</form>
	</div>

</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
				<?
				require_once('../includes/footer.php');
				?>
<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
require_once '../lib/php-opencloud.php';
require_once('../common/common.php');
?>

<div class="right_content">
<?

$dcname = $_GET['dcname'];

$objDcs = new Dcs();

$arguments = array ('dcname' => $dcname);
$data = $objDcs->getApiInfo($arguments);

$apiusername = $data[0]['username'];
$apikey= $data[0]['apikey'];
$authurl = $data[0]['authurl'];

$rackspace = new OpenCloud \ Rackspace($authurl, array (
		'username' => $apiusername,
		'apiKey' => $apikey
));

$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dcname);

syncServer($cloudservers, $dcname);

$serv = $cloudservers->ServerList();
$serv->Sort('name');

syncImage($cloudservers, $dcname);

$list = $cloudservers->ImageList();
$list->Sort('name');
?>
	<div class="fl" style="width: 200px;">
		<h2>
		<?php echo $dcname?>
		</h2>
	</div>

	<form name="frmUser" id="frmUser" method="post" action=""
		class="niceform">
		<fieldset>
			<table id="rounded-corner" summary="Admin">
				<thead>
					<tr>
						<th scope="col" class="rounded">IMAGE NAME</th>
						<th scope="col" class="rounded">CREATED</th>
						<th scope="col" class="rounded">STATUS</th>
						<th scope="col" class="rounded">SERVER</th>
					</tr>
				</thead>

				<tbody>
				<?php
				// display server info in table

				if ($list->Size() > 0) {
					while ($images = $list->Next()) {
						$name = $images->name;
						$created = date('Y-m-d H:i:s', strtotime($images->created));
						$status = $images->status;
						$server = $images->server;
						$serverId = "";
						$servername = "";
						if (isset($server)) {
							$serverId = $server->id;
							echo "<tr>";
							echo "<td class=\"labelcell\">".$name."</td>";
							echo "<td class=\"labelcell\">".$created."</td>";
							echo "<td class=\"labelcell\">".$status."</td>";
							while($servers = $serv->Next()) {
								$Id = $servers->id;
							
								if ($Id == $serverId){
									$servername = $servers->name;
								
								}
							}
							$serv->Reset();
							echo "<td class=\"labelcell\">".$servername."</td>";
								
							echo "</tr>";

						}
					}
				}
				?>
				</tbody>
			</table>
		</fieldset>
	</form>
	<form action="index.php" method="post" class="niceform" name="frmUser"
		id="frmUser">
		<dl class="submit">
			<input type="submit" name="back" id="back" value="Back"
				onclick="document.location='../index.php'; return false;" />
		</dl>
	</form>

</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
				<?
				require_once('../includes/footer.php');
				?>
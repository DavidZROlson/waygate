<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var siteimg = "<?= SITEIMG ?>";</script>
<script
	src="<?= SITEJS ?>datetimepicker_css.js"></script>
<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
require_once '../lib/php-opencloud.php';
require_once ('../common/common.php');
?>

<div class="right_content">
	<div class="form">
	<?

	if (isset ($_POST['schedule'])) { // get report info
		$objDcs = new Dcs();
		$serverselect = $_POST['serverselect'];
		$schedatetime = $_POST['schedatetime'];

		$type = $_POST['type'];

		$arguments = array ('idrackspace' => $serverselect,
						'schedatetime' => $schedatetime,
						'type' => $type,
		);

		$objDcs->adbackup($arguments);

		header("Refresh: 0;url=../index.php");
	}

	$dc = $_GET['dc'];

	$title = "";
	if ($dc != "") {
		$title = ucwords($dc). ": BACKUP";
	}

	$objDcs = new Dcs();

	$arguments = array ('dcname' => $dc);
	$data = $objDcs->getApiInfo($arguments);

	$apiusername = $data[0]['username'];
	$apikey= $data[0]['apikey'];
	$authurl = $data[0]['authurl'];
		
	$rackspace = new OpenCloud \ Rackspace($authurl, array (
			'username' => $apiusername,
			'apiKey' => $apikey
	));
	
	$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dc);
	
	syncServer($cloudservers, $dcname);

	$serv = $cloudservers->ServerList();
	
	?>
		<div>
			<h2>
			<?php echo $title?>
			</h2>
		</div>
		<form action="backup.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label><br> </label>
					</dt>
					<dd>
					<?
					echo "<select name=\"serverselect\" id=\"serverselect\" >";
					echo "<option selected>Select a Server</option>";
					while ($servers = $serv->Next()) {
						echo "<option value=".$servers->id.">" . $servers->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Scheduled Date/Time<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input type="Text" id="schedatetime" maxlength="20" size="20"
							name="schedatetime" /> <img src="<?= SITEIMG ?>cal.gif"
							onclick="javascript:NewCssCal('schedatetime','MMddyyyy','arrow',true,'12','','future')"
							style="cursor: pointer" />
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="schedule" id="schedule"
						value="Schedule" />
				</dl>
			</fieldset>
		</form>
	</div>

</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
					<?
					require_once('../includes/footer.php');
					?>
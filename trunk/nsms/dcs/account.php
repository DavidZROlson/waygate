<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
?>

<div class="right_content">
	<div class="form">
	<?
	$option = $_GET['option'];
	if ($option == "") {
		$option = $_POST['option'];
	}
	$dcname = $_GET['dcname'];
	if ($dcname == "") {
		$dcname = $_POST['dcname'];
	}

	if ($option == "label") { // add new label names
		include('../common/common.php');
		$labelselect = $_POST['labelselect'];

		if (isset ($_POST['labelnameadd']) && $labelselect == "Select a Label") {
			$objDcs = new Dcs();
			$message = "";

			$labelname = $_POST['labelnameadd'];

			// validate info
			if (empty($labelname)) {
				$message = "Please enter a valid Label Name";
			}
			if ($message != "") {
				include("common.php");

				confirm($message);
				$userselect = "";
			} else {
				$arguments = array (
 				'labelname' => $labelname,
				);

				$data = $objDcs->addlbel($arguments);

				if ($data != "Susscess") {
					include("common.php");

					confirm($data);
					header("Refresh: 0;url=account.php");
				}

				$labelselect = null;
				header("Refresh: 0;url=../index.php");
			}
		}  else if (isset ($_POST['labelnameedit']) && isset ($_POST['addedit'])) {
			// edit label names
			$objDcs = new Dcs();
			$message = "";

			$labelname = $_POST['labelnameedit'];
			$oldlabel = $_POST['oldlabel'];

			// validate info
			if (empty($labelname)) {
				$message = "Please enter a valid Label Name";
			}
			if ($message != "") {
				include("common.php");

				confirm($message);
				$userselect = "";
			} else {
				$arguments = array (
				'labelname' => $labelname,
				'oldlabel' => $oldlabel,
				);

				$data = $objDcs->editlbel($arguments);

				if ($data != "Susscess") {
					include("common.php");

					confirm($data);
					header("Refresh: 0;url=account.php");
				}

				$_POST['label'] = null;
				header("Refresh: 0;url=../index.php");
			}
		}
	} else if ($option == "server"){ // add/edit servers to labels
		require_once '../lib/php-opencloud.php';
		require_once ('../common/common.php');
		$labelselectserv = $_POST['labelselectserv'];

		if ($labelselectserv != "") {
			// get server info from rackspace
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dcname);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];
			
			$GetServInfoCreateAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));
			
			$cloudservers = $GetServInfoCreateAuth->Compute('cloudServersOpenStack', $dcname);

			syncServer($cloudservers, $dcname);

			$serv = $cloudservers->ServerList();
			$serv->Sort('name');
		}

		if (isset ($_POST['addeditserv'])) // tie labels to servers
		{
			$objDcs = new Dcs();

			$labelservers[] = $_POST["labelservers"];
			$i = 1;
			$arguments = array();
			$arguments[0] = $labelselectserv;
			while($servers = $serv->Next()) {
				$id = $servers->id;
				$checked = $id . "|N";
				$arguments[$i] = $checked;
				$labels = $labelservers[0];
				$count = count($labels);
				for ($x = 0; $x < $count; $x++) {
					if ($id == $labels[$x]) {
						$checked = $id . "|Y";
						$arguments[$i] = $checked;
						break;
					}
				}
				$i++;
			}
			$data = $objDcs->updataccount($arguments);
			$_POST['server'] = null;
		}
	}

	if ($option == "label") { ?>
		<div>
			<h2>ADD/EDIT LABEL</h2>
		</div>

		<form action="account.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
			<?
			if ($labelselect== "") { ?>
				<dl>
					<dt>
						<label><br> </label>
					</dt>
					<dd>
					<?
					$objDcs = new Dcs();

					$data = $objDcs->getlbels();
					$temp = $data[0];

					echo "<select name=\"labelselect\" id=\"labelselect\" onChange=\"document.frmUser.submit()\">";
					echo "<option selected>Select a Label</option>";
					if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['labelname'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Report Label<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="labelnameadd" id="labelnameadd"
							size="35" />
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=option id="option"
							value="<?=$option?>" />
					</dd>
				</dl>
				<? }else {?>
				<dl>
					<dd>
					<?
					$objDcs = new Dcs();

					$data = $objDcs->getlbels();
					$temp = $data[0];

					echo "<select name=\"labelselect\" id=\"labelselect\" onChange=\"document.frmUser.submit()\">";
					echo "<option selected>Select a Label</option>";
					if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['labelname'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Report Label<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="labelnameedit" id="labelnameedit"
							value="<?=$labelselect?>" size="35" />
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=option id="option"
							value="<?=$option?>" />
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=oldlabel id="oldlabel"
							value="<?=$labelselect?>" />
					</dd>
				</dl>

				<? }?>
			</fieldset>
			<dl class="submit">
				<input type="submit" style="" name="addedit" id="addedit"
					value="Add/Edit" />
			</dl>
		</form>
		<?
	} else if ($option == "server") {?>
		<div>
			<h2>ADD/REMOVE SERVER LABEL</h2>
		</div>

		<form action="account.php" method="post" name="frmUser2" id="frmUser2"
			class="niceform">
			<fieldset>
			<?
			if ($labelselectserv== "") { ?>
				<dl>
					<dt>
						<label><br> </label>
					</dt>
					<dd>
					<?
					$objDcs = new Dcs();

					$data = $objDcs->getlbels();
					$temp = $data[0];

					echo "<select name=\"labelselectserv\" id=\"labelselectserv\" onChange=\"document.frmUser2.submit()\">";
					echo "<option selected>Select a Label</option>";
					if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['labelname'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=option id="option"
							value="<?=$option?>" />
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dcname?>" />
					</dd>
				</dl>
				<? } else {?>
				<dl>
					<dt>
						<h4>
						<?echo $labelselectserv ?>
						</h4>
					</dt>
				</dl>
				<dl>
					<table id="rounded-corner-account" summary="Admin">
						<thead>
							<tr>
								<th scope="col" class="rounded">LABEL</th>
								<th scope="col" class="rounded">SERVER</th>
							</tr>
						</thead>

						<tbody>
						<?php
						$serv->Reset();
						while($servers = $serv->Next()) {
							$rackservname = $servers->name;
							$idrackspace = $servers->id;

							$arguments = array (
							'idrackspace' => $idrackspace,
							'labelname' => $labelselectserv
							);

							$labeldata = $objDcs->getLabelDat($arguments);

							echo "<tr>";
							if ($labeldata[0] == "ERR" || $labeldata[0]['checked'] == "N") {
								echo "<td><input type=\"checkbox\" name=\"labelservers[]\"value=\"$idrackspace\" /></td>";
							} else {
								echo "<td><input type=\"checkbox\" name=\"labelservers[]\"value=\"$idrackspace\" checked = \"checked\" /></td>";
							}
							echo "<td class=\"labelcell\">".$rackservname."</td>";
							echo "</tr>";
						}
						?>
						</tbody>
					</table>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=option id="option"
							value="<?=$option?>" />
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=labelselectserv
							id="labelselectserv" value="<?=$labelselectserv?>" />
					</dd>
				</dl>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dcname?>" />
					</dd>
				</dl>
				<dl class="submit_account">
					<input type="submit" style="" name="addeditserv" id="addeditserv"
						value="Add/Edit" />
				</dl>
				<? }?>
			</fieldset>
		</form>
		<? }?>
	</div>
</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
		<?
		require_once('../includes/footer.php');
		?>
<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
require_once('../common/common.php');
?>

<div class="right_content">
	<div class="form_dns">
	<?php
	$ip = $_GET['ip'];
	$name = $_GET['name'];

	$url = "http://".$ip."/guyjack.php";

	$memory = getcurl($url, " ");
	if (stripos(strtolower($memory), 'Not Found') !== false) {
		$memory = "guyjack.php was not found on server.";
	}

	$url = "http://".$ip."/roseyjohn.php";

	$clientinfo = getcurl($url, " ");
	if (stripos(strtolower($client), 'Not Found') !== false) {
		$memory = "roseyjohn.php was not found on server.";
	}

	$memoryarr = explode(" ", $memory);
	$total = $memoryarr[1];
	$used = $memoryarr[3];
	$free = filter_var($memoryarr[5], FILTER_SANITIZE_STRING);

	$clientlines = explode("\n", $clientinfo);

	?>
		<div>
			<h2>
			<?php echo $name?>
			</h2>
		</div>
		<form action="serverinfo.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<div class="center_dns_add">
				<fieldset>
					<dl>
						<dt>
							<label class="big">Total Memory</label>
						</dt>
						<dt>
							<label class="big"><?echo $total ?> MB</label>
						</dt>
					</dl>
					<dl>
						<dt>
							<label class="big">Used Memory</label>
						</dt>
						<dt>
							<label class="big"><?echo $used ?> MB</label>
						</dt>
					</dl>
					<dl>
						<dt>
							<label class="big">Free Memory</label>
						</dt>
						<dt>
							<label class="big"><?echo $free ?> MB</label>
						</dt>
					</dl>

				</fieldset>
			</div>
			<div class="right_dns_add">
				<div class="form_dns">
					<form action="index.php" method="post" class="niceform"
						name="frmUser" id="frmUser">
						<fieldset>
							<table id="rounded-corner-dns">
								<thead>
									<tr>
										<th scope="col" class="rounded">Local Address</th>
										<th scope="col" class="rounded">Foreign Address</th>
									</tr>
								</thead>

								<tbody>
								<?php
								foreach ($clientlines as $client) {

									$info = explode(" ", preg_replace('/\s\s+/', ' ', $client));

									echo "<tr>";
									echo "<td class=\"labelcell\">".$info[3]."</td>";
									echo "<td class=\"labelcell\">".$info[4]."</td>";
									echo "</tr>";
								}
								?>
								</tbody>
							</table>
						</fieldset>
					</form>
				</div>
			</div>
		</form>
	</div>
</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
								<?
								require_once('../includes/footer.php');
								?>
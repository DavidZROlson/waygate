<?php 
$pagepathnew=$_SERVER['PHP_SELF'];
$pagepatharrnew=  explode("/", $pagepathnew);
$activepagenew=$pagepatharrnew[3];
$pagenameall=  explode(".", $activepagenew);
$pagenameonly= $pagenameall['0'];
$objlogin= new Login();
$retADM=$objlogin->validRights("ADM");

$retDNS=$objlogin->validRights("DNS");

$retmonitor=$objlogin->validRights("MONITORING");

$dcRights=$objlogin->validDCRights();

$objDcs = new Dcs();
$data = $objDcs->retrievedcs();
$temp = $data[0];
?>
<div
	class="left_content">
	<div id="ddsidemenubar" class="markermenu">
		<ul>
			<li><? if($retADM =='success'){?><a href="#" rel="ddsubmenuside1">Admin
					Settings</a> <? }?>
			</li>
			<li><? if($dcRights['success'] =='success'){?><a href="#"
				rel="ddsubmenuside2">DC's</a> <? }?>
			</li>
			<li><? if($retDNS =='success'){?><a href="#" rel="ddsubmenuside3">DNS
			</a> <? }?>
			</li>
		</ul>
	</div>

	<script type="text/javascript">
						ddlevelsmenu.setup("ddsidemenubar", "sidebar") //ddlevelsmenu.setup("mainmenuid", "topbar|sidebar")
					</script>

	<!--Side Drop Down Menu 1 HTML-->

	<ul id="ddsubmenuside1" class="ddsubmenustyle blackwhite">
		<li><a href="<?=SITE_PATH?>admin/index.php">Add/Edit Users & Rights</a>
		</li>
		<li><a href="<?=SITE_PATH?>admin/dcsadmin.php">Add/Edit DC's</a>
		</li>
	</ul>

	<!--Side Drop Down Menu 2 HTML-->

	<ul id="ddsubmenuside2" class="ddsubmenustyle blackwhite">
		<? if ($temp != "ERR") {
			foreach ($data as $temp) {
				$dcname = $temp['dcname'];
				if ($dcRights['rights'][$dcname]['dc'] == "Y") { ?>
		<li><a href="#"><? echo $dcname?> </a>
			<ul>
				<!--  <li><a href="<?=SITE_PATH?>dcs/backup.php?dc=<?=$dcname?>">Backup</a>
				</li> -->
				<? if ($dcRights['rights'][$dcname]['projman'] == "Y") {?>
				<!--  <li><a href="#">Project Management</a></li> -->
				<? }
				if ($dcRights['rights'][$dcname]['serverman'] == "Y") {?>
				<li><a href="#">Server Management</a>
					<ul>
						<li><a href="<?=SITE_PATH?>dcs/index.php?dcname=<?=$dcname?>">Display
								Servers</a>
						</li>
						<li><a href="<?=SITE_PATH?>dcs/images.php?dcname=<?=$dcname?>">Display
								Images</a>
						</li>
						<li><a
							href="<?=SITE_PATH?>dcs/servermanag.php?dc=<?=$dcname?>&option=createserver">Create
								Server</a>
						</li>
						<li><a
							href="<?=SITE_PATH?>dcs/servermanag.php?dc=<?=$dcname?>&option=deleteserver">Delete
								Server</a>
						</li>
						<li><a
							href="<?=SITE_PATH?>dcs/servermanag.php?dc=<?=$dcname?>&option=resizeserver">Resize
								Server</a>
						</li>
						<li><a
							href="<?=SITE_PATH?>dcs/servermanag.php?dc=<?=$dcname?>&option=createimage">Create
								Image</a>
						</li>
						<li><a
							href="<?=SITE_PATH?>dcs/servermanag.php?dc=<?=$dcname?>&option=deleteimage">Delete
								Image</a>
						</li>
					</ul>
				</li>
				<?}
					if ($dcRights['rights'][$dcname]['report'] == "Y") {?>
				<li><a href="<?=SITE_PATH?>dcs/reports.php?dcname=<?=$dcname?>">Report</a>
				</li>
				<li><a href="#">Accounting</a>
					<ul>
						<li><a href="<?=SITE_PATH?>dcs/account.php?option=label">Add/Edit
								Label</a>
						</li>
						<li><a
							href="<?=SITE_PATH?>dcs/account.php?dcname=<?=$dcname?>&option=server">Add/Remove
								Server Label</a>
						</li>
					</ul></li>
				<? }?>
			</ul>
		</li>
		<? }
		}
		}?>
	</ul>


	<!--Side Drop Down Menu 3 HTML-->

	<ul id="ddsubmenuside3" class="ddsubmenustyle blackwhite">
		<li><a href="<?=SITE_PATH?>dns/index.php?option=adddomain">Add Domain</a>
		</li>
		<li><a href="<?=SITE_PATH?>dns/index.php?option=deletedomain">Delete
				Domain</a></li>
		<li><a href="<?=SITE_PATH?>dns/index.php?option=addrecord">Add Record</a>
		</li>
		<li><a href="<?=SITE_PATH?>dns/index.php?option=deleterecord">Delete
				Record</a></li>
	</ul>

	<?php if($note){ ?>
	<div class="sidebar_box">
		<div class="sidebar_box_top"></div>
		<div class="sidebar_box_content">
			<h3>Admin help desk</h3>
			<img src="<?=SITEIMG ?>info.png" alt="" title=""
				class="sidebar_icon_right" />
			<p>
				<?= $note ?>
			</p>
		</div>
		<div class="sidebar_box_bottom"></div>
	</div>
	<?php }?>
	<?php if($notice){ ?>
	<div class="sidebar_box">
		<div class="sidebar_box_top"></div>
		<div class="sidebar_box_content">
			<h4>Important notice</h4>
			<img src="<?=SITEIMG ?>notice.png" alt="" title=""
				class="sidebar_icon_right" />
			<p>
				<?= $notice ?>
			</p>
		</div>
		<div class="sidebar_box_bottom"></div>
	</div>
	<?php }?>

</div>

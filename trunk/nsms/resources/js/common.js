function searchtxt() {
	var searchword = document.getElementById('search').value;
	var hiddenpath = document.getElementById('hiddenpath').value;
	if (!searchword) {
		alert('Please enter text.');
		jQuery('#search').focus();
		return false;
	}
	window.location = hiddenpath + '&search=' + searchword;
	return false;
}
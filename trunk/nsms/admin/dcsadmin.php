<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
?>

<div class="right_content">
	<h2>DC's</h2>

	<div class="form">

	<?
	// get DC info
	if (isset ($_POST['dcselect'])) {
		$objDcs = new Dcs();
		$dcselect = $_POST['dcselect'];

		$arguments = array ('dcname' => $dcselect);

		$data = $objDcs->getDCInformation($arguments);
		$dcinfo = $data[0];
	}


	if (isset ($_POST['add'])) {// add/edit/clear data
		AddEdit($_POST['add']);
	} else if (isset ($_POST['edit'])) {
		AddEdit($_POST['edit']);
	}

	// add/edit rackspace passwords
	function AddEdit($mode) {
		$objDcs = new Dcs();
		$message = "";

		$dcname = $_POST["dcname"];
		$username = $_POST["username"];
		$password = $_POST["password"];
		$apikey = $_POST["apikey"];
		$provider = $_POST["provider"];
		$dcnameold = $_POST["dcnameold"];

		// validate input data
		if (empty($dcname)) {
			$message = "Please enter a valid DC Name";
		}
		if (empty($username)) {
			$message = "Please enter a valid User Name";
		}
		if (empty($password)) {
			$message = "Please enter a valid Password";
		}
		if (empty($apikey)) {
			$message = "Please enter a valid ApiKey";
		}
		if (empty($provider)) {
			$message = "Please enter a valid Provider";
		}

		if ($message != "") {
			include('../common/common.php');

			confirm($message);
			header("Refresh: 0;url=../index.php");
		} else {
			$arguments = array (
				'dcname' => $dcname,
				'username' => $username,
				'password' => $password,
				'apikey' => $apikey,
				'provider' => $provider,
			);

			$data = "";
			if ($mode == "Add") {
				$data = $objDcs->adddcsinfo($arguments);
			} else {
				$arguments['dcnameold'] = $dcnameold;
				$data = $objDcs->editdcsinfo($arguments);
			}

			if ($data == "Susscess") {
				header("Refresh: 0;url=../index.php");
			} else {
				include('../common/common.php');

				confirm($data);
				header("Refresh: 0;url=../index.php");
			}
		}
	}

	// display add DC page
	if ($dcselect == "") {
		?>
		<form action="dcsadmin.php" method="post" class="niceform"
			name="frmUser" id="frmUser">
			<fieldset>
				<dl>
					<dt>
						<label><br> </label>
					</dt>
					<dd>
					<?
					$objDcs = new Dcs();
					$data = $objDcs->retrievedcs();
					$temp = $data[0];

					echo "<select name=\"dcselect\" id=\"dcselect\" onChange=\"document.frmUser.submit()\">";
					echo "<option selected>Select a DC</option>";
					if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['dcname'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>DC Name<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="dcname" id="dcname" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Username<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="username" id="username" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Password<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input type="password" autocomplete="off" class="text"
							name="password" id="password" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Api Key<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="apikey" id="apikey" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Provider<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="provider" id="provider" size="35" />
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="add" id="add" value="Add" />
				</dl>

			</fieldset>
		</form>

		<?
		// display selected DC info
	} else if ($dcselect != "Select a DC") {
		?>
		<form action="dcsadmin.php" method="post" class="niceform" name="frmUser1"
			id="frmUser1">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name="dcnameold" id="dcnameold"
							value="<?=$dcinfo["dcname"]?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>DC Name<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="dcname" id="dcname"
							value="<?=$dcinfo["dcname"]?>" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Username<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="username" id="username"
							value="<?=$dcinfo["username"]?>" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Password<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input type="password" autocomplete="off" class="text"
							name="password" id="password" value="<?=$dcinfo["password"]?>"
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Api Key<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="apikey" id="apikey"
							value="<?=$dcinfo["apikey"]?>" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Provider<span style="color: red">*</span> </label>
					</dt>
					<dd>
						<input class="text" name="provider" id="provider"
							value="<?=$dcinfo["provider"]?>" size="35" />
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="edit" id="edit" value="Edit" />
				</dl>

			</fieldset>
			<dl class="submit">
				<input type="submit" name="back" id="back" value="Back"
					onclick="document.location='../index.php'; return false;" />
			</dl>
		</form>
		<?
	} 	?>

	</div>

</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
	<?
	require_once('../includes/footer.php');
	?>
<?php
require_once('../pi_classes/Admin.php');
$obj = new Admin();


require_once('../includes/header1.php');
?>

<html>
<body>
	<div id="main_container">

		<div class="header_login">
			<div class="logo">
				<a href="<?=SITE_PATH?>"><?= PROJECTNAME ?> </a>
			</div>
		</div>
		<div class="clear">&nbsp;</div>

		<div class="signup_content">
			<div class="error">
				<?= $msg ?>
			</div>
			<h3>Signup Panel</h3>

			<!-- <a href="<?=SITE_PATH?>login/forgot-password.php" class="forgot_pass">Forgot
				password</a> -->

			<form action="" method="post" class="niceform">
				<input type="hidden" name="pagebck" id="pagebck"
					value="<?= $_GET['url_s'] ?>" />
				<fieldset>
					<dl class="signup">
						<table id="rounded-corner-account-signup" summary="Admin">
							<tr>
								<td><label>COMPANY:</label></td>
								<td><input class="text" name="company" id="company"
									size="35" /></td>
							</tr>
							<tr>
								<td><label>NAME - FIRST:</label></td>
								<td><input class="text" name="first_name"
									id="first_name" size="35" /></td>
								<td><label>LAST:</label></td>
								<td><input class="text" name="first_last" id="first_last"
									size="35" /></td>
							</tr>
							<tr>
								<td><label>ADDRESS 1:</label></td>
								<td><input class="text" name="mailing_address1"
									id="mailing_address1" size="35" /></td>
							</tr>
							<tr>
								<td><label>ADDRESS 2:</label></td>
								<td><input class="text" name="mailing_address2"
									id="mailing_address1" size="35" /></td>
							</tr>
							<tr>
								<td><label>CITY:</label></td>
								<td><input class="text" name="city" id="city" size="35" /></td>
								<td><label>STATE:</label></td>
								<td><input class="text" name="state" id="state" size="35" /></td>
								<td><label>ZIP:</label></td>
								<td><input class="text" name="zip" id="zip" size="35" /></td>
							</tr>
							<tr>
								<td><label>PHONE - BUSINESS:</label></td>
								<td><input class="text" name="city" id="city" size="35" /></td>
								<td><label>HOME:</label></td>
								<td><input class="text" name="state" id="state" size="35" /></td>
								<td><label>CELL:</label></td>
								<td><input class="text" name="zip" id="zip" size="35" /></td>
							</tr>
						</table>
					</dl>

					<dl class="submit">
						<dd>
							<input type="submit" name="submit" id="Next1" value="Next"
								tabindex="4" />
						</dd>
					</dl>

				</fieldset>

			</form>
		</div>


		<?php
		require_once('../includes/footer.php');
?>
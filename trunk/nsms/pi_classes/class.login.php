<?php

class Login extends Admin
{
	function chkValidLogin($uname, $pwd, $remember='') {

		if ($uname != "" || $pwd != "") {

			$arguments = array (
				'username' => $uname,
				'password' => $pwd,
			);

			$data = $this->login($arguments);

			if (is_array($data) && $data[0] != "ERR") {
					
				$arguments = array (
					'username' => $uname,
				);
				$this->loginupdate($arguments);

				$_SESSION['PSAdminID']=$data[0]['iduser'];
				$_SESSION['PSAdminNM']=$uname;
				$_SESSION['PSAdminPD']=$pwd;

				if($remember=='yes'){

					setcookie("psadminunm",  base64_encode($uname));
					setcookie("psadminpsd",  base64_encode($pwd));
					setcookie("psadminrem",  'yes');
				}else{
					setcookie("psadminunm",  '',time()-10);
					setcookie("psadminpsd",  '',time()-10);
					setcookie("psadminrem",  '',time()-10);
					unset($_COOKIE);
				}
				$retValue="success";
			} else {
				$retValue = "false";
			}
		}
		return $retValue;
	}

	function validDCRights() {
		$output = array();
		$dc = array();
		$success="false";

		$arguments = array (
			'mode' => "notall",
		);

		$data = $this->getDCs($arguments);

		foreach($data AS $dcs) {
			$rights = array();
				
			$dcname = $dcs['dcname'];

			$arguments = array (
				'dcname' => $dcname,
			);

			$datarights = $this->checkdcrights($arguments);

			if (is_array($datarights) && $datarights[0] != "ERR") {
				$rights['dc'] = $datarights[0]['dc'];
				$rights['report'] = $datarights[0]['report'];
				$rights['serverman'] = $datarights[0]['serverman'];
				$rights['projman'] = $datarights[0]['projman'];
			} else {
				$rights['dc'] = "N";
				$rights['report'] = "N";
				$rights['serverman'] = "N";
				$rights['projman'] = "N";
			}
			if ($rights['dc'] == "Y") {
				$success="success";
			}
			$dc[$dcname] = $rights;
		}

		$output['success'] = $success;
		$output['rights'] = $dc;

		return $output;
	}

	function validRights($dc) {

		$arguments = array (
			'dc' => $dc,
		);

		$data = $this->checkrights($arguments);

		$retValue="success";
		if (is_array($data) && $data[0] == "ERR") {
			$retValue = "false";
		}

		if ($dc == "MONITORING" && $data[0]["monitor"] == "N") {
			$retValue = "false";
		}

		if (($dc == "ADM" || $dc == "DNS") && $data[0]["admin"] == "N") {
			$retValue = "false";
		}

		return $retValue;
	}
}
?>

<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$objDcs = new Dcs();

	foreach ($_POST[LABEL] as $labels) {
		if(isset($labels[checked])){
			if (!empty($labels['new'])) {
				if(isset($labels[old])){
					$arguments = array (
							'labelname' => $labels['new'],
							'oldlabel' => $labels[old],
					);
						
					$data = $objDcs->editlbel($arguments);
				} else {
					$arguments = array (
							'labelname' => $labels['new'],
					);
						
					$data = $objDcs->addlbel($arguments);
				}
			}
		}
	}

	exit();
}
?>

<form id="lbl_list">

	<table style="min-width: 300px; width: auto;">
		<tbody>
			<tr>
				<th class="checkbox"><input id="chk_all" type="checkbox"
					name="chk_all_box" /></th>
				<th>Label Name</th>
				<th>Rename to...</th>
			</tr>
			<?php
			$objDcs = new Dcs();

			$data = $objDcs->getlbels();
			$count = count($data);
			$temp = $data[0];

			$client_servers = array();

			if ($temp != "ERR") {
				for($i = 0; $i < $count; $i++)
				{
					for($n = 0; $n < 1; $n++)
					{
						switch($n)
						{
							case 0:
								$client_servers[$i][$n] = $data[$i]['labelname'];
								break;
							default:
								break;
						}
					}
				}
			}
			//END build TESTing array $client_servers[][]

			$result = "";

			//OUTPUT A ROW FOR EACH SERVER IN THE ARRAY
			$odd_even = 0;
			foreach($client_servers as $server){
				//seperate rows distictly by color
				if($odd_even % 2 == 0)
				{
					$result.='<tr class="odd">';
				}else{
					$result.='<tr class="even">';
				}
				//display row data
				$result.='
		<td class="checkbox"><input id="chkbox_' . $odd_even . '" type="checkbox" name="LABEL[' . $odd_even . '][checked]"/></td>
						<td id="name_' . $odd_even . '">' . $server[0] . '<input type="hidden" name="LABEL['. $odd_even . '][old]" value="' . $server[0] . '" ></td>
					<td><input disabled="disabled" type="text" id="rename_to_' . $odd_even . '" name="LABEL[' . $odd_even . '][new]" /></td>
					</tr>';
					
				$odd_even++;
			}
			echo($result);
			?>

		</tbody>
	</table>
	<input type="button" id="add_row_button" name="add_row_button"
		value="Create a New Label" /><input id="apply_button" type="button"
		value="Apply Changes" />
</form>

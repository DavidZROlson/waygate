<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

$objDcs = new Dcs();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$form = array();
	parse_str($_POST['data'], $form);

	foreach($form as $item => $value){
		switch($item){
			case ($item == 'name'):
				//the item getting applied to is the [name] => Server 3, etc..
				break;
			case ($item == 'chk_all_box'):
				//not sure if we are going to use this for anything...
				break;
			default:
				//do something with the $item => $value
				//also represented as $form[Labels_1] => on, etc...
		}
	}

	echo('Selected DC: ' . $_POST['dc']);
	echo("\r\n" . '$form =>');
	print_r($form);

	exit();
}

//TODO
//IF LABEL RADIO BUTTON IS SELECTED
//STEP 1: QUERY FOR AN ARRAY OF ALL EXISTING LABELS
//STEP 2: APPLY THE RESULTING ARRAY TO A DROP DOWN
//STEP 3: WHEN A DROP DOWN SELECTION IS MADE
//STEP 3A: QUERY TO SEE WHAT SERVERS ALREADY HAVE THE SELECTED LABEL APPLIED
//IF A SERVER ALREADY HAS THE LABEL APPLIED
//STEP 3B: THEN PRE-CHECK THE CHECK BOX IN THE TABLE BELOW
//STEP 4: BUILD THE TABLE WITH ALL SERVERS AND CHECKBOXES FOR THE SELECTED LABEL

//IF SERVER RADIO BUTTON IS SELECTED
//QUERY FOR AN ARRAY OF EXISTING SERVERS
//APPLY THE RESULTING ARRAY TO A DROP DOWN
//WHEN A DROP DOWN SELECTION IS MADE
//QUERY TO SEE WHAT LABELS HAVE ALREADY BEEN APPLIED TO THIS SELECTED SERVER
//IF A LABEL IS ALREADY APPLIED TO THE SELECTED SERVER
//THEN PRE-CHECK THE CHECK BOX IN THE TABLE BELOW
//BUILD THE TABLE WITH ALL LABELS AND CHECKBOXES FOR THE SELECTED SERVER

//OUTPUT A ROW FOR EACH SERVER/LABEL IN THE ARRAY

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	if(isset($_GET['dc'])){
		//echo('HERE IS THE DATACENTER: ' . $_GET['dc']);
		$arguments = array ('idrights' => $_GET['dc']);
		$data = $objDcs->getApiInfo($arguments);
		$dcname = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$rackspace = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dcname);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dcname) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dcname);
		
		$list = $cloudservers->ServerList();
		$list->Sort('name');
		
		$labels = $objDcs->getlbels();
		$temp = $labels[0];
		
		$lbl_list = array();
		foreach($labels as $key => $value) {
			$Id = $key;
			$name = $value['labelname'];
			array_push($lbl_list, array('Id' => $Id, 'name' => $name));
		}
		
		$svr_list = array();
		while ($servers = $list->Next()) {
			$Id = $servers->id;
			$name = $servers->name;
			array_push($svr_list, array('Id' => $Id, 'name' => $name));
		}

	}

	if(isset($_GET['clicked'])){
		
		/**********
		 AJAX FOR RADIO BUTTONS
		**********/
		//IF THE LABEL RADIO BUTTON WAS PRESSED
		if($_GET['clicked'] === 'label'){
			//STEP 1: QUERY FOR AN ARRAY OF ALL EXISTING LABELS
			//
			//
			//STEP 2: APPLY THE ARRAY TO A DROP DOWN

			$dd = array();
			//FOREACH LABEL PUSH() $dd[id][name]
			?>
                <select name="dd" id="dd">
                    <option value="">Select a Label</option>
                    <?php 
                    if ($temp != "ERR") {
                        foreach ($lbl_list as $lbl) {
                            echo('<option value="' . $lbl['Id'] . '">' . $lbl['name'] . '</option>');
                        }
                    }
                    ?>
                </select>
            <?php
//OUTPUT DROP DOWN MENU
		}
		//IF THE SERVER RADIO BUTTON WAS PRESSED
		if($_GET['clicked'] === 'server'){
			//STEP 1: QUERY FOR AN ARRAY OF ALL EXISTING LABELS
			//
			//
			//STEP 2: APPLY THE ARRAY TO A DROP DOWN
			?>
            
                <select name="dd" id="dd">
                    <option value="">Select a Server</option>
                    <?php 
                    if ($temp != "ERR") {
                        foreach ($svr_list as $svr) {
                            echo('<option value="' . $svr['Id'] . '">' . $svr['name'] . '</option>');
                        }
                    }
                    ?>
                </select>
			<?php
		}
		//STEP 3: WHEN A DROP DOWN SELECTION IS MADE
		if($_GET['clicked'] === 'dd'){

			//STEP 3A: QUERY TO SEE WHAT SERVERS ALREADY HAVE THE SELECTED LABEL APPLIED
			//
			//IF A SERVER ALREADY HAS THE LABEL APPLIED
				//STEP 3B: THEN PRE-CHECK THE CHECK BOX IN THE TABLE BELOW
				
			/*
			//begin building testing array
			$rand_num = rand(6, 14);
			$test = array();
			for($i = 0; $i < $rand_num; $i++){
				$checked = rand(0,1) == 1 ? true : false;
				$test[$i]['checked'] = $checked;
				$test[$i]['name'] = $_GET['radio'] . '_Name';
			//end testing array
			}*/

			$test = array();
			$to_apply_to = $_GET['radio'] != "Server" ? "Server" : "Label";
			$test = ($to_apply_to == "Label") ? $lbl_list : $svr_list;
			
			/*******************************************************************
			********************************************************************
			*******************************************************************/
			//CHECK IF SELECTED IS APPLIED TO $test[]
			$selected_id = $_GET['sel'];
			
			$i= 0;
			foreach($test as $value){
				//CHECK $selected_id VS Server/Label $value[id] to see if it is applied already
				//if yes
				//then array_push($test[$i], 'checked' => true);
				$i++;
			}
			

			//STEP 4: BUILD THE TABLE WITH ALL SERVERS AND CHECKBOXES FOR THE SELECTED LABEL
			?>
<form id="lbls_svrs" style="margin: 0 10px;">
	<input type="hidden" value="<?php echo($_GET['sel']); ?>" name="name" />
	<table style="min-width: 300px; width: auto;">
		<tbody>
			<tr>
				<th>#</th>
				<th><?php echo($_GET['radio']); ?></th>
				<th class="checkbox" colspan="2"><input id="chk_all"
					name="chk_all_box" type="checkbox" />
				</th>
				<th>Apply to <?php echo($to_apply_to); ?>
				</th>
			</tr>
			<?php
			//BUILD ADDITIONAL ROWS FOR EACH LABEL/SERVER

			$i=1;
			foreach($test as $key => $value){
				//ALTERNATE ROWS
				switch($i){
					case $i%2==0:
						echo '<tr class="even">';
						break;
					case $i%2==1:
						echo '<tr class="odd">';
						break;
					default:
						echo '<tr>';
						break;
				}//END ODD_EVEN SWITCH

				?>
                <td><?php echo($i); ?></td>
                <!-- LINE NUMBER -->
                <td><?php echo($_GET['name']); ?></td>
                <!-- SELECTED'S NAME -->
                <td class="checkbox"><input type="checkbox"
                    id="<?php $name =  preg_replace('/\s+/', '', $value['name']); echo($name); ?>"
                    name="<?php echo($value['name']); ?>"
                    <?php echo($value['checked'] == true ? 'checked="checked"' : ''); ?> />
                </td>
                <!-- ENABLED CHECK BOX -->
                <td class='checkbox'><input type='checkbox'
                <?php echo($value['checked'] == true ? 'checked="checked" disabled="disabled"' : 'disabled="disabled"'); ?> />
                </td>
                <!-- DISABLED CHECK BOX -->
                <td><?php echo($value['name']); ?>
                </td>
                <!-- ITEM TO APPLY'S NAME -->
                </tr>
                <!-- END ROW -->
                <?php $i++;

			}//END FOREACH
			?>
		</tbody>
	</table>
	<input type='reset' value='Undo Changes' /> <input id='apply_button'
		type='button' value='Apply Changes' />
</form>
<?php
//OUTPUT THE TABLE TO #dd_result

		}//END IF $_GET[ CLICKED ] === DD
		exit();
	}//END IF(ISSET( $_GET[ CLICKED ] )
}

?>

<fieldset style="display: inline-block;">
	<legend>Apply to ...</legend>
	<div align="center">
		<p>
			Label: Select a Label to apply to one or more Servers.<br /> <span
				class="hor_line_120"></span>or<span class="hor_line_120"></span><br />
			Server: Select a Server to apply one or more Labels.
		</p>
	</div>
</fieldset>
<div>
	<div id="lbl_svr_radio">
		<input type="radio" id="lbl_radio" name="lbl_svr_radio"
			checked="checked" /><label for="lbl_radio">Labels</label> <input
			type="radio" id="svr_radio" name="lbl_svr_radio" /><label
			for="svr_radio">Servers</label>
	</div>
	<div id="apply_dd"></div>
	<div id="dd_result">
		<!-- LEFT BLANK ON PURPOSE -->
	</div>
</div>

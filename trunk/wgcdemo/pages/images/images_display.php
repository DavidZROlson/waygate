<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	//IF GET REQUEST IS FROM 'dcs'
	if(isset($_GET['dcs'])){
		$DataCenter = $_GET['dcs'];		
		
		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dc = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$rackspace = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dc);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dc) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dc);

		$serv = $cloudservers->ServerList();
		$serv->Sort('name');

		syncImage($cloudservers, $dc);

		$list = $cloudservers->ImageList();
		$list->Sort('name');

		if ($list->Size() > 0) {
			$client_images = array();
			$i = 0;
			while ($images = $list->Next()) {
				$Id = $images->id;
				$name = $images->name;
				$created = date('Y-m-d H:i:s', strtotime($images->created));
				$status = $images->status;
				$server = $images->server;
				$serverId = "";
				$servername = "";
				if (isset($server)) {
					$data = $objDcs->getCustomersImages();
					$temp = $data[0];
					if ($temp != "ERR") {
						foreach ($data as $temp) {
							if ($Id == $temp['idrackspaceimage']) {
								$serverId = $server->id;
								while($servers = $serv->Next()) {
									$Id = $servers->id;
								
									if ($Id == $serverId){
										$servername = $servers->name;
								
									}
								}
								for($n = 0; $n < 4; $n++)
								{
									switch($n)
									{
										case 0:
											$client_images[$i][$n] = $name;
											break;
										case 1:
											$client_images[$i][$n] = $created;
											break;
										case 2:
											$client_images[$i][$n] = $status;
											break;
										case 3:
											$client_images[$i][$n] = $servername;
											break;
										default:
											break;
									}
								}
								$serv->Reset();
							}
						}
					}
				}
				$i++;
			}
		}
	}
}
?>

<table>
	<tbody>
		<tr>
			<th>Image Name</th>
			<th>Date Created</th>
			<th>Status</th>
			<th>Server</th>
		</tr>
		<?php

		$result = "";

		//OUTPUT A ROW FOR EACH SERVER IN THE ARRAY
		$odd_even = 0;
		foreach($client_images as $image){
			//seperate rows distictly by color
			if($odd_even % 2 == 0)
			{
				$result.='<tr class="odd">';
			}else{
				$result.='<tr class="even">';
			}
			//display row data
			$result.='
				<td id="image_name_' . $odd_even . '">' . $image[0] . '</td>
				<td id="created_' . $odd_even . '">' . $image[1] . '</td>
				<td>' . $image[2] . '</td>
				<td>' . $image[3] . '</td>
				</tr>';

			$odd_even++;
		}
		echo($result);
	?>
	</tbody>
</table>

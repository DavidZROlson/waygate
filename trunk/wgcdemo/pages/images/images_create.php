<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {


	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dc = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dc);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dc) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dc);

		$serv = $cloudservers->ServerList();
		$serv->Sort('name');

	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {


	//IF GET REQUEST IS FROM 'dcs'
	if($_POST['name'] != ""){

		$DataCenter = $_POST['dcs'];
		$serverselect = $_POST['server'];
		$imagename = $_POST['name'];

		echo '<p style="position: absolute; top: 50%; left: 50%;">';
		echo 'DataCenter: ' . $DataCenter . '<br />';
		echo 'Selected IMG: ' . $serverselect . '<br />';
		echo 'Selected Name: ' . $imagename . '</p>';

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dcname = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];
			
		$CreateServAuth = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $CreateServAuth->Compute('cloudServersOpenStack', $dcname);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dcname) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncImage($cloudservers, $dc);

		$server = $cloudservers->Server($serverselect);
		$server->CreateImage($imagename);

		/*$serializeserver=serialize($server);
		 $_SESSION['server']=$serializeserver;
		$_SESSION['terminal'] = 'ACTIVE';
		$_SESSION['timeout'] = 600;

		?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;" id="imgProgress" />
		<?php
		echo "<script type='text/javascript'>WaitForProgress();</script>\n";*/

		$images = $cloudservers->ImageList();
		$images->Sort('name');

		$idrackspaceimage = "";
		while($image = $images->Next()) {
			if($imagename == $image->name) {
				$idrackspaceimage = $image->id;
			}
		}

		$arguments = array ('dcname' => $dcname,
				'username' => $username,
				'imagename' => $imagename,
				'idrackspaceimage' => $idrackspaceimage,);
		$data = $objDcs->addimag($arguments);
	}
	exit();
}
?>

<form>
	<fieldset>
		<legend>Create a New Image from an Existing Server</legend>
		<table>
			<tr>
				<td class="label"><label for="server_DD">Select a Server:</label>
				</td>
				<td><select name="server_DD" id="server_DD">
						<?php
						while($servers = $serv->Next()) {
							$Id = $servers->id;
							$data = $objDcs->getCustomersServers();
							$temp = $data[0];
							if ($temp != "ERR") {
								foreach ($data as $temp) {
									if ($Id == $temp['instance_id']) {
										echo "<option value=".$servers->id.">" . $servers->name . "</option>";
									}
								}
							}
						}
						?>
				</select>*</td>
			</tr>
			<tr>
				<td class="label"><label for="image_name_txt">Image Name:</label>
				</td>
				<td><input name="image_name_txt" id="image_name_txt" type="text" />*
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><input
					id="create_button" value="Create" type="button" />
				</td>
			</tr>
		</table>
	</fieldset>
</form>

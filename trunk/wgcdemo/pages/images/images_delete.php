<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {


	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dc = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$GetServInfoImDeAuth = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $GetServInfoImDeAuth->Compute('cloudServersOpenStack', $dc);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dc) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dc);

		$serv = $cloudservers->ServerList();
		$serv->Sort('name');

		syncImage($cloudservers, $dc);

		$images = $cloudservers->ImageList();
		$images->Sort('name');
	}
}

//CHECK FOR POST REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	//BEGIN CAPTCHA CHECKING
	/*session_start();
	 include_once '../../securimage/securimage.php';
	$securimage = new Securimage();*/

	$DataCenter = $_POST['dcs'];
	$imageId = $_POST['image'];
	$captcha =  $_POST['captcha'];


	/*if ($securimage->check($_POST['captcha']) == false) {
		// the code was incorrect
	echo "The security code entered was incorrect.<br />";
	echo 'Value of DropDown: '. $selected_IMG . "<br />";
	echo 'Value of Captcha: ' . $captcha;
	}else{*/
	//ON CAPTCHA SUCCESS
	//SEND DATA TO DELETE THE SELECTED
	echo "Server is being marked as deleted<br />";
	echo 'Value of DropDown: '. $imageId . "<br />";
	echo 'Value of Captcha: ' . $captcha;

	$objDcs = new Dcs();

	$arguments = array ('idrights' => $DataCenter);
	$data = $objDcs->getApiInfo($arguments);
	$dcname = $data[0]['dcname'];
	/*$apiusername = $data[0]['username'];
		$apikey= $data[0]['apikey'];
	$authurl = $data[0]['authurl'];

	$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
			'username' => $apiusername,
			'apiKey' => $apikey
	));

	$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dcname);*/
	$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dcname) . ".ath";
	$rackspace = file_get_contents($athFile);
	$cloudservers = unserialize($rackspace);

	$image = $cloudservers->Image($imageId);
	$image->Delete();

	$arguments = array ('dcname' => $dcname,
			'username' => $username,
			'idrackspaceimage' => $imageId);
	$data = $objDcs->removimage($arguments);

	//}
	//END CAPTCHA CHECKING
	exit();
}
?>

<form id="delete_image_form">
	<fieldset>
		<legend>Delete an Existing Server</legend>
		<table style="display: inline-block;">
			<tr>
				<td class="label"><label for="image_DD">Select an Image:</label>
				</td>
				<td><select name="image_DD" id="image_DD">
						<?php 
						while($image = $images->Next()) {
							$date = "";

							$Id = $image->id;
							$created = $image->created;
							list($datecreated, $time) = explode("T", $created);
							if ($datecreated != "") {
								$date = date("m/d/Y", strtotime($datecreated));
							}
							$server = $image->server;
							$serverId = "";
							$servername = "";
							if (isset($server)) {
								$data = $objDcs->getCustomersImages();
								$temp = $data[0];
								if ($temp != "ERR") {
									foreach ($data as $temp) {
										if ($Id == $temp['idrackspaceimage']) {
											$serverId = $server->id;
											while($servers = $serv->Next()) {
												$Id = $servers->id;
												if ($Id == $serverId){
													$servername = $servers->name;
												}
											}
											$name = $image->name . " ". $date . " " . $servername;
											echo "<option value=".$image->id.">" . $name . "</option>";
											$serv->Reset();
										}
									}
								}
							}
						}
						?>
				</select>*</td>
			</tr>
			<tr>
				<td class="label"><label for="verify">Verify Intent:</label><br /> <img
					id="captcha" src="securimage/securimage_show.php"
					alt="CAPTCHA Image" />
				</td>
				<td><input type="text" name="captcha_code" size="10" maxlength="6"
					style="left: 0;" /> <a href="#"
					onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><img
						id="captcha_refresh" alt="Refresh"
						src="securimage/images/refresh.png" width="20px" height="20px" />
				</a>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;"><input id="delete_button"
					value="Delete Image" type="button" />
				</td>
			</tr>
		</table>
		<div id="validation"
			style="display: inline-block; vertical-align: top;"></div>
	</fieldset>
</form>

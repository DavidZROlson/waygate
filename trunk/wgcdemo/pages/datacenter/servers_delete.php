<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dc = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$rackspace = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dc);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dc) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dc);

		$serv = $cloudservers->ServerList();
		$serv->Sort('name');

	}
}

//CHECK FOR POST REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	//BEGIN CAPTCHA CHECKING
	/*session_start();
	 include_once '../../securimage/securimage.php';

	$securimage = new Securimage();*/

	$DataCenter = $_POST['dcs'];
	$serverselect = $_POST['server'];
	$captcha =  $_POST['captcha'];

	/*if ($securimage->check($_POST['captcha']) == false) {
	 // the code was incorrect
	echo "The security code entered was incorrect.<br />";
	echo 'Value of DropDown: '. $serverselect . "<br />";
	echo 'Value of Captcha: ' . $captcha;
	}else{*/
	//ON CAPTCHA SUCCESS
	//SEND DATA TO DELETE THE SELECTED
	echo "Server is being marked as deleted<br />";
	echo 'Value of DropDown: '. $serverselect . "<br />";
	echo 'Value of Captcha: ' . $captcha;

	$objDcs = new Dcs();

	$arguments = array ('idrights' => $DataCenter);
	$data = $objDcs->getApiInfo($arguments);
	$dcname = $data[0]['dcname'];
	/*$apiusername = $data[0]['username'];
	 $apikey= $data[0]['apikey'];
	$authurl = $data[0]['authurl'];

	$DeleteServAuth = new OpenCloud \ Rackspace($authurl, array (
			'username' => $apiusername,
			'apiKey' => $apikey
	));

	$cloudservers = $DeleteServAuth->Compute('cloudServersOpenStack', $dcname);*/
	$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dcname) . ".ath";
	$rackspace = file_get_contents($athFile);
	$cloudservers = unserialize($rackspace);

	$server =$cloudservers->Server($serverselect);
	$server->Delete();

	/*$serializeserver=serialize($server);
	 $_SESSION['server']=$serializeserver;
	$_SESSION['terminal'] = 'ACTIVE';
	$_SESSION['timeout'] = 600;
	?>
	<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
	id="imgProgress" />
	<?php
	echo "<script type='text/javascript'>WaitForProgress();</script>\n";

	if ($server->Status() == 'ERROR')
		die("Server create failed with ERROR\n");*/

	$arguments = array ('dcname' => $dcname,
			'username' => $username,
			'instance_id' => $serverselect);
	$data = $objDcs->removserver($arguments);
	//}
	//END CAPTCHA CHECKING


	echo '<p style="position: absolute; top: 50%; left: 50%;">';
	echo 'DataCenter: ' . $DataCenter . '<br />';
	echo 'Selected IMG: ' . $serverselect . '<br />';

	exit();
}
?>

<form id="delete_server_form">
	<fieldset>
		<legend>Delete an Existing Server</legend>
		<table style="display: inline-block;">
			<tr>
				<td class="label"><label for="server_DD">Select a Server:</label>
				</td>
				<td><select name="server_DD" id="server_DD">
						<?php 
						while($servers = $serv->Next()) {
						$Id = $servers->id;
						$data = $objDcs->getCustomersServers();
						$temp = $data[0];
						if ($temp != "ERR") {
							foreach ($data as $temp) {
								if ($Id == $temp['instance_id']) {
									echo "<option value=".$servers->id.">" . $servers->name . "</option>";
								}
							}
						}
					}
					?>
				</select>*</td>
			</tr>
			<tr>
				<td class="label"><label for="verify">Verify Intent:</label><br /> <img
					id="captcha" src="securimage/securimage_show.php"
					alt="CAPTCHA Image" />
				</td>
				<td><input type="text" name="captcha_code" size="10" maxlength="6"
					style="left: 0;" /> <a href="#"
					onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><img
						id="captcha_refresh" alt="Refresh"
						src="securimage/images/refresh.png" width="20px" height="20px" />
				</a>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: right;"><input id="delete_button"
					value="Delete Server" type="button" />
				</td>
			</tr>
		</table>
		<div id="validation"
			style="display: inline-block; vertical-align: top;"></div>
	</fieldset>
</form>

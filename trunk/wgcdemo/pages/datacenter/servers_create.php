<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dc = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$rackspace = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dc);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dc) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dc);

		$serv = $cloudservers->ServerList();
		$serv->Sort('name');

		$flavors = $cloudservers->FlavorList();
		$flavors->Sort('id');

		syncImage($cloudservers, $dc);

		$images = $cloudservers->ImageList();
		$images->Sort('name');

	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	//IF GET REQUEST IS FROM 'dcs'
	if($_POST['name'] != ""){

		$DataCenter = $_POST['dcs'];
		$imageId = $_POST['images'];
		$flavorselect = $_POST['flavor'];
		$servername = $_POST['name'];
		$username = $_SESSION['PSAdminNM'];
		list($flavorid, $ram) = explode("|", $flavorselect);
		$flavorId = (int)$flavorid;

		echo '<p style="position: absolute; top: 50%; left: 50%;">';
		echo 'DataCenter: ' . $DataCenter . '<br />';
		echo 'Selected IMG: ' . $imageId . '<br />';
		echo 'Selected Flavor: ' . $flavorselect . '<br />';
		echo 'Selected Name: ' . $servername . '</p>';

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dcname = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];
			
		$CreateServAuth = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $CreateServAuth->Compute('cloudServersOpenStack', $dcname);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dcname) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		$image = $cloudservers->Image($imageId);
		$flavor = $cloudservers->Flavor($flavorId);
		$server = $cloudservers->Server();
		$server->Create(array(
				'name'=>$servername,
				'image'=>$image,
				'flavor'=>$flavor
		));

		/*$serializeserver=serialize($server);
		 $_SESSION['server']=$serializeserver;
		$_SESSION['terminal'] = 'ACTIVE';
		$_SESSION['timeout'] = 600;
		?>
		<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;"
		id="imgProgress" />
		<?php
		echo "<script type='text/javascript'>WaitForProgress();</script>\n";

		if ($server->Status() == 'ERROR')
			die("Server create failed with ERROR\n");*/

		$new = $cloudservers->Server($server->id);
		$public = $new->accessIPv4;
			
		/*$rackspaceserver = $new-> {'server' };
		 $instance_id = $rackspaceserver-> {'id' };
		$addresses = $rackspaceserver-> {'addresses'};
		$public = $addresses-> {'public' }[0];*/

		$arguments = array ('dcname' => $dcname,
				'username' => $username,
				'servername' => $servername,
				'instance_id' => $server->id,
				'ram' => $ram,
				'ip_address' => $public,);
		$data = $objDcs->adserver($arguments);

	}
	exit();
}

?>

<form id="create_form" name="create_form">
	<fieldset>
		<legend>Create a New Server</legend>
		<table>
			<tr>
				<td class="label"><label for="images">Select an Image:</label>
				</td>
				<td><select name="images" id="images">
						<?php
						while($image = $images->Next()) {
							$date = "";

							$Id = $image->id;
							$created = $image->created;
							list($datecreated, $time) = explode("T", $created);
							if ($datecreated != "") {
								$date = date("m/d/Y", strtotime($datecreated));
							}
							$server = $image->server;
							$serverId = "";
							$servername = "";
							if (isset($server)) {
								$data = $objDcs->getCustomersImages();
								$temp = $data[0];
								if ($temp != "ERR") {
									foreach ($data as $temp) {
										if ($Id == $temp['idrackspaceimage']) {

											$serverId = $server->id;
											while($servers = $serv->Next()) {
												$Id = $servers->id;
												if ($Id == $serverId){
													$servername = $servers->name;
												}
											}
											$name = $image->name . " ". $date . " " . $servername;
											echo "<option value=".$image->id.">" . $name . "</option>";
											$serv->Reset();
										}
									}
								}
							}
						}
						echo "</select>";
						?>
				</select>*</td>
			</tr>
			<tr>
				<td class="label"><label for="flavor">Select a Flavor:</label>
				</td>
				<td><select name="flavor" id="flavor">
						<?
						while($flavor = $flavors->Next()) {
						$item = $flavor->id."|".$flavor->ram;
						echo "<option value=".$item.">" . $flavor->name . "</option>";
					}
					?>
				</select>*</td>
			</tr>
			<tr>
				<td class="label"><label for="server_name_txt">Server Name:</label>
				</td>
				<td><input name="server_name_txt" id="server_name_txt" type="text" />*
				</td>
			</tr>
			<tr>
				<td colspan="2" style="text-align: center;"><input
					id="create_button" value="Create" type="button" />
				</td>
			</tr>
		</table>
	</fieldset>
</form>

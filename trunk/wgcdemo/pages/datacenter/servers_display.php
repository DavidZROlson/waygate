<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
require_once (dirname(__FILE__) . '/../../lib/php-opencloud.php');
require_once (dirname(__FILE__) . '/../../common/common.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	//IF GET REQUEST IS FROM 'dcs'
	if(isset($_GET['dcs'])){
		$DataCenter = $_GET['dcs'];

		$objDcs = new Dcs();

		$arguments = array ('idrights' => $DataCenter);
		$data = $objDcs->getApiInfo($arguments);
		$dcname = $data[0]['dcname'];
		/*$apiusername = $data[0]['username'];
		 $apikey= $data[0]['apikey'];
		$authurl = $data[0]['authurl'];

		$rackspace = new OpenCloud \ Rackspace($authurl, array (
				'username' => $apiusername,
				'apiKey' => $apikey
		));

		$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dcname);*/
		$athFile = dirname(__FILE__). "/../../common/rackspace_" . strtolower($dcname) . ".ath";
		$rackspace = file_get_contents($athFile);
		$cloudservers = unserialize($rackspace);

		syncServer($cloudservers, $dcname);

		$list = $cloudservers->ServerList();
		$list->Sort('name');

		if ($list->Size() > 0) {
			$client_servers = array();
			$i = 0;
			while ($servers = $list->Next()) {
				$Id = $servers->id;
				$name = $servers->name;
				$created = date('Y-m-d H:i:s', strtotime($servers->created));
				$status = $servers->status;
				$public = $servers->accessIPv4;
				$dataaddresses = $servers->addresses;
				$dataprivate = $dataaddresses->private;
				$private = $dataprivate[0]->addr;

				$dataflavor = $servers->flavor;
				$flavorId = $dataflavor->id;
				$flavor = $cloudservers->Flavor($flavorId);
				$ram = $flavor->ram;

				$data = $objDcs->getCustomersServers();
				$temp = $data[0];
				if ($temp != "ERR") {
					foreach ($data as $temp) {
						if ($Id == $temp['instance_id']) {
							for($n = 0; $n < 5; $n++)
							{
								switch($n)
								{
									case 0:
										$client_servers[$i][$n] = $name;
										break;
									case 1:
										$client_servers[$i][$n] = $public;
										break;
									case 2:
										$client_servers[$i][$n] = $private;
										break;
									case 3:
										$client_servers[$i][$n] = $status;
										break;
									case 4:
										$client_servers[$i][$n] = $ram;
										break;
									default:
										break;
								}
							}

						}
					}
				}
				$i++;
			}
		}
	}
}

?>
<table>
	<tbody>
		<tr>
			<th>Server Name</th>
			<th>Primary IP</th>
			<th>Private IP</th>
			<th>Status</th>
			<th>RAM Amount</th>
		</tr>
		<?php

		$result = "";
			
		//OUTPUT A ROW FOR EACH SERVER IN THE ARRAY
		$odd_even = 0;
		foreach($client_servers as $server){
				//seperate rows distictly by color
				if($odd_even % 2 == 0)
				{
					$result.='<tr class="odd">';
				}else{
					$result.='<tr class="even">';
				}
				//display row data
				$result.='
			<td id="server_name_' . $odd_even . '">' . $server[0] . '</td>
				<td id="primary_ip_' . $odd_even . '"><a target="_blank" href="https://' . $server[1] . ':10000">' . $server[1] . '</a></td>
				<td>' . $server[2] . '</td>
				<td>' . $server[3] . '</td>
		 		<td>' . $server[4] . '</td>
				</tr>';
					
				$odd_even++;
			}
			echo($result);
			?>
	</tbody>
</table>

<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$objDcs = new Dcs();
	
	$dcname = $_POST[DC][dc_name];
	$username = $_POST[DC][dc_username];
	$password = $_POST[DC][dc_password];
	$apikey = $_POST[DC][dc_api_key];
	$provider = $_POST[DC][dc_provider];

	$arguments = array (
			'dcname' => $dcname,
			'username' => $username,
			'password' => $password,
			'apikey' => $apikey,
			'vendor' => $provider,
	);
	
	$data = $objDcs->adddcsinfo($arguments);
	//TODO error message from database
	exit();
}
?>

<form id="add_dc_form" name="add_dc_form">
	<div>
		<fieldset>
			<legend>Add New Data Center's Credentials</legend>
			<table>
				<tbody>
					<tr>
						<td class="label"><label for="DC[dc_provider]">Vendor: </label></td>
						<td><select name="DC[dc_provider]" id="dc_provider">
								<option value="">[Vendor...]</option>
								<option value="Rackspace">Rackspace</option>
								<option value="Amazon">Amazon</option>
						</select>
						</td>
					</tr>
					<tr>
						<td class="label"><label for="DC[dc_name]">DC Name: </label></td>
						<td><input id="dc_name" name="DC[dc_name]" type="text"
							placeholder="DC Name" /></td>
					</tr>
					<tr>
						<td class="label"><label for="DC[dc_username]">Username: </label>
						</td>
						<td><input id="dc_username" name="DC[dc_username]" type="text"
							placeholder="Username" /></td>
					</tr>
					<tr>
						<td class="label"><label for="DC[dc_password]">Password: </label>
						</td>
						<td><input id="dc_password" name="DC[dc_password]" type="password"
							placeholder="Passowrd" /></td>
					</tr>
					<tr>
						<td class="label"><label for="DC[dc_api_key]">API Key: </label></td>
						<td><input id="dc_api_key" name="DC[dc_api_key]" type="text"
							placeholder="API Key" /></td>
					</tr>
				</tbody>
			</table>
			<input id="create_btn" value="Create DC" type="button" class="button">
		</fieldset>
	</div>
</form>

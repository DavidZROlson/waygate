<?php
require_once(dirname(__FILE__) . '/../../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	$objUser = new User();

	/****************************************************************************************/
	/************************ ADDED TO CHECK FOR EXISTING USER IN DB	*********************/
	/****************************************************************************************/
	$data = $objUser->getusers();

	//CHECK FOR REQUESSTED USERNAME IN DB ARRAY
	foreach($data as $index => $temp){
		if(strtolower($_POST['username']) == strtolower($temp['username'])){
			$success = false;
			break;
		}
		$success = true;
	}
	echo json_encode(array('success' => $success));
	if($success == false){
		exit();
	}
	/****************************************************************************************/
	/****************************************************************************************/
	/****************************************************************************************/

	$username = $_POST['username'];
	$password = $_POST['password'];

	$arguments = array (
			'username' => $username,
			'password' => $password,
	);

	$data = $objUser->adduserinfo($arguments);
	//Success == $data = 'Susscess';
	echo json_encode(array( 'response' => $data, 'username' => $username));

	exit();
}//END POST

?>

<form id="add_form" name="add_form">
	<fieldset>
		<legend>Add a New User's Credentials</legend>
		<label for="username">Username:</label>
		<div class="input-prepend">
			<span class="add-on"><i class="icon-user"></i> </span> <input
				id="username" name="username" type="text" placeholder="Username"> <span
				class="error" id="username_msg_catch"></span>
		</div>
		<label for="password">Password:</label>
		<div class="input-prepend">
			<span class="add-on"><i class="icon-key"></i> </span> <input
				id="password" name="password" type="password" placeholder="Password">
		</div>
	</fieldset>

	<input id="apply_btn" value="Create User" type="button" class="button">

</form>

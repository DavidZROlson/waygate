<?php

require_once dirname(__FILE__).'/../../../secondgen/secondgen.php';

function httpbodyemlu($url, $type = null ) {
	$parts = explode('/',$url);
	$count = count($parts);
	$split = explode('.',$parts[$count-5]);
	$region = strtoupper($split[0]);
	$str1 = $parts[$count-2];
	$var1 = $parts[$count-1];
	$str2 = $parts[$count-2] . "/" .$parts[$count-1];
	
	// parse the return
	if ($str2 == "servers/detail") {
		$secondgen = new secondgen();
		$rackspace = $secondgen->connectrackspace();
		return json_decode($secondgen->ServerList($rackspace, $region));
	}
	if ($str2 == "flavors/detail") {
		$secondgen = new secondgen();
		$rackspace = $secondgen->connectrackspace();
		return json_decode($secondgen->FlavorList($rackspace));
	}
	if ($str2 == "images/detail") {
		$secondgen = new secondgen();
		$rackspace = $secondgen->connectrackspace();
		return json_decode($secondgen->ImageList($rackspace, $region));
	}
	if ($str1 == "flavors") {
		$secondgen = new secondgen();
		$rackspace = $secondgen->connectrackspace();
		return $secondgen->Flavor($rackspace, $var1);
	}
	if ($str1 == "images") {
		$secondgen = new secondgen();
		$rackspace = $secondgen->connectrackspace();
		if ($type == "DELETE") {
			return $secondgen->DeleteImage($rackspace, $var1);
		} else {
			return $secondgen->Image($rackspace, $var1);
		}
	}
	if ($str1 == "servers") {
		$secondgen = new secondgen();
		$rackspace = $secondgen->connectrackspace();
		if ($type == "DELETE") {
			return $secondgen->DeleteServer($rackspace, $var1);
		} else {
			return $secondgen->Server($rackspace, $var1);
		}
	}
}
?>
<?php

class User extends Admin
{
	public function adduserinfo($arguments) {
		return $this->adduser($arguments);
	}

	public function edituserinfo($arguments){
		return $this->edituser($arguments);
	}

	public function edituseright($arguments){
		return $this->edituserrights($arguments);
	}
	
	public function getRightsInformation($arguments){
		return $this->getRightsInfo($arguments);
	}

	public function getUserRights($arguments){
		return $this->getUserRight($arguments);
	}

	public function getusers(){
		return $this->retrieveUsers();
	}

	public function loaduserrights($arguments){
		return $this->loadrights($arguments);
	}

	public function retrievedcs($arguments) {
		return $this->getDataCenters($arguments);
	}
}
?>
<?php
require_once dirname(__FILE__) . '/AbstractServer.php';

function p($str,$exit=0){
	echo("<pre>");print_r($str);echo("</pre>");
	if($exit!=0) {
		exit();
	}
}

#==TO SET MESSAGE IN TO COOKIE OR SESSION==#
#==STATUS MAY BE success/error/warning==#
function setmessage($msg,$status='success'){
	setcookie("msg",$msg);
	setcookie("msgclass",$status);
}
#==TO GET MESSAGE FROM COOKIE OR SESSION==#

function getmessage(){

	if($_COOKIE['msg']!=''){

		$msg=$_COOKIE['msg'];
		$msgclass=$_COOKIE['msgclass'];

		if($msgclass=='success')
		{
			$class="valid_box";
		}
		elseif($msgclass=='error')
		{
			$class="error_box";
		}
		elseif($msgclass=='warning'){
			$class="warning_box";
		}

		setcookie("msg","",time()-24);
		setcookie("msgclass","",time()-24);
		$data='<div class="'.$class.'">'.$msg.'</div>';
	}

	return $data;
}

#===Logger function by calling this function you can see output of a variable in a console panel of firebug===#
function l($var,$newvar=""){
	$log = FirePHP::getInstance(true);
	$log->log($var,$newvar);
}

function __autoload($class){
	$class=strtolower($class);
	if (file_exists(dirname(__FILE__) . '/class.'.$class.'.php'))
	{
		require_once dirname(__FILE__) . '/class.'.$class.'.php';
	}
	else{
		echo("Class ".$class." file does not exist!!");exit;
	}
}

class Admin extends AbstractServer
{
	public
	$result;
	public function __construct()
	{
		parent::__construct();
		$this->result = NULL;
		return true;
	}
}//END
?>

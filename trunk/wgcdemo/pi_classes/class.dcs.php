<?php

class Dcs extends Admin
{
	public function adbackup($arguments){
		$this->addbackup($arguments);
	}

	public function adddcsinfo($arguments){
		return $this->adddcs($arguments);
	}

	public function addimag($arguments) {
		$this->addimage($arguments);
	}

	public function addlbel($arguments){
		return $this->addlabel($arguments);
	}

	public function addservermont($arguments){
		return $this->addservermonitored($arguments);
	}

	public function adserver($arguments){
		return $this->addserver($arguments);
	}

	public function createtmpimagetable(){
		$this->createtempimagetable();
	}

	public function createtmpservertable(){
		$this->createtempservertable();
	}

	public function editdcsinfo($arguments){
		return $this->editdcs($arguments);
	}

	public function editlbel($arguments){
		return $this->editlabel($arguments);
	}

	public function getApiInfo($arguments){
		return $this->getApiData($arguments);
	}

	public function getcostdat($arguments) {
		return $this->getcostdata($arguments);
	}

	public function getCustomersImages() {
		return $this->getCustsImages();
	}
	
	public function getCustomersServers() {
		return $this->getCustsServers();
	}

	public function getDCInformation($arguments) {
		return $this->getDCInfo($arguments);
	}

	public function getdcsnames($arguments) {
		return $this->getDcsName($arguments);
	}
	
	public function getLabelDat($arguments){
		return $this->getLabelData($arguments);
	}

	public function getlbels() {
		return $this->getlabels();
	}

	public function getservermonitor($arguments) {
		return $this->getservermont($arguments);
	}

	public function loadtmpimagetable($arguments){
		$this->loadtempimagetable($arguments);
	}

	public function loadtmpservertable($arguments){
		$this->loadtempservertable($arguments);
	}

	public function removimage($arguments){
		$this->removeimage($arguments);
	}

	public function removserver($arguments){
		$this->removeserver($arguments);
	}

	public function resizserver($arguments){
		$this->resizeserver($arguments);
	}

	public function retrievedcs() {
		$arguments = array ('mode' => "notall");

		return $this->getDCs($arguments);
	}

	public function synimagedatbase($arguments){
		$this->synimagedatabase($arguments);
	}

	public function synserverdatbase($arguments){
		$this->synserverdatabase($arguments);
	}

	public function updataccount($arguments){
		$this->updateaccount($arguments);
	}
}
?>
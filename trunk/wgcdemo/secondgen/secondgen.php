<?php
include_once 'secondgenmysql.php';

ini_set ( 'display_errors', on );
ini_set ( "precision", 25 );

class secondgen extends secondgenmysql {

	public function __construct() {


	}

	private function bcdechex($dec) {

		$last = bcmod ( $dec, 16 );
		$remain = bcdiv ( bcsub ( $dec, $last ), 16 );

		if ($remain == 0) {
			return dechex ( $last );
		} else {
			return $this->bcdechex ( $remain ) . dechex ( $last );
		}

	}

	private function bchexdec($hex) {

		if (strlen ( $hex ) == 1) {
			return hexdec ( $hex );
		} else {
			$remain = substr ( $hex, 0, - 1 );
			$last = substr ( $hex, - 1 );
			return bcadd ( bcmul ( 16, $this->bchexdec ( $remain ) ), hexdec ( $last ) );
		}

	}

	public function connectrackspacedb() {

		return $this->connectrackspace ();

	}

	public function Create($secondgen, $var) {

		$name = $this->quotes_to_entities ( $secondgen, $var ['name'] );
		$imageid = $this->quotes_to_entities ( $secondgen, $var ['imageid'] );
		$flavorid = $this->quotes_to_entities ( $secondgen, $var ['flavorid'] );
		$region = $this->quotes_to_entities ( $secondgen, $var ['region'] );

		$sql = "SELECT * FROM `common` WHERE idcommon = 1";
		$sqlresult = $this->select ( $secondgen, $sql );

		$tenant_id = $sqlresult [0] ["tenant_id"];
		$user_id = $sqlresult [0] ["user_id"];
		$status = $sqlresult [0] ["status"];
		$progress = $sqlresult [0] ["progress"];

		$created = gmdate("Y-m-d H:i:s");

		$updated = gmdate("Y-m-d H:i:s", strtotime('+5 minutes', strtotime($created)));

		$id = $this->getServerId ($secondgen);

		$hostid = $this->getHostId ($secondgen);

		$accessIPv4 = $this->getIPv4($secondgen);

		$accessIPv6 = $this->getIPv6($secondgen);

		$private = $this->getPrivateIP($secondgen);

		$sql = "INSERT INTO `servers` (id, tenant_id, user_id, name, updated,";
		$sql .= " created, hostId, status, progress, accessIPv4, accessIPv6, region) ";
		$sql .= "VALUES ('$id', $tenant_id, $user_id, '$name', '$updated', '$created',";
		$sql .= " '$hostid', '$status', $progress, '$accessIPv4', '$accessIPv6', '$region')";

		$this->insert($secondgen, $sql);

		$sql = "INSERT INTO `public` (serverid, version, addr) VALUES ";
		$sql .= "('$id', 4, '$accessIPv4'), ('$id', 6, '$accessIPv6')";

		$this->insert($secondgen, $sql);

		$sql = "INSERT INTO `private` (serverid, version, addr) VALUES ";
		$sql .= "('$id', 4, '$private')";

		$this->insert($secondgen, $sql);

		$sql = "INSERT INTO `servflavor` (serverid, flavorid, date) VALUES ";
		$sql .= "('$id', $flavorid, '$created')";

		$this->insert($secondgen, $sql);

		$sql = "INSERT INTO `servimage` (serverid, imageid) VALUES ";
		$sql .= "('$id', '$imageid')";

		$this->insert($secondgen, $sql);

		$output = array ();

		$newserver ["id"] = $id;
		$newserver ["adminPass"] = "QF838fsisVCE";
		$output ["server"] = $newserver;

		return json_encode ( $output );
	}

	public function CreateImage($secondgen, $var) {
		$serverid = $this->quotes_to_entities ( $secondgen, $var ['id'] );
		$name = $this->quotes_to_entities ( $secondgen, $var ['name'] );
		$flavorid = $this->quotes_to_entities ( $secondgen, $var ['flavorid'] );
		$region = $this->quotes_to_entities ( $secondgen, $var ['region'] );
		
		$sql = "SELECT * FROM `common` WHERE idcommon = 1";
		$sqlresult = $this->select ( $secondgen, $sql );

		$status = $sqlresult [0] ["status"];
		$progress = $sqlresult [0] ["progress"];
		
		$created = gmdate("Y-m-d H:i:s");
		
		$updated = gmdate("Y-m-d H:i:s", strtotime('+5 minutes', strtotime($created)));
		
		$id = $this->getImageId($secondgen);
		
		$sql = "SELECT ram, disk FROM `flavors` WHERE id = $flavorid";
		$sqlresult = $this->select ( $secondgen, $sql );
		
		$minRam = $sqlresult [0] ["ram"];
		$minDisk = $sqlresult [0] ["disk"];
		
		$sql = "INSERT INTO `images` (id, name, updated, created, status, progress, ";
		$sql .= "minDisk, minRam, region) VALUES ('$id', '$name', '$updated', '$created',";
		$sql .= " '$status', $progress, $minDisk , $minRam, '$region')";
		
		$this->insert($secondgen, $sql);
		
		$sql = "INSERT INTO `imageserv` (imageid, serverid) VALUES ";
		$sql .= "('$id', '$serverid')";
		
		$this->insert($secondgen, $sql);
	}
	
	public function DeleteImage($secondgen, $var) {
		$deleted = gmdate("Y-m-d H:i:s");
		$sqlupdate = "UPDATE `images` ";
		$sqlupdate .= "SET status = 'DELETED', updated = '$deleted' ";
		$sqlupdate .= "WHERE id = '$var'";
			
		$this->update($secondgen, $sqlupdate);
	}
	
	public function DeleteServer($secondgen, $var) {
		$deleted = gmdate("Y-m-d H:i:s");
		$sqlupdate = "UPDATE `servers` ";
		$sqlupdate .= "SET status = 'DELETED', deleted = '$deleted' ";
		$sqlupdate .= "WHERE id = '$var'";
			
		$this->update($secondgen, $sqlupdate);
	}

	public function Flavor($secondgen, $var) {

		$output = array ();
		$newflavor = array ();

		$sql = "SELECT id, name, ram, disk, vcpus, rxtx_factor, swap ";
		$sql .= "FROM `flavors` WHERE id = $var";
		$sqlresult = $this->select ( $secondgen, $sql );

		$flavor = $sqlresult [0];
		$newflavor ["name"] = $flavor ["name"];
		$newflavor ["ram"] = $flavor ["ram"];
		$newflavor ["vcpus"] = $flavor ["vcpus"];
		$newflavor ["swap"] = $flavor ["swap"];
		$newflavor ["rxtx_factor"] = $flavor ["rxtx_factor"];
		$newflavor ["disk"] = $flavor ["disk"];
		$newflavor ["id"] = $flavor ["id"];

		$output ["flavor"] = $newflavor;

		return json_encode ( $output );

	}

	public function FlavorList($secondgen) {

		$output = array ();
		$list = array ();
		$newflavors = array ();

		$sql = "SELECT id, name, ram, disk, vcpus, rxtx_factor, swap FROM `flavors`";
		$sqlresult = $this->select ( $secondgen, $sql );
		$x = 0;

		foreach ( $sqlresult as $flavors ) {
			$newflavors ["name"] = $flavors ["name"];
			$newflavors ["ram"] = $flavors ["ram"];
			$newflavors ["vcpus"] = $flavors ["vcpus"];
			$newflavors ["swap"] = $flavors ["swap"];
			$newflavors ["rxtx_factor"] = $flavors ["rxtx_factor"];
			$newflavors ["disk"] = $flavors ["disk"];
			$newflavors ["id"] = $flavors ["id"];

			$list [$x] = $newflavors;
			$x ++;
		}

		$output ["flavors"] = $list;

		return json_encode ( $output );

	}

	private function getHostId($secondgen) {

		$sql = "SELECT  max(hostId) AS hostid FROM `servers`";
		$sqlresult = $this->select ( $secondgen, $sql );
		$hostidtemp = $sqlresult [0] ["hostid"];

		return $this->bcdechex (bcadd($this->bchexdec ( $hostidtemp ), 100100100100));
	}

	private function getIPv4($secondgen) {
		$sql = "SELECT  max(one.num) AS num1, max(two.num) AS num2, ";
		$sql .= "max(three.num) AS num3, max(four.num) AS num4 ";
		$sql .= "FROM `ipv4_1` one, `ipv4_2` two, `ipv4_3` three, `ipv4_4` four";
		$sqlresult = $this->select ( $secondgen, $sql );

		$first = $sqlresult [0] ["num1"];
		$second = $sqlresult [0] ["num2"];
		$third = $sqlresult [0] ["num3"];
		$fourth = $sqlresult [0] ["num4"];

		$ipv4 = $first . "." . $second . "." . $third . "." . $fourth;

		if ($fourth == 254) {
			$fourth =9;
			$third +=1;
			$sql = "INSERT INTO `ipv4_3` (num) VALUES ($third)";
			$this->insert($secondgen, $sql);
		}
		$fourth += 1;

		$sql = "INSERT INTO `ipv4_4` (num) VALUES ($fourth)";
		$this->insert($secondgen, $sql);

		return $ipv4;
	}

	private function getIPv6($secondgen) {
		$sql = "SELECT max(one.hex) AS hex1, max(two.hex) AS hex2, max(three.hex) AS hex3,";
		$sql .= " max(four.hex) AS hex4, max(five.hex) AS hex5, max(five.hex) AS hex5,";
		$sql .= " max(six.hex) AS hex6, max(seven.hex) AS hex7, max(eight.hex) AS hex8 ";
		$sql .= "FROM `ipv6_1` one, `ipv6_2` two, `ipv6_3` three, `ipv6_4` four, ";
		$sql .= "`ipv6_5` five, `ipv6_6` six, `ipv6_7` seven, `ipv6_8` eight";
		$sqlresult = $this->select ( $secondgen, $sql );

		$first = $sqlresult [0] ["hex1"];
		$second = $sqlresult [0] ["hex2"];
		$third = $sqlresult [0] ["hex3"];
		$fourth = $sqlresult [0] ["hex4"];
		$fifth = $sqlresult [0] ["hex5"];
		$sixth = $sqlresult [0] ["hex6"];
		$seventh = $sqlresult [0] ["hex7"];
		$eighth = $sqlresult [0] ["hex8"];

		$ip6 = $first . ":" . $second . ":" . $third . ":" . $fourth . ":";
		$ip6 .= $fifth . ":" . $sixth . ":" . $seventh . ":" . $eighth;

		if ($eighth == "fffe") {
			$eighth = 9;
			$fourth = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $fourth )) + 1 ));
			$sql = "INSERT INTO `ipv6_4` (hex) VALUES ($fourth)";
			$this->insert($secondgen, $sql);
		}

		$eighth = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $eighth )) + 1 ));
		$sql = "INSERT INTO `ipv6_8` (hex) VALUES ('$eighth')";
		$this->insert($secondgen, $sql);

		return $ip6;
	}

	private function getServerId($secondgen) {

		$sql = "SELECT  max(id) AS id FROM `servers`";
		$sqlresult = $this->select ( $secondgen, $sql );
		$idtemp = $sqlresult [0] ["id"];

		$idparts = explode ( '-', $idtemp );

		$first = sprintf("%08s", $this->bcdechex ( ( $this->bchexdec ( $idparts [0] )) + 10 ));
		$second = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $idparts [1] )) + 10 ));
		$third = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $idparts [2] )) + 10 ));
		$fourth = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $idparts [3] )) + 10 ));
		$fifth = sprintf("%012s", $this->bcdechex ( ( $this->bchexdec ( $idparts [4] )) + 10 ));


		$id = $first . "-" . $second . "-" . $third;
		$id .= "-" . $fourth . "-" . $fifth;

		return $id;
	}

	private function getImageId($secondgen) {
	
		$sql = "SELECT  max(id) AS id FROM `images`";
		$sqlresult = $this->select ( $secondgen, $sql );
		$idtemp = $sqlresult [0] ["id"];
	
		$idparts = explode ( '-', $idtemp );
	
		$first = sprintf("%08s", $this->bcdechex ( ( $this->bchexdec ( $idparts [0] )) + 15 ));
		$second = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $idparts [1] )) + 15 ));
		$third = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $idparts [2] )) + 15 ));
		$fourth = sprintf("%04s", $this->bcdechex ( ( $this->bchexdec ( $idparts [3] )) + 15 ));
		$fifth = sprintf("%012s", $this->bcdechex ( ( $this->bchexdec ( $idparts [4] )) + 15 ));
	
	
		$id = $first . "-" . $second . "-" . $third;
		$id .= "-" . $fourth . "-" . $fifth;
	
		return $id;
	}
	
	private function getPrivateIP($secondgen) {
		$sql = "SELECT  max(one.num) AS num1, max(two.num) AS num2, ";
		$sql .= "max(three.num) AS num3, max(four.num) AS num4 ";
		$sql .= "FROM `privateip_1` one, `privateip_2` two, `privateip_3` ";
		$sql .= "three, `privateip_4` four";
		$sqlresult = $this->select ( $secondgen, $sql );

		$first = $sqlresult [0] ["num1"];
		$second = $sqlresult [0] ["num2"];
		$third = $sqlresult [0] ["num3"];
		$fourth = $sqlresult [0] ["num4"];

		$ipv4 = $first . "." . $second . "." . $third . "." . $fourth;

		if ($fourth == 254) {
			$fourth =9;
			$third +=1;
			$sql = "INSERT INTO `privateip_3` (num) VALUES ($third)";
			$this->insert($secondgen, $sql);
		}
		$fourth += 1;

		$sql = "INSERT INTO `privateip_4` (num) VALUES ($fourth)";
		$this->insert($secondgen, $sql);

		return $ipv4;

	}

	public function Image($secondgen, $var) {

		$output = array ();
		$newimage = array ();

		$sql = "SELECT id, name, updated, created, status, progress, minDisk, ";
		$sql .= "minRam FROM `images` WHERE id = '$var'";
		$sqlresult = $this->select ( $secondgen, $sql );

		$image = $sqlresult [0];
		$newimage ["status"] = $image ["status"];
		$newimage ["updated"] = $image ["updated"];
		$newimage ["id"] = $image ["id"];
		$newimage ["name"] = $image ["name"];
		$newimage ["created"] = $image ["created"];
		$newimage ["minDisk"] = $image ["minDisk"];

		$sql = "SELECT serverid FROM imageserv WHERE imageid = '$var'";
		$sqlresult = $this->select ( $secondgen, $sql );

		if (is_array ( $sqlresult ) && $sqlresult [0] != "ERR") {
			$newserver = array ();
			$newserver ["id"] = $sqlresult [0] ["serverid"];
			$newimage ["server"] = $newserver;
		}

		$newimage ["progress"] = $image ["progress"];
		$newimage ["minRam"] = $image ["minRam"];

		$output ["image"] = $newimage;

		return json_encode ( $output );

	}

	public function ImageList($secondgen, $region) {

		$output = array ();
		$list = array ();

		$sql = "SELECT id, name, updated, created, status, progress, minDisk, ";
		$sql .= "minRam FROM `images` WHERE status != 'DELETED' AND (region = 'ALL' ";
		$sql .= "OR region = '$region')";
		$sqlresult = $this->select ( $secondgen, $sql );
		$x = 0;

		foreach ( $sqlresult as $images ) {
			$newimages = array ();
			$id = $images ["id"];
			$newimages ["status"] = $images ["status"];
			$newimages ["updated"] = $images ["updated"];
			$newimages ["id"] = $images ["id"];
			$newimages ["name"] = $images ["name"];
			$newimages ["created"] = $images ["created"];
			$newimages ["minDisk"] = $images ["minDisk"];

			$sql = "SELECT serverid FROM imageserv WHERE imageid = '$id'";
			$sqlresult = $this->select ( $secondgen, $sql );

			if (is_array ( $sqlresult ) && $sqlresult [0] != "ERR") {
				$newserver = array ();
				$newserver ["id"] = $sqlresult [0] ["serverid"];
				$newimages ["server"] = $newserver;
			}

			$newimages ["progress"] = $images ["progress"];
			$newimages ["minRam"] = $images ["minRam"];

			$list [$x] = $newimages;
			$x ++;
		}

		$output ["images"] = $list;

		return json_encode ( $output );

	}

	public function Resize($secondgen, $var) {

		$serverid = $this->quotes_to_entities ( $secondgen, $var ['serverid'] );
		$flavorid = $this->quotes_to_entities ( $secondgen, $var ['flavorid'] );

		$sql = "INSERT INTO `servflavor` (serverid, flavorid, date) VALUES ";
		$sql .= "('$serverid', $flavorid, UTC_TIMESTAMP())";

		$temp = $this->insert($secondgen, $sql);
	}

	public function Server($secondgen, $var) {

		$output = array ();
		$newserver = array ();
		$newimage = array ();
		$newflavor = array ();
		$addresses = array ();

		$sql = "SELECT id, tenant_id, user_id, name, updated, created, hostId, ";
		$sql .= "status, progress, accessIPv4, accessIPv6 FROM `servers`";
		$sql .= " WHERE id = '$var' AND status != 'DELETED'";
		$sqlresult = $this->select ( $secondgen, $sql );

		$server = $sqlresult [0];
		$newserver ["status"] = $server ["status"];
		$newserver ["updated"] = $server ["updated"];
		$newserver ["hostId"] = $server ["hostId"];

		$sql = "SELECT version, addr FROM public WHERE serverid = '$var'";
		$sqlresult = $this->select ( $secondgen, $sql );

		$publicip = array ();
		$countpublic = count ( $sqlresult );
		for($y = 0; $y < $countpublic; $y ++) {
			$temp = array ();
			$address = $sqlresult [$y];
			$temp ["version"] = $address ["version"];
			$temp ["addr"] = $address ["addr"];
			$publicip [$y] = $temp;
		}

		$addresses ["public"] = $publicip;

		$sql = "SELECT version, addr FROM private WHERE serverid = '$var'";
		$sqlresult = $this->select ( $secondgen, $sql );

		$privateip = array ();
		$countprivate = count ( $sqlresult );
		for($y = 0; $y < $countprivate; $y ++) {
			$temp = array ();
			$address = $sqlresult [$y];
			$temp ["version"] = $address ["version"];
			$temp ["addr"] = $address ["addr"];
			$privateip [$y] = $temp;
		}

		$addresses ["private"] = $privateip;

		$newserver ["addresses"] = $addresses;

		$sql = "SELECT imageid FROM servimage WHERE serverid = '$var'";
		$sqlresult = $this->select ( $secondgen, $sql );

		$newimage ["id"] = $sqlresult [0] ["imageid"];
		$newserver ["image"] = $newimage;

		$sql = "SELECT flavorid FROM servflavor WHERE serverid = '$var'";
		$sqlresult = $this->select ( $secondgen, $sql );

		$newflavor ["id"] = $sqlresult [0] ["flavorid"];
		$newserver ["flavor"] = $newflavor;

		$newserver ["id"] = $var;
		$newserver ["user_id"] = $server ["user_id"];
		$newserver ["name"] = $server ["name"];
		$newserver ["created"] = $server ["created"];
		$newserver ["tenant_id"] = $server ["tenant_id"];
		$newserver ["accessIPv4"] = $server ["accessIPv4"];
		$newserver ["accessIPv6"] = $server ["accessIPv6"];
		$newserver ["progress"] = $server ["progress"];

		$output ["server"] = $newserver;

		return json_encode ( $output );

	}

	public function ServerList($secondgen, $region) {

		$output = array ();
		$list = array ();
		$newservers = array ();
		$newimage = array ();
		$newflavor = array ();
		$addresses = array ();

		$sql = "SELECT id, tenant_id, user_id, name, updated, created, hostId, ";
		$sql .= "status, progress, accessIPv4, accessIPv6 FROM `servers`";
		$sql .= " WHERE status != 'DELETED' AND region = '$region'";
		$sqlresult = $this->select ( $secondgen, $sql );
		$x = 0;

		if (is_array ( $sqlresult ) && $sqlresult [0] != "ERR") {
			foreach ( $sqlresult as $servers ) {
				$id = $servers ["id"];
				$newservers ["status"] = $servers ["status"];
				$newservers ["updated"] = $servers ["updated"];
				$newservers ["hostId"] = $servers ["hostId"];

				$sql = "SELECT version, addr FROM public WHERE serverid = '$id'";
				$sqlresult = $this->select ( $secondgen, $sql );

				$publicip = array ();
				$countpublic = count ( $sqlresult );
				for($y = 0; $y < $countpublic; $y ++) {
					$temp = array ();
					$address = $sqlresult [$y];
					$temp ["version"] = $address ["version"];
					$temp ["addr"] = $address ["addr"];
					$publicip [$y] = $temp;
				}

				$addresses ["public"] = $publicip;

				$sql = "SELECT version, addr FROM private WHERE serverid = '$id'";
				$sqlresult = $this->select ( $secondgen, $sql );

				$privateip = array ();
				$countprivate = count ( $sqlresult );
				for($y = 0; $y < $countprivate; $y ++) {
					$temp = array ();
					$address = $sqlresult [$y];
					$temp ["version"] = $address ["version"];
					$temp ["addr"] = $address ["addr"];
					$privateip [$y] = $temp;
				}

				$addresses ["private"] = $privateip;

				$newservers ["addresses"] = $addresses;

				$sql = "SELECT imageid FROM servimage WHERE serverid = '$id'";
				$sqlresult = $this->select ( $secondgen, $sql );

				$newimage ["id"] = $sqlresult [0] ["imageid"];
				$newservers ["image"] = $newimage;

				$sql = "SELECT flavorid FROM servflavor WHERE serverid = '$id' ";
				$sql .= "AND date = (SELECT MAX(date) FROM servflavor WHERE serverid = '$id')";
				$sqlresult = $this->select ( $secondgen, $sql );

				$newflavor ["id"] = $sqlresult [0] ["flavorid"];
				$newservers ["flavor"] = $newflavor;

				$newservers ["id"] = $id;
				$newservers ["user_id"] = $servers ["user_id"];
				$newservers ["name"] = $servers ["name"];
				$newservers ["created"] = $servers ["created"];
				$newservers ["tenant_id"] = $servers ["tenant_id"];
				$newservers ["accessIPv4"] = $servers ["accessIPv4"];
				$newservers ["accessIPv6"] = $servers ["accessIPv6"];
				$newservers ["progress"] = $servers ["progress"];

				$list [$x] = $newservers;
				$x ++;
			}
			$output ["servers"] = $list;
		}
		return json_encode ( $output );

	}

	// function to sanitize user inserted harmful data
	protected function quotes_to_entities($db, $str) {

		return mysqli_real_escape_string ( $db, $str );

	}

}
?>

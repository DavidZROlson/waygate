<?php
require_once('../pi_classes/Admin.php');
$obj = new Admin();

require_once('../includes/headersignup.php');
require_once('../common/common.php');
require_once 'ccvalidator.class.php';
?>

<html>
<body>
	<?php 

	if (isset ($_POST['cancel'])) {
		echo $_POST['cancel'] . "<br>";
		exit();
	}

	if (isset ($_POST['back'])) {
		$company = $_POST['company'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$mailing_address1 = $_POST['mailing_address1'];
		$mailing_address2 = $_POST['mailing_address2'];
		$mailing_city = $_POST['mailing_city'];
		$mailing_state = $_POST['mailing_state'];
		$mailing_zip = $_POST['mailing_zip'];
		$main_business_phone = $_POST['main_business_phone'];
		$main_home_phone = $_POST['main_home_phone'];
		$main_cell_phone = $_POST['main_cell_phone'];
		$email = $_POST['email'];
	}

	if (isset ($_POST['next1'])) {
		$next1 = $_POST['next1'];
		$company = $_POST['company'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$mailing_address1 = $_POST['mailing_address1'];
		$mailing_address2 = $_POST['mailing_address2'];
		$mailing_city = $_POST['mailing_city'];
		$mailing_state = $_POST['mailing_state'];
		$mailing_zip = $_POST['mailing_zip'];
		$main_business_phone = $_POST['main_business_phone'];
		$main_home_phone = $_POST['main_home_phone'];
		$main_cell_phone = $_POST['main_cell_phone'];
		$email = $_POST['email'];

		$sp_mailing_zip = explode('-', $mailing_zip);
		$poszip = strpos($sp_mailing_zip[0], '_');
		$posmbp = strpos($main_business_phone, '_');
		$posmhp = strpos($main_home_phone, '_');
		$posmcp = strpos($main_cell_phone, '_');
		$posemail = strpos($email, '@');

		if (empty($company)) {
			confirm("Please enter a Company Name");
			$next1="";
		} elseif (empty($first_name)){
			confirm("Please enter a First Name");
			$next1="";
		} elseif (empty($last_name)){
			confirm("Please enter a Last Name");
			$next1="";
		} elseif (empty($mailing_address1)){
			confirm("Please enter a Address");
			$next1="";
		} elseif (empty($mailing_city)){
			confirm("Please enter a City");
			$next1="";
		} elseif (empty($mailing_state)){
			confirm("Please enter a State");
			$next1="";
		} elseif ($mailing_zip == "_____-____"){
			confirm("Please enter Zip Code");
			$next1="";
		} elseif($poszip !== false) {
			confirm("Please enter a Correct Zip Code");
			$next1="";
		} elseif ($main_business_phone == "(___) ___-____" && $main_home_phone == "(___) ___-____" && $main_cell_phone == "(___) ___-____"){
			confirm("Please enter at least one Phone number");
			$next1="";
		} elseif ($posmbp !== false && $posmhp !== false && $posmcp !== false){
			confirm("Please enter a Complete Phone number");
			$next1="";
		} elseif (empty($email)){
			confirm("Please enter a Email address");
			$next1="";
		} elseif($posemail === false) {
			confirm("Please enter a Valid Email Address");
			$next1="";
		}

		$pos = strpos($sp_mailing_zip[1], '_');
		if($pos !== false) {
			$mailing_zip = $sp_mailing_zip[0];
		}
	}

	if (isset ($_POST['save'])) {

		$objSignup = new Signup();
		$objlogin= new Login();

		$save = $_POST['save'];
		$company = $_POST['company'];
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$mailing_address1 = $_POST['mailing_address1'];
		$mailing_address2 = $_POST['mailing_address2'];
		$mailing_city = $_POST['mailing_city'];
		$mailing_state = $_POST['mailing_state'];
		$mailing_zip = $_POST['mailing_zip'];
		$main_business_phone = $_POST['main_business_phone'];
		$main_home_phone = $_POST['main_home_phone'];
		$main_cell_phone = $_POST['main_cell_phone'];
		$email = $_POST['email'];

		$contact_first_name = $_POST['contact_first_name'];
		$contact_last_name = $_POST['contact_last_name'];
		$bill_address1 = $_POST['bill_address1'];
		$bill_address2 = $_POST['bill_address2'];
		$bill_city = $_POST['bill_city'];
		$bill_state = $_POST['bill_state'];
		$bill_zip = $_POST['bill_zip'];
		$contact_business_phone = $_POST['contact_business_phone'];
		$contact_home_phone = $_POST['contact_home_phone'];
		$contact_cell_phone = $_POST['contact_cell_phone'];
		$bill_email = $_POST['bill_email'];

		$username= $_POST['username'];
		$password= $_POST['password'];

		$card_name= $_POST['card_name'];
		$card_number = $_POST['card_number'];
		$card_exp = $_POST['card_exp'];

		$name = $first_name . " " . $last_name;
		$number = str_replace(array('-'), '', $card_number);
		$sp_card_exp = explode('/', $card_exp);
		$month = intval($sp_card_exp[0]);
		$year = intval($sp_card_exp[1]);

		if ($card_name == "VISA") {
			$card = CCV_VISA;
		} elseif ($card_name == "MASTERCARD") {
			$card = CCV_MASTER_CARD;
		} elseif ($card_name == "DISCOVER") {
			$card = CCV_DISCOVER;
		}

		$ccv = new CCValidator($name, $card, $number, $month, $year);

		$arguments = array ('username' => $username,
				'password' => $password,);
		$duplicate = $objSignup->usernamepassword($arguments);

		$sp_bill_zip = explode('-', $bill_zip);
		$poszip = strpos($sp_bill_zip[0], '_');
		$poscbp = strpos($contact_business_phone, '_');
		$poschp = strpos($contact_home_phone, '_');
		$posccp = strpos($contact_cell_phone, '_');
		$posemail = strpos($bill_email, '@');

		if ($bill_zip == "_____-____"){
			confirm("Please enter Zip Code");
			$next1="Next";
		} elseif ($poszip !== false) {
			confirm("Please enter a Correct Zip Code");
			$next1="Next";
		} elseif ($contact_business_phone == "(___) ___-____" && $contact_home_phone == "(___) ___-____" && $contact_cell_phone == "(___) ___-____"){
			confirm("Please enter at least one Phone number");
			$next1="Next";
		} elseif ($poscbp !== false && $poschp !== false && $posccp !== false){
			confirm("Please enter a Complete Phone number");
			$next1="";
		} elseif (empty($bill_email)){
			confirm("Please enter a Email address");
			$next1="Next";
		} elseif($posemail === false) {
			confirm("Please enter a Valid Email Address");
			$next1="Next";
		}elseif (empty($username)) {
			confirm("Please enter a Username");
			$next1="Next";
		} elseif (empty($password)){
			confirm("Please enter a Password");
			$next1="Next";
		} elseif ($duplicate == "true"){
			confirm("Username and Password pair already exists. Please enter a different Username or Password");
			$next1="Next";
		} elseif (empty($card_name)){
			confirm("Please enter Credit Card Name");
			$next1="Next";
		} elseif (empty($card_number)){
			confirm("Please enter Credit Card Number");
			$next1="Next";
		} elseif (empty($card_exp)){
			confirm("Please enter Expiration Date");
			$next1="Next";
		} elseif (empty($card_exp)){
			confirm("Please enter Expiration Date");
			$next1="Next";
		} elseif ($validCard = $ccv->validate()){
			if ($validCard & CCV_RES_ERR_HOLDER)
			{
				confirm("Card holder\'s name is missing or incorrect.");
			}

			if ($validCard & CCV_RES_ERR_TYPE)
			{
				confirm("Incorrect credit card type.");
			}

			if ($validCard & CCV_RES_ERR_DATE)
			{
				confirm("Incorrect expiration date.");
			}

			if ($validCard & CCV_RES_ERR_FORMAT)
			{
				confirm("Incorrect credit card number format.");
			}

			if ($validCard & CCV_RES_ERR_NUMBER)
			{
				confirm("Invalid credit card number.");
			}
			$next1="Next";
		} else {


			$pos = strpos($sp_bill_zip[1], '_');
			if($pos !== false) {
				$bill_zip = $sp_bill_zip[0];
			}

			$posmbp = strpos($main_business_phone, '_');
			if($posmbp !== false) {
				$main_business_phone = "";
			}
			$posmhp = strpos($main_home_phone, '_');
			if($posmhp !== false) {
				$main_home_phone = "";
			}
			$posmcp = strpos($main_cell_phone, '_');
			if($posmcp !== false) {
				$main_cell_phone = "";
			}
			$poscbp = strpos($contact_business_phone, '_');
			if($poscbp !== false) {
				$contact_business_phone = "";
			}
			$poschp = strpos($contact_home_phone, '_');
			if($poschp !== false) {
				$contact_home_phone = "";
			}
			$posccp = strpos($contact_cell_phone, '_');
			if($posccp !== false) {
				$contact_cell_phone = "";
			}

			$arguments = array ('company' => $company,
					'first_name' => $first_name,
					'last_name' => $last_name,
					'mailing_address1' => $mailing_address1,
					'mailing_address2' => $mailing_address2,
					'mailing_city' => $mailing_city,
					'mailing_state' => $mailing_state,
					'mailing_zip' => $mailing_zip,
					'main_business_phone' => $main_business_phone,
					'main_home_phone' => $main_home_phone,
					'main_cell_phone' => $main_cell_phone,
					'email' => $email,
					'contact_first_name' => $contact_first_name,
					'contact_last_name' => $contact_last_name,
					'bill_address1' => $bill_address1,
					'bill_address2' => $bill_address2,
					'bill_city' => $bill_city,
					'bill_state' => $bill_state,
					'bill_zip' => $bill_zip,
					'contact_business_phone' => $contact_business_phone,
					'contact_home_phone' => $contact_home_phone,
					'contact_cell_phone' => $contact_cell_phone,
					'bill_email' => $bill_email,
					'username' => $username,
					'password' => $password,
					'card_name' => $card_name,
					'card_number' => $card_number,
					'card_exp' => $card_exp,);

			$objSignup->addcustomer($arguments);

			$retdata=$objlogin->chkValidLogin($username, $password);

			if($retdata=='success'){
				//header('Location:'.SITE_PATH.'index.php?signup=signup');
			} else {
				confirm("Invalid login.");
			}
		}
	}
	?>
	<div id="main_container">

		<div class="header_login">
			<div class="logo">
				<a href="<?=SITE_PATH?>"><?= PROJECTNAME ?> </a>
			</div>
		</div>
		<div class="clear">&nbsp;</div>

		<div class="signup_content">
			<div class="error">
				<?= $msg ?>
			</div>
			<h1>Signup</h1>

			<!-- <a href="<?=SITE_PATH?>login/forgot-password.php" class="forgot_pass">Forgot
				password</a> -->

			<form action="index.php" method="post" class="niceform">
				<input type="hidden" name="pagebck" id="pagebck"
					value="<?= $_GET['url_s'] ?>" />
				<fieldset>
					<?if ($next1 == "" ) {
					 ?>
					<h2>Customer Info</h2>
					<dl class="signup">
						<table id="rounded-corner-account-signup" summary="Admin">
							<tr>
								<td><label>COMPANY:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="company" id="company"
									value="<?=$company?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>NAME - FIRST:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="first_name" id="first_name"
									value="<?=$first_name?>" size="35" /></td>
								<td><label>LAST:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="last_name" id="last_name"
									value="<?=$last_name?>" size="35" /></td>
							</tr>
							<tr>
								<td><label>ADDRESS 1:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="mailing_address1"
									id="mailing_address1" value="<?=$mailing_address1?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>ADDRESS 2:</label></td>
								<td><input class="text" name="mailing_address2"
									id="mailing_address2" value="<?=$mailing_address2?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>CITY:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="mailing_city" id="mailing_city"
									value="<?=$mailing_city?>" size="35" /></td>
								<td><label>STATE:<span style="color: red">*</span>
								</label></td>
								<td><select name='mailing_state'>
										<option value=''>Choose a State</option>
										<option value='AK'
										<?php if ($_POST['mailing_state'] == 'AK') echo 'selected'; ?>>AK</option>
										<option value='AL'
										<?php if ($_POST['mailing_state'] == 'AL') echo 'selected'; ?>>AL</option>
										<option value='AR'
										<?php if ($_POST['mailing_state'] == 'AR') echo 'selected'; ?>>AR</option>
										<option value='AZ'
										<?php if ($_POST['mailing_state'] == 'AZ') echo 'selected'; ?>>AZ</option>
										<option value='CA'
										<?php if ($_POST['mailing_state'] == 'CA') echo 'selected'; ?>>CA</option>
										<option value='CO'
										<?php if ($_POST['mailing_state'] == 'CO') echo 'selected'; ?>>CO</option>
										<option value='CT'
										<?php if ($_POST['mailing_state'] == 'CT') echo 'selected'; ?>>CT</option>
										<option value='DC'
										<?php if ($_POST['mailing_state'] == 'DC') echo 'selected'; ?>>DC</option>
										<option value='DE'
										<?php if ($_POST['mailing_state'] == 'DE') echo 'selected'; ?>>DE</option>
										<option value='FL'
										<?php if ($_POST['mailing_state'] == 'FL') echo 'selected'; ?>>FL</option>
										<option value='GA'
										<?php if ($_POST['mailing_state'] == 'GA') echo 'selected'; ?>>GA</option>
										<option value='HI'
										<?php if ($_POST['mailing_state'] == 'HI') echo 'selected'; ?>>HI</option>
										<option value='IA'
										<?php if ($_POST['mailing_state'] == 'IA') echo 'selected'; ?>>IA</option>
										<option value='ID'
										<?php if ($_POST['mailing_state'] == 'ID') echo 'selected'; ?>>ID</option>
										<option value='IL'
										<?php if ($_POST['mailing_state'] == 'IL') echo 'selected'; ?>>IL</option>
										<option value='IN'
										<?php if ($_POST['mailing_state'] == 'IN') echo 'selected'; ?>>IN</option>
										<option value='KS'
										<?php if ($_POST['mailing_state'] == 'KS') echo 'selected'; ?>>KS</option>
										<option value='KY'
										<?php if ($_POST['mailing_state'] == 'KY') echo 'selected'; ?>>KY</option>
										<option value='LA'
										<?php if ($_POST['mailing_state'] == 'LA') echo 'selected'; ?>>LA</option>
										<option value='MA'
										<?php if ($_POST['mailing_state'] == 'MA') echo 'selected'; ?>>MA</option>
										<option value='MD'
										<?php if ($_POST['mailing_state'] == 'MD') echo 'selected'; ?>>MD</option>
										<option value='ME'
										<?php if ($_POST['mailing_state'] == 'ME') echo 'selected'; ?>>ME</option>
										<option value='MI'
										<?php if ($_POST['mailing_state'] == 'MI') echo 'selected'; ?>>MI</option>
										<option value='MN'
										<?php if ($_POST['mailing_state'] == 'MN') echo 'selected'; ?>>MN</option>
										<option value='MO'
										<?php if ($_POST['mailing_state'] == 'MO') echo 'selected'; ?>>MO</option>
										<option value='MS'
										<?php if ($_POST['mailing_state'] == 'MS') echo 'selected'; ?>>MS</option>
										<option value='MT'
										<?php if ($_POST['mailing_state'] == 'MT') echo 'selected'; ?>>MT</option>
										<option value='NC'
										<?php if ($_POST['mailing_state'] == 'NC') echo 'selected'; ?>>NC</option>
										<option value='ND'
										<?php if ($_POST['mailing_state'] == 'ND') echo 'selected'; ?>>ND</option>
										<option value='NE'
										<?php if ($_POST['mailing_state'] == 'NE') echo 'selected'; ?>>NE</option>
										<option value='NH'
										<?php if ($_POST['mailing_state'] == 'NH') echo 'selected'; ?>>NH</option>
										<option value='NJ'
										<?php if ($_POST['mailing_state'] == 'NJ') echo 'selected'; ?>>NJ</option>
										<option value='NM'
										<?php if ($_POST['mailing_state'] == 'NM') echo 'selected'; ?>>NM</option>
										<option value='NV'
										<?php if ($_POST['mailing_state'] == 'NV') echo 'selected'; ?>>NV</option>
										<option value='NY'
										<?php if ($_POST['mailing_state'] == 'NY') echo 'selected'; ?>>NY</option>
										<option value='OH'
										<?php if ($_POST['mailing_state'] == 'OH') echo 'selected'; ?>>OH</option>
										<option value='OK'
										<?php if ($_POST['mailing_state'] == 'OK') echo 'selected'; ?>>OK</option>
										<option value='OR'
										<?php if ($_POST['mailing_state'] == 'OR') echo 'selected'; ?>>OR</option>
										<option value='PA'
										<?php if ($_POST['mailing_state'] == 'PA') echo 'selected'; ?>>PA</option>
										<option value='PR'
										<?php if ($_POST['mailing_state'] == 'PR') echo 'selected'; ?>>PR</option>
										<option value='RI'
										<?php if ($_POST['mailing_state'] == 'RI') echo 'selected'; ?>>RI</option>
										<option value='SC'
										<?php if ($_POST['mailing_state'] == 'SC') echo 'selected'; ?>>SC</option>
										<option value='SD'
										<?php if ($_POST['mailing_state'] == 'SD') echo 'selected'; ?>>SD</option>
										<option value='TN'
										<?php if ($_POST['mailing_state'] == 'TN') echo 'selected'; ?>>TN</option>
										<option value='TX'
										<?php if ($_POST['mailing_state'] == 'TX') echo 'selected'; ?>>TX</option>
										<option value='UT'
										<?php if ($_POST['mailing_state'] == 'UT') echo 'selected'; ?>>UT</option>
										<option value='VA'
										<?php if ($_POST['mailing_state'] == 'VA') echo 'selected'; ?>>VA</option>
										<option value='VT'
										<?php if ($_POST['mailing_state'] == 'VT') echo 'selected'; ?>>VT</option>
										<option value='WA'
										<?php if ($_POST['mailing_state'] == 'WA') echo 'selected'; ?>>WA</option>
										<option value='WI'
										<?php if ($_POST['mailing_state'] == 'WI') echo 'selected'; ?>>WI</option>
										<option value='WV'
										<?php if ($_POST['mailing_state'] == 'WV') echo 'selected'; ?>>WV</option>
										<option value='WY'
										<?php if ($_POST['mailing_state'] == 'WY') echo 'selected'; ?>>WY</option>
								</select>
								</td>
								<td><label>ZIP:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="mailing_zip" id="zip"
									value="<?=$mailing_zip?>" size="35" /></td>
							</tr>
							<tr>
								<td><label>PHONE - BUSINESS:</label></td>
								<td><input class="text" name="main_business_phone"
									id="business_phone" value="<?=$main_business_phone?>" size="35" />
								</td>
								<td><label>HOME:</label></td>
								<td><input class="text" name="main_home_phone" id="home_phone"
									value="<?=$main_home_phone?>" size="35" />
								</td>
								<td><label>CELL:</label></td>
								<td><input class="text" name="main_cell_phone" id="cell_phone"
									value="<?=$main_cell_phone?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>EMAIL:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="email" id="email"
									value="<?=$email?>" size="35" />
								</td>
							</tr>
						</table>
					</dl>

					<dl class="submit">
						<table>
							<tr>
								<td><input type="submit" name="next1" id="next1" value="Next"
									tabindex="4" /></td>
								<td><input type="submit" name="cancel" id="cancel"
									value="Cancel" tabindex="4" /></td>
							</tr>
						</table>
					</dl>
					<?php
					} else {
				?>
					<h2>Contact Info</h2>
					<dl class="signup">
						<input class="text" type="hidden" name="company" id="company"
							value="<?=$company?>" />
						<input class="text" type="hidden" name="first_name"
							id="first_name" value="<?=$first_name?>" />
						<input class="text" type="hidden" name="last_name" id="last_name"
							value="<?=$last_name?>" />
						<input class="text" type="hidden" name="mailing_address1"
							id="mailing_address1" value="<?=$mailing_address1?>" />
						<input class="text" type="hidden" name="mailing_address2"
							id="mailing_address2" value="<?=$mailing_address2?>" />
						<input class="text" type="hidden" name="mailing_city"
							id="mailing_city" value="<?=$mailing_city?>" />
						<input class="text" type="hidden" name="mailing_state"
							id="mailing_state" value="<?=$mailing_state?>" />
						<input class="text" type="hidden" name="mailing_zip"
							id="first_name" value="<?=$mailing_zip?>" />
						<input class="text" type="hidden" name="main_business_phone"
							id="main_business_phone" value="<?=$main_business_phone?>" />
						<input class="text" type="hidden" name="main_home_phone"
							id="main_home_phone" value="<?=$main_home_phone?>" />
						<input class="text" type="hidden" name="main_cell_phone"
							id="main_cell_phone" value="<?=$main_cell_phone?>" />
						<input class="text" type="hidden" name="email" id="email"
							value="<?=$email?>" />

						<table id="rounded-corner-account-signup" summary="Admin">
							<tr>
								<td><label>NAME - FIRST:</label></td>
								<td><input class="text" name="contact_first_name"
									id="contact_first_name" value="<?=$first_name?>" size="35" /></td>
								<td><label>LAST:</label></td>
								<td><input class="text" name="contact_last_name"
									id="contact_last_name" value="<?=$last_name?>" size="35" /></td>
							</tr>
							<tr>
								<td><label>ADDRESS 1:</label></td>
								<td><input class="text" name="bill_address1" id="bill_address1"
									value="<?=$mailing_address1?>" size="35" /></td>
							</tr>
							<tr>
								<td><label>ADDRESS 2:</label></td>
								<td><input class="text" name="bill_address2" id="bill_address2"
									value="<?=$mailing_address2?>" size="35" /></td>
							</tr>
							<tr>
								<td><label>CITY:</label></td>
								<td><input class="text" name="bill_city" id="bill_city"
									value="<?=$mailing_city?>" size="35" /></td>
								<td><label>STATE:</label></td>
								<td><select name='bill_state'>
										<option value=''>Choose a State</option>
										<option value='AK'
										<?php if ($_POST['mailing_state'] == 'AK') echo 'selected'; ?>>AK</option>
										<option value='AL'
										<?php if ($_POST['mailing_state'] == 'AL') echo 'selected'; ?>>AL</option>
										<option value='AR'
										<?php if ($_POST['mailing_state'] == 'AR') echo 'selected'; ?>>AR</option>
										<option value='AZ'
										<?php if ($_POST['mailing_state'] == 'AZ') echo 'selected'; ?>>AZ</option>
										<option value='CA'
										<?php if ($_POST['mailing_state'] == 'CA') echo 'selected'; ?>>CA</option>
										<option value='CO'
										<?php if ($_POST['mailing_state'] == 'CO') echo 'selected'; ?>>CO</option>
										<option value='CT'
										<?php if ($_POST['mailing_state'] == 'CT') echo 'selected'; ?>>CT</option>
										<option value='DC'
										<?php if ($_POST['mailing_state'] == 'DC') echo 'selected'; ?>>DC</option>
										<option value='DE'
										<?php if ($_POST['mailing_state'] == 'DE') echo 'selected'; ?>>DE</option>
										<option value='FL'
										<?php if ($_POST['mailing_state'] == 'FL') echo 'selected'; ?>>FL</option>
										<option value='GA'
										<?php if ($_POST['mailing_state'] == 'GA') echo 'selected'; ?>>GA</option>
										<option value='HI'
										<?php if ($_POST['mailing_state'] == 'HI') echo 'selected'; ?>>HI</option>
										<option value='IA'
										<?php if ($_POST['mailing_state'] == 'IA') echo 'selected'; ?>>IA</option>
										<option value='ID'
										<?php if ($_POST['mailing_state'] == 'ID') echo 'selected'; ?>>ID</option>
										<option value='IL'
										<?php if ($_POST['mailing_state'] == 'IL') echo 'selected'; ?>>IL</option>
										<option value='IN'
										<?php if ($_POST['mailing_state'] == 'IN') echo 'selected'; ?>>IN</option>
										<option value='KS'
										<?php if ($_POST['mailing_state'] == 'KS') echo 'selected'; ?>>KS</option>
										<option value='KY'
										<?php if ($_POST['mailing_state'] == 'KY') echo 'selected'; ?>>KY</option>
										<option value='LA'
										<?php if ($_POST['mailing_state'] == 'LA') echo 'selected'; ?>>LA</option>
										<option value='MA'
										<?php if ($_POST['mailing_state'] == 'MA') echo 'selected'; ?>>MA</option>
										<option value='MD'
										<?php if ($_POST['mailing_state'] == 'MD') echo 'selected'; ?>>MD</option>
										<option value='ME'
										<?php if ($_POST['mailing_state'] == 'ME') echo 'selected'; ?>>ME</option>
										<option value='MI'
										<?php if ($_POST['mailing_state'] == 'MI') echo 'selected'; ?>>MI</option>
										<option value='MN'
										<?php if ($_POST['mailing_state'] == 'MN') echo 'selected'; ?>>MN</option>
										<option value='MO'
										<?php if ($_POST['mailing_state'] == 'MO') echo 'selected'; ?>>MO</option>
										<option value='MS'
										<?php if ($_POST['mailing_state'] == 'MS') echo 'selected'; ?>>MS</option>
										<option value='MT'
										<?php if ($_POST['mailing_state'] == 'MT') echo 'selected'; ?>>MT</option>
										<option value='NC'
										<?php if ($_POST['mailing_state'] == 'NC') echo 'selected'; ?>>NC</option>
										<option value='ND'
										<?php if ($_POST['mailing_state'] == 'ND') echo 'selected'; ?>>ND</option>
										<option value='NE'
										<?php if ($_POST['mailing_state'] == 'NE') echo 'selected'; ?>>NE</option>
										<option value='NH'
										<?php if ($_POST['mailing_state'] == 'NH') echo 'selected'; ?>>NH</option>
										<option value='NJ'
										<?php if ($_POST['mailing_state'] == 'NJ') echo 'selected'; ?>>NJ</option>
										<option value='NM'
										<?php if ($_POST['mailing_state'] == 'NM') echo 'selected'; ?>>NM</option>
										<option value='NV'
										<?php if ($_POST['mailing_state'] == 'NV') echo 'selected'; ?>>NV</option>
										<option value='NY'
										<?php if ($_POST['mailing_state'] == 'NY') echo 'selected'; ?>>NY</option>
										<option value='OH'
										<?php if ($_POST['mailing_state'] == 'OH') echo 'selected'; ?>>OH</option>
										<option value='OK'
										<?php if ($_POST['mailing_state'] == 'OK') echo 'selected'; ?>>OK</option>
										<option value='OR'
										<?php if ($_POST['mailing_state'] == 'OR') echo 'selected'; ?>>OR</option>
										<option value='PA'
										<?php if ($_POST['mailing_state'] == 'PA') echo 'selected'; ?>>PA</option>
										<option value='PR'
										<?php if ($_POST['mailing_state'] == 'PR') echo 'selected'; ?>>PR</option>
										<option value='RI'
										<?php if ($_POST['mailing_state'] == 'RI') echo 'selected'; ?>>RI</option>
										<option value='SC'
										<?php if ($_POST['mailing_state'] == 'SC') echo 'selected'; ?>>SC</option>
										<option value='SD'
										<?php if ($_POST['mailing_state'] == 'SD') echo 'selected'; ?>>SD</option>
										<option value='TN'
										<?php if ($_POST['mailing_state'] == 'TN') echo 'selected'; ?>>TN</option>
										<option value='TX'
										<?php if ($_POST['mailing_state'] == 'TX') echo 'selected'; ?>>TX</option>
										<option value='UT'
										<?php if ($_POST['mailing_state'] == 'UT') echo 'selected'; ?>>UT</option>
										<option value='VA'
										<?php if ($_POST['mailing_state'] == 'VA') echo 'selected'; ?>>VA</option>
										<option value='VT'
										<?php if ($_POST['mailing_state'] == 'VT') echo 'selected'; ?>>VT</option>
										<option value='WA'
										<?php if ($_POST['mailing_state'] == 'WA') echo 'selected'; ?>>WA</option>
										<option value='WI'
										<?php if ($_POST['mailing_state'] == 'WI') echo 'selected'; ?>>WI</option>
										<option value='WV'
										<?php if ($_POST['mailing_state'] == 'WV') echo 'selected'; ?>>WV</option>
										<option value='WY'
										<?php if ($_POST['mailing_state'] == 'WY') echo 'selected'; ?>>WY</option>
								</select></td>
								<td><input class="text" name="bill_zip" id="zip"
									value="<?=$mailing_zip?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>PHONE - BUSINESS:</label></td>
								<td><input class="text" name="contact_business_phone"
									id="business_phone" value="<?=$main_business_phone?>" size="35" />
								</td>
								<td><label>HOME:</label></td>
								<td><input class="text" name="contact_home_phone"
									id="home_phone" value="<?=$main_home_phone?>" size="35" />
								</td>
								<td><label>CELL:</label></td>
								<td><input class="text" name="contact_cell_phone"
									id="cell_phone" value="<?=$main_cell_phone?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>EMAIL:</label></td>
								<td><input class="text" name="bill_email" id="bill_email"
									value="<?=$email?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>USERNAME:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="username" id="username"
									value="<?=$username?>" size="35" />
								</td>
								<td><label>PASSWORD:<span style="color: red">*</span>
								</label></td>
								<td><input type="password" autocomplete="off" class="text"
									name="password" id="password" value="<?=$password?>" size="35" />
								</td>
							</tr>
							<tr>
								<td><label>CARD NAME:<span style="color: red">*</span>
								</label></td>
								<td><select name="card_name" id="card_name"
									onChange=document.frmUser4.submit()>
										<option selected>VISA</option>
										<option value=MASTERCARD>MASTERCARD</option>
										<option value=DISCOVER>DISCOVER</option>
								</select>
								</td>
								<td><label>CARD NUMBER:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="card_number" id="card_number"
									value="<?=$card_number?>" size="35" /></td>
								<td><label>EXPIRATION DATE:<span style="color: red">*</span>
								</label></td>
								<td><input class="text" name="card_exp" id="exp_date"
									value="<?=$card_exp?>" size="35" />
								</td>
							</tr>
						</table>
					</dl>
					<dl class="submit">
						<table>
							<tr>
								<td><input type="submit" name="back" id="back" value="Back"
									tabindex="4" /></td>
								<td><input type="submit" name="save" id="save" value="Save"
									tabindex="4" /></td>
								<td><input type="submit" name="cancel" id="cancel"
									value="Cancel" tabindex="4" /></td>
							</tr>
						</table>
					</dl>
					<? }?>
				</fieldset>

			</form>
		</div>


		<?php
		require_once('../includes/footer.php');
		?>
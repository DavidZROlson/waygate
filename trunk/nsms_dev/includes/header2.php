<link rel="stylesheet" type="text/css" href="<?=SITECSS ?>modalbox.css" />
<link
	rel="stylesheet" type="text/css"
	href="<?=SITECSS ?>ddlevelsmenu-base.css" />
<link
	rel="stylesheet" type="text/css"
	href="<?=SITECSS ?>ddlevelsmenu-sidebar.css" />
<script type="text/javascript">var siteimg = "<?= SITEIMG ?>";</script>
<script type="text/javascript" src="<?= SITEJS ?>ddlevelsmenu.js">
	/***********************************************
	 * All Levels Navigational Menu- (c) Dynamic Drive DHTML code library (http://www.dynamicdrive.com)
	 * This notice MUST stay intact for legal use
	 * Visit Dynamic Drive at http://www.dynamicdrive.com/ for full source code
	 ***********************************************/
</script>
</head>
<body>
<?php $pagepath=$_SERVER['PHP_SELF']; $pagepatharr=  explode("/", $pagepath);$activepage=$pagepatharr[2];$activepagenew=$pagepatharr[3]; $pagenameall=  explode(".", $activepagenew);$pagenameonly= $pagenameall['0']; 	?>


	<div id="main_container">
		<div class="header">
			<div class="logo">
				<a href="<?=SITE_PATH?>"><?= PROJECTNAME ?> </a>
			</div>
			<div class="right_header">
				Welcome
				<?= ucfirst($_SESSION['PSAdminNM']) ?>
				| <a href="<?=SITE_PATH?>login/logout.php" class="logout">Logout</a>
			</div>
			<div id="clock_a"></div>
		</div>
		<div class="main_content">
			<?php if($_SERVER['HTTP_REFERER']){ ?>
			<div class="back">
				<a href="javascript:void(0);" onClick="javascript:history.back(-1)">Back</a>
			</div>
			<?php } ?>


			<div class="center_content">
<?php
require_once dirname(__FILE__) . '/mysql.php';

##Session start
session_start();
ob_start();


##display error on browser hide show setting
ini_set('display_errors', On);

##log errors in log file yes/no setting
ini_set('log_errors', 1);

##error log file path
ini_set('error_log', dirname(__FILE__) . '/error_log.txt');

##which type of error to display (warning/notice/fatal error)
error_reporting(E_ALL & ~E_NOTICE);

##set default timezone
date_default_timezone_set('America/Los_Angeles');


abstract class AbstractServer extends mysql
{
	protected $pas;

	public function __construct()
	{
		$this->pas = $this->connectpas();
		##Set constant getting from sitesettings table.
		$this->setConstant();
	}

	public function addbackup($arg) {
		$instance_id = $this->quotes_to_entities($this->pas, $arg['instance_id']);
		$schedatetime= $this->quotes_to_entities($this->pas, $arg['schedatetime']);
		$type = $this->quotes_to_entities($this->pas, $arg['type']);

		$crontime = date('Y-m-d G:i', strtotime($schedatetime));

		$sql = "INSERT INTO `backup` (instance_id, crontime, type) VALUES ";
		$sql .= "('$instance_id', '$crontime', '$type')";

		$sqlresult = $this->insert($this->pas, $sql);
	}

	public function addcustomers($arg) {
		$company = $this->quotes_to_entities($this->pas, $arg['company']);
		$first_name = $this->quotes_to_entities($this->pas, $arg['first_name']);
		$last_name = $this->quotes_to_entities($this->pas, $arg['last_name']);
		$mailing_address1 = $this->quotes_to_entities($this->pas, $arg['mailing_address1']);
		$mailing_address2 = $this->quotes_to_entities($this->pas, $arg['mailing_address2']);
		$mailing_city = $this->quotes_to_entities($this->pas, $arg['mailing_city']);
		$mailing_state = $this->quotes_to_entities($this->pas, $arg['mailing_state']);
		$mailing_zip = $this->quotes_to_entities($this->pas, $arg['mailing_zip']);
		$main_business_phone = $this->quotes_to_entities($this->pas, $arg['main_business_phone']);
		$main_home_phone = $this->quotes_to_entities($this->pas, $arg['main_home_phone']);
		$main_cell_phone = $this->quotes_to_entities($this->pas, $arg['main_cell_phone']);
		$email = $this->quotes_to_entities($this->pas, $arg['email']);
		$contact_first_name = $this->quotes_to_entities($this->pas, $arg['contact_first_name']);
		$contact_last_name = $this->quotes_to_entities($this->pas, $arg['contact_last_name']);
		$bill_address1 = $this->quotes_to_entities($this->pas, $arg['bill_address1']);
		$bill_address2 = $this->quotes_to_entities($this->pas, $arg['bill_address2']);
		$bill_city = $this->quotes_to_entities($this->pas, $arg['bill_city']);
		$bill_state = $this->quotes_to_entities($this->pas, $arg['bill_state']);
		$bill_zip = $this->quotes_to_entities($this->pas, $arg['bill_zip']);
		$contact_business_phone = $this->quotes_to_entities($this->pas, $arg['contact_business_phone']);
		$contact_home_phone = $this->quotes_to_entities($this->pas, $arg['contact_home_phone']);
		$contact_cell_phone = $this->quotes_to_entities($this->pas, $arg['contact_cell_phone']);
		$bill_email = $this->quotes_to_entities($this->pas, $arg['bill_email']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);
		$card_name = $this->quotes_to_entities($this->pas, $arg['card_name']);
		$card_number = $this->quotes_to_entities($this->pas, $arg['card_number']);
		$card_exp = $this->quotes_to_entities($this->pas, $arg['card_exp']);

		$number = str_replace(array('-'), '', $card_number);

		$appended=0;
		$chunk1 = "";
		$chunk2 = "";
		if (!empty($mailing_address2)){
			$chunk1 = "mailing_address2";
			$chunk2 = "'$mailing_address2'";
			$appended++;
		}
		if (!empty($main_business_phone)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$mbp = str_replace(array('(', ')', '-', ' '), '', $main_business_phone);
			$chunk1 .= "main_business_phone";
			$chunk2 .= "'$mbp'";
		}
		if (!empty($main_home_phone)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$mhp = str_replace(array('(', ')', '-', ' '), '', $main_home_phone);
			$chunk1 .= "main_home_phone";
			$chunk2 .= "'$mhp'";
		}
		if (!empty($main_cell_phone)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$mcp = str_replace(array('(', ')', '-', ' '), '', $main_cell_phone);
			$chunk1 .= "main_cell_phone";
			$chunk2 .= "'$mcp'";
		}
		if (!empty($contact_first_name)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "contact_first_name";
			$chunk2 .= "'$contact_first_name'";
		}
		if (!empty($contact_last_name)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "contact_last_name";
			$chunk2 .= "'$contact_last_name'";
		}
		if (!empty($bill_address1)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "bill_address1";
			$chunk2 .= "'$bill_address1'";
		}
		if (!empty($bill_address2)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "bill_address2";
			$chunk2 .= "'$bill_address2'";
		}
		if (!empty($bill_city)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "bill_city";
			$chunk2 .= "'$bill_city'";
		}
		if (!empty($bill_state)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "bill_state";
			$chunk2 .= "'$bill_state'";
		}
		if (!empty($bill_zip)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "bill_zip";
			$chunk2 .= "'$bill_zip'";
		}
		if (!empty($contact_business_phone)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$cbp = str_replace(array('(', ')', '-', ' '), '', $contact_business_phone);
			$chunk1 .= "contact_business_phone";
			$chunk2 .= "'$cbp'";
		}
		if (!empty($contact_home_phone)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chp = str_replace(array('(', ')', '-', ' '), '', $contact_home_phone);
			$chunk1 .= "contact_home_phone";
			$chunk2 .= "'$chp'";
		}
		if (!empty($contact_cell_phone)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$ccp = str_replace(array('(', ')', '-', ' '), '', $contact_cell_phone);
			$chunk1 .= "contact_cell_phone";
			$chunk2 .= "'$ccp'";
		}
		if (!empty($bill_email)){
			if($appended != "0")
			{
				$chunk1 .=", ";
				$chunk2 .=", ";
			}
			$appended++;
			$chunk1 .= "bill_email";
			$chunk2 .= "'$bill_email'";
		}
		$querystr = "INSERT INTO customers (company_name, first_name, last_name, mailing_address1, ";
		$querystr .= "mailing_city, mailing_state, mailing_zip, email, createdate, " . $chunk1 . ") VALUES ";
		$querystr .= "('$company', '$first_name', '$last_name', '$mailing_address1', '$mailing_city', ";
		$querystr .= "'$mailing_state', '$mailing_zip', '$email', NOW(), " .$chunk2 . ")";

		$idcustomers = $this->insert($this->pas, $querystr);

		$sql = "INSERT INTO usercreditdetails (idcustomers, card_name, card_number, card_exp) VALUES ";
		$sql .= "($idcustomers, '$card_name', '$number', '$card_exp')";

		$data = $this->insert($this->pas, $sql);

		$sql = "INSERT INTO user (username, password, idcustomers) VALUES ";
		$sql .= "('$username', '$password', $idcustomers)";

		$iduser = $this->insert($this->pas, $sql);

		$sqlinsert = "INSERT INTO `rights` (iduser, idapilogin, admin) VALUES ";
		$sqlinsert .= "('$iduser', 1, 'Y')";

		$data = $this->insert($this->pas, $sqlinsert);

	}

	public function adddcs($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);
		$apikey = $this->quotes_to_entities($this->pas, $arg['apikey']);
		$provider = $this->quotes_to_entities($this->pas, $arg['provider']);

		$message = "Susscess";

		$checkNameSQL = "SELECT * FROM `apilogin` WHERE dcname = '$dcname'";
		$sqlresult = $this->select($this->pas, $checkNameSQL);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			$message = "Duplicate DC";
			return $message;
		}

		$sql = "INSERT INTO `apilogin` (dcname, username, password, apikey, provider) VALUES ";
		$sql .= "('$dcname', '$username', '$password', '$apikey', '$provider')";

		$id = $this->insert($this->pas, $sql);

		if (is_array($id) && $id[0] == "ERR") {
			$message = $id[3];
			return $message;
		}

		return $message;
	}

	public function adduser($arg) {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);
		$admin = $this->quotes_to_entities($this->pas, $arg['admin']);
		$dns = $this->quotes_to_entities($this->pas, $arg['dns']);
		$monitor = $this->quotes_to_entities($this->pas, $arg['monitor']);

		$message = "Susscess";

		$checkNameSQL = "SELECT * FROM `user` WHERE username = '$username'";
		if ($idcustomers > 1) {
			$sql .= " AND idcustomers = $idcustomers";
		}
		$sqlresult = $this->select($this->pas, $checkNameSQL);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			$message = "Duplicate username";
			return $message;
		}

		$sql = "INSERT INTO `user` (username, password, idcustomers) VALUES ";
		$sql .= "('$username', '$password', $idcustomers)";

		$id = $this->insert($this->pas, $sql);

		if (is_array($id) && $id[0] == "ERR") {
			$message = $id[3];
			return $message;
		}

		$sql = "INSERT INTO `rights` (iduser, idapilogin, admin, dns, monitor) VALUES ('$id',1,'$admin', '$dns', '$monitor')";
		$id = $this->insert($this->pas, $sql);

		if (is_array($id) && $id[0] == "ERR") {
			$message = $id[3];
			return $message;
		}

		return $message;
	}

	// add image info
	public function addimage($arg) {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$imagename = $this->quotes_to_entities($this->pas, $arg['imagename']);
		$idrackspaceimage = $this->quotes_to_entities($this->pas, $arg['idrackspaceimage']);

		$sql = "INSERT INTO `images` (dcname, idrackspaceimage, imagename, created, usercreate, idcustomers) VALUES ";
		$sql .= "('$dcname', '$idrackspaceimage', '$imagename', NOW(), '$username', $idcustomers)";

		$id = $this->insert($this->pas, $sql);
	}

	// add label info
	public function addlabel($arg) {
		$labelname = $this->quotes_to_entities($this->pas, $arg['labelname']);

		$message = "Susscess";

		$sql = "INSERT INTO `reportlabels` (labelname) VALUES ('$labelname')";

		$id = $this->insert($this->pas, $sql);

		if (is_array($id) && $id[0] == "ERR") {
			$message = $id[3];
			return $message;
		}

		return $message;
	}

	// add server info
	public function addserver($arg) {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$servername = $this->quotes_to_entities($this->pas, $arg['servername']);
		$instance_id = $this->quotes_to_entities($this->pas, $arg['instance_id']);
		$ram = $this->quotes_to_entities($this->pas, $arg['ram']);
		$ip_address = $this->quotes_to_entities($this->pas, $arg['ip_address']);

		$sql = "INSERT INTO `servers` (dcname, instance_id, servername, ram, created, usercreate, ";
		$sql .= "ip_address, idcustomers) VALUES ('$dcname', '$instance_id', '$servername',";
		$sql .= "$ram, UTC_TIMESTAMP(), '$username', '$ip_address', $idcustomers)";

		$this->insert($this->pas, $sql);
	}

	public function addservermonitored($arg) {

		$count = count($arg);
		for ($x=0; $x < $count; $x++){
			list($instance_id, $checked) = explode("|",$arg[$x]);
			$instance_id = $this->quotes_to_entities($this->pas, $instance_id);
			$checked = $this->quotes_to_entities($this->pas, $checked);

			$sqlupdate = "UPDATE `servers` ";
			$sqlupdate .= "SET monitored = '$checked' ";
			$sqlupdate .= "WHERE instance_id = '$instance_id'";
			$this->update($this->pas, $sqlupdate);
		}
	}

	// calcuate the cost of a server
	private function calculatecost($servername, $from, $to, $instance_id, $ram, $createdDate, $deleteDate) {

		$coststr = $this->checkchange($servername, $instance_id, $from, $to, $ram, $createdDate, $deleteDate);

		if ($coststr != "") {
			return $coststr;
		}

		if (strtotime($createdDate) > strtotime($from)) {
			$hours = $this->createdtohours($servername, $to, $instance_id);
			if ($deleteDate != "0000-00-00 00:00:00" && strtotime($deleteDate) < strtotime($to)) {
				$hours = $this->createdtohours($servername, $deleteDate, $instance_id);
			}
		} else {
			$hours = $this->fromtohrs($servername, $instance_id, $from, $to);
			if ($deleteDate != "0000-00-00 00:00:00" && strtotime($deleteDate) < strtotime($to)) {
				$hours = $this->fromtohrs($servername, $instance_id, $from, $deleteDate);
			}
		}

		$costperhr = $this->ramcost($ram);

		$cost = $hours * $costperhr;
		$cost = money_format('$%(#10n', $cost);
		return $cost;
	}

	// check for size change and calcuate cost
	private function checkchange($servername, $instance_id, $from, $to, $ram, $createdDate, $deleteDate){
		$coststr = "";

		$sql = "SELECT changedate, changeram FROM `changes` WHERE instance_id = '$instance_id' ";
		$sql .= "AND changedate > '$from'";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] == "ERR") {
			return $coststr;
		}

		$sqlram = "SELECT changedate, changeram FROM `changes` WHERE instance_id = '$instance_id' ";
		$sqlram .= "AND changedate < '$from' AND changedate > '$createdDate'";

		$sqlresultram = $this->select($this->pas, $sqlram);

		if (is_array($sqlresultram) && $sqlresultram[0] != "ERR") {
			$ram =  $sqlresultram[0]['changeram'];
		}

		$count = count($sqlresult);

		$cost = 0;
		for ($x = 0; $x < $count + 1; $x++){
			if (strtotime($createdDate) > strtotime($from) && $x == 0) {
				$hours = $this->createdtohours($servername, $sqlresult[$x]['changedate'], $instance_id);
				$costperhr = $this->ramcost($ram);
				$cost = $hours * $costperhr;
			} elseif ($x == 0) {
				$costperhr = 0;

				$hours = $this->fromtohrs($servername, $instance_id, $from, $sqlresult[$x]['changedate']);

				if (strtotime($from) < strtotime($sqlresult[$x]['changedate'])) {
					$costperhr = $this->ramcost($ram);
				} else {
					$costperhr = $this->ramcost($sqlresult[$x]['changeram']);
				}

				$cost = $cost + ($hours * $costperhr);
			} elseif ($x == $count) {
				$hours = $this->fromtohrs($servername, $instance_id, $sqlresult[$x-1]['changedate'], $to);
				if ($deleteDate != "0000-00-00 00:00:00" && strtotime($deleteDate) < strtotime($to)) {
					$hours = $this->fromtohrs($servername, $instance_id, $sqlresult[$x-1]['changedate'], $deleteDate);
				}
				$costperhr = $this->ramcost($sqlresult[$x-1]['changeram']);
				$cost = $cost + ($hours * $costperhr);
			} elseif (strtotime($sqlresult[$x]['changedate']) > strtotime($to)) {
				$hours = $this->fromtohrs($servername, $instance_id, $sqlresult[$x-1]['changedate'], $to);
				if ($deleteDate != "0000-00-00 00:00:00" && strtotime($deleteDate) < strtotime($to)) {
					$hours = $this->fromtohrs($servername, $instance_id, $sqlresult[$x-1]['changedate'], $deleteDate);
				}
				$costperhr = $this->ramcost($sqlresult[$x-1]['changeram']);
				$cost = $cost + ($hours * $costperhr);
				break;
			}else {
				$hours = $this->fromtohrs($servername, $instance_id, $sqlresult[$x-1]['changedate'], $sqlresult[$x]['changedate']);
				$costperhr = $this->ramcost($sqlresult[$x-1]['changeram']);
				$cost = $cost + ($hours * $costperhr);
			}

		}

		$cost = money_format('$%(#10n', $cost);

		return $cost;
	}

	public function checkdcrights($arg){
		$username = $this->quotes_to_entities($this->pas, $_SESSION['PSAdminNM']);
		$password = $this->quotes_to_entities($this->pas, $_SESSION['PSAdminPD']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);

		$sql = "SELECT u.iduser, r.dc, r.report, r.serverman, r.projman ";
		$sql .= "FROM `user` u, `rights` r, `apilogin` a ";
		$sql .= "WHERE u.iduser = r.iduser AND a.idapilogin = r.idapilogin ";
		$sql .= "AND u.username = '$username' AND a.dcname = '$dcname'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;

	}

	public function checkrights($arg){
		$username = $this->quotes_to_entities($this->pas, $_SESSION['PSAdminNM']);
		$password = $this->quotes_to_entities($this->pas, $_SESSION['PSAdminPD']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dc']);


		$sql = "SELECT u.iduser, r.admin, r.monitor, r.dns FROM `user` u, `rights` r ";
		$sql .= "WHERE u.iduser = r.iduser AND r.idapilogin = 1 ";
		$sql .= "AND username = '$username' AND password = '$password'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;

	}

	// get hours between created data and to date
	private function createdtohours($servername, $to, $instance_id){
		$sqlhrs = "SELECT TIMESTAMPDIFF(MINUTE,created,'$to') AS hours ";
		$sqlhrs .= "FROM `reports` WHERE servername = '$servername' AND instance_id = '$instance_id'";
		$sqlresulthrs = $this->select($this->pas, $sqlhrs);
		$hours = $sqlresulthrs[0]['hours']/60;
		return $hours;
	}

	public function createtempdnstable(){
		$create = "CREATE TEMPORARY TABLE `tmpdns` (name varchar(100) )";
		$this->create($this->pas, $create);
	}

	public function createtempimagetable(){
		$create = "CREATE TEMPORARY TABLE `tmpimage` (id varchar(38), name varchar(45), created datetime, dcname varchar(4) )";
		$this->create($this->pas, $create);
	}

	public function createtempservertable(){
		$create = "CREATE TEMPORARY TABLE `rackspace` (id varchar(38), name varchar(45), created datetime, dcname varchar(4), ram int(11), ip_address varchar(45) )";
		$this->create($this->pas, $create);
	}

	public function editdcs($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);
		$apikey = $this->quotes_to_entities($this->pas, $arg['apikey']);
		$provider = $this->quotes_to_entities($this->pas, $arg['provider']);
		$dcnameold = $this->quotes_to_entities($this->pas, $arg['dcnameold']);

		$message = "Susscess";

		$sql = "UPDATE `apilogin` SET dcname = '$dcname', username = '$username', password = '$password',";
		$sql .= "apikey = '$apikey', provider = '$provider' WHERE dcname = '$dcnameold'";

		$result = $this->update($this->pas, $sql);

		if (is_array($result) && $result[0] == "ERR") {
			$message = $result[3];
			return $message;
		}

		return $message;
	}

	// edit label info
	public function editlabel($arg) {
		$labelname = $this->quotes_to_entities($this->pas, $arg['labelname']);
		$oldlabel = $this->quotes_to_entities($this->pas, $arg['oldlabel']);

		$message = "Susscess";

		$sql = "UPDATE `reportlabels` SET labelname = '$labelname'";
		$sql .= " WHERE labelname = '$oldlabel'";

		$result = $this->update($this->pas, $sql);

		if (is_array($result) && $result[0] == "ERR") {
			$message = $result[3];
			return $message;
		}
		return $message;
	}

	public function edituser($arg) {
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);
		$admin = $this->quotes_to_entities($this->pas, $arg['admin']);
		$dns = $this->quotes_to_entities($this->pas, $arg['dns']);
		$monitor = $this->quotes_to_entities($this->pas, $arg['monitor']);
		$usernameold = $this->quotes_to_entities($this->pas, $arg['usernameold']);

		$message = "Susscess";

		$sql = "UPDATE `user` SET username = '$username', password = '$password'";
		$sql .= " WHERE username = '$usernameold'";

		$result = $this->update($this->pas, $sql);

		if (is_array($result) && $result[0] == "ERR") {
			$message = $result[3];
			return $message;
		}

		$sql = "UPDATE `rights` r, `user` u SET admin = '$admin', dns = '$dns', monitor = '$monitor' WHERE u.iduser = r.iduser";
		$sql .= " AND r.idapilogin = 1 AND u.username = '$username'";

		$result = $this->update($this->pas, $sql);

		if (is_array($result) && $result[0] == "ERR") {
			$message = $result[3];
			return $message;
		}

		return $message;

	}

	// get hour between form and to dates
	private function fromtohrs($servername, $instance_id, $from, $to){
		$sqlhrs = "SELECT TIMESTAMPDIFF(MINUTE,'$from','$to') AS hours ";
		$sqlhrs .= "FROM reports WHERE servername = '$servername' AND instance_id = '$instance_id'";
		$sqlresulthrs = $this->select($this->pas, $sqlhrs);
		$hours = $sqlresulthrs[0]['hours']/60;
		return $hours;
	}

	// get API data
	public function getApiData($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);

		$sql = "SELECT username, apikey FROM `apilogin` WHERE dcname = '$dcname'";

		$sqlresult = $this->select($this->pas, $sql);

		$sql = "SELECT authurl FROM `authurl` WHERE idauthurl = 1";
		$sqlauth = $this->select($this->pas, $sql);

		$sqlresult[0]['authurl'] = $sqlauth[0]['authurl'];

		return $sqlresult;
	}

	protected function getConstant(){
		$sql = "SELECT * FROM `sitesettings` WHERE 1";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function getcostdata($arg) {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$label = $this->quotes_to_entities($this->pas, $arg['label']);

		$from = date('Y-m-d', strtotime($this->quotes_to_entities($this->pas, $arg['from'])));
		$to = date('Y-m-d', strtotime($this->quotes_to_entities($this->pas, $arg['to'])));

		$datalist = array ();

		$create = "CREATE TEMPORARY TABLE `reports` (id INT NOT NULL AUTO_INCREMENT, ";
		$create .= "instance_id varchar(38), servername varchar(45), created DATETIME, ";
		$create .= "deleted DATETIME, ram int(11), PRIMARY KEY (`id`))";
		$result = $this->create($this->pas, $create);

		if ($label != "ALL") {
			$sqlinsert = "INSERT INTO `reports` (instance_id, servername, created, deleted, ";
			$sqlinsert .= "ram) SELECT s.instance_id, s.servername, s.created, ";
			$sqlinsert .= "'0000-00-00 00:00:00' AS deleted, s.ram FROM `servers` s, ";
			$sqlinsert .= "`reportlabels` r, `accounting` a WHERE dcname = '$dcname' AND ";
			$sqlinsert .= "created <= '$to' AND a.idservers = s.idservers AND ";
			$sqlinsert .= "a.idreportlabels = r.idreportlabels AND r.labelname = '$label' ";
			$sqlinsert .= "AND a.checked = 'Y'";
			if ($idcustomers > 1) {
				$sqlinsert .= " AND idcustomers = $idcustomers";
			}

			$this->insert($this->pas, $sqlinsert);

			$sqlcount = "SELECT count(*) as count FROM `reports`";

			$result = $this->select($this->pas, $sqlcount);

			$count = $result[0]["count"];

			if ($count == 0) {
				$new = array('servername' => "",
						'instance_id' => "",
						'created' => "00/00/0000",
						'deleted'=>"00/00/0000",
						'cost'=>"0",
				);

				$datalist[0] = $new;
				return $datalist;
			}
		} else {
			$sqlinsert = "INSERT INTO `reports` (instance_id, servername, created, deleted, ";
			$sqlinsert .= "ram) SELECT instance_id, servername, created, ";
			$sqlinsert .= "'0000-00-00 00:00:00' AS deleted, ram FROM `servers` ";
			$sqlinsert .= "WHERE dcname = '$dcname' AND created <= '$to' ";
			if ($idcustomers > 1) {
				$sqlinsert .= "AND idcustomers = $idcustomers ";
			}
			$sqlinsert .= "UNION SELECT instance_id, servername, created, deleted, ram ";
			$sqlinsert .= "FROM `deleted_servers` WHERE dcname = '$dcname' AND ";
			$sqlinsert .= "created <= '$to'";
			if ($idcustomers > 1) {
				$sqlinsert .= "AND idcustomers = $idcustomers";
			}

			$result = $this->insert($this->pas, $sqlinsert);

			$sqlcount = "SELECT count(*) as count FROM `reports`";

			$result = $this->select($this->pas, $sqlcount);

			$count = $result[0]["count"];

			if ($count == 0) {
				$new = array('servername' => "",
						'instance_id' => "",
						'created' => "00/00/0000",
						'deleted'=>"00/00/0000",
						'cost'=>"0",
				);

				$datalist[0] = $new;
				return $datalist;
			}
		}

		$sql = "SELECT * FROM `reports` ORDER BY created";

		$sqlresult = $this->select($this->pas, $sql);

		$count = count($sqlresult);

		for ($x=0; $x < $count; $x++){
			$new = array();
			$data = $sqlresult[$x];
			$instance_id = $data['instance_id'];
			$servername = $data['servername'];
			$ram = $data['ram'];
			$createdDate = $data['created'];
			$deleteDate = $data['deleted'];
			$new['servername'] = $servername;
			$new['instance_id'] = $instance_id;
			$new['created'] = date("m/d/Y",strtotime($data['created']));
			if ($deleteDate == "0000-00-00 00:00:00") {
				$new['deleted'] = " ";
			} else {
				$new['deleted'] = date("m/d/Y",strtotime($deleteDate));
			}

			if (strtotime($from) > strtotime($deleteDate) && $deleteDate != "0000-00-00 00:00:00") {
				continue;
			}

			$cost = $this->calculatecost($servername, $from, $to, $instance_id, $ram, $createdDate, $deleteDate);

			$new['cost'] = $cost;

			$datalist[$x] = $new;
		}

		$drop = "DROP TEMPORARY TABLE `reports`";
		$this->drop($this->pas, $drop);

		return $datalist;
	}

	public function getCustsDns() {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);

		$sql = "SELECT name FROM `dnsdata`";
		if ($idcustomers > 1) {
			$sql .= " WHERE idcustomers = $idcustomers";
		}
		
		$sqlresult = $this->select($this->pas, $sql);
		
		return $sqlresult;
	}

	public function getCustsImages() {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);

		$sql = "SELECT idrackspaceimage FROM `images`";
		if ($idcustomers > 1) {
			$sql .= " WHERE idcustomers = $idcustomers";
		}

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function getCustsServers() {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);

		$sql = "SELECT instance_id FROM `servers`";
		if ($idcustomers > 1) {
			$sql .= " WHERE idcustomers = $idcustomers";
		}

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	// get DC's info
	public function getDCInfo($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);

		$sql = "SELECT * FROM `apilogin` WHERE dcname = '$dcname'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function getDNSData() {
		$username = $this->quotes_to_entities($this->pas, $_SESSION['PSAdminNM']);

		$sql = "SELECT apikey, secretkey FROM `dnsapi`";
		/*$sql = "SELECT d.apikey, d.secretkey FROM `dnsapi` d, `user` u ";
		$sql .= "WHERE u.idcustomers = d.idcustomers AND username = '$username'";*/

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	// get label check status
	public function getLabelData($arg) {
		$labelname = $this->quotes_to_entities($this->pas, $arg['labelname']);
		$instance_id = $this->quotes_to_entities($this->pas, $arg['instance_id']);

		$sql = "SELECT a.checked FROM `servers` s, `reportlabels` r, `accounting` a ";
		$sql .= "WHERE a.idservers = s.idservers AND a.idreportlabels = r.idreportlabels ";
		$sql .= "AND r.labelname = '$labelname' AND s.instance_id = '$instance_id'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}


	// get list of labels
	public function getlabels() {
			
		$sql = "SELECT labelname FROM `reportlabels`";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function getDCs($arg) {
		$mode = $arg['mode'];

		$sql = "";
		if ($mode == "all") {
			$sql = "SELECT * FROM `apilogin`";
		} else {
			$sql = "SELECT * FROM `apilogin` WHERE dcname != 'ADM'";
		}

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	// get user rights
	public function getRightsInfo($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);

		$sql = "SELECT r.dc, r.report, r.serverman, r.projman ";
		$sql .= "FROM `user` u, `rights` r, `apilogin` a ";
		$sql .= "WHERE u.iduser = r.iduser AND a.idapilogin = r.idapilogin ";
		$sql .= "AND u.username = '$username' AND a.dcname = '$dcname'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function getserverip($pas) {
		$sql = "SELECT servername, ip_address ";
		$sql .= "FROM `servers` ";
		$sql .= "WHERE monitored = 'Y'";

		$sqlresult = $this->select($pas, $sql);

		return $sqlresult;
	}

	public function getservermont($arg) {
		$instance_id = $this->quotes_to_entities($this->pas, $arg['id']);

		$sql = "SELECT monitored ";
		$sql .= "FROM `servers` ";
		$sql .= "WHERE instance_id = '$instance_id'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function getUserInfo($arg) {
		$username = $this->quotes_to_entities($this->pas, $arg['username']);

		$sql = "SELECT u.username, u.password, r.admin, r.dns, r.monitor ";
		$sql .= "FROM `user` u, `rights` r ";
		$sql .= "WHERE u.iduser = r.iduser AND username = '$username'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	public function loadrights($arg) {
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$dc = $this->quotes_to_entities($this->pas, $arg['dc']);
		$report = $this->quotes_to_entities($this->pas, $arg['report']);
		$serverman = $this->quotes_to_entities($this->pas, $arg['serverman']);
		$projman = $this->quotes_to_entities($this->pas, $arg['projman']);

		$message = "Susscess";

		$sql = "SELECT iduser FROM `user` WHERE username = '$username'";
		$result = $this->select($this->pas, $sql);
		$iduser = $result[0]['iduser'];

		$sql = "SELECT idapilogin FROM `apilogin` WHERE dcname = '$dcname'";
		$result = $this->select($this->pas, $sql);
		$idapilogin = $result[0]['idapilogin'];

		$sql = "SELECT r.idrights FROM `user` u, `rights` r, `apilogin` a ";
		$sql .= "WHERE u.iduser = r.iduser AND a.idapilogin = r.idapilogin ";
		$sql .= "AND u.username = '$username' AND a.dcname = '$dcname'";

		$sqlresult = $this->select($this->pas, $sql);
		if (is_array($sqlresult) && $sqlresult[0] == "ERR") {
			$sqlinsert = "INSERT INTO `rights` (iduser, idapilogin, dc, report, serverman, projman) VALUES ";
			$sqlinsert .= "('$iduser','$idapilogin','$dc','$report', '$serverman', '$projman')";

			$data = $this->insert($this->pas, $sqlinsert);

			if (is_array($data) && $data[0] == "ERR") {
				$message = $data[3];
				return $message;
			}
		} else {
			$sql = "UPDATE `rights` r, `user` u, `apilogin` a SET dc = '$dc', report = '$report', ";
			$sql .= "serverman = '$serverman', projman = '$projman' WHERE u.iduser = r.iduser";
			$sql .= " AND r.idapilogin = '$idapilogin' AND u.username = '$username'";
			$result = $this->update($this->pas, $sql);

			if (is_array($result) && $result[0] == "ERR") {
				$message = $result[3];
				return $message;
			}
		}

		return $message;
	}

	public function loadtempdnstable($arg) {
		$name = $this->quotes_to_entities($this->pas, $arg['name']);

		$sqlinsert = "INSERT INTO `tmpdns` (name) VALUES ";
		$sqlinsert .= "('$name')";

		$data = $this->insert($this->pas, $sqlinsert);
	}

	public function loadtempimagetable($arg) {
		$id = $this->quotes_to_entities($this->pas, $arg['id']);
		$name = $this->quotes_to_entities($this->pas, $arg['name']);
		$created = $this->quotes_to_entities($this->pas, $arg['created']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);

		$sqlinsert = "INSERT INTO `tmpimage` (id, name, created, dcname) VALUES ";
		$sqlinsert .= "('$id','$name','$created', '$dcname')";

		$data = $this->insert($this->pas, $sqlinsert);
		if (is_array($data) && $data[0] == "ERR") {
			$message = $data[3];
			echo $message;
			//return $message;
		}
	}

	public function loadtempservertable($arg) {
		$id = $this->quotes_to_entities($this->pas, $arg['id']);
		$name = $this->quotes_to_entities($this->pas, $arg['name']);
		$created = $this->quotes_to_entities($this->pas, $arg['created']);
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$ram = $this->quotes_to_entities($this->pas, $arg['ram']);
		$ip_address = $this->quotes_to_entities($this->pas, $arg['ip_address']);

		$sqlinsert = "INSERT INTO `rackspace` (id, name, created, dcname, ram, ip_address) VALUES ";
		$sqlinsert .= "('$id','$name','$created', '$dcname','$ram', '$ip_address')";

		$data = $this->insert($this->pas, $sqlinsert);
		if (is_array($data) && $data[0] == "ERR") {
			$message = $data[3];
			echo $message;
			//return $message;
		}
	}

	public function login($arg) {
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);

		$sql = "SELECT * FROM `user` WHERE username = '$username' AND password = '$password'";

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	// timestamp last login
	public function loginupdate($arg) {
		$username = $this->quotes_to_entities($this->pas, $arg['username']);

		$sql = "UPDATE `user` SET lastlogin = NOW() ";
		$sql .= " WHERE username = '$username'";

		$result = $this->update($this->pas, $sql);

		return $sqlresult;
	}

	function namepass($arg) {
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$password = $this->quotes_to_entities($this->pas, $arg['password']);

		$sql = "SELECT count(*) AS count FROM `user` WHERE username = '$username' AND password = '$password'";

		$sqlresult = $this->select($this->pas, $sql);

		$count = $sqlresult[0]['count'];

		if ($count > 0) {
			return "true";
		} else {
			return "false";
		}
	}
	##function to check admin is logged in or not, if not logged in redirect to login page
	function not_login()
	{
		if(!$_SESSION['PSAdminID'])
		{
			$url_s=base64_encode("http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']);
			header("Location:".SITE_PATH."login/?mode=nologin&url_s=".$url_s);
			exit;
		}
	}

	##function to sanitize user inserted harmful data
	protected function quotes_to_entities($db, $str)
	{
		return mysqli_real_escape_string($db, $str);
	}

	// get cost of server per size/hr
	private function ramcost($ram) {
		$sqlcost = "SELECT cost FROM `cost` WHERE ram = $ram";

		$sqlresultcost = $this->select($this->pas, $sqlcost);
		$costperhr = $sqlresultcost[0]['cost'];
		return $costperhr;
	}

	// set image as deleted
	public function removeimage($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$idrackspaceimage = $this->quotes_to_entities($this->pas, $arg['idrackspaceimage']);

		$sql = "UPDATE `images` SET deleted = NOW(), userdelete = '$username'";
		$sql .= " WHERE dcname = '$dcname' AND idrackspaceimage = '$idrackspaceimage'";

		$this->update($this->pas, $sql);

	}

	// set server as deleted
	public function removeserver($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$instance_id = $this->quotes_to_entities($this->pas, $arg['instance_id']);

		$sql = "SELECT * FROM `servers` WHERE dcname = '$dcname' AND instance_id = '$instance_id'";

		$data = $this->select($this->pas, $sql);

		$servername = $data[0]['servername'];
		$instance_id = $data[0]['instance_id'];
		$ip_address= $data[0]['ip_address'];
		$dcname = $data[0]['dcname'];
		$created =  $data[0]['created'];
		$ram = $data[0]['ram'];
		$account = $data[0]['account'];
		$usercreate = $data[0]['usercreate'];
		$idcustomers = $data[0]['idcustomers'];

		$sql = "INSERT INTO `deleted_servers` (servername, instance_id, ip_address, dcname, created, ";
		$sql .= "deleted, ram, account, usercreate, userdelete, idcustomers) VALUES ";
		$sql .= "('$servername', '$instance_id', '$ip_address', '$dcname', '$created', UTC_TIMESTAMP(),";
		$sql .= "$ram, $account, '$usercreate', '$username', '$idcustomers')";

		$data = $this->insert($this->pas, $sql);

		$deletestring = "DELETE FROM `servers` WHERE instance_id = '$instance_id' ";
		$deletestring .= "AND servername = '$servername'";
		$result = $this->delete($this->pas, $deletestring);
	}

	public function retrieveUsers() {
		$idcustomers = $this->quotes_to_entities($this->pas, $_SESSION['idcustomers']);

		$sql = "SELECT * FROM `user`";
		if ($idcustomers > 1) {
			$sql .= " WHERE idcustomers = $idcustomers";
		}

		$sqlresult = $this->select($this->pas, $sql);

		return $sqlresult;
	}

	// set resize info
	public function resizeserver($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$username = $this->quotes_to_entities($this->pas, $arg['username']);
		$instance_id = $this->quotes_to_entities($this->pas, $arg['instance_id']);
		$changeram = $this->quotes_to_entities($this->pas, $arg['ram']);

		$sqlselect = "SELECT idservers FROM `servers` ";
		$sqlselect .= "WHERE dcname = '$dcname' AND instance_id = '$instance_id' ";

		$sqlresult = $this->select($this->pas, $sqlselect);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			$idservers = $sqlresult[0]['idservers'];

			$sqlinsert = "INSERT INTO `changes` (instance_id, dcname, changedate, changeram, userchange) VALUES ";
			$sqlinsert .= "('$instance_id','$dcname', NOW(), '$changeram', '$username')";

			$data = $this->insert($this->pas, $sqlinsert);
		}
	}

	##===THIS FUNCTION DECLARES THE CONST VARIABLES===##
	function setConstant(){
		$res=$this->getConstant();
		foreach ($res as $constant) {
			if($constant['name'] == "SITE_PATH") {
				define(SITEJS,$constant['value']."resources/js/");
				define(SITECSS,$constant['value']."resources/css/");
				define(SITEIMG,$constant['value']."resources/images/");
			}
			define($constant['name'],$constant['value']);
		}
	}

	public function syndnsdatabase(){

		$sql = "SELECT * FROM `tmpdns` t ";
		$sql .= "WHERE NOT EXISTS (";
		$sql .= "SELECT * FROM `dnsdata` d ";
		$sql .= "WHERE d.name = t.name ";
		$sql .= ") order by t.name";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $tmpimage) {
				$name = $tmpimage['name'];

				$sql = "INSERT INTO `dnsdata` (name, idcustomers ";
				$sql .= ") VALUES ('$name', 1 )";

				$data = $this->insert($this->pas, $sql);
			}
		}

		$sql = "SELECT iddnsdata, name FROM `dnsdata` d ";
		$sql .= "WHERE NOT EXISTS (";
		$sql .= "SELECT * FROM `tmpdns` t ";
		$sql .= "WHERE d.name = t.name ";
		$sql .= ") order by d.name";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $images) {
				$iddnsdata = $images['iddnsdata'];
				$name = $images['name'];

				$deletestring = "DELETE FROM `dnsdata` WHERE iddnsdata = '$iddnsdata' ";
				$deletestring .= "AND name = '$name'";

				$this->delete($this->pas, $deletestring);
			}
		}
	}

	public function synimagedatabase($arg){
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);
		$sql = "SELECT * FROM `tmpimage` t ";
		$sql .= "WHERE NOT EXISTS (";
		$sql .= "SELECT * FROM `images` i ";
		$sql .= "WHERE i.imagename = t.name ";
		$sql .= "AND i.idrackspaceimage = t.id ";
		$sql .= ") AND t.dcname = '$dcname' order by t.name";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $tmpimage) {
				$Id = $tmpimage['id'];
				$name = $tmpimage['name'];
				$created = $tmpimage['created'];
				$dc = $tmpimage['dcname'];

				$sql = "INSERT INTO `images` (imagename, idrackspaceimage, dcname, ";
				$sql .= "created, usercreate) VALUES ('$name', '$Id', '$dc', ";
				$sql .= "'$created', 'system')";

				$data = $this->insert($this->pas, $sql);
			}
		}

		$sql = "SELECT idimages, imagename FROM `images` i ";
		$sql .= "WHERE NOT EXISTS (";
		$sql .= "SELECT * FROM `tmpimage` t ";
		$sql .= "WHERE i.imagename = t.name ";
		$sql .= "AND i.idrackspaceimage = t.id ";
		$sql .= ") AND i.dcname = '$dcname' order by i.imagename";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $images) {
				$idimages = $images['idimages'];
				$imagename = $images['imagename'];

				$deletestring = "DELETE FROM `images` WHERE idimages = '$idimages' ";
				$deletestring .= "AND imagename = '$imagename'";

				$this->delete($this->pas, $deletestring);
			}
		}

		$sql = "SELECT i.idrackspaceimage, t.created FROM `images` i, `tmpimage` t ";
		$sql .= "WHERE i.created != t.created AND i.idrackspaceimage = t.id";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $data) {
				$idrackspaceimage = $data['idrackspaceimage'];
				$created = $data['created'];

				$sqlupdate = "UPDATE `images` SET created = '$created' ";
				$sqlupdate .= "WHERE idrackspaceimage = '$idrackspaceimage'";

				$this->update($this->pas, $sqlupdate);
			}
		}

		$drop = "DROP TEMPORARY TABLE `tmpimage`";
		$this->drop($this->pas, $drop);
	}

	public function synserverdatabase($arg) {
		$dcname = $this->quotes_to_entities($this->pas, $arg['dcname']);

		$sql = "SELECT * FROM `rackspace` r ";
		$sql .= "WHERE NOT EXISTS (";
		$sql .= "SELECT * FROM `servers` s ";
		$sql .= "WHERE s.servername = r.name ";
		$sql .= "AND s.instance_id = r.id ";
		$sql .= ") AND r.dcname = '$dcname' order by r.name";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $rackspace) {
				$Id = $rackspace['id'];
				$name = $rackspace['name'];
				$created = $rackspace['created'];
				$dc = $rackspace['dcname'];
				$ram = $rackspace['ram'];
				$ip_address= $rackspace['ip_address'];

				$sql = "INSERT INTO `servers` (dcname, instance_id, servername, ram, ";
				$sql .= "ip_address, created, usercreate) VALUES ('$dc', '$Id', '$name', ";
				$sql .= "$ram, '$ip_address', '$created', 'system')";

				$data = $this->insert($this->pas, $sql);
			}
		}

		$sql = "SELECT idservers, servername FROM `servers` s ";
		$sql .= "WHERE NOT EXISTS (";
		$sql .= "SELECT * FROM `rackspace` r ";
		$sql .= "WHERE s.servername = r.name ";
		$sql .= "AND s.instance_id = r.id ";
		$sql .= ") AND s.dcname = '$dcname' order by s.servername";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $server) {
				$idservers = $server['idservers'];
				$servername = $server['servername'];

				$sql = "SELECT * FROM `servers` WHERE idservers = '$idservers' AND servername = '$servername'";

				$data = $this->select($this->pas, $sql);

				$servername = $data[0]['servername'];
				$instance_id = $data[0]['instance_id'];
				$ip_address= $data[0]['ip_address'];
				$dcname = $data[0]['dcname'];
				$created =  $data[0]['created'];
				$ram = $data[0]['ram'];
				$account = $data[0]['account'];
				$usercreate = $data[0]['usercreate'];
				$idcustomer = $data[0]['idcustomer'];

				$sql = "INSERT INTO `deleted_servers` (servername, instance_id, ip_address, ";
				$sql .= "dcname, created, deleted, ram, account, usercreate, userdelete, ";
				$sql .= "idcustomer) VALUES ('$servername', '$instance_id', '$ip_address',";
				$sql .= "'$dcname','$created', UTC_TIMESTAMP(), $ram, $account, ";
				$sql .= "'$usercreate', 'system', '$idcustomer')";

				$data = $this->insert($this->pas, $sql);

				$deletestring = "DELETE FROM `servers` WHERE idservers = '$idservers' ";
				$deletestring .= "AND servername = '$servername'";
				$result = $this->delete($this->pas, $deletestring);
			}
		}

		$sqlipaddress = "SELECT * FROM `rackspace` WHERE dcname = '$dcname'";

		$sqlresult = $this->select($this->pas, $sqlipaddress);

		foreach($sqlresult AS $rackspace) {
			$Id = $rackspace['id'];
			$name = $rackspace['name'];
			$ip_address= $rackspace['ip_address'];

			$sql = "SELECT * FROM `servers` WHERE instance_id = '$Id' AND servername = '$name'";

			$data = $this->select($this->pas, $sql);

			if ($ip_address != $data[0]['ip_address']) {
				$sqlupdate = "UPDATE `servers` SET ip_address = '$ip_address' ";
				$sqlupdate .= "WHERE instance_id = '$Id' AND servername = '$name'";

				$this->update($this->pas, $sqlupdate);
			}
		}

		$sql = "SELECT * FROM `deleted_servers` ds, `servers` s WHERE ds.instance_id = s.instance_id ";
		$sql .= "AND ds.servername = s.servername";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $data) {
				$servername = $data['servername'];
				$instance_id = $data['instance_id'];
					
				$deletestring = "DELETE FROM `deleted_servers` WHERE instance_id = '$instance_id' ";
				$deletestring .= "AND servername = '$servername'";

				$result = $this->delete($this->pas, $deletestring);
			}
		}

		$sql = "SELECT s.instance_id, r.created FROM `servers` s, `rackspace` r ";
		$sql .= "WHERE s.created != r.created AND s.instance_id = r.id";

		$sqlresult = $this->select($this->pas, $sql);

		if (is_array($sqlresult) && $sqlresult[0] != "ERR") {
			foreach($sqlresult AS $data) {
				$instance_id = $data['instance_id'];
				$created = $data['created'];

				$sqlupdate = "UPDATE `servers` SET created = '$created' ";
				$sqlupdate .= "WHERE instance_id = '$instance_id'";

				$this->update($this->pas, $sqlupdate);
			}
		}

		$drop = "DROP TEMPORARY TABLE `rackspace`";
		$this->drop($this->pas, $drop);

	}

	public function updateaccount($arg) {
		$label = $this->quotes_to_entities($this->pas, $arg[0]);

		$count = count($arg);
		for ($x=1; $x < $count; $x++){
			list($instance_id, $checked) = explode("|",$arg[$x]);

			$sql = "SELECT a.checked FROM `servers` s, `reportlabels` r, `accounting` a ";
			$sql .= "WHERE a.idservers = s.idservers AND a.idreportlabels = r.idreportlabels ";
			$sql .= "AND r.labelname = '$label' AND s.instance_id = '$instance_id'";

			$sqlresult = $this->select($this->pas, $sql);

			if (is_array($sqlresult) && $sqlresult[0] == "ERR") {
				if ($checked == "N") {
					continue;
				} else {
					$sqlinsert = "INSERT INTO `accounting` (checked, idservers, idreportlabels) VALUES ";
					$sqlinsert .= "('Y', (SELECT idservers FROM servers WHERE instance_id = '$instance_id'), ";
					$sqlinsert .= "(SELECT idreportlabels FROM reportlabels WHERE labelname = '$label'))";

					$this->insert($this->pas, $sqlinsert);
				}
			} else {
				if ($sqlresult[0]['checked'] == $checked) {
					continue;
				} else {
					$sqlupdate = "UPDATE `servers` s, `reportlabels` r, `accounting` a ";
					$sqlupdate .= "SET a.checked = '$checked' WHERE a.idservers = s.idservers ";
					$sqlupdate .= "AND a.idreportlabels = r.idreportlabels AND ";
					$sqlupdate .= "r.labelname = '$label' AND s.instance_id = '$instance_id'";

					$this->update($this->pas, $sqlupdate);
				}
			}
		}
	}
}
?>
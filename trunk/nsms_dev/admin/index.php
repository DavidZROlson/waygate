<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
?>

<div class="right_content">
	<h2>USERS</h2>

	<div class="form">
		<?php
		if (isset ($_POST['userselect'])) {
			$objUser = new User();
			$userselect = $_POST['userselect'];

			$arguments = array ('username' => $userselect);

			$data = $objUser->getUserInformation($arguments);
			$userinfo = $data[0];
		}

		// get rights for selected user
		if (isset ($_POST['dcselect'])) {
			$dcselect= $_POST['dcselect'];
			$username = $_POST['userselect'];

			// select a DC
			if ($dcselect != "Select a DC") {
				$objUser = new User();
				$arguments = array (
						'dcname' => $dcselect,
						'username' => $username,
				);

				$data = $objUser->getRightsInformation($arguments);
				$rightsinfo = $data[0];

				if ($rightsinfo == "ERR") {
					$rightsinfo["dc"] = "N";
					$rightsinfo["report"] = "N";
					$rightsinfo["serverman"] = "N";
					$rightsinfo["projman"] = "N";
				}
			} else {
				// do nothing
			}
		}

		if (isset ($_POST['add'])) {// add a new user
			$objUser = new User();
			AddEditUser($_POST['add']);
			$username = $_POST["username"];

			$arguments = array ('username' => $username);

			$data = $objUser->getUserInformation($arguments);
			$userinfo = $data[0];
		} else if (isset ($_POST['editr'])) { // edit a user
			$objUser = new User();
			$dc = "N";
			$report = "N";
			$serverman = "N";
			$projman = "N";

			$username = $_POST["username"];
			$dcname = $_POST['dcselect'];
			if (isset ($_POST["dc"])) {
				$dc = "Y";
			}

			// set right to Y if box is checked
			if (isset ($_POST["report"])) {
				$report = "Y";
			}
			if (isset ($_POST["serverman"])) {
				$serverman = "Y";
			}
			if (isset ($_POST["projman"])) {
				$projman = "Y";
			}

			$arguments = array (
					'username' => $username,
					'dcname' => $dcname,
					'dc' => $dc,
					'report' => $report,
					'serverman' => $serverman,
					'projman' => $projman,
			);

			$data = $objUser->loaduserrights($arguments);

			if ($data != "Susscess") {
				include('../common/common.php');

				confirm($data);
				header("Refresh: 0;url=../index.php");
			}
			header("Refresh: 0;url=../index.php");
		} else if (isset ($_POST['editu'])) { // edit select users rights
			AddEditUser($_POST['editu']);
			$username = $_POST["username"];

			$arguments = array ('username' => $username);

			$data = $objUser->getUserInformation($arguments);
			$userinfo = $data[0];
		}

		// add/edit user and rights
		function AddEditUser($mode) {
			$objUser = new User();
			$message = "";
			$admin = "N";
			$monitor = "N";
			$dns = "N";

			$username = $_POST["username"];
			$password = $_POST["password"];
			$usernameold = $_POST["userselect"];

			if (isset ($_POST["admin"])) {
				$admin = "Y";
			}
			if (isset ($_POST["monitor"])) {
				$monitor = "Y";
			}
			if (isset ($_POST["dns"])) {
				$dns = "Y";
			}

			// validate username and password
			if (empty($username)) {
				$message = "Please enter a valid User Name";
			}
			if (empty($password)) {
				$message = "Please enter a valid Password";
			}
			if ($message != "") {
				include('../common/common.php');

				confirm($message);
				header("Refresh: 0;url=../index.php");
			} else {
				$arguments = array (
						'username' => $username,
						'password' => $password,
						'admin' => $admin,
						'monitor' => $monitor,
						'dns' => $dns,
				);

				$data = "";
				if ($mode == "Add") {
				$data = $objUser->adduserinfo($arguments);
			} else {
				$arguments['usernameold'] = $usernameold;
				$data = $objUser->edituserinfo($arguments);
			}

			if ($data != "Susscess") {
				include('../common/common.php');

				confirm($data);
				header("Refresh: 0;url=../index.php");
			}
			}
		}

		if ($userselect == "") {
		?>
		<form action="index.php" method="post" class="niceform" name="frmUser"
			id="frmUser">
			<fieldset>
				<dl>
					<dt>
						<label><br> </label>
					</dt>
					<dd>
						<?
						$objUser = new User();
						$data = $objUser->getusers();
						$temp = $data[0];

						echo "<select name=\"userselect\" id=\"userselect\" onChange=\"document.frmUser.submit()\">";
						echo "<option selected>Select a User</option>";
						if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['username'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Username<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input class="text" name="username" id="username" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Password<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input type="password" autocomplete="off" class="text"
							name="password" id="password" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Admin Rights</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="admin" id="admin"
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>DNS Rights</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="dns" id="dns" size="35" />
					</dd>
				</dl>
				<!-- <dl>
					<dt>
						<label>Monitor Rights</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="monitor" id="monitor"
							size="35" />
					</dd>
				</dl> -->
				<dl class="submit">
					<input type="submit" style="" name="add" id="add" value="Add" />
				</dl>
			</fieldset>
		</form>
		<?
		// display info for selected users
	} else {
		?>
		<form action="index.php" method="post" class="niceform"
			name="frmUser1" id="frmUser1">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=userselect id="userselect"
							value="<?=$userinfo["username"]?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Username<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input class="text" name="username" id="username"
							value="<?=$userinfo["username"]?>" size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Password<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input type="password" autocomplete="off" class="text"
							name="password" id="password" value="<?=$userinfo["password"]?>"
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Admin Rights</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="admin" id="admin"
							value="Y"
							<?php if($userinfo["admin"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>DNS Rights</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="dns" id="dns" value="Y"
						<?php if($userinfo["dns"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<!-- <dl>
					<dt>
						<label>Monitor Rights</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="monitor" id="monitor"
							value="Y"
							<?php if($userinfo["monitor"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl> -->

				<dl class="submit">
					<?
					//  hide button
					if ($userselect != "" && $dcselect != "") {
					echo "<input type=\"hidden\" style=\"hidden\" name=\"editu\" id=\"editu\" value=\"Edit\"/>";
				}else if ($userselect != "") {
					echo "<input type=\"submit\" style=\"submit\" name=\"editu\" id=\"editu\" value=\"Edit\"/>";
				}
				?>
				</dl>

			</fieldset>

			<?
			// display selected user and rights to be set
			if ($dcselect == "") {
				?>
			<fieldset>
				<dl>
					<dt class="blank">
						<label><br> </label>
					</dt>
					<dd class="blank">
						<?
						$objUser = new User();
						$data = $objUser->retrievedcs();
						$temp = $data[0];

						echo "<select name=\"dcselect\" id=\"dcselect\" onChange=\"document.frmUser1.submit()\">";
						echo "<option selected>Select a DC</option>";
						if ($temp != "ERR") {
						foreach ($data as $temp) {
							echo "<option>" . $temp['dcname'] . "</option>";
						}
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt class="second">
						<label>DC</label>
					</dt>
					<dd class="second">
						<input class="text" type='checkbox' name="dc" id="dc" value="Y"
						<?php if($rightsinfo["dc"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt class="second">
						<label>Reports</label>
					</dt>
					<dd class="second">
						<input class="text" type='checkbox' name="report" id="report"
							value="Y"
							<?php if($rightsinfo["report"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt class="second">
						<label>Server Management</label>
					</dt>
					<dd class="second">
						<input class="text" type='checkbox' name="serverman"
							id="serverman" value="Y"
							<?php if($rightsinfo["serverman"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<!-- <dl>
					<dt class="second">
						<label>Project Management</label>
					</dt>
					<dd class="second">
						<input class="text" type='checkbox' name="projman" id="projman"
							value="Y"
							<?php if($rightsinfo["projman"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl> -->
			</fieldset>
			<? } else {?>
			<fieldset>
				<dl>
					<dt>
						<h2>
							<?echo $dcselect?>
						</h2>
					</dt>

					<dd>
						<input class="text" type="hidden" name=dcselect id="dcselect"
							value="<?=$dcselect?>" />
					</dd>
				</dl>

				<dl>
					<dt>
						<label>DC</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="dc" id="dc" value="Y"
						<?php if($rightsinfo["dc"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Reports</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="report" id="report"
							value="Y"
							<?php if($rightsinfo["report"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Server Management</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="serverman"
							id="serverman" value="Y"
							<?php if($rightsinfo["serverman"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl>
				<!-- <dl>
					<dt>
						<label>Project Management</label>
					</dt>
					<dd>
						<input class="text" type='checkbox' name="projman" id="projman"
							value="Y"
							<?php if($rightsinfo["projman"] == "Y"){echo 'checked="checked"';}?> "
							size="35" />
					</dd>
				</dl> -->
				<dl class="submit">
					<?
					//  hide button
					if ($dcselect != "") {
					echo "<input type=\"submit\" style=\"submit\" name=\"editr\" id=\"editr\" value=\"Edit\"/>";
				}
				?>
				</dl>
			</fieldset>
			<? }?>
			<dl class="submit">
				<input type="submit" name="back" id="back" value="Back"
					onclick="document.location='../index.php'; return false;" />
			</dl>
		</form>
		<? }?>

	</div>
</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
<?
require_once('../includes/footer.php');
?>
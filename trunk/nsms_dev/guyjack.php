<?php

$total = exec("cat /proc/meminfo | grep \"MemTotal:\" | awk '{print $2}'");
$total = number_format($total/1024, 2, '.', '');

$used = exec("cat /proc/meminfo | grep \"MemFree:\" | awk '{print $2}'");
$used = number_format($used/1024, 2, '.', '');

$free = $total - $used;

echo "Total: " . $total . " Used: " . $used . " Free: " . $free . "<br>";

?>
<?php
function confirm($msg)
{
	echo "<script type=\"text/javascript\">window.onload=function()
	{alert(\"".$msg."\");
}</script>";
}//end function

function getcurl($url, $args) {

	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_HEADER, 0); // set to 0 to eliminate header info from response
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Returns response data instead of TRUE(1)
	curl_setopt($ch, CURLOPT_POSTFIELDS, $args); // use HTTP POST to send form data
	$resp = trim(curl_exec($ch)); //execute post and get results
	curl_close ($ch);

	return $resp;
}

function is_date( $str ) {
	try {
		$dt = new DateTime( trim($str) );
	}
	catch( Exception $e ) {
		return false;
	}
	$month = $dt->format('m');
	$day = $dt->format('d');
	$year = $dt->format('Y');
	if( checkdate($month, $day, $year) ) {
		return true;
	}
	else {
		return false;
	}
}

function syncDns($list) {

	$objDns = new Dns();

	$objDns->createtmpdnstable();

	if (count($list) > 0) {
		foreach ($list as $name) {
			$arguments = array (
					'name' => $name,);
			$objDns->loadtmpdnstable($arguments);
		}
	}
	$objDns->syndnsdatbase();
}

function syncImage($cloudservers, $dcname) {
	$serv = $cloudservers->ServerList();
	$serv->Sort('name');

	$list = $cloudservers->ImageList();
	$list->Sort('name');

	$objDcs = new Dcs();

	$objDcs->createtmpimagetable();

	if ($list->Size() > 0) {
		while ($images = $list->Next()) {
			$Id = $images->id;
			$name = $images->name;
			$created = date('Y-m-d H:i:s', strtotime($images->created));
			$status = $images->status;
			$server = $images->server;
			if (isset($server)) {
				$arguments = array (
						'id' => $Id,
						'name' => $name,
						'dcname' => $dcname,
						'created' => $created,);
				$objDcs->loadtmpimagetable($arguments);
			}
		}
	}
	$arguments = array ('dcname' => $dcname);
	$objDcs->synimagedatbase($arguments);
}

function syncServer($cloudservers, $dcname) {
	$list = $cloudservers->ServerList();
	$list->Sort('name');

	$objDcs = new Dcs();

	$objDcs->createtmpservertable();

	if ($list->Size() > 0) {
		while ($servers = $list->Next()) {
			$Id = $servers->id;
			$name = $servers->name;
			$created = date('Y-m-d H:i:s', strtotime($servers->created));
			$public = $servers->accessIPv4;

			$dataflavor = $servers->flavor;
			$flavorId = $dataflavor->id;
			$flavor = $cloudservers->Flavor($flavorId);
			$ram = $flavor->ram;

			$arguments = array (
					'id' => $Id,
					'name' => $name,
					'dcname' => $dcname,
					'created' => $created,
					'ram' => $ram,
					'ip_address' => $public,);
			$objDcs->loadtmpservertable($arguments);
		}
	}
	$arguments = array ('dcname' => $dcname);
	$objDcs->synserverdatbase($arguments);
}
?>
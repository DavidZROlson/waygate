<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2.php');
require_once('../includes/left_menu.php');
require_once '../lib/php-opencloud.php';
require_once ('../common/common.php');
?>

<div class="right_content">
	<?

	$dcname = $_GET['dcname'];

	$arguments = array ('dcname' => $dcname);
	$data = $objDcs->getApiInfo($arguments);

	$apiusername = $data[0]['username'];
	$apikey= $data[0]['apikey'];
	$authurl = $data[0]['authurl'];

	$rackspace = new OpenCloud \ Rackspace($authurl, array (
			'username' => $apiusername,
			'apiKey' => $apikey
	));

	$cloudservers = $rackspace->Compute('cloudServersOpenStack', $dcname);

	syncServer($cloudservers, $dcname);

	$list = $cloudservers->ServerList();
	$list->Sort('name');

	/*if (isset ($_POST['monitored'])) {// add monitoring to server

	$monitoredservers[] = $_POST["monitoredservers"];
	$i = 0;

	$arguments = array();
	while ($servers = $list->Next()) {
	$id = $servers->id;
	$checked = $id . "|N";
	$arguments[$i] = $checked;
	$monitored = $monitoredservers[0];
	$count = count($monitored);
	for ($x = 0; $x < $count; $x++) {
			if ($id == $monitored[$x]) {
				$checked = $id . "|Y";
	$arguments[$i] = $checked;
	break;
	}
	}
	$i++;
	}

	$data = $objDcs->addservermont($arguments);
	$list->Reset();
	}*/

	?>
	<div class="fl" style="width: 200px;">
		<h2>
			<?php echo $dcname?>
		</h2>
	</div>

	<form name="frmUser" id="frmUser" method="post" action=""
		class="niceform">
		<fieldset>
			<table id="rounded-corner" summary="Admin">
				<thead>
					<tr>
						<th scope="col" class="rounded">SERVER NAME</th>
						<th scope="col" class="rounded">PRIMARY IP</th>
						<th scope="col" class="rounded">PRIVATE IP</th>
						<th scope="col" class="rounded">STATUS</th>
						<th scope="col" class="rounded">RAM AMOUNT</th>
						<!-- <th scope="col" class="rounded">MONITORED</th> -->
					</tr>
				</thead>

				<tbody>
					<?php
					// display server info in table
					if ($list->Size() > 0) {
					while ($servers = $list->Next()) {
						$Id = $servers->id;
						$name = $servers->name;
						$created = date('Y-m-d H:i:s', strtotime($servers->created));
						$status = $servers->status;
						$public = $servers->accessIPv4;
						$dataaddresses = $servers->addresses;
						$dataprivate = $dataaddresses->private;
						$private = $dataprivate[0]->addr;

						$dataflavor = $servers->flavor;
						$flavorId = $dataflavor->id;
						$flavor = $cloudservers->Flavor($flavorId);
						$ram = $flavor->ram;

						$data = $objDcs->getCustomersServers();
						$temp = $data[0];
						if ($temp != "ERR") {
							foreach ($data as $temp) {
								if ($Id == $temp['instance_id']) {

									/*$arguments = array (
									'id' => $Id,
									);

									$servermonit = $objDcs->getservermonitor($arguments);*/

									echo "<tr>";
									/*echo "<td class=\"labelcell\"><a href=\"serverinfo.php?ip=".$public."&name=".$name."\">".$name."</td>";*/
									echo "<td class=\"labelcell\">".$name."</td>";
									echo "<td class=\"labelcell\"><A HREF=https://".$public.":10000 target=\"_blank\">".$public."</A></td>";
									echo "<td class=\"labelcell\">".$private."</td>";
									echo "<td class=\"labelcell\">".$status."</td>";
									echo "<td class=\"labelcell\">".$ram."</td>";
									/*if ($servermonit[0]['monitored'] == "N") {
										echo "<td><input type=\"checkbox\" name=\"monitoredservers[]\"value=\"$Id\" /></td>";
									} else {
										echo "<td><input type=\"checkbox\" name=\"monitoredservers[]\"value=\"$Id\" checked = \"checked\" /></td>";
									}*/
									echo "</tr>";
								}
							}
						}
					}
				}
				?>
				</tbody>
			</table>
			<!-- <dl class="submit">
				<input type="submit" style="" name="monitored" id="monitored"
					value="Set Monitored" />
			</dl> -->
		</fieldset>
	</form>
	<form action="index.php" method="post" class="niceform" name="frmUser"
		id="frmUser">
		<dl class="submit">
			<input type="submit" name="back" id="back" value="Back"
				onclick="document.location='../index.php'; return false;" />
		</dl>
	</form>

</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
<?
require_once('../includes/footer.php');
?>
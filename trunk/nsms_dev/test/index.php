<?

require_once('../pi_classes/Admin.php');
$obj = new Admin();
$obj->not_login();

require_once('../includes/header1s.php');
?>

<script type="text/javascript">var thisFormName='frmUser';</script>
<?
require_once('../includes/header2s.php');
require_once('../includes/left_menu.php');
require_once '../lib/php-opencloud.php';
?>

<div class="right_content">
	<script type="text/javascript">
						function WaitForProgress() {
							$("#imgProgress").show();
							$.get("waitfor.php", function(data, textStatus) 
							{
								$("#imgProgress").hide();
								alert(data);
							});
						}
					</script>
	<div class="form">
		<?php 
		$dc = 'ORD';
		$option = 'createimage';

		$title = "";
		if ($dc != "") {
			$title = "TEST " . ucwords($dc). ":";
		}

		if ($option == "createimage") {
			$title = $title. " CREATE IMAGE";
			$objDcs = new Dcs();

			$arguments = array ('dcname' => $dc);
			$data = $objDcs->getApiInfo($arguments);

			$apiusername = $data[0]['username'];
			$apikey= $data[0]['apikey'];
			$authurl = $data[0]['authurl'];

			$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
			));

			$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dc);
			$serv = $cloudservers->ServerList();
			$serv->Sort('name');
		}

		if (isset ($_POST['createimag'])) {
			include('../common/common.php');
			$serverselect = $_POST['serverselect'];
			$imagename = $_POST['imagename'];

			// validate info
			if ($serverselect == "Select a Server") {
				confirm($serverselect);
			} elseif (empty($imagename)){
				confirm("Please enter a Image Name");
			} else {
				$dcname = $_POST['dcname'];
				$username = $_SESSION['PSAdminNM'];
				//$instance_id = $serverselect;
				echo $dcname . ", " . $username . "<br>";
				$objDcs = new Dcs();

				$arguments = array ('dcname' => $dcname);
				$data = $objDcs->getApiInfo($arguments);

				$apiusername = $data[0]['username'];
				$apikey= $data[0]['apikey'];
				$authurl = $data[0]['authurl'];

				$GetServInfoImCrAuth = new OpenCloud \ Rackspace($authurl, array (
					'username' => $apiusername,
					'apiKey' => $apikey
				));

				$cloudservers = $GetServInfoImCrAuth->Compute('cloudServersOpenStack', $dcname);
				
				$server = $cloudservers->Server($serverselect);
				$server->CreateImage($imagename);
				
				session_start();
				$serializeserver=serialize($server);
				$_SESSION['server']=$serializeserver;
				$_SESSION['terminal'] = 'ACTIVE';
				$_SESSION['timeout'] = 600;
				?>
					<img src="<?= SITEIMG ?>ajax-loader.gif" style="display: none;" id="imgProgress" />
				<?php 
				echo "<script type='text/javascript'>WaitForProgress();</script>\n";
			}
		}
		?>
		<div>
			<?php echo $title?>
		</div>

		<form action="index.php" method="post" class="niceform" name="frmUser"
			id="frmUser">
			<fieldset>
				<dl>
					<dd>
						<input class="text" type="hidden" name=dcname id="dcname"
							value="<?=$dc?>" />
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Server<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<?
						echo "<select name=\"serverselect\" id=\"serverselect\" >";
						echo "<option selected>Select a Server</option>";
						while($servers = $serv->Next()) {
						echo "<option value=".$servers->id.">" . $servers->name . "</option>";
					}
					echo "</select>";
					?>
					</dd>
				</dl>
				<dl>
					<dt>
						<label>Image Name<span style="color: red">*</span>
						</label>
					</dt>
					<dd>
						<input class="text" name="imagename" id="imagename" size="35" />
					</dd>
				</dl>
				<dl class="submit">
					<input type="submit" style="" name="createimag" id="createimag"
						value="Create" />
				</dl>
			</fieldset>
		</form>
	</div>

</div>
<!-- end of right content-->


</div>
<!--end of center content -->


<div class="clear"></div>
</div>
<!--end of main content-->
<?
require_once('../includes/footer.php');
?>
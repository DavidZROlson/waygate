<?php
require_once('../pi_classes/Admin.php');
$obj=new Admin();

if($_SESSION['PSAdminID']!='')
{
	header('Location:'.SITE_PATH);
}

$objlogin= new Login();
if($_POST['submit']){
	if(($_POST['email']!='')&&($_POST['password'])!='')
	{
		$uname=$_POST['email'];
		$pwd=$_POST['password'];
		$remember=$_POST['remember'];
		$pagebck=$_POST['pagebck'];
		$retdata=$objlogin->chkValidLogin($uname, $pwd,$remember);
		if($retdata=='success'){
			if($pagebck!=''){
				$page=  base64_decode($pagebck);
				header('Location:'.$page);
			}else{
				header('Location:'.SITE_PATH);
			}

		}
		else{
			$msg="Wrong username or password.";
		}

	}else{
		$msg="Please enter username and password.";
	}
}

$clock='no';
require_once('../includes/header1.php');

?>

<html>
<body>
	<div id="main_container">

		<div class="header_login">
			<div class="logo">
				<a href="<?=SITE_PATH?>"><?= PROJECTNAME ?> </a>
			</div>
		</div>
		<div class="clear">&nbsp;</div>

		<div class="login_form">
			<div class="error">
			<?= $msg ?>
			</div>
			<h3>Login Panel</h3>

			<!-- <a href="<?=SITE_PATH?>login/forgot-password.php" class="forgot_pass">Forgot
				password</a> -->

			<form action="" method="post" class="niceform">
				<input type="hidden" name="pagebck" id="pagebck"
					value="<?= $_GET['url_s'] ?>" />
				<fieldset>
					<dl>
						<dt>
							<label for="email">Username:</label>
						</dt>
						<dd>
							<input type="text" name="email" id="email" size="54" tabindex="1"
								value="<?= base64_decode($_COOKIE['psadminunm']) ?>" />
						</dd>
					</dl>
					<dl>
						<dt>
							<label for="password">Password:</label>
						</dt>
						<dd>
							<input type="password" name="password" id="password" size="54"
								tabindex="2"
								value="<?= base64_decode($_COOKIE['psadminpsd']) ?>" />
						</dd>
					</dl>

					<!-- <dl>
						<dt>
							<label></label>
						</dt>
						<dd>
							<input type="checkbox" name="remember" id="remember" value="yes"
								tabindex="3"
								<? if($_COOKIE['psadminrem']=='yes'){ echo('checked="checked"');  } ?> /><label
								class="check_label">Remember me</label>
						</dd>
					</dl> -->

					<dl class="submit">
						<dd>
							<input type="submit" name="submit" id="submit" value="Login"
								tabindex="4" />
						</dd>
					</dl>

				</fieldset>

			</form>
		</div>



		<div class="footer_login">

			<div class="left_footer_login">
			<?= PROJECTNAME ?>
			</div>
		</div>

	</div>
</body>
</html>

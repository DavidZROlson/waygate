<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {


	/************************************
	*  ACCESS API TO POPULATE D-DOWNS	*
	************************************/
	$sel_vendor = (isset($_GET['vendor'])) ? $_GET['vendor'] : "Not Set" ;
	
	//if the Vendor is chagned to [Vendor...] clear and disable the dc_DD
	if($sel_vendor == ""){
		?>
            <select name="dc_DD" id="dc_DD" disabled="disabled">
                <option value="">[Data Center...]</option>
            </select>
        <?php exit();
		
	}else if($sel_vendor == 'Rackspace'){
		/********************************************
		*  ACCESS RACKSPACE TO POPULATE DATACENTERS	*
		********************************************/
	
		$dcs_in_DB = array('ORD', 'SYD', 'DFW'); // populated by DB QUERY
		
		
		//BUILD DROP DOWN LIST OF DCs
		$dcs_DD_list = array();
		foreach($dcs_in_DB as $key => $value){
			array_push($dcs_DD_list, '<option value="' . $value . '">' . $value . '</option>');
		}//end foreach
		
			?>
            <select name="dc_DD" id="dc_DD">
                <option value="">[Data Center...]</option>
                <?php
                
                /****************************************
                *	POPULATE THE SELECT LIST OPTIONS	*
                ****************************************/
                
                if(count($dcs_DD_list) > 0){
                    foreach($dcs_DD_list as $key => $value){
                        echo $value;
                    }
                }else{
                    echo '<option value="">No Data Centers</option>';
                }
                ?>
            </select>
            <?php exit();
	
	}else if($sel_vendor == 'Amazon'){
		/********************************************
		*  ACCESS AMAZON TO POPULATE DATACENTERS	*
		********************************************/
		
		$dcs_in_DB = array(); // populated by DB QUERY
		
		//BUILD DROP DOWN LIST OF DCs
		$dcs_DD_list = array();
		foreach($dcs_in_DB as $key => $value){
			array_push($dcs_DD_list, '<option value="' . $value . '">' . $value . '</option>');
		}//end foreach
		
			?>
            <select name="dc_DD" id="dc_DD">
                <option value="">[Data Center...]</option>
                <?php
                
                /****************************************
                *	POPULATE THE SELECT LIST OPTIONS	*
                ****************************************/
                
                if(count($dcs_DD_list) > 0){
                    foreach($dcs_DD_list as $key => $value){
                        echo $value;
                    }
                }else{
                    echo '<option value="">No Data Centers</option>';
                }
                ?>
            </select>
            <?php exit();
	}
	
	
	
	//IF A DC IS SELECTED
	
	$sel_datacenter = ($sel_vendor == "Not Set" && isset($_GET['datacenter'])) ? $_GET['datacenter'] : "Not Set" ;
	if($sel_datacenter == ""){exit();} // if [Data Center...] is selected, exit();
	if($sel_datacenter != "Not Set"){
		/********************************************
		*		QUERY DB FOR DATACENTER INFO		*
		********************************************/
		
				
		//LOAD THIS HTML ONLY IF $sel_datacenter != "Not Set"
		?>
        <fieldset>
        <legend>Data Center's Credentials</legend>
        	<table>
                <tbody>
                    <tr>
                        <td class="label"><label for="DC[dc_name]">DC Name: </label></td>
                        <td><input id="dc_name" name="DC[dc_name]" type="text" placeholder="DC Name" /></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="DC[dc_username]">Username: </label></td>
                        <td><input id="dc_username" name="DC[dc_username]" type="text" placeholder="Username" /></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="DC[dc_password]">Password: </label></td>
                        <td><input id="dc_password" name="DC[dc_password]" type="password" placeholder="Passowrd" /></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="DC[dc_api_key]">API Key: </label></td>
                        <td><input id="dc_api_key" name="DC[dc_api_key]" type="text" placeholder="API Key" /></td>
                    </tr>
                    <tr>
                        <td class="label"><label for="DC[dc_provider]">Provider: </label></td>
                        <td><input id="dc_provider" name="DC[dc_provider]" type="text" placeholder="Provider" /></td>
                    </tr>
                </tbody>
            </table> 
        <input id="apply_btn" value="Apply Changes" type="button" class="button" >
        </fieldset>
        <?php 
            
        exit();
	} // END if DC is Selected
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
	/***************************************************
	****************************************************
	****************************************************
	THE $_POST[] WILL RESEMBLE THIS SET UP OF VALUES
	
	$_POST = Array(
		[vendors_DC_DD] => Rackspace
		[dc_DD] => ORD
		[DC] => Array
			(
				['dc_name'] => ORD
				['dc_username'] => rosincloud
				['dc_password'] => password
				['dc_api_key'] => 1234_API_Key_5678
				['dc_provider'] => Rackspace
			)
	
	)
	
	MAKE YOUR CALLS HERE BELOW THE
	IF($_SERVER['REQUEST_METHOD'] === 'POST')
	*****************************************************
	*****************************************************
	*****************************************************/
	
	
	
	exit();
}//END POST
?>

<form id="edit_form" name="edit_form">
    <div>
        <fieldset><legend>Select a Data Center</legend>
            <label for="vendors_DC_DD">Vendor:</label>
            <select name="vendors_DC_DD" id="vendors_DC_DD">
                <option value="">[Vendor...]</option>
                <option value="Rackspace">Rackspace</option>
                <option value="Amazon">Amazon</option>        
            </select>
            <span id="dc_DD_catch">
                <select name="dc_DD" id="dc_DD" disabled="disabled">
                    <option value="">[Data Center...]</option>
                </select>
            </span>
        </fieldset>
    </div>
    <div id="DC_info">

    </div>
</form>
<?php

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
	/***************************************************
	****************************************************
	****************************************************
	THE $_POST[] WILL RESEMBLE THIS SET UP OF VALUES
	
	$_POST = array(
			[username] =>
			[password] =>
			)
	
	MAKE YOUR CALLS HERE BELOW THE
	IF($_SERVER['REQUEST_METHOD'] === 'POST')
	*****************************************************
	*****************************************************
	*****************************************************/
	
	//MAKE CONTACT WITH THE DB TO CHECK GET USERS()
	//$objUser = new User();
	//$data = $objUser->getusers();
	
	//testing array of usernames
	$data = array(
				 array('username' => 'Jim')
				,array('username' => 'Bob')
				,array('username' => 'Suzy')
				,array('username' => 'Glen')
				,array('username' => 'Ruth')
			); // populated by DB QUERY
			
	
	//CHECK FOR REQUESSTED USERNAME IN DB ARRAY
	foreach($data as $index => $temp){
		if(strtolower($_POST['username']) == strtolower($temp['username'])){
			$success = false;
			break;
		}
		$success = true;
	}
	echo json_encode(array('response' => $success, 'username' => $_POST['username']));
	if($success == false){exit();}

	exit();
}//END POST

?>

<form id="add_form" name="add_form">
    <fieldset>
    <legend>Add a New User's Credentials</legend>
        <label for="username">Username:</label>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-user"></i></span>
            <input id="username" name="username" type="text" placeholder="Username">
        	<span class="error" id="username_msg_catch"></span>
        </div>
        <label for="password">Password:</label>
        <div class="input-prepend">
            <span class="add-on"><i class="icon-key"></i> </span>
            <input id="password" name="password" type="password" placeholder="Password">
        </div>
    </fieldset>
    
    <input id="apply_btn" value="Create User" type="button" class="button" >
    
</form>
<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {


	/************************************
	*  ACCESS API TO POPULATE D-DOWNS	*
	************************************/
	
	$users_in_DB = array('Jim', 'Bob', 'Suzy', 'Glen', 'Ruth'); // populated by DB QUERY
	
	
	//BUILD DROP DOWN LIST OF USERS
	$user_DD_list = array();
	foreach($users_in_DB as $key => $value){
		array_push($user_DD_list, '<option value="' . $key . '">' . $value . '</option>');
	}
	
	
	//IF A USER && VENDOR IS SELECTED
	if(isset($_GET['user']) && $_GET['user'] == ""){echo'<span class="error">Select a User</span>'; exit();}
	if(isset($_GET['vendor']) && $_GET['vendor'] == ""){echo'<span class="error">Select a Vendor</span>'; exit();}
	
	if(isset($_GET['user']) && isset($_GET['vendor']) && $_GET['user'] != "" && $_GET['vendor'] != ""){
		/********************************************
		*		QUERY DB FOR USER'S PRIVLIGES		*
		********************************************/
		
		
		//TESTING ARRAY[$dc][$index] = $value FOR DATACENTER PERMISSIONS
		$visable_dcs_in_DB = array( array(  'title' => 'ORD',
											'DC' => 'y',
											'Reports' => 'y',
											'Svr_Mgnt' => 'y'),
									array(  'title' => 'SYD',
											'DC' => 'n',
											'Reports' => 'n',
											'Svr_Mgnt' => 'y'),
									array(  'title' => 'DFW',
											'DC' => 'y',
											'Reports' => 'n',
											'Svr_Mgnt' => 'y')); // populated by DB QUERY
		
		$table_rows = array();
		
		//BUILD ROWS FOR TABLE OUTPUT
		$odd_even = 0;
		foreach($visable_dcs_in_DB as $key => $dc){
				// Start Row
				switch($odd_even % 2){
					case 0: array_push($table_rows, '<tr class="even">');
						break;
					case 1: array_push($table_rows, '<tr class="odd">');
						break;
					default: array_push($table_rows, '<tr>');
						break;
				}
			
			foreach($dc as $index => $value){
				//$dc may contains [title] [DC] [Reports] [Svr_Mgnt]
				// if $dc[] == y then create a row with a checked box, else, don't check the box
				if($index != 'title'){
					$value = ($value == 'y') ? '<td class="checkbox"><input name="' . $dc['title'] . '[' . $index . ']" type="checkbox" checked="checked"></td>' : '<td class="checkbox"><input name="' . $dc['title'] . '[' . $index . ']" type="checkbox"></td>';
					array_push($table_rows, $value);
				}else{
					// push the $dc[title] to the $table_rows array()
					array_push($table_rows, '<td>' . $value . '</td>');
				}
			}
			//end a row
			array_push($table_rows, '</tr>'); 
			$odd_even+=1;
		}
		
		//LOAD THIS HTML ONLY IF A $_GET['user'] IS SELECTED
		?>
        <fieldset>
        <legend>Permissions</legend>
        
			<style type="text/css">
            
                .usr_creds_chk{
                    margin: 0 15px 5px 0;
                    background-color: #EEE;
                    border 1px solid #CCC;
                    border-radius: 4px;
                    box-shadow: 0 1px 1px rgba(0, 0, 0, 0.75) inset;
                    padding: 5px 5px 0px;
					display: inline-block;
                }
            
            </style>
            <div>
                <label for="admin_chk" class="usr_creds_chk">
                Admin<input name="admin_chk" id="admin_chk" type="checkbox" />
                </label>
                <label for="dns_chk" class="usr_creds_chk">
                DNS<input name="dns_chk" id="dns_chk" type="checkbox" />
                </label>
            </div>
            <table>
                <tr class="odd">
                    <th>Data Center Name</th>
                    <th>DC</th>
                    <th>Reports</th>
                    <th>Server Managment</th>
                </tr>
                
                <?php
                    foreach($table_rows as $row)
                    echo $row;
                ?>
            </table>
        
        </fieldset>
        <input id="apply_btn" value="Apply Changes" type="button" class="button" >
        <?php 
            
        exit();
	} // END if USER is Selected
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	print_r($_POST);
	
	/***************************************************
	****************************************************
	****************************************************
	THE $_POST[] WILL RESEMBLE THIS SET UP OF VALUES
	
	$_POST = Array(
				[users_DD] => 2
				[vendors_permissions_DD] => RackSpace
				[admin_chk] => on
				[dns_chk] => on
				[ORD] => Array
					(
						[DC] => on
						[Reports] => on
						[Svr_Mgnt] => on
					)
			
				[SYD] => Array
					(
						[Svr_Mgnt] => on
					)
			
				[DFW] => Array
					(
						[DC] => on
						[Svr_Mgnt] => on
					)
			
			)
	
	MAKE YOUR CALLS HERE BELOW THE
	IF($_SERVER['REQUEST_METHOD'] === 'POST')
	*****************************************************
	*****************************************************
	*****************************************************/
	
	
	exit();
}//END POST
?>

<form id="edit_form" name="edit_form">
    <div>
        <label for="users_DD">Select a User:</label>
        <select name="users_DD" id="users_DD">
        	<option value="">[User...]</option>
            <?php
            
            /****************************************
            *	POPULATE THE SELECT LIST OPTIONS	*
            ****************************************/
            
            if(count($user_DD_list) > 0){
                foreach($user_DD_list as $key => $value){
                echo $value;
                }
            }else{
                echo '<option value="">No Users</option>';
            }
            ?>
        </select>
        
        <label for="vendors_permissions_DD">Vendor:</label>
        <select name="vendors_permissions_DD" id="vendors_permissions_DD">
        	<option value="">[Vendor...]</option>
            <option value="RackSpace">Rackspace</option>
            <option value="Amazon">Amazon</option>
        </select>
    </div>
    <div id="user_info">

    </div>
</form>
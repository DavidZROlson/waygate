<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {


	/************************************
	*  ACCESS API TO POPULATE D-DOWNS	*
	************************************/
	
	$users_in_DB = array('Jim', 'Bob', 'Suzy', 'Glen', 'Ruth'); // populated by DB QUERY
	
	
	//BUILD DROP DOWN LIST OF USERS
	$user_DD_list = array();
	foreach($users_in_DB as $key => $value){
		array_push($user_DD_list, '<option value="' . $key . '">' . $value . '</option>');
	}
	
	
	//IF A USER IS SELECTED
	if(isset($_GET['user'])){
		/********************************************
		*		QUERY DB FOR USER'S PRIVLIGES		*
		********************************************/
		
		//LOAD THIS HTML ONLY IF A $_GET['user'] IS SELECTED
		?>
        <fieldset>
        <legend>Edit User's Credentials</legend>
            <label for="username">Username:</label>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-user"></i> </span>
                <input id="username" name="username" type="text" placeholder="Username">
            </div>
            <label for="password">Password:</label>
            <div class="input-prepend">
                <span class="add-on"><i class="icon-key"></i> </span>
                <input id="password" name="password" type="password" placeholder="Password">
            </div>
        </fieldset>
        
        <input id="apply_btn" value="Apply Changes" type="button" class="button" >
        <?php 
            
        exit();
	} // END if USER is Selected
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	
	/***************************************************
	****************************************************
	****************************************************
	THE $_POST[] WILL RESEMBLE THIS SET UP OF VALUES
	
	$_POST = array(
			[users_DD] => 2
			[username] => 
			[password] => 
			)
	
	MAKE YOUR CALLS HERE BELOW THE
	IF($_SERVER['REQUEST_METHOD'] === 'POST')
	*****************************************************
	*****************************************************
	*****************************************************/
	
	
	
	exit();
}//END POST
?>

<form id="edit_form" name="edit_form">
    <fieldset>
    	<legend>Select a User</legend>
        <select name="users_DD" id="users_DD">
            <?php
            
            /****************************************
            *	POPULATE THE SELECT LIST OPTIONS	*
            ****************************************/
            
            if(count($user_DD_list) > 0){
                foreach($user_DD_list as $key => $value){
                echo $value;
                }
            }else{
                echo '<option value="">No Users</option>';
            }
            ?>
        </select>
    </fieldset>
    <div id="user_info">

    </div>
</form>

<style type="text/css">
	form{
		margin: 0 auto;	
	}
	
	.label{
		text-align:right;
		padding-right: 10px;	
	}
</style>
<form>
<fieldset>
    <legend>Add Record</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="domain_nm">Select Domain:</label>
            </td>
            <td>
                <select name="domain_nm" id="domain_nm" style="width:40%">
                    <option value="1">Domain 1</option>
                    <option value="2">Domain 2</option>
                    <option value="3">Domain 3</option>
                    <option value="4">Domain 4</option>                    
                </select>*
            </td>
        </tr>    
        <tr>
            <td class="label">
                <label for="domain_type">Select Type:</label>
            </td>
            <td>
                <select name="domain_type" id="domain_type" style="width:40%">
                    <option value="1">Type 1</option>
                    <option value="2">Type 2</option>
                    <option value="3">Type 3</option>
                    <option value="4">Type 4</option>                    
                </select>*
            </td>
        </tr>   
      
        <tr>
            <td class="label">
                <label for="nm">Name:</label>
            </td>
            <td><input type="text" name="nm">*
            </td>
        </tr>
        <tr >
            <td class="label">
                <label for="ip_add">Ip Address (V6):</label>
            </td>
            <td><input type="text" name="ip_add">*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="ttl">TTL:</label>
            </td>
            <td><input type="text" name="ttl">*
            </td>
        </tr>
        <tr>
            <td colspan="2" style="margin-left:50%">
                <input id="add_record_btn" style="margin-left:34%" value="Add" type="button" />
            </td>
        </tr>
    </table>
    <div id="load_table_contents">
        
    </div>    
    </fieldset>
</form>
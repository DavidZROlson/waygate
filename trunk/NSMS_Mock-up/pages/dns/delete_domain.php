
<style type="text/css">
	form{
		margin: 0 auto;	
	}
	
	.label{
		text-align:right;
		padding-right: 10px;	
	}
</style>

<form>
	<fieldset>
    <legend>Delete Domain</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="delete_domain">Select a Server:</label>
            </td>
            <td>
                <select name="delete_domain" id="delete_domain">
                    <option value="1">Domain 1</option>
                    <option value="2">Domain 2</option>
                    <option value="3">Domain 3</option>
                    <option value="4">Domain 4</option>
                    
                </select>*
            </td>
        </tr>
        
        <tr>
            <td colspan="2" style="text-align:center;">
                <input id="delete_domain_btn" value="Delete Domain" type="button" />
            </td>
        </tr>
    </table>
    </fieldset>
</form>
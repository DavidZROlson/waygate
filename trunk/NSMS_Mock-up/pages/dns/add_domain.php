


<style type="text/css">
	form{
		margin: 0 auto;	
	}
	
	.label{
		text-align:right;
		padding-right: 10px;	
	}
</style>
<form>
<fieldset>
    <legend>Add New Domain</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="add_domain">Domain Name:</label>
            </td>
            <td><input type="text" name="add_domain">*
            </td>
        </tr>        
        <tr>
            <td colspan="2" style="margin-left:50%">
                <input id="add_domain_btn" style="margin-left:34%" value="Create" type="button" />
            </td>
        </tr>
    </table>
    </fieldset>
</form>
<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];
		
		/************************************
		*  ACCESS API TO POPULATE D-DOWNS	*
		************************************/
		
		$img_array = array(); // populated by DB QUERY
		
		$img_options = array();
		foreach($img_array as $key => $value){
			$img_options.push('<option value="' . $value . '">' . $key . '</option>');
		}
		
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_POST['name'] != ""){
		
		$DataCenter = $_POST['dcs'];
		$selected_IMG = $_POST['images'];
		$selected_FLAVOR = $_POST['flavor'];
		$selected_NAME = $_POST['name'];
		
		/************************************
		*  ACCESS API TO CREATE NEW SERVER	*
		************************************/
		echo '<p style="position: absolute; top: 50%; left: 50%;">';
		echo 'DataCenter: ' . $DataCenter . '<br />';
		echo 'Selected IMG: ' . $selected_IMG . '<br />';
		echo 'Selected Flavor: ' . $selected_FLAVOR . '<br />';
		echo 'Selected Name: ' . $selected_NAME . '</p>';
		
	}
	exit();
}

?>

<form id="create_form" name="create_form">
	<fieldset>
    <legend>Create a New Server</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="images">Select an Image:</label>
            </td>
            <td>
                <select name="images" id="images">
                	<?php
					
					/****************************************
					*	POPULATE THE SELECT LIST OPTIONS	*
					****************************************/
					
					if(count($img_options) > 0){
                    	foreach($img_options as $key => $value){
						echo $value;
						}
					}else{
						echo '<option value="">No Images</option>';
					}
					?>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="flavor">Select a Flavor:</label>
            </td>
            <td>
                <select name="flavor" id="flavor">
                    <option value="0">512 MB</option>
                    <option value="1">1 GB</option>
                    <option value="2">2 GB</option>
                    <option value="4">4 GB</option>
                    <option value="8">8 GB</option>
                    <option value="15">15 GB</option>
                    <option value="30">30 Gb</option>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="server_name_txt">Server Name:</label>
            </td>
            <td>
                <input name="server_name_txt" id="server_name_txt" type="text" />*
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input id="create_button" value="Create" type="button" />
            </td>
        </tr>
    </table>
    </fieldset>
</form>
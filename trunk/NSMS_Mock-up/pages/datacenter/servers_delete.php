<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];
		
		/********************************
		*   Query DB for DC's Servers	*
		********************************/
		
		$svr_array = array(); // populated by DB QUERY
		
		$svr_options = array();
		foreach($svr_array as $key => $value){
			$svr_options.push('<option value="' . $value . '">' . $key . '</option>');
		}
		
	}
}

//CHECK FOR POST REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	
	//BEGIN CAPTCHA CHECKING
	session_start();
	include_once '../../securimage/securimage.php';
	$securimage = new Securimage();
	
	$DataCenter = $_POST['dcs'];
	$selected_SVR = $_POST['server'];
	$captcha =  $_POST['captcha'];
	
	
	if ($securimage->check($_POST['captcha']) == false) {
	  // the code was incorrect
	  echo "The security code entered was incorrect.<br />";
	  echo 'Value of DropDown: '. $selected_SVR . "<br />";
	  echo 'Value of Captcha: ' . $captcha;
	}else{
		//ON CAPTCHA SUCCESS
		//SEND DATA TO DELETE THE SELECTED
	  echo "Server is being marked as deleted<br />";
	  echo 'Value of DropDown: '. $selected_SVR . "<br />";
	  echo 'Value of Captcha: ' . $captcha;
	}
	//END CAPTCHA CHECKING
	exit();
}
?>

<form id="delete_server_form">
	<fieldset>
    <legend>Delete an Existing Server</legend>
    <table style="display:inline-block;">
    	<tr>
            <td class="label">
                <label for="server_DD">Select a Server:</label>
            </td>
            <td>
                <select name="server_DD" id="server_DD">
                	<?php 
					
					/****************************************
					*	POPULATE THE SELECT LIST OPTIONS	*
					****************************************/
					
					if(count($svr_options) > 0){
                    	foreach($svr_options as $key => $value){
						echo $value;
						}
					}else{
						echo '<option value="">No Images</option>';
					}
					?>
                    </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="verify">Verify Intent:</label><br />
                <img id="captcha" src="securimage/securimage_show.php" alt="CAPTCHA Image" />
            </td>
            <td>
                    <input type="text" name="captcha_code" size="10" maxlength="6" style="left: 0;" />
                    <a href="#" onclick="document.getElementById('captcha').src = 'securimage/securimage_show.php?' + Math.random(); return false"><img id="captcha_refresh" alt="Refresh" src="securimage/images/refresh.png" width="20px" height="20px" /></a>
                
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:right;">
                <input id="delete_button" value="Delete Server" type="button" />
            </td>
        </tr>
    </table>
    <div id="validation" style="display:inline-block; vertical-align:top;"></div>
    </fieldset>
</form>
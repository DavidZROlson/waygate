<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];
		
		/********************************
		*   Query DB for DC's Servers	*
		********************************/
		
		$svr_array = array(); // populated by DB QUERY
		
		$svr_options = array();
		foreach($svr_array as $key => $value){
			$svr_options.push('<option value="' . $value . '">' . $key . '</option>');
		}
		
	}
}



if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_POST['dcs'] != ""){
		
		$DataCenter = $_POST['dcs'];
		$selected_SVR = $_POST['svr'];
		$selected_FLAVOR = $_POST['flavor'];
		
		/****************************************
		*  ACCESS API TO RESIZE EXISTING SERVER	*
		****************************************/
		
		
		echo '<p style="position: absolute; top: 50%; left: 50%;">';
		echo 'DataCenter: ' . $DataCenter . '<br />';
		echo 'Selected SVR: ' . $selected_SVR . '<br />';
		echo 'Selected Flavor: ' . $selected_FLAVOR . '</p>';
		
	}
		exit();
}

?>

<form>
	<fieldset>
    <legend>Resize an Existing Server</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="server_DD">Select a Server:</label>
            </td>
            <td>
                <select name="server_DD" id="server_DD">
                	<?php 
					
					/****************************************
					*	POPULATE THE SELECT LIST OPTIONS	*
					****************************************/
					
					if(count($svr_options) > 0){
                    	foreach($svr_options as $key => $value){
						echo $value;
						}
					}else{
						echo '<option value="">No Images</option>';
					}
					?>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="flavor_DD">Select a Flavor:</label>
            </td>
            <td>
                <select name="flavor_DD" id="flavor_DD">
                    <option value="0">512 MB</option>
                    <option value="1">1 GB</option>
                    <option value="2">2 GB</option>
                    <option value="4">4 GB</option>
                    <option value="8">8 GB</option>
                    <option value="15">15 GB</option>
                    <option value="30">30 Gb</option>
                </select>*
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input id="resize_button" value="Resize" type="button" />
            </td>
        </tr>
    </table>
    </fieldset>
</form>
<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if(isset($_GET['dcs'])){
		$DataCenter = $_GET['dcs'];
		
		/********************************
		*   Query DB for DC's Servers	*
		********************************/
	}
}

echo 'Testing value of selected Datacenter: ';
echo($_GET['dcs']);


?>
	<table>
        <tbody>
        <tr>
            <th></th>
            <th>Server Name</th>
            <th>Primary IP</th>
            <th>Private IP</th>
            <th>Status</th>
            <th>RAM Amount</th>
        </tr>
        <?php
			
            /************************************************
            *			FOREACH SERVER LIST A NEW ROW		*
            *			ALTERNATING CLASS 'odd' 'even' 		*
            ************************************************/
			
			$client_servers = array();
			
			//BEGIN build a TESTing array $client_servers[][]
				for($i = 0; $i < 20; $i++)
				{
					for($n = 0; $n < 5; $n++)
					{
						switch($n)
						{
							case 0:
								$client_servers[$i][$n] = "Name";
								break;
							case 1:
								$client_servers[$i][$n] = "Primary IP";
								break;
							case 2:
								$client_servers[$i][$n] = "Private IP";
								break;
							case 3:
								$client_servers[$i][$n] = "Status";
								break;
							case 4:
								$client_servers[$i][$n] = "RAM";
								break;
							default:
								break;
						}
					}
				}
			//END build TESTing array $client_servers[][]
			
			$result = "";
			
			//OUTPUT A ROW FOR EACH SERVER IN THE ARRAY
			$odd_even = 0;
			foreach($client_servers as $server){
				//seperate rows distictly by color
				if($odd_even % 2 == 0)
				{
					$result.='<tr class="odd">';
				}else{
					$result.='<tr class="even">';
				}
				//display row data
				$result.='
							<td class="checkbox"><input type="checkbox" name="chkbox_' . $odd_even . '"/></td>
							<td id="server_name_' . $odd_even . '">' . $server[0] . $odd_even . '</td>
							<td id="primary_ip_' . $odd_even . '"><a href="https://' . $server[1] . '">' . $server[1] . $odd_even . '</a></td>
							<td>' . $server[2] . '</td>
							<td>' . $server[3] . '</td>
							<td>' . $server[4] . '</td>
						</tr>';
					
				$odd_even++;
			}
			echo($result);
        ?>
        </tbody>
    </table>
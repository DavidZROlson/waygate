<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if(isset($_GET['dcs'])){
		$DataCenter = $_GET['dcs'];
		
		/********************************
		*   Query DB for DC's Servers	*
		********************************/
	}
}

echo 'Testing value of selected Datacenter: ';
echo $DataCenter;


?>

<table>
        <tbody>
        <tr>
            <th></th>
            <th>Image Name</th>
            <th>Date Created</th>
            <th>Status</th>
            <th>Server</th>
        </tr>
        <?php
			
            /************************************************
            *			FOREACH SERVER LIST A NEW ROW		*
            *			ALTERNATING ROW CLASS 'odd' 'even' 	*
            ************************************************/
			
			$client_images = array();
			
			//BEGIN build a TESTing array $client_servers[][]
				for($i = 0; $i < 20; $i++)
				{
					for($n = 0; $n < 4; $n++)
					{
						switch($n)
						{
							case 0:
								$client_images[$i][$n] = "Name";
								break;
							case 1:
								$client_images[$i][$n] = "Created";
								break;
							case 2:
								$client_images[$i][$n] = "Status";
								break;
							case 3:
								$client_images[$i][$n] = "Server";
								break;
							default:
								break;
						}
					}
				}
			//END build TESTing array $client_servers[][]
			
			$result = "";
			
			//OUTPUT A ROW FOR EACH SERVER IN THE ARRAY
			$odd_even = 0;
			foreach($client_images as $image){
				//seperate rows distictly by color
				if($odd_even % 2 == 0)
				{
					$result.='<tr class="odd">';
				}else{
					$result.='<tr class="even">';
				}
				//display row data
				$result.='
							<td id="image_name_' . $odd_even . '">' . $image[0] . '</td>
							<td id="created_' . $odd_even . '">' . $image[1] . '</td>
							<td>' . $image[2] . '</td>
							<td>' . $image[3] . '</td>
						</tr>';
					
				$odd_even++;
			}
			echo($result);
        ?>
        </tbody>
    </table>
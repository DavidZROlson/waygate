<?php

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_GET['dcs'] != ""){
		$DataCenter = $_GET['dcs'];
		
		/************************************
		*  ACCESS API TO POPULATE D-DOWNS	*
		************************************/
		
		$svr_array = array(); // populated by DB QUERY
		
		$svr_options = array();
		foreach($svr_array as $key => $value){
			$svr_options.push('<option value="' . $value . '">' . $key . '</option>');
		}
		
	}
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	
	//IF GET REQUEST IS FROM 'dcs'
	if($_POST['name'] != ""){
		
		$DataCenter = $_POST['dcs'];
		$selected_SVR = $_POST['server'];
		$selected_NAME = $_POST['name'];
		
		/************************************
		*  ACCESS API TO CREATE NEW SERVER	*
		************************************/
		echo '<p style="position: absolute; top: 50%; left: 50%;">';
		echo 'DataCenter: ' . $DataCenter . '<br />';
		echo 'Selected IMG: ' . $selected_SVR . '<br />';
		echo 'Selected Name: ' . $selected_NAME . '</p>';
		
	}
	exit();
}
?>

<form>
	<fieldset>
    <legend>Create a New Image from an Existing Server</legend>
    <table>
    	<tr>
            <td class="label">
                <label for="server_DD">Select a Server:</label>
            </td>
            <td>
                <select name="server_DD" id="server_DD">
				<?php
					
					/****************************************
					*	POPULATE THE SELECT LIST OPTIONS	*
					****************************************/
					
					if(count($svr_options) > 0){
						foreach($svr_options as $key => $value){
						echo $value;
						}
					}else{
						echo '<option value="">No Servers</option>';
					}
				?>
                </select>*
            </td>
        </tr>
        <tr>
            <td class="label">
                <label for="image_name_txt">Image Name:</label>
            </td>
            <td>
                <input name="image_name_txt" id="image_name_txt" type="text" />*
            </td>
        </tr>
        <tr>
            <td colspan="2" style="text-align:center;">
                <input id="create_button" value="Create" type="button" />
            </td>
        </tr>
    </table>
    </fieldset>
</form>
<form id="reports_form" style="display: inline-block;">
    <table>
        <tbody>
        	<tr>
            	<td class="label">
                	<label for="label_DD">Select a Label:</label>
                </td>
                <td>
                    <select name="label_DD" id="label_DD">
                        <option value="">Select a Label</option>
                        <option value="Label 1">Label 1</option>
                        <option value="Label 2">Label 2</option>
                        <option value="Label 3">Label 3</option>
                        <option value="Label 4">Label 4</option>
                        <option value="Label 5">Label 5</option>
                        <option value="Label 6">Label 6</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="start_date">From:</label>
                </td>
                <td>
                    <input class="datepicker" type="date" value="" name="start_date"/>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <label for="end_date">To:</label>
                </td>
                <td>
                    <input class="datepicker" type="date" value="" name="end_date"/>
                </td>
            </tr>
            <tr>
            	<td class="label" colspan="2" style="border: 0px none;">
                	<input class="button" type="button" id="search_btn" name="search_btn" value="Search" />
                </td>
            </tr>
        </tbody>
    </table>
    
</form>
<style type="text/css">
	#reports_results_catch{
		border: 1px solid #000000;
		display: inline-block;
		height: 100px;
		vertical-align: top;
		width: 50%;
	}
</style>
<div id="reports_results_catch">
</div>
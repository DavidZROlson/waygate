<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WayGateCloud | Login</title>

<!-- links and recources -->
    <!-- CSS -->
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    
    <!-- STEPY WIZARD CSS -->
    <link href="css/jquery.stepy.css" rel="stylesheet" type="text/css" />
    
    <!-- Font-Awesome http://fortawesome.github.io/Font-Awesome/icons/ -->
    <link href="css/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- JQUERY UI THEME AND SOURCES -->
    <link rel="stylesheet" href="js/jquery-ui-1.10.3/themes/base/jquery.ui.all.css" />
    
	<script type="text/javascript" src="js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    
    <!-- CUSTOM JQUERY SCRIPTS
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/ux.js"></script> -->
    
    <!-- REQUIRED FOR SIGN_UP WIZARD + FORM VALIDATION http://jqueryvalidation.org/documentation/ -->
    <script type="text/javascript" src="js/stepy/jquery.stepy.js"></script>
    <script type="text/javascript" src="js/jquery-validation-1.11.1/dist/jquery.validate.js"></script>
    <script type="text/javascript" src="js/jquery-validation-1.11.1/dist/additional-methods.js"></script>
    
    <!-- REQUIRED FOR FORM MASKS http://igorescobar.github.io/jQuery-Mask-Plugin/ -->
	<script type="text/javascript" src="js/jQuery-Mask-Plugin-master/jquery.mask.js"></script>

</head>

<body>
    <!-- BEGIN HEADER -->
    <?php include_once('includes/front_header.php'); ?>
    <!-- END HEADER -->
    
    <!-- BEGIN MAIN_WRAPPER -->
    <div id="main_wrapper">
    
        <!-- BEGIN MAIN_CONTAINER -->
        <div id="main_container">
            
            <!-- BEGIN CONTENT_WRAPPER -->
            <div id="content_wrapper">
            
            <div id="content">
            <div id="error_box1" style="position: absolute; right: 14%; top: 180px"><ul></ul></div>
                <form id="signup_form" name="signup_form" action="sign_up.php" method="post">
                <fieldset title="Customer Info">
                <legend>Give us a way to contact you</legend>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-building"></i></span>
                        <input id="company" name="company" type="text" placeholder="Company Name"
                            required />
                        <span class="add-on"><i class="icon-envelope-alt"></i></span>
                        <input id="email" name="email" type="email" placeholder="Email Address"
                        	required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input id="fname" name="fname" type="text" placeholder="First Name"
                            required />
                        <span class="add-on"><i class="icon-user"></i></span>
                        <input id="lname" name="lname" type="text" placeholder="Last Name"
                            required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pushpin"></i></span>
                        <input id="address1" name="address1" type="text" placeholder="Address Line 1"
                            required />
                        <span class="add-on"><i class="icon-pushpin"></i></span>
                        <input id="address2" name="address2" type="text" placeholder="Address Line 2"
                        	/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pushpin"></i></span>
                        <input id="city" name="city" type="text" placeholder="City"
                            required />
                        <span class="add-on"><i class="icon-pushpin"></i></span>
                            <select id="state" name="state" 
                            style="margin-right: 4px; margin-left: -3px; width: 196px; font-size: 15pt; padding-top: 2px;">
                                <option value="">State</option>
                                <option value="AL">Alabama</option>
                                <option value="AK">Alaska</option>
                                <option value="AZ">Arizona</option>
                                <option value="AR">Arkansas</option>
                                <option value="CA">California</option>
                                <option value="CO">Colorado</option>
                                <option value="CT">Connecticut</option>
                                <option value="DE">Delaware</option>
                                <option value="DC">District of Columbia</option>
                                <option value="FL">Florida</option>
                                <option value="GA">Georgia</option>
                                <option value="HI">Hawaii</option>
                                <option value="ID">Idaho</option>
                                <option value="IL">Illinois</option>
                                <option value="IN">Indiana</option>
                                <option value="IA">Iowa</option>
                                <option value="KS">Kansas</option>
                                <option value="KY">Kentucky</option>
                                <option value="LA">Louisiana</option>
                                <option value="ME">Maine</option>
                                <option value="MD">Maryland</option>
                                <option value="MA">Massachusetts</option>
                                <option value="MI">Michigan</option>
                                <option value="MN">Minnesota</option>
                                <option value="MS">Mississippi</option>
                                <option value="MO">Missouri</option>
                                <option value="MT">Montana</option>
                                <option value="NE">Nebraska</option>
                                <option value="NV">Nevada</option>
                                <option value="NH">New Hampshire</option>
                                <option value="NJ">New Jersey</option>
                                <option value="NM">New Mexico</option>
                                <option value="NY">New York</option>
                                <option value="NC">North Carolina</option>
                                <option value="ND">North Dakota</option>
                                <option value="OH">Ohio</option>
                                <option value="OK">Oklahoma</option>
                                <option value="OR">Oregon</option>
                                <option value="PA">Pennsylvania</option>
                                <option value="RI">Rhode Island</option>
                                <option value="SC">South Carolina</option>
                                <option value="SD">South Dakota</option>
                                <option value="TN">Tennessee</option>
                                <option value="TX">Texas</option>
                                <option value="UT">Utah</option>
                                <option value="VT">Vermont</option>
                                <option value="VA">Virginia</option>
                                <option value="WA">Washington</option>
                                <option value="WV">West Virginia</option>
                                <option value="WI">Wisconsin</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        <span class="add-on"><i class="icon-pushpin"></i></span>
                        <input id="zipcode" class="zipcode" name="zipcode" type="text" placeholder="Zip Code"
                           required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-phone"></i></span>
                        <input id="b_phone" class="phone p_bank1" name="b_phone" type="text" placeholder="Business Phone"
                        	/>
                        <span class="add-on"><i class="icon-phone"></i></span>
                        <input id="h_phone" class="phone p_bank1" name="h_phone" type="text" placeholder="Home Phone"
                            />
                        <span class="add-on"><i class="icon-phone"></i></span>
                        <input id="c_phone" class="phone p_bank1" name="c_phone" type="text" placeholder="Cell Phone"
                            />
                    </div>
                    </fieldset>
                    
                    
                    <fieldset title="Account Info"><legend>Set up your account</legend>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i> </span>
                        <input class="span2" id="user" name="user" type="text" placeholder="Username"
                        	required />
                        <span class="add-on"><i class="icon-key"></i> </span>
                        <input class="span2" id="password" name="password" type="password" placeholder="Password"
                        	required />
                        <span class="add-on"><i class="icon-key"></i> </span>
                        <input class="span2" id="p_confirm" name="p_confirm" type="password" placeholder="Confirm Password"
                        	required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-credit-card"></i> </span>
                        <select id="cc_type" name="cc_type" 
                        style="margin-right: 4px; margin-left: -3px; width: 196px; font-size: 15pt; padding-top: 2px;">
                            <option value="">Type of Card</option>
                            <option value="v">Visa</option>
                            <option value="m">MasterCard</option>
                            <option value="d">Discover</option>
                        </select>
                        <span class="add-on"><i class="icon-credit-card"></i> </span>
                        <input class="span2 cc" id="cc_num" name="cc_num" type="text" placeholder="####-####-####-####"
                        	required />
                        <span class="add-on"><i class="icon-credit-card"></i> </span>
                        <input class="span2 cc_date" id="cc_exp_date" name="cc_exp_date" type="text" placeholder="mm/YYYY"
                        	required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-building"></i> </span>
                        <input id="b_company" name="b_company" type="text" placeholder="Company Name" value=""
                        	required />
                        <span class="add-on"><i class="icon-envelope-alt"></i> </span>
                        <input id="b_email" name="b_email" type="email" placeholder="Email Address" value=""
                            required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-user"></i> </span>
                        <input id="b_fname" name="b_fname" type="text" placeholder="First Name" value=""
                        	required />
                        <span class="add-on"><i class="icon-user"></i> </span>
                        <input id="b_lname" name="b_lname" type="text" placeholder="Last Name" value=""
                            required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pushpin"></i> </span>
                        <input id="b_address1" name="b_address1" type="text" placeholder="Address Line 1" value=""
                        	required />
                        <span class="add-on"><i class="icon-pushpin"></i> </span>
                        <input id="b_address2" name="b_address2" type="text" placeholder="Address Line 2" value=""
                        	/>
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-pushpin"></i> </span>
                        <input id="b_city" name="b_city" type="text" placeholder="City" value=""
                        	required />
                        <span class="add-on"><i class="icon-pushpin"></i> </span>
                        <select id="b_state" name="b_state"
                            style="margin-right: 4px; margin-left: -3px; width: 196px; font-size: 15pt; padding-top: 2px;">
                            <option value="">State</option>
                            <option value="AL">Alabama</option>
                            <option value="AK">Alaska</option>
                            <option value="AZ">Arizona</option>
                            <option value="AR">Arkansas</option>
                            <option value="CA">California</option>
                            <option value="CO">Colorado</option>
                            <option value="CT">Connecticut</option>
                            <option value="DE">Delaware</option>
                            <option value="DC">District of Columbia</option>
                            <option value="FL">Florida</option>
                            <option value="GA">Georgia</option>
                            <option value="HI">Hawaii</option>
                            <option value="ID">Idaho</option>
                            <option value="IL">Illinois</option>
                            <option value="IN">Indiana</option>
                            <option value="IA">Iowa</option>
                            <option value="KS">Kansas</option>
                            <option value="KY">Kentucky</option>
                            <option value="LA">Louisiana</option>
                            <option value="ME">Maine</option>
                            <option value="MD">Maryland</option>
                            <option value="MA">Massachusetts</option>
                            <option value="MI">Michigan</option>
                            <option value="MN">Minnesota</option>
                            <option value="MS">Mississippi</option>
                            <option value="MO">Missouri</option>
                            <option value="MT">Montana</option>
                            <option value="NE">Nebraska</option>
                            <option value="NV">Nevada</option>
                            <option value="NH">New Hampshire</option>
                            <option value="NJ">New Jersey</option>
                            <option value="NM">New Mexico</option>
                            <option value="NY">New York</option>
                            <option value="NC">North Carolina</option>
                            <option value="ND">North Dakota</option>
                            <option value="OH">Ohio</option>
                            <option value="OK">Oklahoma</option>
                            <option value="OR">Oregon</option>
                            <option value="PA">Pennsylvania</option>
                            <option value="RI">Rhode Island</option>
                            <option value="SC">South Carolina</option>
                            <option value="SD">South Dakota</option>
                            <option value="TN">Tennessee</option>
                            <option value="TX">Texas</option>
                            <option value="UT">Utah</option>
                            <option value="VT">Vermont</option>
                            <option value="VA">Virginia</option>
                            <option value="WA">Washington</option>
                            <option value="WV">West Virginia</option>
                            <option value="WI">Wisconsin</option>
                            <option value="WY">Wyoming</option>
                        </select> <span class="add-on"><i class="icon-pushpin"></i> </span>
                        <input id="b_zipcode" class="zipcode" name="b_zipcode" type="text" placeholder="Zip Code" value=""
                        	required />
                    </div>
                    <div class="input-prepend">
                        <span class="add-on"><i class="icon-phone"></i> </span>
                        <input id="b_b_phone" class="phone p_bank2" name="b_b_phone" type="text" placeholder="Business Phone" value=""
                        	/>
                        <span class="add-on"><i class="icon-phone"></i> </span>
                        <input id="b_h_phone" class="phone p_bank2" name="b_h_phone" type="text" placeholder="Home Phone" value=""
                            />
                        <span class="add-on"><i class="icon-phone"></i> </span>
                        <input id="b_c_phone" class="phone p_bank2" name="b_c_phone" type="text" placeholder="Cell Phone" value=""
                        	/>
                    </div>
                </fieldset>
                
                    <input class="finish" type="button" value="Sign-Up" />
                </form>
            </div>
                
            </div>
            <!-- END CONTENT_WRAPPER -->
            
        </div>
        <!-- END MAIN_CONTAINER -->
        
    
    </div>
    <!-- END MAIN_WRAPPER -->
    
    <!-- BEGIN FOOTER -->
    <?php include_once('includes/footer.php'); ?>
    <!-- END FOOTER -->
    <script type="text/javascript">
		$(document).ready(function(){
			
			function pass_values(){
				$('#b_company').val( $('#company').val() );
				$('#b_email').val( $('#email').val() );
				$('#b_fname').val( $('#fname').val() );
				$('#b_lname').val( $('#lname').val() );
				$('#b_address1').val( $('#address1').val() );
				$('#b_address2').val( $('#address2').val() );
				$('#b_city').val( $('#city').val() );
				$('#b_state').val( $('#state').val() );
				$('#b_zipcode').val( $('#zipcode').val() );
				$('#b_b_phone').val( $('#b_phone').val() );
				$('#b_h_phone').val( $('#h_phone').val() );
				$('#b_c_phone').val( $('#c_phone').val() );
			}
			
		//SETUP THE INPUT MASKS
			$('.phone').mask('(000) 000-0000');
			$('.cc_date').mask('00/0000');
			$('.zipcode').mask('00000');
			$('.cc').mask('0000-0000-0000-0000');
				
		//SETUP THE WIZARD
			$('#signup_form').stepy({
				backLabel:      'Back',
				block:          true,
				errorImage:     true,
				nextLabel:      'Next',
				title:			true,
				titleClick:     false,
				legend:			true,
				description:	false,
				validate:       true,
				next:			function(index){ if($('#signup_form-step-' + String(index -2)).valid() == true){ pass_values(); } },
				finish:			function(index){ if($('#signup_form-step-' + String(index -2)).valid() == true){ /* do something */ } else { return false; } }
			});
				
		//SETUP RULES AND MESSAGES FOR FORM VALIDATION
			$('#signup_form').validate({
				onsubmit:	true,
				errorContainer: "#error_box1 ul",
				errorLabelContainer: "#error_box1 ul",
				wrapper: "li",
				errorPlacement:	function(error, element){
					$('#error_box1').append(error);
				},
				rules: {
					company:{
						required: 	true
					},
					email: {
						required:	true,
						email: 		true
					},
					fname: {
						required:	true,
						minlength:	3
					},
					lname: {
						required:	true,
						minlength:	3
					},
					address1: {
						required:	true
					},
					city: {
						required:	true
					},
					state: {
						required:	true
					},
					zipcode: {
						required:	true,
						minlength:	5
					},
					b_phone: {
						require_from_group: [1,".p_bank1"],
						phoneUS:	true
					},
					h_phone: {
						require_from_group: [1,".p_bank1"],
						phoneUS:	true
					},
					c_phone: {
						require_from_group: [1,".p_bank1"],
						phoneUS:	true
					},
					
				/********
				STEP TWO INPUTS RULES
				********/
					
					user: {
						required:	true,
						minlength:	4
					},
					password: {
						required:	true,
						minlength:	4
					},
					p_confirm: {
						equalTo:	"#password"
					},
					cc_type: {
						required:	true
					},
					cc_num: { 
						required: 	true,
						creditcard:	true
					},
					cc_exp_date: {
						required:	true,
						pattern: 	/^((0[1-9])|(1[0-2]))\/(\d{4})$/
					},
					b_company: {
						required:	true
					},
					b_email: {
						required:	true,
						email: true
					},
					b_fname: {
						required:	true,
						minlength:	3
					},
					b_lname: {
						required:	true,
						minlength:	3
					},
					b_address1: {
						required:	true
					},
					b_city: {
						required:	true
					},
					b_state: {
						required:	true
					},
					b_zipcode: {
						required:	true
					},
					b_b_phone: {
						require_from_group: [1,".p_bank2"]
					},
					b_h_phone: {
						require_from_group: [1,".p_bank2"]
					},
					b_c_phone: {
						require_from_group: [1,".p_bank2"]
					}
				}//End Rules
			});
			
			$(window).resize(function(e) {
				$('#main_container').height($(window).height() - 170);
				$('#content').height($('#main_container').height() -20);
			});
			
			$(window).trigger('resize');
		});
    </script>

</body>
</html>
// JavaScript Document
$(document).ready(function(){

/********************************************************************************************
************************************NAV FUNCTIONS********************************************
********************************************************************************************/
	
	/***********
		LOGOUT BUTTON PRESS
	***********/
    	$('#logout').click(function(){
			$.ajax({
				type: 'POST',
				url: 'ajax/ajax_nav.php',
				data: {clicked: 'logout'},
				cache: false,
				dataType: 'json',
				success: function(data){
					if(data.success == true){
						window.location = "front.php";
					}else if(data.success == false){
						alert('ERROR:\nPSAdminNM was not found in $_SESSION["PSAdminNM"]');
					} 
				}// end success function
			}); // end ajax call
		}); // end logout click
	
	/***********
		ADMIN SETTINGS BUTTON PRESS
	***********/
	//CLEAR THE VENDOR AND DATACENTER NAV DROP DOWNS
		$('#admin_nav').click(function(){
			$('#vendors>option:eq(0)').prop('selected', true);
			var _selected = $('#vendors').val();
			
			$.ajax({
				type: 'GET',
				url: 'ajax/ajax_nav.php',
				data: {vendors: _selected},
				async: false,
				success: function(result){
					$('#dcs_DD').html(result);
				}
			});
			
			
			$.ajax({
				type: 'GET',
				url: 'pages/admin/main_tabs.php',
				beforeSend: function(){
					$('#loading_div').toggleClass("hidden");
				},
				success: function(data){
					$('#content_wrapper').html(data);
/********************************************************************************************
*********************************DEFAULT ADMIN FUNCTIONS*************************************
********************************************************************************************/
					$.ajax({
						type: 'GET',
						url: 'pages/admin/user_sub_tabs.php',
						success: function(data){
							$('#content_sub_tabs').html(data);
						//ADD USER LOADED BY DEFAULT
							$.ajax({
								type: 'GET',
								url: 'pages/admin/add_users.php',
								success: function(html){
									$('#content').html(html);
									$(window).trigger('resize');
									$('#loading_div').toggleClass("hidden");
									
									var add_form = $('#add_form');
									add_form.validate({
										rules:{
											username:{
												minlength: 4,
												pattern: /^[A-Za-z0-9_-]{4,16}$/,
												required: true
												},//end rules for username
											password:{
												minlength: 4,
												pattern: /^[a-zA-Z0-9@#$%^&_\-]{4,16}$/,
												required: true
											}// end rules for password
										
										},//end rules
										messages:{
											username:{
												pattern: 'A-Z, a-z, 0-9, underscores _ and hyphens - only'
											},
											password:{
												pattern: 'A-Z, a-z, 0-9, @#$%^&_- only'
											}
										}
									});//end validation
									
									$('#apply_btn').click(function(){
										if(add_form.valid() == true){
											$.ajax({
												type: 'POST',
												url: 'pages/admin/add_users.php',
												data: $('#add_form').serialize(),
												dataType: "json",
												beforeSend: function(){
													$('#loading_div').toggleClass("hidden");
												},
												success: function(json){
													$('#loading_div').toggleClass("hidden");
													if(json.response != 'Susscess'){
														$('#username').toggleClass('valid', 'error');
														$('#username_msg_catch').html(json.response + ': ' + json.username);
														
														$('#username_msg_catch').show();
														
														$('#username').focus(function(e){
															if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																$('#username_msg_catch').show();
															}else{
																$('#username_msg_catch').hide();
															}
														});
														$('#username').keyup(function(e){
															if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																$('#username_msg_catch').show();
															}else{
																$('#username_msg_catch').hide();
															}
														});
														
													}else if(json.response == 'Susscess'){
														alert('Success!');
														$('#add_sub').click(); // re-direct to [USERS / ADD]
													}
												}//END APPLY BUTTON SUCCESS
											});//END AJAX
										}//END IF ADD_FORM == VALID
									});//END APPLY BUTTON EVENT LISTENER
										
								//ADD USER SUB MENU CLICK
									$('#add_sub').click(function(){
										$.ajax({
											type: 'GET',
											url: 'pages/admin/add_users.php',
											beforeSend: function(){
												$('#loading_div').toggleClass("hidden");
											},
											success: function(data){
												$('#content').html(data);
												$(window).trigger('resize');
												$('#loading_div').toggleClass("hidden");
									
												var add_form = $('#add_form');
												add_form.validate({
													rules:{
														username:{
															minlength: 4,
															pattern: /^[A-Za-z0-9_-]{4,16}$/,
															required: true
															},//end rules for username
														password:{
															minlength: 4,
															pattern: /^[a-zA-Z0-9@#$%^&_\-]{4,16}$/,
															required: true
														}// end rules for password
													
													},//end rules
													messages:{
														username:{
															pattern: 'A-Z, a-z, 0-9, underscores _ and hyphens - only'
														},
														password:{
															pattern: 'A-Z, a-z, 0-9, @#$%^&_- only'
														}
													}
												});//end validation
												
												$('#apply_btn').click(function(){
													if(add_form.valid() == true){
														$.ajax({
															type: 'POST',
															url: 'pages/admin/add_users.php',
															data: $('#add_form').serialize(),
															dataType: "json",
															beforeSend: function(){
																$('#loading_div').toggleClass("hidden");
															},
															success: function(json){
																$('#loading_div').toggleClass("hidden");
																if(json.response != 'Susscess'){
																	$('#username').toggleClass('valid', 'error');
																	$('#username_msg_catch').html(json.response + ': ' + json.username);
																	
																	$('#username_msg_catch').show();
																	
																	$('#username').focus(function(e){
																		if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																			$('#username_msg_catch').show();
																		}else{
																			$('#username_msg_catch').hide();
																		}
																	});
																	$('#username').keyup(function(e){
																		if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																			$('#username_msg_catch').show();
																		}else{
																			$('#username_msg_catch').hide();
																		}
																	});
																	
																}else if(json.response == 'Susscess'){
																	alert('Success!');
																	$('#add_sub').click(); // re-direct to [USERS / ADD]
																}
															}//END APPLY BUTTON SUCCESS
														});//END AJAX
													}//END IF ADD_FORM == VALID
												});//END APPLY BUTTON EVENT LISTENER
												
											}//END GET AJAX ADD_USER SUCCESS
										});//END GET AJAX ADD_USER
									});//END ADD USER SUB MENU CLICK
										
									//EDIT USER SUB MENU CLICK
										$('#edit_sub').click(function(){
											$.ajax({
												type: 'GET',
												url: 'pages/admin/edit_users.php',
												beforeSend: function(){
													$('#loading_div').toggleClass("hidden");
												},
												success: function(data){
													$('#content').html(data);
													$(window).trigger('resize');
													$('#loading_div').toggleClass("hidden");
													
													$('#users_DD').change(function(){
														var user = $('#users_DD').val();
														
														$.ajax({
															type: 'GET',
															url: 'pages/admin/edit_users.php',
															data: {user: user},
															beforeSend: function(){
																$('#loading_div').toggleClass("hidden");
															},
															success: function(html){
																$('#user_info').html(html);
																$(window).trigger('resize');
																$('#loading_div').toggleClass("hidden");
																
																$('#apply_btn').click(function(){
																	$.ajax({
																		type: 'POST',
																		url: 'pages/admin/edit_users.php',
																		data: $('#edit_form').serialize(),
																		beforeSend: function(){
																			$('#loading_div').toggleClass("hidden");
																		},
																		success: function(data){
																			$('#loading_div').toggleClass("hidden");
																			alert(data);
																			$('#edit_sub').click(); // re-direct to [USERS / EDIT]
																		}//END APPLY BUTTON SUCCESS
																	});//END AJAX
																});//END APPLY BUTTON EVENT LISTENER
															}//END GET EDIT_USERS SUCCESS W/SELECTED USER
														});//END GET AJAX EDIT_USERS
													});//END USERS_DD CHANGE EVENT LISTENER
													
												}//END GET EDIT USERS SUCCESS
											});//END GET EDIT USERS AJAX
										});//END EDIT USER SUB MENU CLICK
									
/********************************************************************************************
************************************ADMIN USERS FUNCTIONS************************************
********************************************************************************************/
								//USERS TAB CLICK EVENT LISTENER
									$('#admin_USERS_tab').click(function(){
										
										$.ajax({
											type: 'GET',
											url: 'pages/admin/user_sub_tabs.php',
											beforeSend: function(){
												$('#loading_div').toggleClass("hidden");
											},
											success: function(data){
												$('#content_sub_tabs').html(data);
												$.ajax({
													type: 'GET',
													url: 'pages/admin/add_users.php',
													success: function(data){
														$('#content').html(data);
														$(window).trigger('resize');
														$('#loading_div').toggleClass("hidden");
														
														var add_form = $('#add_form');
														add_form.validate({
															rules:{
																username:{
																	minlength: 4,
																	pattern: /^[A-Za-z0-9_-]{4,16}$/,
																	required: true
																	},//end rules for username
																password:{
																	minlength: 4,
																	pattern: /^[a-zA-Z0-9@#$%^&_\-]{4,16}$/,
																	required: true
																}// end rules for password
															
															},//end rules
															messages:{
																username:{
																	pattern: 'A-Z, a-z, 0-9, underscores _ and hyphens - only'
																},
																password:{
																	pattern: 'A-Z, a-z, 0-9, @#$%^&_- only'
																}
															}
														});//end validation
														
														$('#apply_btn').click(function(){
															if(add_form.valid() == true){
																$.ajax({
																	type: 'POST',
																	url: 'pages/admin/add_users.php',
																	data: $('#add_form').serialize(),
																	dataType: "json",
																	beforeSend: function(){
																		$('#loading_div').toggleClass("hidden");
																	},
																	success: function(json){
																		$('#loading_div').toggleClass("hidden");
																		if(json.response != 'Susscess'){
																			$('#username').toggleClass('valid', 'error');
																			$('#username_msg_catch').html(json.response + ': ' + json.username);
																			$('#username_msg_catch').show();
																			
																			$('#username').focus(function(e){
																				if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																					$('#username_msg_catch').show();
																				}else{
																					$('#username_msg_catch').hide();
																				}
																			});
																			$('#username').keyup(function(e){
																				if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																					$('#username_msg_catch').show();
																				}else{
																					$('#username_msg_catch').hide();
																				}
																			});
																			
																		}else if(json.response == 'Susscess'){
																			alert('Success!');
																			$('#add_sub').click(); // re-direct to [USERS / ADD]
																		}
																	}//END APPLY BUTTON SUCCESS
																});//END AJAX
															}//END IF ADD_FORM == VALID
														});//END APPLY BUTTON EVENT LISTENER
									
													}//END GET AJAX ADD_USER.PHP SUCCESS
												});//END GET AJAX ADD_USER.PHP
												
											//ADD USER SUB MENU CLICK
												$('#add_sub').click(function(){
													$.ajax({
														type: 'GET',
														url: 'pages/admin/add_users.php',
														beforeSend: function(){
															$('#loading_div').toggleClass("hidden");
														},
														success: function(data){
															$('#content').html(data);
															$(window).trigger('resize');
															$('#loading_div').toggleClass("hidden");
															
															var add_form = $('#add_form');
															add_form.validate({
																rules:{
																	username:{
																		minlength: 4,
																		pattern: /^[A-Za-z0-9_-]{4,16}$/,
																		required: true
																		},//end rules for username
																	password:{
																		minlength: 4,
																		pattern: /^[a-zA-Z0-9@#$%^&_\-]{4,16}$/,
																		required: true
																	}// end rules for password
																
																},//end rules
																messages:{
																	username:{
																		pattern: 'A-Z, a-z, 0-9, _ - only'
																	},
																	password:{
																		pattern: 'A-Z, a-z, 0-9, @#$%^&_- only'
																	}
																}
															});//end validation
															
															$('#apply_btn').click(function(){
																if(add_form.valid() == true){
																	$.ajax({
																		type: 'POST',
																		url: 'pages/admin/add_users.php',
																		data: $('#add_form').serialize(),
																		dataType: "json",
																		beforeSend: function(){
																			$('#loading_div').toggleClass("hidden");
																		},
																		success: function(json){
																			$('#loading_div').toggleClass("hidden");
																			if(json.response != 'Susscess'){
																				$('#username').toggleClass('valid', 'error');
																				$('#username_msg_catch').html(json.response + ': ' + json.username);
																				
																				$('#username_msg_catch').show();
																				
																				$('#username').focus(function(e){
																					if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																						$('#username_msg_catch').show();
																					}else{
																						$('#username_msg_catch').hide();
																					}
																				});
																				$('#username').keyup(function(e){
																					if(json.username.toLowerCase() == $('#username').val().toLowerCase()){
																						$('#username_msg_catch').show();
																					}else{
																						$('#username_msg_catch').hide();
																					}
																				});
																			
																			}else if(json.response == 'Susscess'){
																				alert('Success!');
																				$('#add_sub').click(); // re-direct to [USERS / ADD]
																			}
																		}//END APPLY BUTTON SUCCESS
																	});//END AJAX
																}//END IF ADD_FORM == VALID
															});//END APPLY BUTTON EVENT LISTENER
															
														}//END GET AJAX ADD_USER SUCCESS
													});//END GET AJAX ADD_USER
												});//END ADD USER SUB MENU CLICK
												
											//EDIT USER SUB MENU CLICK
												$('#edit_sub').click(function(){
													$.ajax({
														type: 'GET',
														url: 'pages/admin/edit_users.php',
														beforeSend: function(){
															$('#loading_div').toggleClass("hidden");
														},
														success: function(data){
															$('#content').html(data);
															$(window).trigger('resize');
															$('#loading_div').toggleClass("hidden");
															
															$('#users_DD').change(function(){
																var user = $('#users_DD').val();
																
																$.ajax({
																	type: 'GET',
																	url: 'pages/admin/edit_users.php',
																	data: {user: user},
																	beforeSend: function(){
																		$('#loading_div').toggleClass("hidden");
																	},
																	success: function(html){
																		$('#user_info').html(html);
																		$(window).trigger('resize');
																		$('#loading_div').toggleClass("hidden");
																		
																		$('#apply_btn').click(function(){
																			$.ajax({
																				type: 'POST',
																				url: 'pages/admin/edit_users.php',
																				data: $('#edit_form').serialize(),
																				beforeSend: function(){
																					$('#loading_div').toggleClass("hidden");
																				},
																				success: function(data){
																					$('#loading_div').toggleClass("hidden");
																					alert(data);
																					$('#edit_sub').click(); // re-direct to [USERS / EDIT]
																				}//END APPLY BUTTON SUCCESS
																			});//END AJAX
																		});//END APPLY BUTTON EVENT LISTENER
																	}//END GET EDIT_USERS SUCCESS W/SELECTED USER
																});//END GET AJAX EDIT_USERS
															});//END USERS_DD CHANGE EVENT LISTENER
															
														}//END GET EDIT USERS SUCCESS
													});//END GET EDIT USERS AJAX
												});//END EDIT USER SUB MENU CLICK
												
											}//END SUCCESS
										});//END AJAX
									});// END USERS TAB CLICK EVENT LISTENER
									
									
/********************************************************************************************
************************************ADMIN RIGHTS FUNCTIONS************************************
********************************************************************************************/
								//USERS TAB CLICK EVENT LISTENER
									$('#admin_RIGHTS_tab').click(function(){
										
										$.ajax({
											type: 'GET',
											url: 'pages/admin/rights_sub_tabs.php',
											beforeSend: function(){
												$('#loading_div').toggleClass("hidden");
											},
											success: function(data){
												$('#content_sub_tabs').html(data);
												$.ajax({
													type: 'GET',
													url: 'pages/admin/edit_permissions.php',
													success: function(data){
														$('#content').html(data);
														$(window).trigger('resize');
														$('#loading_div').toggleClass("hidden");
															
														var edit_form = $('#edit_form');
														edit_form.validate({
															rules:{
																users_DD:{
																	required: true
																},
																vendors_permissions_DD:{
																	required: true	
																}
															},
															messages:{
																users_DD:{
																	required: ""	
																},
																vendors_permissions_DD:{
																	required: ""	
																}
															}
														});
														
														$('#users_DD, #vendors_permissions_DD').change(function(){
															
															if(edit_form.valid() == true){
																var user = $('#users_DD').val();
																var vendor = $('#vendors_permissions_DD').val();
																
																$.ajax({
																	type: 'GET',
																	url: 'pages/admin/edit_permissions.php',
																	data: {user: user,
																		vendor: vendor
																	},
																	beforeSend: function(){
																		$('#loading_div').toggleClass("hidden");
																	},
																	success: function(html){
																		$('#user_info').html(html);
																		$(window).trigger('resize');
																		$('#loading_div').toggleClass("hidden");
																		
																		$('#apply_btn').click(function(){
																			$.ajax({
																				type: 'POST',
																				url: 'pages/admin/edit_permissions.php',
																				data: $('#edit_form').serialize(),
																				beforeSend: function(){
																					$('#loading_div').toggleClass("hidden");
																				},
																				success: function(data){
																					$('#loading_div').toggleClass("hidden");
																					alert(data);
																					$('#permissions_sub').click(); // re-direct to [USERS / EDIT]
																				}//END APPLY BUTTON SUCCESS
																			});//END AJAX
																		});//END APPLY BUTTON EVENT LISTENER
																	}//END GET EDIT_USERS SUCCESS W/SELECTED USER
																});//END GET AJAX EDIT_USERS
															}//END IF FORM IS VALID
														});//END vendor_permissions_DD CHANGE EVENT LISTENER
														
													}//END GET EDIT_PERMISSIONS SUCCESS
												});
											
											//EDIT PERMISSIONS SUB MENU CLICK
												$('#permissions_sub').click(function(){
													$.ajax({
														type: 'GET',
														url: 'pages/admin/edit_permissions.php',
														beforeSend: function(){
															$('#loading_div').toggleClass("hidden");
														},
														success: function(data){
															$('#content').html(data);
															$(window).trigger('resize');
															$('#loading_div').toggleClass("hidden");
															
															var edit_form = $('#edit_form');
															edit_form.validate({
																rules:{
																	users_DD:{
																		required: true
																	},
																	vendors_permissions_DD:{
																		required: true	
																	}
																},
																messages:{
																	users_DD:{
																		required: ""
																	},
																	vendors_permissions_DD:{
																		required: ""	
																	}
																}
															});
															
															$('#users_DD, #vendors_permissions_DD').change(function(){
																
																if(edit_form.valid() == true){
																	var user = $('#users_DD').val();
																	var vendor = $('#vendors_permissions_DD').val();
																	
																	$.ajax({
																		type: 'GET',
																		url: 'pages/admin/edit_permissions.php',
																		data: {user: user,
																			vendor: vendor
																		},
																		beforeSend: function(){
																			$('#loading_div').toggleClass("hidden");
																		},
																		success: function(html){
																			$('#user_info').html(html);
																			$(window).trigger('resize');
																			$('#loading_div').toggleClass("hidden");
																			
																			$('#apply_btn').click(function(){
																				$.ajax({
																					type: 'POST',
																					url: 'pages/admin/edit_permissions.php',
																					data: $('#edit_form').serialize(),
																					beforeSend: function(){
																						$('#loading_div').toggleClass("hidden");
																					},
																					success: function(data){
																						$('#loading_div').toggleClass("hidden");
																						alert(data);
																						$('#permissions_sub').click(); // re-direct to [USERS / EDIT]
																					}//END APPLY BUTTON SUCCESS
																				});//END AJAX
																			});//END APPLY BUTTON EVENT LISTENER
																		}//END GET EDIT_USERS SUCCESS W/SELECTED USER
																	});//END GET AJAX EDIT_USERS
																}//END IF FORM IS VALID
															});//END vendor_permissions_DD CHANGE EVENT LISTENER
															
														}//END GET EDIT_PERMISSIONS SUCCESS
													});//END GET EDIT_PERMISSIONS AJAX
												});//END EDIT_PERMISSIONS SUB MENU CLICK
												
											}//END SUCCESS
										});//END AJAX
									});// END RIGHTS TAB CLICK EVENT LISTENER


/********************************************************************************************
*********************************ADMIN DATACENTER FUNCTIONS**********************************
********************************************************************************************/
								//DATACENTER TAB CLICK EVENT LISTENER
									$('#admin_DC_tab').click(function(){
										
										$.ajax({
											type: 'GET',
											url: 'pages/admin/dc_sub_tabs.php',
											beforeSend: function(){
												$('#loading_div').toggleClass("hidden");
											},
											success: function(data){
												$('#content_sub_tabs').html(data);
												$.ajax({
													type: 'GET',
													url: 'pages/admin/add_dc.php',
													success: function(data){
														$('#content').html(data);
														$(window).trigger('resize');
														$('#loading_div').toggleClass("hidden");
														
													//begin form validation
														
														var add_dc_form = $('#add_dc_form');
														add_dc_form.validate({
															rules:{
																"DC[dc_provider]":{
																	required: true
																}//END VENDOR RULES
																,"DC[dc_name]":{
																	minlength: 3,
																	pattern: /^[A-Za-z0-9_-]{3,16}$/,
																	required: true
																}//END DC['DC_NAME'] RULES
																,"DC[dc_username]":{
																	minlength: 3,
																	pattern: /^[A-Za-z0-9_-]{3,16}$/,
																	required: true
																}//END DC[DC_USERNAME] RULES
																,"DC[dc_password]":{
																	minlength: 4,
																	pattern: /^[a-zA-Z0-9@#$%^&_\-]{4,16}$/,
																	required: true
																}
																,"DC[dc_api_key]":{
																	required: true
																}
																,"DC[dc_secret_key]":{
																	required: true
																}
															},//END RULES
															messages:{
																"DC[dc_provider]":{
																	required: ""
																}//END VENDOR MESSAGES
																,"DC[dc_name]":{
																	pattern: 'A-Z, a-z, 0-9, _ - only',
																	required: 'Enter a name for the new data center'
																}//END DC_NAME MESSAGES
																,"DC[dc_username]":{
																	pattern: 'A-Z, a-z, 0-9, _ - only',
																	required: 'Enter a username'
																}//END DC[DC_USERNAME] MESSAGES
																,"DC[dc_password]":{
																	pattern: 'A-Z, a-z, 0-9, @#$%^&_- only',
																	required: 'Enter a password'
																}
																,"DC[dc_api_key]":{
																	required: 'Enter a API Key'
																}
																,"DC[dc_secret_key]":{
																	required: 'Enter a Secret Key'
																}
															}//END MESSAGES
														});//end form validation
														
													//SELECT VENDOR DROP DOWN CHANGE EVENT LISTENER
														$('#dc_provider').change(function(){
															var vendor = $('#dc_provider').val();
															if(vendor == "Amazon"){
																$('tbody').append('<tr id="secret_key"><td class="label">Secret Key:</td><td><input id="dc_secret_key" name="DC[dc_secret_key]" type="text" placeholder="Secret Key" /></td></tr>');
															}else if($('#secret_key').length != 0){
																$('#secret_key').remove();
															}
															
														});//END VENDOR DROP DOWN CHANGE EVENT LISTENER
													//CREATE DC BUTTON CLICK EVENT LISTENER	
														$('#create_btn').click(function(){
															if(add_dc_form.valid() == true){
																$.ajax({
																	type: 'POST',
																	url: 'pages/admin/add_dc.php',
																	data: $('#add_dc_form').serialize(),
																	beforeSend: function(){
																		$('#loading_div').toggleClass("hidden");
																	},
																	success: function(data){
																		$('#loading_div').toggleClass("hidden");
																		alert(data);
																		$('#add_sub').click();
																	}//END POST AJAX ADD_DC.PHP SUCCESS
																});//END POST AJAX ADD_DC.PHP
															}//END IF ADD_DC_FORM.VALID == TRUE
														});//END CREATE BUTTON CLICK LISTENER
																									
													}//END GET AJAX ADD_DC.PHP SUCCESS
												});//END GET AJAX ADD_DC.PHP
												
											//ADD DC SUB MENU CLICK
												$('#add_sub').click(function(){
													$.ajax({
														type: 'GET',
														url: 'pages/admin/add_dc.php',
														beforeSend: function(){
															$('#loading_div').toggleClass("hidden");
														},
														success: function(data){
															$('#content').html(data);
															$(window).trigger('resize');
															$('#loading_div').toggleClass("hidden");
															
														//begin form validation
															
															var add_dc_form = $('#add_dc_form');
															add_dc_form.validate({
																rules:{
																	"DC[dc_provider]":{
																		required: true
																	}//END VENDOR RULES
																	,"DC[dc_name]":{
																		minlength: 3,
																		pattern: /^[A-Za-z0-9_-]{3,16}$/,
																		required: true
																	}//END DC['DC_NAME'] RULES
																	,"DC[dc_username]":{
																		minlength: 3,
																		pattern: /^[A-Za-z0-9_-]{3,16}$/,
																		required: true
																	}//END DC[DC_USERNAME] RULES
																	,"DC[dc_password]":{
																		minlength: 4,
																		pattern: /^[a-zA-Z0-9@#$%^&_\-]{4,16}$/,
																		required: true
																	}
																	,"DC[dc_api_key]":{
																		required: true
																	}
																	,"DC[dc_secret_key]":{
																		required: true
																	}
																},//END RULES
																messages:{
																	"DC[dc_provider]":{
																		required: ""
																	}//END VENDOR MESSAGES
																	,"DC[dc_name]":{
																		pattern: 'A-Z, a-z, 0-9, _ - only',
																		required: 'Enter a name for the new data center'
																	}//END DC_NAME MESSAGES
																	,"DC[dc_username]":{
																		pattern: 'A-Z, a-z, 0-9, _ - only',
																		required: 'Enter a username'
																	}//END DC[DC_USERNAME] MESSAGES
																	,"DC[dc_password]":{
																		pattern: 'A-Z, a-z, 0-9, @#$%^&_- only',
																		required: 'Enter a password'
																	}
																	,"DC[dc_api_key]":{
																		required: 'Enter a API Key'
																	}
																	,"DC[dc_secret_key]":{
																		required: 'Enter a Secret Key'
																	}
																}//END MESSAGES
															});//end form validation
															
														//SELECT VENDOR DROP DOWN CHANGE EVENT LISTENER
															$('#dc_provider').change(function(){
																var vendor = $('#dc_provider').val();
																if(vendor == "Amazon"){
																	$('tbody').append('<tr id="secret_key"><td class="label">Secret Key:</td><td><input id="dc_secret_key" name="DC[dc_secret_key]" type="text" placeholder="Secret Key" /></td></tr>');
																}else if($('#secret_key').length != 0){
																	$('#secret_key').remove();
																}
																
															});//END VENDOR DROP DOWN CHANGE EVENT LISTENER
														//CREATE DC BUTTON CLICK EVENT LISTENER	
															$('#create_btn').click(function(){
																if(add_dc_form.valid() == true){
																	$.ajax({
																		type: 'POST',
																		url: 'pages/admin/add_dc.php',
																		data: $('#add_dc_form').serialize(),
																		beforeSend: function(){
																			$('#loading_div').toggleClass("hidden");
																		},
																		success: function(data){
																			$('#loading_div').toggleClass("hidden");
																			alert(data);
																			$('#add_sub').click();
																		}//END POST AJAX ADD_DC.PHP SUCCESS
																	});//END POST AJAX ADD_DC.PHP
																}//END IF ADD_DC_FORM.VALID == TRUE
															});//END CREATE BUTTON CLICK LISTENER
																										
														}//END GET AJAX ADD_DC.PHP SUCCESS
													});//END GET AJAX ADD_DC.PHP
												});//END ADD DC SUB MENU CLICK
												
											//EDIT DC SUB MENU CLICK
												$('#edit_sub').click(function(){
													$.ajax({
														type: 'GET',
														url: 'pages/admin/edit_dc.php',
														beforeSend: function(){
															$('#loading_div').toggleClass("hidden");
														},
														success: function(data){
															$('#content').html(data);
															$(window).trigger('resize');
															$('#loading_div').toggleClass("hidden");
															
														//VENDOR DROP DOWN ON CHANGE
															$('#vendors_DC_DD').change(function(){
																var vend_sel = $('#vendors_DC_DD').val();
																																	
																$.ajax({
																	type: 'GET',
																	url: 'pages/admin/edit_dc.php',
																	data: {vendor: vend_sel},
																	beforeSend: function(){
																		$('#loading_div').toggleClass("hidden");
																		$('#DC_info').html('');
																	},
																	success: function(html){
																		$('#dc_DD_catch').html(html);
																		$(window).trigger('resize');
																		$('#loading_div').toggleClass("hidden");
																		
																	//DATACENTER DROP DOWN ON CHANGE
																		$('#dc_DD').change(function(){
																			var dc_sel = $('#dc_DD').val();
																			
																			$.ajax({
																				type: 'GET',
																				url: 'pages/admin/edit_dc.php',
																				data: {datacenter: dc_sel},
																				beforeSend: function(){
																					$('#loading_div').toggleClass("hidden");
																				},
																				success: function(html){
																					$('#DC_info').html(html);
																					$(window).trigger('resize');
																					$('#loading_div').toggleClass("hidden");
																					
																					$('#apply_btn').click(function(){
																						$.ajax({
																							type: 'POST',
																							url: 'pages/admin/edit_dc.php',
																							data: $('#edit_form').serialize(),
																							beforeSend: function(){
																								$('#loading_div').toggleClass("hidden");
																							},
																							success: function(){
																								$('#loading_div').toggleClass("hidden");
																								alert('UPDATE SUCCESS');
																								$('#edit_sub').click(); // re-direct to [DC / EDIT]
																								
																							}//END POST AJAX EDIT_DC.PHP SUCCESS
																						});//END POST AJAX EDIT_DC.PHP
																					});//END APPLY BUTTON EVENT LISTENER
																					
																				}//END GET EDIT_DC SUCCESS W/SELECTED DC
																			});//END GET AJAX EDIT_DC
																		});//END DC_DD CHANGE EVENT LISTENER
																		
																	}//END GET AJAX EDIT_DC.PHP SUCCESS
																});//END GET AJAX EDIT_DC.PHP				
															});//END VENDOR DROP DOWN ON CHANGE EVENT LISTENER
															
														}//END GET AJAX EDIT_DC SUCCESS
													});//END GET AJAX EDIT_DC
												});//END DC USER SUB MENU CLICK
												
											}//END GET AJAX ADD_DC.PHP SUCCESS
										});//END GET AJAX ADD_DC.PHP
									});// END DATACENTER TAB CLICK EVENT LISTENER
									
								}//END GET AJAX ADD_USER.PHP SUCCESS - DEFAULT
							});//END GET AJAX ADD_USER.PHP - DEFAULT
							
						}//END GET AJAX SUB_TABS.PHP SUCCESS
					});//END GET AJAX SUB_TABS.PHP AJAX
					
				}//END GET AJAX MAIN_TABS.PHP SUCCESS
			});//END GET AJAX MAIN_TABS.PHP
			
		});//END ADMIN SETTINGS CLICK EVENT LISTENER


	/***********
		DNS INFO NAV BUTTON PRESS
	***********/
	
//DNS INFO BUTTON NAV CLICK EVENT LISTENER.
$('#dns_nav').click(function(event){
/********************************************************************************************
*********************************DEFAULT DNS FUNCTIONS***************************************
********************************************************************************************/
//DISPLAY MAIN_TABS.PHP
	$.ajax({
		type: 'GET',
		url: 'pages/dns/main_tabs.php',
		beforeSend: function(){
			$('#loading_div').toggleClass("hidden");												
		},
		success: function(data){
			$('#content_wrapper').html(data);
			$('#loading_div').toggleClass("hidden");
			
		//DISPLAY SUB_TABS.PHP
			$.ajax({
				type: 'GET',
				dataType: 'html',
				url: 'pages/dns/sub_tabs.php',                
				data: 'tab_select=domain',
				beforeSend: function(){
					$('#loading_div').toggleClass("hidden");												
				},
				success: function(data){
					$('#content_sub_tabs').html(data);
				//DEFAULT LOAD ADD_DOMAIN.PHP
					$.ajax({
						type: 'GET',
						dataType: 'html',
						url: 'pages/dns/add_domain.php',
						success: function(data){
							$('#content').html(data);
							$('#loading_div').toggleClass("hidden");
							
						}//END DEFAULT LOAD ADD_DOMAIN.PHP SUCCESS
					});//END DEFAULT LOAD ADD_DOMAIN.PHP
				
/********************************************************************************************
************************************DNS DOMAIN FUNCTIONS*************************************
********************************************************************************************/
			//DOMAIN MAIN_TAB CLICK EVENT LISTENER
				$('#domain_tab').click(function(){
					$.ajax({
						type:'GET',
						dataType:'html',
						url:'pages/dns/sub_tabs.php',                
						data:'tab_select=domain',
						beforeSend: function(){
							$('#loading_div').toggleClass("hidden");												
						},
						success: function(data){
							$('#content_sub_tabs').html(data);
							//DEFAULT LOAD ADD_DOMAIN.PHP
								$.ajax({
									type:'GET',
									dataType:'html',
									url:'pages/dns/add_domain.php',
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										
									}//END GET AJAX ADD_DOMAIN.PHP SUCCESS
								});//END GET AJAX ADD_DOMAIN.PHP
								
							//ADD DOMAIN SUB_TAB CLICK EVENT LISTENER
								$('#add_domain').click(function(){     
									$.ajax({
										type:'GET',
										dataType:'html',
										url:'pages/dns/add_domain.php',                
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");												
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											
										}//END GET AJAX ADD_DOMAIN.PHP SUCCESS
									});//END GET AJAX ADD_DOMAIN.PHP
								});//END ADD_DOMAIN CLICK EVENT LISTENER
								
							//DELETE DOMAIN SUB_TAB CLICK
								$('#delete_domain').click(function(){
									$.ajax({
										type:'GET',
										dataType:'html',
										url:'pages/dns/delete_domain.php',                
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");												
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											
										}//END GET AJAX DELETE_DOMAIN.PHP SUCCESS
									});//END GET AJAX DELETE_DOMAIN.PHP
								});//END DELETE_DOMAIN SUB TAB CLICK EVENT LISTENER
								  
								  
							}//END GET AJAX SUB_TABS.PHP SUCCESS
						});//END GET AJAX SUB_TABS.PHP
						  
					});//END DOMAIN SUB_TAB CLICK EVENT LISTENER
								
				//ADD DOMAIN SUB_TAB CLICK EVENT LISTENER
					$('#add_domain').click(function(){     
						$.ajax({
							type:'GET',
							dataType:'html',
							url:'pages/dns/add_domain.php',                
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content').html(data);
								$('#loading_div').toggleClass("hidden");
								
							}//END GET AJAX ADD_DOMAIN.PHP SUCCESS
						});//END GET AJAX ADD_DOMAIN.PHP
					});//END ADD_DOMAIN CLICK EVENT LISTENER
					
				//DELETE DOMAIN SUB_TAB CLICK
					$('#delete_domain').click(function(){
						$.ajax({
							type:'GET',
							dataType:'html',
							url:'pages/dns/delete_domain.php',                
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content').html(data);
								$('#loading_div').toggleClass("hidden");
								
							}//END GET AJAX DELETE_DOMAIN.PHP SUCCESS
						});//END GET AJAX DELETE_DOMAIN.PHP
					});//END DELETE_DOMAIN SUB TAB CLICK EVENT LISTENER

/********************************************************************************************
************************************DNS RECORDS FUNCTIONS*************************************
********************************************************************************************/
				//RECORDS MAIN_TAB CLICK EVENT LISTENER
					$('#records_tab').click(function(){
						$.ajax({
							type:'GET',
							dataType:'html',
							url:'pages/dns/sub_tabs.php',                
							data:'tab_select=records',
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content_sub_tabs').html(data);
							//DEFAULT LOAD ADD_RECORD.PHP
								$.ajax({
									type:'GET',
									dataType:'html',
									url:'pages/dns/add_record.php',
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										
									}//END DEFAULT GET AJAX ADD_RECORD.PHP SUCCESS
								});//END DEFAULT GET AJAX ADD_RECORD.PHP
								
							}//END GET AJAX SUB_TABS.PHP SUCCESS
						});//END GET AJAX SUB_TABS.PHP
						
					});//END RECORDS SUB_TAB CLICK EVENT LISTENER

				//ADD_RECORD SUB_TAB CLICK EVENT LISTENER
					$('#add_record').click(function(){
						$.ajax({
							type:'GET',
							dataType:'html',
							url:'pages/dns/add_record.php',                
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content').html(data);
								$('#loading_div').toggleClass("hidden");
								
							}//END GET AJAX ADD_RECORD.PHP SUCCESS
						});//END GET AJAX ADD_RECORD.PHP
					});//END SUB TAB CLICK EVENT LISTENER
					
				//DELETE_RECORD SUB_TAB CLICK EVENT
					$('#delete_record').click(function(){
						$.ajax({
							type:'GET',
							dataType:'html',
							url:'pages/dns/delete_record.php',                
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content').html(data);
								$('#loading_div').toggleClass("hidden");
								
							}//END GET AJAX DELETE_RECORD.PHP SUCCESS
						});//END GET AJAX DELETE_RECORD.PHP
					});//END DELETE_RECORD SUB TAB CLICK EVENT LISTENER

				}//END GET AJAX SUB_TABS.PHP SUCCESS
			});//END GET AJAX SUB_TABS.PHP
	
													
		}//END GET AJAX MAIN_TABS.PHP SUCCESS
	});//END GET AJAX MAIN_TABS.PHP
	
});//END DNS_NAV CLICK EVENT LISTENER
	
		
/***********
	SUB NAV SHOW/HIDE
***********/
	$(".nav_dir").hover(function(){
	  $(this).find(".nav_sub").css("display", "block");
	  },function(){
	  $(this).find(".nav_sub").css("display", "none");
	});
	
/***********
	NAV DROPDOWN ON CHANGE - UPDATE/CLEAR OTHER NAV DROPDOWN
***********/
			
/***********
VENDORS ON CHANGE - UPDATE DATACENTER DROPDOWN
***********/
$('#vendors').change(function() {
	var _selected = $(this).val();
	if(_selected != ""){				
		$.ajax({
			type: 'GET',
			url: "pages/vendor_info/" + _selected + ".php",
			success: function(result){
				$('#content_wrapper').html(result);
				$(window).trigger('resize');
			}
		});
	}else{
		$.ajax({
			type: 'GET',
			url: "pages/vendor_info/cost_compare.php",
			success: function(result){
				$('#content_wrapper').html(result);
				$(window).trigger('resize');
			}
		});
	}
	
	$('#content_wrapper').html();
	$.ajax({
		type: 'GET',
		url: 'ajax/ajax_nav.php',
		data: {vendors: _selected}
	}).done(function(result){
		$('#dcs_DD').html(result);
		/***********
			DATACENTER ON CHANGE - load 'content_menu.php'
		***********/
			$('#dcs_sel').change(function() {
				var _selected = $(this).val();
				$.ajax({
					type: 'GET',
					dataType: 'json',
					url: 'ajax/ajax_nav.php',
					data: {dcs: _selected}
				}).done(function(data){
					$('#content_wrapper').html(data.html_string);
				/***********
				CONTENT MENU TAB CLICK FUNCTIONS
				***********/
				
					$.ajax({ //load sub_tabs html into content_sub_tabs
						type: 'GET',
						url: 'pages/datacenter/sub_tabs.php' // by default
					}).done(function(result){
						$('#content_sub_tabs').html(result);//load sub_tabs for default [servers] tab									
							$.ajax({
								type: 'GET',
								data: {dcs: _selected},
								url: 'pages/datacenter/servers_display.php',
								beforeSend: function(){
									$('#loading_div').toggleClass("hidden");												
								},
								success: function(data){
									$('#content').html(data);
									$('#loading_div').toggleClass("hidden");
									$(window).trigger('resize');
								}
							});

/********************************************************************************************
************************************DEFAULT FUNCTIONS****************************************
********************************************************************************************/
						
						//DISPLAY SUB-TAB CLICKED
							$('#display_sub').click(function(){
								var _selected = $('#dcs_sel').val();
								
								$.ajax({
									type: 'GET',
									data: {dcs: _selected},
									url: 'pages/datacenter/servers_display.php',
									beforeSend: function(){
										$('#loading_div').toggleClass("hidden");
									},
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										$(window).trigger('resize');
									}
								});
							});
						
						//CREATE SUB-TAB CLICKED
							$('#create_sub').click(function(){
								$.ajax({
									type: 'GET',
									url: 'pages/datacenter/servers_create.php',
									data: { dcs: $('#dcs_sel').val()},
									beforeSend: function(){
										$('#loading_div').toggleClass("hidden");
									},
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										$(window).trigger('resize');
											
									//CREATE BUTTON CLICK
										$('#create_button').click(function(){
											$.ajax({
												type: "POST",
												async: true,
												url: 'pages/datacenter/servers_create.php',
												data: { dcs: $('#dcs_sel').val(), 
														images: $('#images').val(), 
														flavor: $('#flavor').val(), 
														name: $('#server_name_txt').val() 
												},
												beforeSend: function(){
													$('#loading_div').toggleClass("hidden");
												},
												success: function(){
													$('#loading_div').toggleClass("hidden");
													$('#display_sub').click();
												}
											}); // END $.ajax
										});//END CREATE BUTTON CLICK
									}
								});
							});
						
						//RESIZE SUB-TAB CLICKED
							$('#resize_sub').click(function(){
								$.ajax({
									type: 'GET',
									url: 'pages/datacenter/servers_resize.php',
									data: { dcs: $('#dcs_sel').val()},
									beforeSend: function(){
										$('#loading_div').toggleClass("hidden");
									},
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										$(window).trigger('resize');
										
									//RESIZE BUTTON CLICK
										$('#resize_button').click(function(){
											$.ajax({
												type: "POST",
												async: true,
												url: 'pages/datacenter/servers_resize.php',
												data: { dcs: $('#dcs_sel').val(), 
														svr: $('#server_DD').val(), 
														flavor: $('#flavor_DD').val() 
												},
												beforeSend: function(){
													$('#loading_div').toggleClass("hidden");
												},
												success: function(){
													$('#loading_div').toggleClass("hidden");
													$('#display_sub').click();
												}
											}); // END $.ajax
										}); //END RESIZE BUTTON CLICK
										
									}//END RESIZE SUB-TAB CLICKED SUCCESS
								});
							});
						
						//DELETE SUB-TAB CLICKED
							$('#delete_sub').click(function(){
								$.ajax({
									type: 'GET',
									url: 'pages/datacenter/servers_delete.php',
									data: { dcs: $('#dcs_sel').val()},
									beforeSend: function(){
										$('#loading_div').toggleClass("hidden");
									},
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										$(window).trigger('resize');
									/***********
										DELETE SERVERS FUNCTIONS GO HERE
									***********/
									
										//ELIMANATE THE NORMAL FORM OPERATION SO IT DOESN'T SEND TWICE
										$("#delete_server_form").unbind('submit');
										
										//DELETE ONCLICK EVENT HANDLER
										$("#delete_button").click(function(event){
											/* stop form from submitting normally */
											event.preventDefault();
											$('#captcha_refresh').click();
											
											$.ajax({
												type: 'POST',
												data: { dcs: $('#dcs_sel').val(), 
														server: $('#server_DD').val(),
														captcha: $('input[name="captcha_code"]').val()
												},
												url: 'pages/datacenter/servers_delete.php',
												beforeSend: function(){
													$('#loading_div').toggleClass("hidden");
												},
												success: function(){
													$('#loading_div').toggleClass("hidden");
													$('#display_sub').click();
												},
												complete: function(data){
													$('#validation').html(data.responseText);
												}//end ajax:success function																
											});
										});//END DELETE BUTTON CLICK
										
									}//end SUB TAB CLICK success
								});//END ajax
							});//END DELETE SUB TAB CLICKED
						
					});//end done DEFAULT LOAD [SERVER] sub_tabs functions and click listeners
				
					//SERVER_TAB CLICK
					$('#server_tab').click(function(){
						$.ajax({
							type: 'GET',
							url: 'pages/datacenter/sub_tabs.php',
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content_sub_tabs').html(data);
								$.ajax({
									type: 'GET',
									data: {dcs: _selected},
									url: 'pages/datacenter/servers_display.php',
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										$(window).trigger('resize');
									}
								});
								
/********************************************************************************************
***********************************SERVER FUNCTIONS******************************************
********************************************************************************************/
								
						/***********
						[SERVER][DISPLAY] CLICK LOAD FUNCTIONS GO HERE
						***********/
							//DISPLAY SUB-TAB CLICKED
								$('#display_sub').click(function(){
									var _selected = $('#dcs_sel').val();
									
									$.ajax({
										type: 'GET',
										data: {dcs: _selected},
										url: 'pages/datacenter/servers_display.php',
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#loading_div').toggleClass("hidden");
											$('#content').html(data);
											$(window).trigger('resize');
										}
									});
								});
							
							//CREATE SUB-TAB CLICKED
								$('#create_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/datacenter/servers_create.php',
										data: { dcs: $('#dcs_sel').val()},
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											$(window).trigger('resize');
												
										//CREATE BUTTON CLICK
											$('#create_button').click(function(){
												$.ajax({
													type: "POST",
													async: true,
													url: 'pages/datacenter/servers_create.php',
													data: { dcs: $('#dcs_sel').val(), 
															images: $('#images').val(), 
															flavor: $('#flavor').val(), 
															name: $('#server_name_txt').val() 
													},
													beforeSend: function(){
														$('#loading_div').toggleClass("hidden");
													},
													success: function(data){
														$('#loading_div').toggleClass("hidden");
														$('#display_sub').click();
													}
												}); // END $.ajax
											});//END CREATE BUTTON CLICK
										}
									});
								});
							
								//RESIZE SUB-TAB CLICKED
								$('#resize_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/datacenter/servers_resize.php',
										data: { dcs: $('#dcs_sel').val()},
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											$(window).trigger('resize');
											
										//RESIZE BUTTON CLICK
											$('#resize_button').click(function(){
												$.ajax({
													type: "POST",
													async: true,
													url: 'pages/datacenter/servers_resize.php',
													data: { dcs: $('#dcs_sel').val(), 
															svr: $('#server_DD').val(), 
															flavor: $('#flavor_DD').val() 
													},
													beforeSend: function(){
														$('#loading_div').toggleClass("hidden");
													},
													success: function(){
														$('#loading_div').toggleClass("hidden");
														$('#display_sub').click();
													}
												}); // END $.ajax
											}); //END RESIZE BUTTON CLICK
											
										}//END RESIZE SUB-TAB CLICKED SUCCESS
									});
								});
							
							//DELETE SUB-TAB CLICKED
								$('#delete_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/datacenter/servers_delete.php',
										data: { dcs: $('#dcs_sel').val()},
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											$(window).trigger('resize');
										/***********
											DELETE SERVERS FUNCTIONS GO HERE
										***********/
										
											//ELIMANATE THE NORMAL FORM OPERATION SO IT DOESN'T SEND TWICE
											$("#delete_server_form").unbind('submit');
											
											//DELETE ONCLICK EVENT HANDLER
											$("#delete_button").click(function(event){
												/* stop form from submitting normally */
												event.preventDefault();
												$('#captcha_refresh').click();
												
												$.ajax({
													type: 'POST',
													data: { dcs: $('#dcs_sel').val(), 
															server: $('#server_DD').val(),
															captcha: $('input[name="captcha_code"]').val()
													},
													url: 'pages/datacenter/servers_delete.php',
													beforeSend: function(){
														$('#loading_div').toggleClass("hidden");
													},
													success: function(){
														$('#loading_div').toggleClass("hidden");
														$('#validation').html(data.responseText);
														$('#display_sub').click();
													}//end ajax:success function																
												});
											});//END DELETE BUTTON CLICK
											
										}//end SUB TAB CLICK success
									});//END ajax
								});//END DELETE SUB TAB CLICKED
								
							} // end success
						});// end ajax
					});// end server tab click event
					
					//IMAGES_TAB CLICK
					$('#image_tab').click(function(){
						$.ajax({
							type: 'GET',
							url: 'pages/images/sub_tabs.php',
							data: {dcs: $('#dcs_sel').val()},
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");												
							},
							success: function(data){
								$('#content_sub_tabs').html(data);
								
								$.ajax({ // load images_display.php by default
									type: 'GET',
									url: 'pages/images/images_display.php',
									data: { dcs: $('#dcs_sel').val() },
									success: function(data){
										$('#content').html(data);
										$('#loading_div').toggleClass("hidden");
										$(window).trigger('resize');
									}// END SUCCESS CALLBACK
								});//END AJAX
								
								
/********************************************************************************************
************************************IMAGE FUNCTIONS******************************************
********************************************************************************************/
							//DISPLAY SUB-TAB CLICKED
								$('#display_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/images/images_display.php',
										data: {dcs: $('#dcs_sel').val()},
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											$(window).trigger('resize');
										}// END SUCCESS CALLBACK
									});//END AJAX
								});// END DISPLAY SUB-TAB CLICK
							
							//CREATE SUB-TAB CLICKED
								$('#create_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/images/images_create.php',
										data: {dcs: $('#dcs_sel').val()},
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											$(window).trigger('resize');
											
										//CREATE BUTTON CLICK
											$('#create_button').click(function(){
												$.ajax({
													type: "POST",
													async: true,
													url: 'pages/images/images_create.php',
													data: { dcs: $('#dcs_sel').val(), 
															server: $('#server_DD').val(), 
															name: $('#image_name_txt').val() 
													},
													beforeSend: function(){
														$('#loading_div').toggleClass("hidden");
													},
													success: function(data){
														$('#loading_div').toggleClass("hidden");
														$('#display_sub').click();
													}
												}); // END $.ajax
											});// END CREATE BUTTON CLICK
										}// END SUCCESS CALLBACK
									});//END AJAX
								});//END CREATE SUB TAB CLICK
								
							//DELETE SUB-TAB CLICKED
								$('#delete_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/images/images_delete.php',
										data: { dcs: $('#dcs_sel').val()},
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");
										},
										success: function(data){
											$('#content').html(data);
											$('#loading_div').toggleClass("hidden");
											$(window).trigger('resize');
										/***********
											DELETE SERVERS FUNCTIONS GO HERE
										***********/
										
											//ELIMANATE THE NORMAL FORM OPERATION SO IT DOESN'T SEND TWICE
											$("#delete_server_form").unbind('submit');
											
											//DELETE ONCLICK EVENT HANDLER
											$("#delete_button").click(function(event){
												/* stop form from submitting normally */
												event.preventDefault();
												$('#captcha_refresh').click();
												
												$.ajax({
													type: 'POST',
													data: { dcs: $('#dcs_sel').val(), 
															image: $('#image_DD').val(),
															captcha: $('input[name="captcha_code"]').val()
													},
													url: 'pages/images/images_delete.php',
													beforeSend: function(){
														$('#loading_div').toggleClass("hidden");
													},
													success: function(){
														$('#loading_div').toggleClass("hidden");
														$('#validation').html(data.responseText);
														$('#display_sub').click();
													}//end ajax:success function																
												});
											});//END DELETE BUTTON CLICK
											
										}//end SUB TAB CLICK success
									});//END ajax
								});//END DELETE SUB TAB CLICKED
								
								
							} // end success
						});// end ajax
					});// end image tab click event
					
					//ACCOUNTING_TAB CLICK
					$('#accounting_tab').click(function(){
						$.ajax({
							type: 'GET',
							url: 'pages/accounting/sub_tabs.php',
							beforeSend: function(){
								$('#loading_div').toggleClass("hidden");											
							},
							success: function(data){
								$('#content_sub_tabs').html(data);
								$(window).trigger('resize');
								$('#loading_div').toggleClass("hidden");
/********************************************************************************************
*********************************ACCOUNTING FUNCTIONS****************************************
********************************************************************************************/
								//CHK_ALL event listener
								function chk_all() {
									//CHECK ALL CHECK-BOXES
									$('#chk_all').change(function() {
										var chk_all_check;
										$.each($('input[type=checkbox]:not(:disabled)'), function(k, v) {
											if (v['name'] == "chk_all_box") {
												chk_all_check = v['checked'];
											} else {
												v['checked'] = chk_all_check;
												toggle_input(v['id']);
											}
										});
									});
								}// end chk_all function definition

								//TOGGLE_INPUT funciton definition
								function toggle_input(id) {
									if (id != "chk_all") {
										var num = id.split('_');
										var txt_input = $('#rename_to_' + num[1]);
										var label = $("#name_" + num[1]);
										var chk_box = $('#' + id);
										if (txt_input.attr('disabled') && chk_box[0].checked == true) {
											txt_input.removeAttr('disabled');
											label.css('text-decoration', 'line-through');
											txt_input.css('text-decoration', 'none');
											txt_input.addClass('active');
										} else if (chk_box[0].checked == false) {
											txt_input.value = "";
											txt_input.attr('disabled', 'disabled');
											label.css('text-decoration', 'none');
											txt_input.css('text-decoration', 'line-through');
											txt_input.removeClass('active');
										}//end else
									}//end if($(this
								}// end toggle_input function definition
							
							//REPORT SUB-TAB CLICKED
								$('#report_sub').click(function(){
									$.ajax({
										type: 'GET',
										url: 'pages/accounting/report_query.php',
										beforeSend: function(){
											$('#loading_div').toggleClass("hidden");										
										},
										success: function(data){
											$('#content').html(data);
											$(window).trigger('resize');
											$('.datepicker').datepicker();
											$('#loading_div').toggleClass("hidden");
										}//END GET AJAX REPORT_QUERY.PHP SUCCESS
									});//END GET AJAX REPORT_QUERY.PHP
								});//END REPORT SUB_TAB CLICK EVENT LISTENER

								//ADD & EDIT SUB-TAB CLICKED
								$('#add_edit_sub').click(function() {
									$.ajax({
										type : 'GET',
										url : 'pages/accounting/add_edit_labels.php',
										success : function(data) {
											$('#content').html(data);
											$(window).trigger('resize');
											/***********
												ACCOUNTING ADD/EDIT FUNCTIONS GO HERE
											***********/

											//CHECK ALL CHECK-BOXES LISTENER
											chk_all();
											$('input[type=checkbox]:not(:disabled #chk_all)').change(function() {
												toggle_input(this.id);
											});

											//WHEN CHECKED ENABLE TEXT INPUT

											//CREATE NEW LABEL ROW BUTTON CLICK EVENT LISTENER
											$('#add_row_button').click(function() {
												var num = $('tbody tr').length - 1;
												if (num % 2 == 0) {
													$('tbody').append('<tr class="odd"><td class="checkbox"><input type="hidden" name="LABEL[' + num + '][checked]" value="on" ><input class="active" name=LABEL[' + num + '][checked] type="checkbox" checked="true" disabled="disabled" ></td><td>New Label</td><td><input class="active" type="text" id="rename_to_" name="LABEL[' + num + '][new]"  /></td></tr>');
												} else {
													$('tbody').append('<tr class="even"><td class="checkbox"><input type="hidden" name="LABEL[' + num + '][checked]" value="on" ><input class="active" name=LABEL[' + num + '][checked] type="checkbox" checked="true" disabled="disabled" ></td><td>New Label</td><td><input class="active" type="text" id="rename_to_" name="LABEL[' + num + '][new]"  /></td></tr>');
												}
											});

											//APPLY_BUTTON CLICK EVENT LISTENER
											$('#apply_button').click(function() {
												$.ajax({
													type : 'POST',
													url : 'pages/accounting/add_edit_labels.php',
													data : $('#lbl_list').serialize(),
													beforeSend : function() {
														$('#loading_div').toggleClass("hidden");
													},
													success : function(data) {
														alert(data);
														$('#loading_div').toggleClass("hidden");
														$(window).trigger('resize');
													}//END POST AJAX ADD_EDIT_LABELS.PHP SUCCESS
												});//END POST AJAX ADD_EDIT_LABELS.PHP
											});//END APPLY_BUTTON CLICK EVENT LISTENER
										}//end success function
									});//end add / edit sub tab click
								});

								//APPLY_LABELS SUB_TAB CLICK EVENT LISTENER
								$('#apply_sub').click(function() {

									$.ajax({
										type : 'GET',
										url : 'pages/accounting/apply_labels.php',
										data : {
											dc : $('#dcs_sel').val()
										},
										success : function(data) {
											$('#content').html(data);
											$('#lbl_svr_radio').buttonset();
											$(window).trigger('resize');

											/***********
												RADIO BUTTON SELECTED
											***********/
											//LABEL RADIO BUTTON CLICKED
											$('#lbl_radio').click(function() {
												$.ajax({
													type : 'GET',
													url : 'pages/accounting/apply_labels.php',
													data : {
														clicked : 'label',
														dc : $('#dcs_sel').val()
													},
													success : function(data) {
														$('#apply_dd').html(data);
														$('#dd_result').html('');
														lbls_svrs_dd_handler('Label');
													}// end success function
												}); // end ajax call
											}); // end label radio button click

											$('#lbl_radio').click(); // for default load simulated radio button click
											
											//SERVER RADIO BUTTON CLICKED
											$('#svr_radio').click(function() {
												$.ajax({
													type : 'GET',
													url : 'pages/accounting/apply_labels.php',
													data : {
														clicked : 'server',
														dc : $('#dcs_sel').val()
													},
													success : function(data) {
														$('#apply_dd').html(data);
														$('#dd_result').html('');
														lbls_svrs_dd_handler('Server');
													}// end success function
												}); // end ajax call
											}); // end server radio button click
											/***********
												DROP DOWN SELECTED
											***********/
											//LABEL DROP DOWN SELECTED
											function lbls_svrs_dd_handler(type) {
												$('#dd').change(function() {

													$.ajax({
														type : 'GET',
														url : 'pages/accounting/apply_labels.php',
														data : {
															clicked : 'dd',
															dc : $('#dcs_sel').val(),
															radio : type,
															sel : $("#dd").val(),
															name : $('#dd option:selected').text()
														},
														success : function(data) {
															$('#dd_result').html(data);
															chk_all();

															$('#apply_button').click(function() {
																var sel_dc = $('#dcs_sel').val();

																$.ajax({
																	type : 'POST',
																	url : 'pages/accounting/apply_labels.php',
																	data : {
																		data : $('#lbls_svrs').serialize(),
																		dc : $('#dcs_sel').val()
																	},
																	beforeSend : function() {
																		$('#loading_div').toggleClass("hidden");
																	},
																	success : function(data) {
																		$(window).trigger('resize');
																		$('#loading_div').toggleClass("hidden");
																		alert(data);

																	}//END POST AJAX APPLY_LABELS.PHP SUCCESS
																});//END POST AJAX APPLY_LABELS.PHP
															});//END APPLY_BUTTON CLICK EVENT LISTENER

														}//END GET AJAX APPLY_LABELS.PHP SUCCESS
													});//END GET AJAX APPLY_LABELS.PHP

												});//END DD DROP DOWN ON CHANGE EVENT LISTENER
											}//END set_lbls_svrs_dd_handler() function declaration
											
										}//END GET AJAX APPLY_LABELS.PHP SUCCESS
									});//END GET AJAX APPLY_LABELS.PHP
								});//END APPLY LABELS TO SERVERS SUB_TAB CLICK EVENT LISTENER
								
								$('#report_sub').click();//REPORT TAB LOADED BY DEFAULT
								
							} //END GET AJAX ACCOUNTING/SUB_TABS.PHP SUCCESS
						});//END GET AJAX ACCOUNTING/SUB_TABS.PHP
					});//END ACCOUNTING MAIN TAB CLICK EVENT LISTENER
					
				});//end done() loading main tabs
			});//end dcs.change
		});//end done()
	});//end vendors.change
				
});//END DOCUMENT.READY

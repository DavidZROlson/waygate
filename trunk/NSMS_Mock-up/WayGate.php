<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>WayGateCloud</title>

<!-- links and recources -->
    <!-- CSS -->
    <link href="css/css2.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    
    <!-- Font-Awesome http://fortawesome.github.io/Font-Awesome/icons/ -->
    <link href="css/font-awesome/css/font-awesome.css" rel="stylesheet" />
    
    <!-- JQUERY UI THEME AND SOURCES -->
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
    
	<script type="text/javascript" src="js/jquery-ui-1.10.3/jquery-1.9.1.js"></script>
	<script type="text/javascript" src="js/jquery-ui-1.10.3/ui/jquery-ui.js"></script>
    
    <!-- CUSTOM JQUERY SCRIPTS -->
    <script type="text/javascript" src="js/nav.js"></script>
    <script type="text/javascript" src="js/ux.js"></script>
    
    <!-- REQUIRED FOR FORM MASKS http://igorescobar.github.io/jQuery-Mask-Plugin/ -->
	<script src="js/jQuery-Mask-Plugin-master/jquery.mask.js"></script>
    
    <!-- REQUIRED FOR FORM VALIDATION http://jqueryvalidation.org/documentation/ -->
    <script type="text/javascript" src="js/jquery-validation-1.11.1/dist/jquery.validate.js"></script>
    <script type="text/javascript" src="js/jquery-validation-1.11.1/dist/additional-methods.js"></script>

</head>

<body>
    <!-- BEGIN HEADER -->
    <?php include_once('includes/header.php'); ?>
    <!-- END HEADER -->
        
    <!-- BEGIN MAIN_WRAPPER -->
    <div id="main_wrapper">
    
        <!-- BEGIN MAIN_CONTAINER -->
        <div id="main_container">
            
            <!-- BEGIN NAV -->
            <div id="nav" >
                <ul>
                    <li id="admin_nav" class="nav_top nav_dir">
                        <a class="icon icon-tasks">
                            <span>Admin Settings</span>
                        </a>
                    </li>
                    <li id="dns_nav" class="nav_top nav_dir">
                        <a class="icon icon-globe">
                            <span>DNS Info</span>
                        </a>
                    </li>
                    
                    <li class="nav_top nav_dir">
                        <span class="nav_label">Vendor:</span>
                        <span id="vender_DD" class="dropdown">
                            <select name="vendors" id="vendors">
                                <option selected="selected" value="">[Select...]</option>
                                <option value="rackspace">RackSpace</option>
                                <option value="amazon">Amazon</option>
                                <option value="rosincloud">RosinCloud</option>
                            </select>
                        </span>
                    </li>
                    <li class="nav_top nav_dir">
                        <span class="nav_label">DataCenter:</span>
                        <span id="dcs_DD" class="dropdown">
                            <select name="dcs_sel" id="dcs_sel" disabled="disabled">
                                <option selected="selected" value="">[Select...]</option>
                            </select>
                        </span>
                    </li>
                </ul>
            </div>
            <!-- END NAV -->
            
            <!-- BEGIN CONTENT_WRAPPER -->
            <div id="content_wrapper">
				<?php 
                include_once('pages/vendor_info/cost_compare.php');	
                ?>
            </div>
            <!-- END CONTENT_WRAPPER -->
            
        </div>
        <!-- END MAIN_CONTAINER -->
        
    
    </div>
    <!-- END MAIN_WRAPPER -->
    
    <!-- BEGIN FOOTER -->
    <?php include_once('includes/footer.php'); ?>
    <!-- END FOOTER -->
<div id="status" class="hidden overlay_full"></div>
<div id="loading_div" class="hidden overlay_full"><img class="center_absolute" style="width: 100px;" src="images/loading.gif" alt="Loading..." longdesc="images/loading.gif" /></div>
</body>
</html>
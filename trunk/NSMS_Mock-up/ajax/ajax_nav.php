<?php

//TODO
/********************************************************
 *	PRIOR TO PAGE LOAD CHECK SERVER FOREACH
*		VENDORS:
*			NAME: string
*	AND POPULATE 'vendor' DROP DOWN SELECT LIST OPTIONS
********************************************************/


session_start();

//CHECK FOR POST REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'POST') {

	if($_POST['clicked'] === 'logout'){
		if(isset($_SESSION['PSAdminNM'])){
			echo json_encode(array('success' => true));
		}else{
			echo json_encode(array('success' => false));
		}
		session_unset(); // clears all session values
		session_destroy(); // destroys the session keys
		session_write_close(); // closes the current session
		setcookie(session_name(),'',0,'/'); // clears the session cookie
		session_regenerate_id(true); // allows for the same session id to be used
		exit();
	}//end IF $_POST['clicked'] === 'logout'

}// end IF $_SERVER['REQUEST_METHOD'] === 'POST'

//CHECK FOR GET REQUEST
if ($_SERVER['REQUEST_METHOD'] === 'GET') {

	/********************************************************
	 *														*
	*					NAV DROP DOWN OPTIONS				*
	*														*
	********************************************************/

	/********************************************************
	 *					VENDOR	SELECTED					*
	********************************************************/

	//TODO
	/***********************************************************
		*	WHEN A VENDOR IS SELECTED, CHECK SERVER FOREACH
	*		DATA_CENTERS:
	*			NAME: string
	*	AND POPULATE 'dcs' DROP DOWN SELECT LIST
	***********************************************************/

	//IF GET REQUEST IS FROM 'vendors'
	if(isset($_GET['vendors'])){
		$vendors = $_GET['vendors'];
			
		if($vendors == 'rackspace'){
			//load rackspace Datacenters
			$result =  '<select name="dcs_sel" id="dcs_sel">
					<option value="">[Select...]</option>
					<option value="rk_1">rack_DC1</option>
					<option value="rk_2">rack_DC2</option>
					<option value="rk_3">rack_DC3</option>
					</select>';
		}elseif($vendors == 'amazon'){
			//load ec2 Datacenters
			$result =  '<select name="dcs_sel" id="dcs_sel">
					<option value="">[Select...]</option>
					<option value="az_1">Amazon_DC1</option>
					<option value="az_2">Amazon_DC2</option>
					<option value="az_3">Amazon_DC3</option>
					</select>';

		}elseif($vendors == 'rosincloud'){
			//load rosincloud Datacenters
			$result =  '<select name="dcs_sel" id="dcs_sel">
					<option value="">[Select...]</option>
					<option value="ord">ORD</option>
					</select>';
		}else{
			$result =  '<select name="dcs_sel" id="dcs_sel" disabled="disabled">
					<option value="">[Select...]</option>
					</select>';
		}
		echo $result;
		exit();
	}

	/********************************************************
	 *					DATA CENTER	SELECTED				*
	********************************************************/

	//IF GET REQUEST IS FROM 'dcs'
	if(isset($_GET['dcs'])){
		$DataCenter = $_GET['dcs'];
			
		if($DataCenter == 'rackspace'){
			//load rackspace
			//Query for Rackspace servers
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->
						
					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';
				
		}elseif($DataCenter == 'amazon'){
			//load amazon
			//Query for Amazon servers
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->
						
					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';

		}elseif($DataCenter == 'ord'){
			//load rosincloud
			//Query for Rosincloud servers
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->
						
					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';
				
		}else{
			//default tabs load
			$result = '<!-- BEGIN CONTENT_MENU_ -->
					<div id="content_menu">
					<ul>
					<li id="server_tab">[Servers]</li>
					<li id="image_tab">[Images]</li>
					<li id="accounting_tab">[Accounting]</li>
					</ul>
					</div>
					<!-- END CONTENT MENU -->
						
					<div id="content_sub_tabs"></div><!-- END CONTENT_MENU -->';
				
		}
		
		echo json_encode(array('html_string' => $result));
		exit();
	}
}

?>
<style type="text/css">

	#main_container{
		border-style: none;	
	}
	#content_wrapper{
		background-color: transparent;
		border-radius: 10px 10px 10px 10px;
		border-style: none;
		clear: left;
		margin: 0;
		min-height: 95%;
		padding: 5px 0;
		vertical-align: top;
	}
	#content{
		margin: 0 auto;
	}
	
	#login_cell{
		margin: 35px;
	}
	
	#login_cell:after{
		background-image:url(images/RosinGlobe.png);
		background-position:center center;
		background-size: contain;
		background-repeat: no-repeat;
		height: 300px;
		margin: 0 auto;
		position: absolute;
		top: 110px;
		left: 0;
		right: 25px;
		bottom: 0;
		z-index: -1;
		opacity: 0.40;
		content: "";
	}
	body{
		overflow: hidden;
	}

</style>

<div id="header">
    <img class="header_logo" src="images/RosinGlobe.png" alt="logo" />
    <span class="header_slogan">
    	<h1>WayGateCloud</h1>
        <p> Provided By RosinCloud.com</p>
    </span>
    <span class="header_nav">
    	<a href="login.php"><span id="login">Login</span></a> <span class="vertical_bar1"></span> <a href="sign_up.php"><span id="sign_up">Sign-up</span></a>
    </span>
</div>
